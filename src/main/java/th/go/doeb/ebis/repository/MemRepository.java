package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.LoginBean;
import th.go.doeb.ebis.bean.MemBean;



@Repository
public interface MemRepository extends CrudRepository<MemBean, Long>{
	MemBean findById(long id);
	MemBean findByuser(String user);
	MemBean findBypass(String pass);
	MemBean findByUserAndPass(String user, String pass);
}

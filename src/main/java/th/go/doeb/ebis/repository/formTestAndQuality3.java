package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.formTestAndQuality2;
@Repository
public interface formTestAndQuality3 extends CrudRepository<formTestAndQuality2, Long>{

}

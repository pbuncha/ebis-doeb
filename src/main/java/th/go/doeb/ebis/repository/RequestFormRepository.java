package th.go.doeb.ebis.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.Form;
import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestForm;

@Repository
public interface RequestFormRepository extends CrudRepository<RequestForm, Long> {
	Iterable<RequestForm> findByRequest(Request request);
	RequestForm findByRequestAndForm(Request request, Form form);
	RequestForm findByRfId(long rfId);
	
	//@Transactional
   // @Modifying
	@Query("SELECT RF FROM RequestForm RF WHERE RF.request = :request ")
	Iterable<RequestForm> findByRequestJustGetType(@Param("request")Request request);
	
	@Query("SELECT RF FROM RequestForm RF WHERE RF.request = :request AND RF.requestType.requestTypeId = :requestTypeId ")
	Iterable<RequestForm> findByRequestAndTypeId(@Param("request")Request request,@Param("requestTypeId")Long requestTypeId);
	
	@Query("SELECT RF FROM RequestForm RF WHERE RF.request = :request AND RF.form = :form AND RF.requestType.requestTypeId = :requestTypeId ")
	RequestForm findByRequestAndFormAadTypeId(@Param("request")Request request,@Param("form") Form form,@Param("requestTypeId")Long requestTypeId);
	//List<RequestForm> findByRtId(Long rqId);
}

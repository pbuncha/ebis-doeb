package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.BeOilInspector;

@Repository
public interface BeOilInspectorRepository extends CrudRepository<BeOilInspector, Long>{

}

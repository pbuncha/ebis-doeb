package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.Menu;

@Repository
public interface MenuRepository extends CrudRepository<Menu, Long> {
	Menu findByMenuTitle(String menuTitle);
}

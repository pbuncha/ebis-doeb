package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import th.go.doeb.ebis.entity.RequestChecklist;

public interface RequestChecklistRepository extends CrudRepository<RequestChecklist, Long> {
	@Query("SELECT RC FROM RequestChecklist RC WHERE RC.request.requestId = :requestId ORDER BY RC.id ASC ")
	Iterable<RequestChecklist> findAllByRequestId(@Param("requestId")Long requestId);
}

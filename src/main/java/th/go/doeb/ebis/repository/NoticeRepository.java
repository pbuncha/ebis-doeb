package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.Notice;
@Repository
public interface NoticeRepository extends CrudRepository<Notice, Long>{
	Notice findById(long id);
	

}

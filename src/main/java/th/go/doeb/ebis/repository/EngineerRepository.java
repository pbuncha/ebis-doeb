package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import th.go.doeb.ebis.entity.Engineer;
import th.go.doeb.ebis.entity.RequestTransaction;
import th.go.doeb.ebis.entity.TitleName;

@Repository
public interface EngineerRepository extends CrudRepository<Engineer, Long>{
	Engineer findByengineerId(Long engineerId);

	@Query("SELECT E FROM Engineer E WHERE E.company.dbdId = :dbdId AND (E.deleted = '0' OR E.deleted IS NULL ) ")
	Iterable<Engineer> findAllbyDbdId(@Param("dbdId") String dbdId);

	@Query("SELECT E FROM Engineer E WHERE E.rt_id = :rt_id AND (E.deleted = '0' OR E.deleted IS NULL ) ")
	Iterable<Engineer> findAllbyRTID(@Param("rt_id") Long rt_id);

	@Query("SELECT E FROM Engineer E WHERE E.id = :id AND (E.deleted = '0' OR E.deleted IS NULL ) ")
	Iterable<Engineer> findAllbyID(@Param("id") String id);
	
	

//	@Query("SELECT E Engineer E JOIN COMPANY C WHERE C.dbdId = :dbdId")
//	Iterable<Engineer> findAllbyDbdId(@Param("dbdId") String dbdId);
	@Transactional
	@Modifying
	@Query("UPDATE Engineer E SET E.engineerId = :engineerId, E.titleName = :titleName, E.firstname = :firstname, E.lastname = :lastname, E.type = :type, E.faculty = :faculty  WHERE id = :engineerIdOriginal ")
	int updateEngineerCustom(@Param("engineerId") String engineerId,
			@Param("titleName") TitleName titleName,
			@Param("firstname") String firstname,
			@Param("lastname") String lastname,
			@Param("type") String type,
			@Param("faculty") String faculty,
			@Param("engineerIdOriginal") Long engineerIdOriginal);

	@Transactional
	@Modifying
	@Query("UPDATE Engineer E SET E.deleted = '1' WHERE E.id = :id ")
	int deleteEngineerCustom(@Param("id") Long id);


}

package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.FuelOperator;

@Repository
public interface FuelOperatorRepository extends CrudRepository<FuelOperator, Long> {
	
	FuelOperator findByFoId(Long foId);
}

package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.RequestTransaction;
import th.go.doeb.ebis.entity.RequestType;

@Repository
public interface RequestTypeRepository extends CrudRepository<RequestType, Long> {
	
	@Query("SELECT R FROM RequestType R WHERE R.requestTypeId = :requestTypeId ")
	RequestType findByRequestTypeId(@Param("requestTypeId")long requestTypeId );
	
}

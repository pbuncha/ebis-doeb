package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import th.go.doeb.ebis.entity.GovDocumentType;

@Repository
public interface DocumenttypeRepository extends CrudRepository<GovDocumentType, Long>{
	
	GovDocumentType findBydoctypeId(Long doctypeId);

}

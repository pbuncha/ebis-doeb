package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.Tester;

@Repository
public interface TesterRepository extends CrudRepository<Tester, Long>{
	Tester findById(long id);
}

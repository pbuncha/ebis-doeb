package th.go.doeb.ebis.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.LoginBean;

@Repository
public interface LoginRepository extends CrudRepository<LoginBean, Long>{
	LoginBean findByuser(String user);
	LoginBean findBypass(String pass);
}

package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.PersonElectricBean;

@Repository
public interface PersonElectricRepository extends CrudRepository<PersonElectricBean, Long> {

}

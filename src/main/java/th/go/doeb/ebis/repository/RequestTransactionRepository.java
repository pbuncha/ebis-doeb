package th.go.doeb.ebis.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.Evaluate;
import th.go.doeb.ebis.entity.Holiday;
import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestStatus;
import th.go.doeb.ebis.entity.RequestTransaction;

@Repository
public interface RequestTransactionRepository extends CrudRepository<RequestTransaction, Long> {

	@Query("SELECT R FROM RequestTransaction R WHERE R.Company.dbdId = :DbdId AND R.request.requestId = :request AND R.requestStatus.status = :requestStatus AND ROWNUM = 1  ORDER BY R.created DESC")
	RequestTransaction findByDbdIdAndRequestAndRequestStatus(@Param("DbdId")String DbdId,@Param("request")Long request  , @Param("requestStatus")String requestStatus );
	
	//Iterable<RequestTransaction> findByCompany(String dbdId);
	RequestTransaction findByRtId(long rtId);
	
	@Query("SELECT R FROM RequestTransaction R WHERE R.requestStatus = '0' AND R.requestType.requestTypeId = '1' ORDER BY R.created DESC ")
	Iterable<RequestTransaction> findAllStatus0New();
	
	@Query("SELECT R FROM RequestTransaction R WHERE R.requestStatus = '0' AND R.requestType.requestTypeId = '2' ORDER BY R.created DESC ")
	Iterable<RequestTransaction> findAllStatus0Edit();
	
	@Query("SELECT R FROM RequestTransaction R WHERE R.requestStatus = '0' AND R.requestType.requestTypeId = '3' ORDER BY R.created DESC ")
	Iterable<RequestTransaction> findAllStatus0Change();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '101' OR R.requestStatus = '102' OR R.requestStatus = '206' OR R.requestStatus = '207' OR R.requestStatus = '208' OR R.requestStatus = '302' OR R.requestStatus = '204' OR R.requestStatus = '205') AND R.requestType.requestTypeId = '1' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatus101New();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '101' OR R.requestStatus = '102' OR R.requestStatus = '206' OR R.requestStatus = '207' OR R.requestStatus = '208' OR R.requestStatus = '302' OR R.requestStatus = '204' OR R.requestStatus = '205') AND ( R.requestType.requestTypeId = '2' OR R.requestType.requestTypeId = '4') ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatus101Edit();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '101' OR R.requestStatus = '102' OR R.requestStatus = '206' OR R.requestStatus = '207' OR R.requestStatus = '208' OR R.requestStatus = '302' OR R.requestStatus = '204' OR R.requestStatus = '205') AND R.requestType.requestTypeId = '3' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatus101Change();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '206' OR R.requestStatus = '207' OR R.requestStatus = '208') AND R.requestType.requestTypeId = '1' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatus206New();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '206' OR R.requestStatus = '207' OR R.requestStatus = '208') AND R.requestType.requestTypeId = '2' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatus206Edit();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '206' OR R.requestStatus = '207' OR R.requestStatus = '208') AND R.requestType.requestTypeId = '3' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatus206Change();
	
	@Query("SELECT R FROM RequestTransaction R WHERE R.requestStatus = '303' AND R.requestType.requestTypeId = '1' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatus303New();
	
	@Query("SELECT R FROM RequestTransaction R WHERE R.requestStatus = '303' AND R.requestType.requestTypeId = '2' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatus303Edit();
	
	@Query("SELECT R FROM RequestTransaction R WHERE R.requestStatus = '303' AND R.requestType.requestTypeId = '3' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatus303Change();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '101' OR R.requestStatus = '102' OR R.requestStatus = '103' OR R.requestStatus = '206'OR R.requestStatus = '204' OR R.requestStatus = '205') AND R.requestType.requestTypeId = '1' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatusallNew();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '101' OR R.requestStatus = '102' OR R.requestStatus = '103' OR R.requestStatus = '206'OR R.requestStatus = '204' OR R.requestStatus = '205') AND ( R.requestType.requestTypeId = '2' OR R.requestType.requestTypeId = '4') ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatusallEdit();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '101' OR R.requestStatus = '102' OR R.requestStatus = '103' OR R.requestStatus = '206'OR R.requestStatus = '204' OR R.requestStatus = '205') AND R.requestType.requestTypeId = '3' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllStatusallChange();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '103' OR R.requestStatus = '104' OR R.requestStatus = '204') AND R.requestType.requestTypeId = '1' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllWithdrawNew();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '103' OR R.requestStatus = '104' OR R.requestStatus = '204') AND ( R.requestType.requestTypeId = '2' OR R.requestType.requestTypeId = '4') ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllWithdrawEdit();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '103' OR R.requestStatus = '104' OR R.requestStatus = '204') AND R.requestType.requestTypeId = '3' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllWithdrawChange();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '209') AND R.requestType.requestTypeId = '1' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllCompleteNew();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '209') AND R.requestType.requestTypeId = '2' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllCompleteEdit();
	
	@Query("SELECT R FROM RequestTransaction R WHERE (R.requestStatus = '209') AND R.requestType.requestTypeId = '3' ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllCompleteChange();
	
	
	
	/*@Transactional
	@Modifying
	@Query("SELECT E FROM Evaluate E WHERE E.request.requestId = :rtId ")
	Iterable<Evaluate> findEvaluateListByRtId(@Param("rtId")long rtId);*/
	
	@Query("SELECT R FROM RequestTransaction R WHERE R.Company.dbdId = :dbdId ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllbydbdId(@Param("dbdId")String dbdId);
	
	@Query("SELECT R FROM RequestTransaction R WHERE R.Company.dbdId = :dbdId ORDER BY R.created DESC")
	Iterable<RequestTransaction> findByDbdId(@Param("dbdId")String dbdId);
	
	@Query("SELECT R FROM RequestTransaction R WHERE R.Company.companyName Like :dbdId ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllbydbdIdJoinCustomer(@Param("dbdId")String dbdId);
	
	@Query("SELECT R FROM RequestTransaction R WHERE R.Company.dbdId Like :dbdId ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllbyLikedbdId(@Param("dbdId")String dbdId);
	
	@Query("SELECT R FROM RequestTransaction R WHERE R.actualGaranteeDay.actualGaranteeDay Like :actualGaranteeDay ORDER BY R.created DESC")
	Iterable<RequestTransaction> findAllbyDay(@Param("actualGaranteeDay")String string);
}

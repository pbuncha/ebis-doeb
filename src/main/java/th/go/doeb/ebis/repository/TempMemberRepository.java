package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;

import th.go.doeb.ebis.bean.TempMember;

public interface TempMemberRepository extends CrudRepository<TempMember, Long> {
	TempMember findByMemberUsernameAndMemberPassword(String memberUsername, String memberPassword);
}

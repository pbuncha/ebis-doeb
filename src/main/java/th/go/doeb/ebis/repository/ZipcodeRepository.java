package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.ZipcodeBean;

@Repository
public interface ZipcodeRepository extends CrudRepository<ZipcodeBean, Long> {

	Iterable<ZipcodeBean> findByDistrictId(String districtId);

}

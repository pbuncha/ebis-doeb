package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.EModel;

@Repository
public interface StcRepository extends CrudRepository<EModel, Long>{
	EModel findById(long id);
}

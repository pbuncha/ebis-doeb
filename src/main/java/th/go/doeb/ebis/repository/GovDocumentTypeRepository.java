package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;

import th.go.doeb.ebis.entity.GovDocumentType;

public interface GovDocumentTypeRepository extends CrudRepository<GovDocumentType, Long> {
	GovDocumentType findByDoctypeId(Long doctypeId);
}

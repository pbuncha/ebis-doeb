package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.FormEntry;
import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.entity.RequestTransaction;
//comment test push
@Repository
public interface FormEntryRepository extends CrudRepository<FormEntry, Long> {
	FormEntry findByEntryId(long entryId);
	FormEntry findByRequestTransactionAndRequestForm(RequestTransaction requestTransaction, RequestForm requestForm);
	FormEntry findByRequestTransactionAndRequestFormAndIdEngineer(RequestTransaction requestTransaction, RequestForm requestForm, String engiId);

	FormEntry findFirstByRequestTransaction(RequestTransaction requestTransaction);
	
	@Query("SELECT FE FROM FormEntry FE WHERE FE.requestTransaction.rtId = :rtId AND FE.requestForm.rfId = :rfId ")
	//Iterable<RequestForm> findByRequestJustGetType(@Param("request")Request request);
	Iterable<FormEntry> findAllByRequestTransactionAndRequestForm(Long rtId, Long rfId);
}

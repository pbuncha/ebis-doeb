package th.go.doeb.ebis.repository;

import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.BeTechnicalTestingValidation;

import org.springframework.data.repository.CrudRepository;

@Repository
public interface BeTechnicalTestingValidationRepository extends CrudRepository<BeTechnicalTestingValidation, Long> {

}

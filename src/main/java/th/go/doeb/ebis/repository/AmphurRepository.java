package th.go.doeb.ebis.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import th.go.doeb.ebis.bean.AmphurBean;
import th.go.doeb.ebis.entity.GovDocumentCreate;

@Repository
public interface AmphurRepository extends CrudRepository<AmphurBean, Long> {
	
	//@Transactional
	//@Modifying
	@Query("SELECT A FROM AmphurBean A WHERE A.amphurId = :IdAmphur ")
	AmphurBean findByIdAmphur(@Param("IdAmphur")long IdAmphur);
	
	Iterable<AmphurBean> findByProvinceId(BigDecimal provinceId);
	
}

package th.go.doeb.ebis.repository;

import java.math.BigDecimal;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.Form;

@Repository
public interface FormRepository extends CrudRepository<Form, Long> {
	Form findByFormId(long formId);
	Iterable<Form> findByFormActive(BigDecimal formActive);
}

package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.CompanyAttorney;

@Repository
public interface CompanyAttorneyRepository extends CrudRepository<CompanyAttorney, Long> {
	
	//@Query("SELECT CA FROM CompanyAttorney CA WHERE ROWNUM = 1  ORDER BY CA.attorneyId DESC")
	//CompanyAttorney findByDbdIdLimit1(@Param("DbdId")String DbdId );
}

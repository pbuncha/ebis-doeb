package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.Zone;

@Repository
public interface ZoneRepository extends CrudRepository<Zone, Long> {

}

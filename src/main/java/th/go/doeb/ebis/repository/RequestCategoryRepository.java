package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestCategory;

@Repository
public interface RequestCategoryRepository extends CrudRepository <RequestCategory, Long> {
	
	// Category ของ สพพ
	@Query("SELECT RC FROM RequestCategory RC WHERE RC.rqCategoryType = '1' ")
	Iterable<RequestCategory> findAllFilter1();
	
	// Category ของ สธช
	@Query("SELECT RC FROM RequestCategory RC WHERE RC.rqCategoryType = '2'  ")
	Iterable<RequestCategory> findAllFilter2();
	
	// Category ของ สธน
	@Query("SELECT RC FROM RequestCategory RC WHERE RC.rqCategoryType = '3'  ")
	Iterable<RequestCategory> findAllFilter3();

	// Category ของ สพพ
	@Query("SELECT RC FROM RequestCategory RC WHERE RC.rqCategoryType = '4'  ")
	Iterable<RequestCategory> findAllFilter4();
	
	// Category ของ สบส
	@Query("SELECT RC FROM RequestCategory RC WHERE RC.rqCategoryType = '5'  ")
	Iterable<RequestCategory> findAllFilter5();
		
	// Category ของ สคน
	@Query("SELECT RC FROM RequestCategory RC WHERE RC.rqCategoryType = '6'  ")
	Iterable<RequestCategory> findAllFilter6();
	
	// Category ของ ศบธ
	@Query("SELECT RC FROM RequestCategory RC WHERE RC.rqCategoryType = '7'  ")
	Iterable<RequestCategory> findAllFilter7();
	
	RequestCategory findByRqCategoryId(Long rqCategoryId);
}

package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.Department;
import th.go.doeb.ebis.entity.Employee;

@Repository
public interface DepartmentRepository  extends CrudRepository<Department, Long> {
	Department findByDepartmentId(Long departmentId);
}

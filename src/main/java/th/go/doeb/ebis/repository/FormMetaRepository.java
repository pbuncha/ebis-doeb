package th.go.doeb.ebis.repository;

import java.math.BigDecimal;

import org.springframework.data.repository.CrudRepository;

import th.go.doeb.ebis.entity.FormMeta;

public interface FormMetaRepository extends CrudRepository<FormMeta, Long> {
	FormMeta findByMetaNameAndFormId(String metaName, BigDecimal formId);
}

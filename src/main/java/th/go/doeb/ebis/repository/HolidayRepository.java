package th.go.doeb.ebis.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import th.go.doeb.ebis.entity.Holiday;

@Repository
public interface HolidayRepository extends CrudRepository<Holiday, Long> {
	Holiday findByIdHoliday(Long idHoliday);
	
	@Query("SELECT h FROM Holiday h WHERE h.deleted = NULL ")
    List<Holiday> findAll();
}

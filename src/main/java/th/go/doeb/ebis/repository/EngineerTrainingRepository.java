package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.EngineerTraining;

@Repository
public interface EngineerTrainingRepository extends CrudRepository<EngineerTraining, Long> {
	
	@Query("SELECT ET FROM EngineerTraining ET WHERE ET.company.dbdId = :dbdId AND (ET.deleted = '0' OR ET.deleted IS NULL ) AND ET.rtid = :RTID ")
	Iterable<EngineerTraining> findAllbyDbdIdAndRtid(@Param("dbdId") String dbdId,@Param("RTID") Long RTID);
//EngineerTraining
}

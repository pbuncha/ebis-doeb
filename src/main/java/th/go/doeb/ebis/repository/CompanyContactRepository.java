package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.CompanyContact;

@Repository
public interface CompanyContactRepository extends CrudRepository<CompanyContact, Long>{
//	@Query("SELECT CC FROM CompanyContact CC WHERE CC.Company.dbdId = :DbdId AND ROWNUM = 1  ORDER BY CC.contactId DESC")
	//CompanyContact findByDbdIdLimit1(@Param("DbdId")String DbdId );
	
}

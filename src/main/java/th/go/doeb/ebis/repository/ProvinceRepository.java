package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import th.go.doeb.ebis.bean.ProvinceBean;

@Repository
public interface ProvinceRepository extends CrudRepository<ProvinceBean, Integer> {
	//@Transactional
	//@Modifying
	@Query("SELECT P FROM ProvinceBean P WHERE P.provinceId = :IdProvince ")
	ProvinceBean findByIdProvince(@Param("IdProvince")Integer IdProvince);
}

package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.Form2;
import th.go.doeb.ebis.bean.FormBean;

@Repository
public interface EFRepository extends CrudRepository<FormBean, Long> {
	
}

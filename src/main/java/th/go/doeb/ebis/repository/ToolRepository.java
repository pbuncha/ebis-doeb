package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import th.go.doeb.ebis.entity.Tool;

@Repository
public interface ToolRepository extends CrudRepository<Tool, Long> {
	
	Tool findByToolId(Long toolId);
	
	@Query("SELECT T FROM Tool T WHERE T.company.dbdId = :dbdId AND (T.deleted = '0' OR T.deleted IS NULL ) ")
	Iterable<Tool> findAllbyDbdId(@Param("dbdId") String dbdId);
	
	@Transactional
	@Modifying
	@Query("UPDATE Tool T SET T.deleted = '1' WHERE T.id = :id ")
	int deleteToolCustom(@Param("id") Long id);
	
}

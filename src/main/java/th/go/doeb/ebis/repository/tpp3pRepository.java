package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.tpp3pModel;

@Repository
public interface tpp3pRepository extends CrudRepository<tpp3pModel, Long>{
	tpp3pModel findById(long id);
}

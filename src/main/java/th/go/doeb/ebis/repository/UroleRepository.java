package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.Urole;

@Repository
public interface UroleRepository extends CrudRepository<Urole, Long> {
	Urole findByRoleName(String roleName);
}

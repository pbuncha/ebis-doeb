package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.EmployeeDoeb;

@Repository
public interface EmployeeDoebRepository extends CrudRepository<EmployeeDoeb, Long> {
	
	EmployeeDoeb findByEmployeeUsername(String employeeUsername);
}

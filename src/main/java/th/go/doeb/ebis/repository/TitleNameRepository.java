package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import th.go.doeb.ebis.entity.TitleName;

public interface TitleNameRepository extends CrudRepository<TitleName, Long>{
	
	@Query("SELECT T FROM TitleName T WHERE T.active = 1  ORDER BY T.code ASC")
	Iterable<TitleName> findTitleNameActive();
	
	@Query("SELECT T FROM TitleName T WHERE T.code = :code")
	TitleName findTitleNameByCode(@Param("code")String code);
	
}

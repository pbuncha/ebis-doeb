package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;

import th.go.doeb.ebis.bean.Notice;
import th.go.doeb.ebis.bean.Pentition;

public interface PentitionRepository extends CrudRepository<Pentition, Long>{
	Pentition findById(long id);
}

package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import th.go.doeb.ebis.entity.Evaluate;
import th.go.doeb.ebis.entity.RequestTransaction;

@Repository
public interface EvaluateRepository extends CrudRepository<Evaluate, Long>{
	@Transactional
	@Modifying
	@Query("SELECT E FROM Evaluate E WHERE E.request.requestId = :rtId And  E.requestType.requestTypeId = :requestTypeId ")
	Iterable<Evaluate> findEvaluateListByRtIdAndRequestType(@Param("rtId")long rtId,@Param("requestTypeId")long requestTypeId);
}

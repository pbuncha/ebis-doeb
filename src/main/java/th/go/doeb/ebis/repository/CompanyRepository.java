package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.bean.CompanyBean;


@Repository
public interface CompanyRepository extends CrudRepository<CompanyBean, String> {
	CompanyBean findByDbdId(String dbdId);
	CompanyBean findByMemberId(String memberId);
}

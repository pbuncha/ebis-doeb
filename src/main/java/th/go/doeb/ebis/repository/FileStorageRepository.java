package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.FileStorage;
import th.go.doeb.ebis.entity.FormEntry;
import th.go.doeb.ebis.entity.Holiday;
import th.go.doeb.ebis.entity.RequestTransaction;
@Repository
public interface FileStorageRepository extends CrudRepository<FileStorage, Long>{
	FileStorage findByfileId(long fileId);
	

	
	@Query("SELECT f FROM FileStorage f WHERE f.deleted = '1' AND f.requestTransaction.rtId = :rtId ")
	Iterable<FileStorage> findByrtId(@Param("rtId") long rtId);
	
	@Query("SELECT f FROM FileStorage f WHERE f.deleted = '1' AND f.requestTransaction.rtId = :fileStoreName ")
	Iterable<FileStorage> findByrtIdfile(@Param("fileStoreName") String fileStoreName);
	
}

package th.go.doeb.ebis.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.Holiday;

@Repository
public interface CompanyNewRepository extends CrudRepository<Company, String> {

	Company findByDbdId(String dbdId);
	
	@Query("SELECT C.companyName FROM Company C WHERE C.dbdId = ?1 ")
	String findCompanyNameByDbdId(String dbdId);
}

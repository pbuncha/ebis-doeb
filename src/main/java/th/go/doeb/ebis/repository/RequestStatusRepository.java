package th.go.doeb.ebis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.doeb.ebis.entity.RequestStatus;

@Repository
public interface RequestStatusRepository extends CrudRepository<RequestStatus, String> {
	RequestStatus findByStatus(String status);
}

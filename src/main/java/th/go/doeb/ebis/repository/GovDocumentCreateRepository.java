package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import th.go.doeb.ebis.entity.GovDocumentCreate;

public interface GovDocumentCreateRepository extends CrudRepository<GovDocumentCreate, Long> {
	@Transactional
	@Modifying
	@Query("SELECT G FROM GovDocumentCreate G WHERE G.request.requestId = :rtId ")
	Iterable<GovDocumentCreate> findGovDocumentCreateListByRtId(@Param("rtId")long rtId);
}

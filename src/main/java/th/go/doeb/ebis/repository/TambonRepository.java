package th.go.doeb.ebis.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import th.go.doeb.ebis.bean.TambonBean;

@Repository
public interface TambonRepository extends CrudRepository<TambonBean, Integer> {

	//@Transactional
	//@Modifying
	@Query("SELECT D FROM TambonBean D WHERE D.districtId = :IdTambon ")
	TambonBean findByIdTambon(@Param("IdTambon")long IdTambon);
	
	Iterable<TambonBean> findByAmphurId(Long amphurId);

}

package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DISTRICT")
public class TambonBean {
	
	@Id
	@Column(name="DISTRICT_ID")
	private Long districtId;
	
	@Column(name="DISTRICT_CODE")
	private String districtCode;
	
	@Column(name="DISTRICT_NAME")
	private String districtName;
	
	@Column(name="AMPHUR_ID")
	private Long amphurId;
	
	@Column(name="PROVINCE_ID")
	private Long provinceId;
	
	@Column(name="GEO_ID")
	private Long geoId;
	
	public Long getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}
	public String getDistrictCode() {
		return districtCode;
	}
	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public Long getAmphurId() {
		return amphurId;
	}
	public void setAmphurId(Long amphurId) {
		this.amphurId = amphurId;
	}
	public Long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}
	public Long getGeoId() {
		return geoId;
	}
	public void setGeoId(Long geoId) {
		this.geoId = geoId;
	}
	
}

package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pentition_form")
public class Pentition {

	@Id
	@Column(name="id_pentition_form")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(name="write_pentition")
    private String write_pentition;
	
	@Column(name="name_pentition")
    private String name_pentition;
	
	@Column(name="idcard_pentition")
    private String idcard_pentition;
	
	public Long getId() {
		return id;
	}

	public String getWrite_pentition() {
		return write_pentition;
	}

	public String getName_pentition() {
		return name_pentition;
	}

	public String getIdcard_pentition() {
		return idcard_pentition;
	}

	public String getNumber_pentition() {
		return number_pentition;
	}

	public String getWork_pentition() {
		return work_pentition;
	}

	public String getSoi_pentition() {
		return soi_pentition;
	}

	public String getRoad_pentition() {
		return road_pentition;
	}

	public String getMoo_pentition() {
		return moo_pentition;
	}

	public String getTumbom_pentition() {
		return tumbom_pentition;
	}

	public String getAumphur_pentition() {
		return aumphur_pentition;
	}

	public String getProvice_pentition() {
		return provice_pentition;
	}

	public String getZipcode_pentition() {
		return zipcode_pentition;
	}

	public String getTel_pentition() {
		return tel_pentition;
	}

	public String getFax_pentition() {
		return fax_pentition;
	}

	public String getPost_pentition() {
		return post_pentition;
	}

	public String getCheck_certificate() {
		return check_certificate;
	}

	public String getCheck_license() {
		return check_license;
	}

	public String getName_send() {
		return name_send;
	}

	public String getFile_pentition1() {
		return file_pentition1;
	}

	public String getFile_pentition2() {
		return file_pentition2;
	}

	public String getFile_pentition3() {
		return file_pentition3;
	}

	public String getFile_pentition4() {
		return file_pentition4;
	}

	public String getFile_pentition5() {
		return file_pentition5;
	}

	public String getFile_pentition6() {
		return file_pentition6;
	}

	public String getFile_pentition7() {
		return file_pentition7;
	}

	public String getFile_pentition8() {
		return file_pentition8;
	}

	public String getFile_pentition9() {
		return file_pentition9;
	}

	public String getFile_pentition10() {
		return file_pentition10;
	}

	public String getCreated_pentition() {
		return created_pentition;
	}

	public String getUpdate_pentition() {
		return update_pentition;
	}

	public int getDeleted() {
		return deleted;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setWrite_pentition(String write_pentition) {
		this.write_pentition = write_pentition;
	}

	public void setName_pentition(String name_pentition) {
		this.name_pentition = name_pentition;
	}

	public void setIdcard_pentition(String idcard_pentition) {
		this.idcard_pentition = idcard_pentition;
	}

	public void setNumber_pentition(String number_pentition) {
		this.number_pentition = number_pentition;
	}

	public void setWork_pentition(String work_pentition) {
		this.work_pentition = work_pentition;
	}

	public void setSoi_pentition(String soi_pentition) {
		this.soi_pentition = soi_pentition;
	}

	public void setRoad_pentition(String road_pentition) {
		this.road_pentition = road_pentition;
	}

	public void setMoo_pentition(String moo_pentition) {
		this.moo_pentition = moo_pentition;
	}

	public void setTumbom_pentition(String tumbom_pentition) {
		this.tumbom_pentition = tumbom_pentition;
	}

	public void setAumphur_pentition(String aumphur_pentition) {
		this.aumphur_pentition = aumphur_pentition;
	}

	public void setProvice_pentition(String provice_pentition) {
		this.provice_pentition = provice_pentition;
	}

	public void setZipcode_pentition(String zipcode_pentition) {
		this.zipcode_pentition = zipcode_pentition;
	}

	public void setTel_pentition(String tel_pentition) {
		this.tel_pentition = tel_pentition;
	}

	public void setFax_pentition(String fax_pentition) {
		this.fax_pentition = fax_pentition;
	}

	public void setPost_pentition(String post_pentition) {
		this.post_pentition = post_pentition;
	}

	public void setCheck_certificate(String check_certificate) {
		this.check_certificate = check_certificate;
	}

	public void setCheck_license(String check_license) {
		this.check_license = check_license;
	}

	public void setName_send(String name_send) {
		this.name_send = name_send;
	}

	public void setFile_pentition1(String file_pentition1) {
		this.file_pentition1 = file_pentition1;
	}

	public void setFile_pentition2(String file_pentition2) {
		this.file_pentition2 = file_pentition2;
	}

	public void setFile_pentition3(String file_pentition3) {
		this.file_pentition3 = file_pentition3;
	}

	public void setFile_pentition4(String file_pentition4) {
		this.file_pentition4 = file_pentition4;
	}

	public void setFile_pentition5(String file_pentition5) {
		this.file_pentition5 = file_pentition5;
	}

	public void setFile_pentition6(String file_pentition6) {
		this.file_pentition6 = file_pentition6;
	}

	public void setFile_pentition7(String file_pentition7) {
		this.file_pentition7 = file_pentition7;
	}

	public void setFile_pentition8(String file_pentition8) {
		this.file_pentition8 = file_pentition8;
	}

	public void setFile_pentition9(String file_pentition9) {
		this.file_pentition9 = file_pentition9;
	}

	public void setFile_pentition10(String file_pentition10) {
		this.file_pentition10 = file_pentition10;
	}

	public void setCreated_pentition(String created_pentition) {
		this.created_pentition = created_pentition;
	}

	public void setUpdate_pentition(String update_pentition) {
		this.update_pentition = update_pentition;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}

	@Column(name="number_pentition")
    private String number_pentition;
	
	@Column(name="work_pentition")
    private String work_pentition;
	
	@Column(name="soi_pentition")
    private String soi_pentition;
	
	@Column(name="road_pentition")
    private String road_pentition;
	
	@Column(name="moo_pentition")
    private String moo_pentition;
	
	@Column(name="tumbom_pentition")
    private String tumbom_pentition;
	
	@Column(name="aumphur_pentition")
    private String aumphur_pentition;
	
	@Column(name="provice_pentition")
    private String provice_pentition;
	
	@Column(name="zipcode_pentition")
    private String zipcode_pentition;
	
	@Column(name="tel_pentition")
    private String tel_pentition;
	
	@Column(name="fax_pentition")
    private String fax_pentition;
	
	@Column(name="post_pentition")
    private String post_pentition;
	
	@Column(name="check_certificate")
    private String check_certificate;
	
	@Column(name="check_license")
    private String check_license;
	
	@Column(name="name_send")
    private String name_send;
	
	@Column(name="file_pentition1")
    private String file_pentition1;
	
	@Column(name="file_pentition2")
    private String file_pentition2;
	
	@Column(name="file_pentition3")
    private String file_pentition3;
	
	@Column(name="file_pentition4")
    private String file_pentition4;
	
	@Column(name="file_pentition5")
    private String file_pentition5;
	
	@Column(name="file_pentition6")
    private String file_pentition6;
	
	@Column(name="file_pentition7")
    private String file_pentition7;
	
	@Column(name="file_pentition8")
    private String file_pentition8;
	
	@Column(name="file_pentition9")
    private String file_pentition9;
	
	@Column(name="file_pentition10")
    private String file_pentition10;
	
	@Column(name="created_pentition")
    private String created_pentition;
	
	@Column(name="update_pentition")
    private String update_pentition;
	
	@Column(name="deleted")
    private int deleted;
	
}

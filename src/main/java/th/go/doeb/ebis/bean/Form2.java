package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "be_person_electric_form" )
public class Form2 {
	@Id
	@Column(name="id_person_electric_form")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_person_electric_form;
	@Column(name="no_certificate_engineer")
	private String lfpp;
	@Column(name="purpose1")
	private String purpose1;
	@Column(name="purpose2")
	private String purpose2;
	
	public Long getId_person_electric_form() {
		return id_person_electric_form;
	}
	public void setId(Long id_person_electric_form) {
		this.id_person_electric_form = id_person_electric_form;
	}
	public String getLfpp() {
		return lfpp;
	}
	public void setLfpp(String lfpp) {
		this.lfpp = lfpp;
	}
	public String getPurpose1() {
		return purpose1;
	}
	public void setPurpose1(String purpose1) {
		this.purpose1 = purpose1;
	}
	public String getPurpose2() {
		return purpose2;
	}
	public void setPurpose2(String purpose2) {
		this.purpose2 = purpose2;
	}
	public void setId_person_electric_form(Long id_person_electric_form) {
		this.id_person_electric_form = id_person_electric_form;
	}	
}

package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ROLE")
public class RoleBean {

	@Id
	@Column(name="ROLE_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_ROLE")
	@SequenceGenerator(name="SEQUENCE_ROLE", sequenceName="SEQUENCE_ROLE", allocationSize=1)
	private Long roleId;
	
	@Column(name="ROLE_NAME")
	private String roleName;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	
}

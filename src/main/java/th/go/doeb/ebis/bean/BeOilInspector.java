package th.go.doeb.ebis.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="be_oil_inspector")
public class BeOilInspector {

	@Id
	@Column(name="id_oil_inspector")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE1")
	@SequenceGenerator(name="SEQUENCE1", sequenceName="SEQUENCE1", allocationSize=1)
	private long id;
	
	@Column(name="write_at")
	private String write_at;
	
	@Column(name="type_service")
	private int type_service;
	
	@Column(name="type_service3_a")
	private int type_service3_a;
	
	@Column(name="level_service")
	private int level_service;
	
	
	public int getLevel_service() {
		return level_service;
	}

	public void setLevel_service(int level_service) {
		this.level_service = level_service;
	}

	@Column(name="id_member")
	private int id_member;
	
	@Column(name="name_applicant")
	private int name_applicant;
	
	@Column(name="name_applicant_eng")
	private int name_applicant_eng;

	@Column(name="corporate_number")
	private int corporate_number;
	
	@Column(name="registered_capital")
	private int registered_capital;
	
	@Column(name="no_address_applicant")
	private int no_address_applicant;
	
	@Column(name="room_address_applicant")
	private int room_address_applicant;

	@Column(name="level_address_applicant")
	private int level_address_applicant;
	
	@Column(name="building_address_applicant")
	private int building_address_applicant;
	
	@Column(name="moo_address_applicant")
	private int moo_address_applicant;
	
	@Column(name="soi_address_applicant")
	private int soi_address_applicant;
	
	@Column(name="road_address_applicant")
	private int road_address_applicant;
	
	@Column(name="district_applicant")
	private int district_applicant;
	
	@Column(name="area_applicant")
	private int area_applicant;
	
	@Column(name="province_applicant")
	private int province_applicant;
	
	@Column(name="postcode_applicant")
	private int postcode_applicant;
	
	@Column(name="phone_applicant")
	private int phone_applicant;
	
	@Column(name="phone_ext_applicant")
	private int phone_ext_applicant;
	
	@Column(name="fax_applicant")
	private int fax_applicant;
	
	@Column(name="fax_ext_applicant")
	private int fax_ext_applicant;
	
	@Column(name="mobile_applicant")
	private int mobile_applicant;
	
	@Column(name="email_applicant")
	private int email_applicant;
	
	@Column(name="web_applicant")
	private int web_applicant;

	@Column(name="prefix_name_recipients")
	private String prefix_name_recipients;
	
	@Column(name="name_recipients")
	private String name_recipients;
	
	@Column(name="lastname_recipients")
	private String lastname_recipients;
	
	@Column(name="no_address_recipients")
	private String no_address_recipients;
	
	@Column(name="room_address_recipients")
	private String room_address_recipients;
	
	@Column(name="level_address_recipients")
	private String level_address_recipients;
	
	@Column(name="building_address_recipients")
	private String building_address_recipients;
	
	@Column(name="soi_address_recipients")
	private String soi_address_recipients;
	
	@Column(name="road_address_recipients")
	private String road_address_recipients;
	
	@Column(name="moo_address_recipients")
	private String moo_address_recipients;
	
	@Column(name="district_recipients")
	private int district_recipients;
	
	@Column(name="area_recipients")
	private String area_recipients;
	
	@Column(name="province_recipients")
	private String province_recipients;
	
	@Column(name="postcode_recipients")
	private String postcode_recipients;
	
	@Column(name="phone_recipients")
	private String phone_recipients;
	
	@Column(name="phone_ext_recipients")
	private String phone_ext_recipients;
	
	@Column(name="fax_recipients")
	private String fax_recipients;
	
	@Column(name="fax_ext_recipients")
	private String fax_ext_recipients;
	
	@Column(name="mobile_recipients")
	private String mobile_recipients;
	
	@Column(name="email_recipients")
	private String email_recipients;
	
	@Column(name="web_recipients")
	private String web_recipients;
	
	@Column(name="prefix_name_coordination")
	private String prefix_name_coordination;
	
	@Column(name="name_coordination")
	private String name_coordination;
	
	@Column(name="lastname_coordination")
	private String lastname_coordination;
	
	@Column(name="phone_coordination")
	private String phone_coordination;
	
	@Column(name="phone_ext_coordination")
	private String phone_ext_coordination;
	
	@Column(name="fax_coordination")
	private String fax_coordination;
	
	@Column(name="fax_ext_coordination")
	private String fax_ext_coordination;
	
	@Column(name="mobile_coordination")
	private String mobile_coordination;
	
	@Column(name="email_coordination")
	private String email_coordination;
	
	@Column(name="web_coordination")
	private String web_coordination;
	
	@Column(name="ch3_1")
	private String ch3_1;
	
	@Column(name="ch3_2")
	private String ch3_2;
	
	@Column(name="ch3_3")
	private String ch3_3;
	
	@Column(name="ch3_4")
	private String ch3_4;
	
	@Column(name="ch3_5")
	private String ch3_5;
	
	@Column(name="ch3_6")
	private String ch3_6;
	
	@Column(name="ch3_7")
	private String ch3_7;
	
	@Column(name="ch3_8")
	private String ch3_8;
	
	@Column(name="ch3_9")
	private String ch3_9;
	
	@Column(name="file_ch3_1")
	private String file_ch3_1;
	
	@Column(name="file_ch3_2")
	private String file_ch3_2;
	
	@Column(name="file_ch3_3")
	private String file_ch3_3;
	
	@Column(name="file_ch3_4")
	private String file_ch3_4;
	
	@Column(name="file_ch3_5")
	private String file_ch3_5;
	
	@Column(name="file_ch3_6")
	private String file_ch3_6;
	
	@Column(name="file_ch3_7")
	private String file_ch3_7;
	
	@Column(name="file_ch3_8")
	private String file_ch3_8;
	
	@Column(name="file_ch3_9")
	private String file_ch3_9;
	
	@Column(name="ch4_1")
	private String ch4_1;
	
	@Column(name="ch4_2")
	private String ch4_2;
	
	@Column(name="ch4_3")
	private String ch4_3;
	
	@Column(name="ch4_4")
	private String ch4_4;
	
	@Column(name="ch4_5")
	private String ch4_5;
	
	@Column(name="ch4_6")
	private String ch4_6;
	
	@Column(name="ch4_7")
	private String ch4_7;
	
	@Column(name="file_ch4_1")
	private String file_ch4_1;

	@Column(name="file_ch4_2")
	private String file_ch4_2;
	
	@Column(name="file_ch4_3")
	private String file_ch4_3;
	
	@Column(name="file_ch4_4")
	private String file_ch4_4;
	
	@Column(name="file_ch4_5")
	private String file_ch4_5;
	
	@Column(name="file_ch4_6")
	private String file_ch4_6;
	
	@Column(name="file_ch4_7")
	private String file_ch4_7;
	
	public int getType_service3_a() {
		return type_service3_a;
	}

	public void setType_service3_a(int type_service3_a) {
		this.type_service3_a = type_service3_a;
	}

	public int getName_applicant() {
		return name_applicant;
	}

	public void setName_applicant(int name_applicant) {
		this.name_applicant = name_applicant;
	}

	public int getName_applicant_eng() {
		return name_applicant_eng;
	}

	public void setName_applicant_eng(int name_applicant_eng) {
		this.name_applicant_eng = name_applicant_eng;
	}

	public int getCorporate_number() {
		return corporate_number;
	}

	public void setCorporate_number(int corporate_number) {
		this.corporate_number = corporate_number;
	}

	public int getRegistered_capital() {
		return registered_capital;
	}

	public void setRegistered_capital(int registered_capital) {
		this.registered_capital = registered_capital;
	}

	public int getNo_address_applicant() {
		return no_address_applicant;
	}

	public void setNo_address_applicant(int no_address_applicant) {
		this.no_address_applicant = no_address_applicant;
	}

	public int getRoom_address_applicant() {
		return room_address_applicant;
	}

	public void setRoom_address_applicant(int room_address_applicant) {
		this.room_address_applicant = room_address_applicant;
	}

	public int getLevel_address_applicant() {
		return level_address_applicant;
	}

	public void setLevel_address_applicant(int level_address_applicant) {
		this.level_address_applicant = level_address_applicant;
	}

	public int getBuilding_address_applicant() {
		return building_address_applicant;
	}

	public void setBuilding_address_applicant(int building_address_applicant) {
		this.building_address_applicant = building_address_applicant;
	}

	public int getMoo_address_applicant() {
		return moo_address_applicant;
	}

	public void setMoo_address_applicant(int moo_address_applicant) {
		this.moo_address_applicant = moo_address_applicant;
	}

	public int getSoi_address_applicant() {
		return soi_address_applicant;
	}

	public void setSoi_address_applicant(int soi_address_applicant) {
		this.soi_address_applicant = soi_address_applicant;
	}

	public int getRoad_address_applicant() {
		return road_address_applicant;
	}

	public void setRoad_address_applicant(int road_address_applicant) {
		this.road_address_applicant = road_address_applicant;
	}

	public int getDistrict_applicant() {
		return district_applicant;
	}

	public void setDistrict_applicant(int district_applicant) {
		this.district_applicant = district_applicant;
	}

	public int getArea_applicant() {
		return area_applicant;
	}

	public void setArea_applicant(int area_applicant) {
		this.area_applicant = area_applicant;
	}

	public int getProvince_applicant() {
		return province_applicant;
	}

	public void setProvince_applicant(int province_applicant) {
		this.province_applicant = province_applicant;
	}

	public int getPostcode_applicant() {
		return postcode_applicant;
	}

	public void setPostcode_applicant(int postcode_applicant) {
		this.postcode_applicant = postcode_applicant;
	}

	public int getPhone_applicant() {
		return phone_applicant;
	}

	public void setPhone_applicant(int phone_applicant) {
		this.phone_applicant = phone_applicant;
	}

	public int getPhone_ext_applicant() {
		return phone_ext_applicant;
	}

	public void setPhone_ext_applicant(int phone_ext_applicant) {
		this.phone_ext_applicant = phone_ext_applicant;
	}

	public int getFax_applicant() {
		return fax_applicant;
	}

	public void setFax_applicant(int fax_applicant) {
		this.fax_applicant = fax_applicant;
	}

	public int getFax_ext_applicant() {
		return fax_ext_applicant;
	}

	public void setFax_ext_applicant(int fax_ext_applicant) {
		this.fax_ext_applicant = fax_ext_applicant;
	}

	public int getMobile_applicant() {
		return mobile_applicant;
	}

	public void setMobile_applicant(int mobile_applicant) {
		this.mobile_applicant = mobile_applicant;
	}

	public int getEmail_applicant() {
		return email_applicant;
	}

	public void setEmail_applicant(int email_applicant) {
		this.email_applicant = email_applicant;
	}

	public int getWeb_applicant() {
		return web_applicant;
	}

	public void setWeb_applicant(int web_applicant) {
		this.web_applicant = web_applicant;
	}

	public int getDistrict_recipients() {
		return district_recipients;
	}

	public void setDistrict_recipients(int district_recipients) {
		this.district_recipients = district_recipients;
	}

	public String getArea_recipients() {
		return area_recipients;
	}

	public void setArea_recipients(String area_recipients) {
		this.area_recipients = area_recipients;
	}

	public String getProvince_recipients() {
		return province_recipients;
	}

	public void setProvince_recipients(String province_recipients) {
		this.province_recipients = province_recipients;
	}

	public String getPostcode_recipients() {
		return postcode_recipients;
	}

	public void setPostcode_recipients(String postcode_recipients) {
		this.postcode_recipients = postcode_recipients;
	}

	public String getFile_ch3_1() {
		return file_ch3_1;
	}

	public void setFile_ch3_1(String file_ch3_1) {
		this.file_ch3_1 = file_ch3_1;
	}

	public String getFile_ch3_2() {
		return file_ch3_2;
	}

	public void setFile_ch3_2(String file_ch3_2) {
		this.file_ch3_2 = file_ch3_2;
	}

	public String getFile_ch3_3() {
		return file_ch3_3;
	}

	public void setFile_ch3_3(String file_ch3_3) {
		this.file_ch3_3 = file_ch3_3;
	}

	public String getFile_ch3_4() {
		return file_ch3_4;
	}

	public void setFile_ch3_4(String file_ch3_4) {
		this.file_ch3_4 = file_ch3_4;
	}

	public String getFile_ch3_5() {
		return file_ch3_5;
	}

	public void setFile_ch3_5(String file_ch3_5) {
		this.file_ch3_5 = file_ch3_5;
	}

	public String getFile_ch3_6() {
		return file_ch3_6;
	}

	public void setFile_ch3_6(String file_ch3_6) {
		this.file_ch3_6 = file_ch3_6;
	}

	public String getFile_ch3_7() {
		return file_ch3_7;
	}

	public void setFile_ch3_7(String file_ch3_7) {
		this.file_ch3_7 = file_ch3_7;
	}

	public String getFile_ch3_8() {
		return file_ch3_8;
	}

	public void setFile_ch3_8(String file_ch3_8) {
		this.file_ch3_8 = file_ch3_8;
	}

	public String getFile_ch3_9() {
		return file_ch3_9;
	}

	public void setFile_ch3_9(String file_ch3_9) {
		this.file_ch3_9 = file_ch3_9;
	}

	public String getFile_ch4_1() {
		return file_ch4_1;
	}

	public void setFile_ch4_1(String file_ch4_1) {
		this.file_ch4_1 = file_ch4_1;
	}

	public String getFile_ch4_2() {
		return file_ch4_2;
	}

	public void setFile_ch4_2(String file_ch4_2) {
		this.file_ch4_2 = file_ch4_2;
	}

	public String getFile_ch4_3() {
		return file_ch4_3;
	}

	public void setFile_ch4_3(String file_ch4_3) {
		this.file_ch4_3 = file_ch4_3;
	}

	public String getFile_ch4_4() {
		return file_ch4_4;
	}

	public void setFile_ch4_4(String file_ch4_4) {
		this.file_ch4_4 = file_ch4_4;
	}

	public String getFile_ch4_5() {
		return file_ch4_5;
	}

	public void setFile_ch4_5(String file_ch4_5) {
		this.file_ch4_5 = file_ch4_5;
	}

	public String getFile_ch4_6() {
		return file_ch4_6;
	}

	public void setFile_ch4_6(String file_ch4_6) {
		this.file_ch4_6 = file_ch4_6;
	}

	public String getFile_ch4_7() {
		return file_ch4_7;
	}

	public void setFile_ch4_7(String file_ch4_7) {
		this.file_ch4_7 = file_ch4_7;
	}

	@Column(name="createdate")
	private Date createdate;
	
	@Column(name="updatedate")
	private Date updatedate;
	
	@Column(name="deleted")
	private int deleted;

	public long getId() {
		return id;
	}

	public int getType_service() {
		return type_service;
	}

	public int getId_member() {
		return id_member;
	}

	public String getPrefix_name_recipients() {
		return prefix_name_recipients;
	}

	public String getWrite_at() {
		return write_at;
	}

	public void setWrite_at(String write_at) {
		this.write_at = write_at;
	}

	public String getName_recipients() {
		return name_recipients;
	}

	public String getLastname_recipients() {
		return lastname_recipients;
	}

	public String getNo_address_recipients() {
		return no_address_recipients;
	}

	public String getRoom_address_recipients() {
		return room_address_recipients;
	}

	public String getLevel_address_recipients() {
		return level_address_recipients;
	}

	public String getBuilding_address_recipients() {
		return building_address_recipients;
	}

	public String getSoi_address_recipients() {
		return soi_address_recipients;
	}

	public String getRoad_address_recipients() {
		return road_address_recipients;
	}

	public String getPhone_recipients() {
		return phone_recipients;
	}

	public String getPhone_ext_recipients() {
		return phone_ext_recipients;
	}

	public String getFax_recipients() {
		return fax_recipients;
	}

	public String getFax_ext_recipients() {
		return fax_ext_recipients;
	}

	public String getMobile_recipients() {
		return mobile_recipients;
	}

	public String getEmail_recipients() {
		return email_recipients;
	}

	public String getWeb_recipients() {
		return web_recipients;
	}

	public String getPrefix_name_coordination() {
		return prefix_name_coordination;
	}

	public String getName_coordination() {
		return name_coordination;
	}

	public String getLastname_coordination() {
		return lastname_coordination;
	}

	public String getPhone_coordination() {
		return phone_coordination;
	}

	public String getPhone_ext_coordination() {
		return phone_ext_coordination;
	}

	public String getFax_coordination() {
		return fax_coordination;
	}

	public String getFax_ext_coordination() {
		return fax_ext_coordination;
	}

	public String getMobile_coordination() {
		return mobile_coordination;
	}

	public String getEmail_coordination() {
		return email_coordination;
	}

	public String getWeb_coordination() {
		return web_coordination;
	}

	public String getCh3_1() {
		return ch3_1;
	}

	public String getCh3_2() {
		return ch3_2;
	}

	public String getCh3_3() {
		return ch3_3;
	}

	public String getCh3_4() {
		return ch3_4;
	}

	public String getCh3_5() {
		return ch3_5;
	}

	public String getCh3_6() {
		return ch3_6;
	}

	public String getCh3_7() {
		return ch3_7;
	}

	public String getCh3_8() {
		return ch3_8;
	}

	public String getCh3_9() {
		return ch3_9;
	}

	public String getCh4_1() {
		return ch4_1;
	}

	public String getCh4_2() {
		return ch4_2;
	}

	public String getCh4_3() {
		return ch4_3;
	}

	public String getCh4_4() {
		return ch4_4;
	}

	public String getCh4_5() {
		return ch4_5;
	}

	public String getCh4_6() {
		return ch4_6;
	}

	public String getCh4_7() {
		return ch4_7;
	}

	

	public Date getCreatedate() {
		return createdate;
	}

	public Date getUpdatedate() {
		return updatedate;
	}

	public int getDeleted() {
		return deleted;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setType_service(int type_service) {
		this.type_service = type_service;
	}

	public void setId_member(int id_member) {
		this.id_member = id_member;
	}

	public void setPrefix_name_recipients(String prefix_name_recipients) {
		this.prefix_name_recipients = prefix_name_recipients;
	}

	public void setName_recipients(String name_recipients) {
		this.name_recipients = name_recipients;
	}

	public void setLastname_recipients(String lastname_recipients) {
		this.lastname_recipients = lastname_recipients;
	}

	public void setNo_address_recipients(String no_address_recipients) {
		this.no_address_recipients = no_address_recipients;
	}

	public void setRoom_address_recipients(String room_address_recipients) {
		this.room_address_recipients = room_address_recipients;
	}

	public void setLevel_address_recipients(String level_address_recipients) {
		this.level_address_recipients = level_address_recipients;
	}

	public void setBuilding_address_recipients(String building_address_recipients) {
		this.building_address_recipients = building_address_recipients;
	}

	public void setSoi_address_recipients(String soi_address_recipients) {
		this.soi_address_recipients = soi_address_recipients;
	}

	public void setRoad_address_recipients(String road_address_recipients) {
		this.road_address_recipients = road_address_recipients;
	}

	public void setPhone_recipients(String phone_recipients) {
		this.phone_recipients = phone_recipients;
	}

	public void setPhone_ext_recipients(String phone_ext_recipients) {
		this.phone_ext_recipients = phone_ext_recipients;
	}

	public void setFax_recipients(String fax_recipients) {
		this.fax_recipients = fax_recipients;
	}

	public void setFax_ext_recipients(String fax_ext_recipients) {
		this.fax_ext_recipients = fax_ext_recipients;
	}

	public void setMobile_recipients(String mobile_recipients) {
		this.mobile_recipients = mobile_recipients;
	}

	public void setEmail_recipients(String email_recipients) {
		this.email_recipients = email_recipients;
	}

	public void setWeb_recipients(String web_recipients) {
		this.web_recipients = web_recipients;
	}

	public void setPrefix_name_coordination(String prefix_name_coordination) {
		this.prefix_name_coordination = prefix_name_coordination;
	}

	public void setName_coordination(String name_coordination) {
		this.name_coordination = name_coordination;
	}

	public void setLastname_coordination(String lastname_coordination) {
		this.lastname_coordination = lastname_coordination;
	}

	public void setPhone_coordination(String phone_coordination) {
		this.phone_coordination = phone_coordination;
	}

	public void setPhone_ext_coordination(String phone_ext_coordination) {
		this.phone_ext_coordination = phone_ext_coordination;
	}

	public void setFax_coordination(String fax_coordination) {
		this.fax_coordination = fax_coordination;
	}

	public void setFax_ext_coordination(String fax_ext_coordination) {
		this.fax_ext_coordination = fax_ext_coordination;
	}

	public void setMobile_coordination(String mobile_coordination) {
		this.mobile_coordination = mobile_coordination;
	}

	public void setEmail_coordination(String email_coordination) {
		this.email_coordination = email_coordination;
	}

	public void setWeb_coordination(String web_coordination) {
		this.web_coordination = web_coordination;
	}

	public void setCh3_1(String ch3_1) {
		this.ch3_1 = ch3_1;
	}

	public void setCh3_2(String ch3_2) {
		this.ch3_2 = ch3_2;
	}

	public void setCh3_3(String ch3_3) {
		this.ch3_3 = ch3_3;
	}

	public void setCh3_4(String ch3_4) {
		this.ch3_4 = ch3_4;
	}

	public void setCh3_5(String ch3_5) {
		this.ch3_5 = ch3_5;
	}

	public void setCh3_6(String ch3_6) {
		this.ch3_6 = ch3_6;
	}

	public void setCh3_7(String ch3_7) {
		this.ch3_7 = ch3_7;
	}

	public void setCh3_8(String ch3_8) {
		this.ch3_8 = ch3_8;
	}

	public void setCh3_9(String ch3_9) {
		this.ch3_9 = ch3_9;
	}

	public void setCh4_1(String ch4_1) {
		this.ch4_1 = ch4_1;
	}

	public void setCh4_2(String ch4_2) {
		this.ch4_2 = ch4_2;
	}

	public void setCh4_3(String ch4_3) {
		this.ch4_3 = ch4_3;
	}

	public void setCh4_4(String ch4_4) {
		this.ch4_4 = ch4_4;
	}

	public void setCh4_5(String ch4_5) {
		this.ch4_5 = ch4_5;
	}

	public void setCh4_6(String ch4_6) {
		this.ch4_6 = ch4_6;
	}

	public void setCh4_7(String ch4_7) {
		this.ch4_7 = ch4_7;
	}

	

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}

	public String getMoo_address_recipients() {
		return moo_address_recipients;
	}

	public void setMoo_address_recipients(String moo_address_recipients) {
		this.moo_address_recipients = moo_address_recipients;
	}
	
	
	
}

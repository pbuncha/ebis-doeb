package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PROVINCE")
public class ProvinceBean {
	
	@Id
	@Column(name="PROVINCE_ID")
	private Integer provinceId;
	
	@Column(name="PROVINCE_CODE")
	private String provinceCode;
	
	@Column(name="PROVINCE_NAME")
	private String provinceName;
	
	@Column(name="GEO_ID")
	private Integer geoId;
	
	public Integer getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}
	public String getProvinceCode() {
		return provinceCode;
	}
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public Integer getGeoId() {
		return geoId;
	}
	public void setGeoId(Integer geoId) {
		this.geoId = geoId;
	}
}

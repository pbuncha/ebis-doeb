package th.go.doeb.ebis.bean;


import java.util.List;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="notice_form")

public class Notice {
	@Id
	@Column(name="id_notice_form")
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE2")
	@SequenceGenerator(name="SEQUENCE2", sequenceName="SEQUENCE2", allocationSize=1)
	Long id;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	
	
	public int getId_member() {
		return id_member;
	}
	public String getWrite_at() {
		return write_at;
	}
	public String getPurpose1() {
		return purpose1;
	}
	public String getPurpose2() {
		return purpose2;
	}
	public String getPurpose3() {
		return purpose3;
	}
	public String getPurpose4() {
		return purpose4;
	}
	public String getPurpose5() {
		return purpose5;
	}
	public String getPurpose6() {
		return purpose6;
	}
	public String getPurpose7() {
		return purpose7;
	}
	public String getPurpose8() {
		return purpose8;
	}
	public String getPurpose9() {
		return purpose9;
	}
	public String getPurpose10() {
		return purpose10;
	}
	
	public String getResult_channel1() {
		return result_channel1;
	}
	public String getResult_channel2() {
		return result_channel2;
	}
	public String getDate_approval_document() {
		return date_approval_document;
	}
	public String getCreatedate() {
		return createdate;
	}
	public int getId_status_document() {
		return id_status_document;
	}
	public int getDeleted() {
		return deleted;
	}
	
	public void setId_member(int id_member) {
		this.id_member = id_member;
	}
	public void setWrite_at(String write_at) {
		this.write_at = write_at;
	}
	public void setPurpose1(String purpose1) {
		this.purpose1 = purpose1;
	}
	public void setPurpose2(String purpose2) {
		this.purpose2 = purpose2;
	}
	public void setPurpose3(String purpose3) {
		this.purpose3 = purpose3;
	}
	public void setPurpose4(String purpose4) {
		this.purpose4 = purpose4;
	}
	public void setPurpose5(String purpose5) {
		this.purpose5 = purpose5;
	}
	public void setPurpose6(String purpose6) {
		this.purpose6 = purpose6;
	}
	public void setPurpose7(String purpose7) {
		this.purpose7 = purpose7;
	}
	public void setPurpose8(String purpose8) {
		this.purpose8 = purpose8;
	}
	public void setPurpose9(String purpose9) {
		this.purpose9 = purpose9;
	}
	public void setPurpose10(String purpose10) {
		this.purpose10 = purpose10;
	}
	
	public void setResult_channel1(String result_channel1) {
		this.result_channel1 = result_channel1;
	}
	public void setResult_channel2(String result_channel2) {
		this.result_channel2 = result_channel2;
	}
	public void setDate_approval_document(String date_approval_document) {
		this.date_approval_document = date_approval_document;
	}
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	public void setId_status_document(int id_status_document) {
		this.id_status_document = id_status_document;
	}
	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}
	
	@Column(name="id_member")
	private int id_member;
	
	@Column(name="write_at")
    private String write_at;
	
	@Column(name="date_notice")
    private String date_notice;
	
	@Column(name="moth_notice")
    private String moth_notice;
	
	@Column(name="year_notice")
    private String year_notice;
	
	
	public String getDate_notice() {
		return date_notice;
	}
	public String getMoth_notice() {
		return moth_notice;
	}
	public String getYear_notice() {
		return year_notice;
	}
	public void setDate_notice(String date_notice) {
		this.date_notice = date_notice;
	}
	public void setMoth_notice(String moth_notice) {
		this.moth_notice = moth_notice;
	}
	public void setYear_notice(String year_notice) {
		this.year_notice = year_notice;
	}

	@Column(name="name_member")
    private String name_member;
	
	@Column(name="age")
    private String age;
	
	@Column(name="idcard")
    private String idcard;
	
	@Column(name="numberhome")
    private String numberhome;
	
	@Column(name="moo")
    private String moo;
	
	@Column(name="soi")
    private String soi;
	
	@Column(name="road")
    private String road;
	
	@Column(name="tumbon")
    private String tumbon;
	
	@Column(name="aumphur")
    private String aumphur;
	
	@Column(name="province")
    private String province;
	
	@Column(name="zipcode")
    private String zipcode;
	
	@Column(name="tel")
    private String tel;
	
	@Column(name="fax")
    private String fax;
	
	@Column(name="email")
    private String email;
	
	@Column(name="purpose1")
    private String purpose1;
	
	@Column(name="purpose2")
    private String purpose2;
	
	@Column(name="purpose3")
    private String purpose3;
	
	@Column(name="purpose4")
    private String purpose4;
	
	@Column(name="purpose5")
    private String purpose5;
	
	@Column(name="purpose6")
    private String purpose6;
	
	@Column(name="purpose7")
    private String purpose7;
	
	@Column(name="purpose8")
    private String purpose8;
	
	@Column(name="purpose9")
    private String purpose9;
	
	@Column(name="purpose10")
    private String purpose10;
	
	@Column(name="file_purpose1")
    private String file_purpose1;
	
	@Column(name="file_purpose2")
    private String file_purpose2;
	
	@Column(name="file_purpose3")
    private String file_purpose3;
	
	@Column(name="file_purpose4")
    private String file_purpose4;
	
	@Column(name="file_purpose5")
    private String file_purpose5;
	
	@Column(name="file_purpose6")
    private String file_purpose6;
	
	@Column(name="file_purpose7")
    private String file_purpose7;
	
	@Column(name="file_purpose8")
    private String file_purpose8;
	
	@Column(name="file_purpose9")
    private String file_purpose9;
	
	@Column(name="file_purpose10")
    private String file_purpose10;
	
	@Column(name="place_service")
    private String place_service;
	
	@Column(name="numhome_service")
    private String numhome_service;
	
	@Column(name="moo_service")
    private String moo_service;
	
	@Column(name="soi_service")
    private String soi_service;
	
	@Column(name="road_service")
    private String road_service;
	
	@Column(name="tumbon_service")
    private String tumbon_service;
	
	@Column(name="aumphur_service")
    private String aumphur_service;
	
	@Column(name="province_service")
    private String province_service;
	
	@Column(name="zipcode_service")
    private String zipcode_service;
	
	@Column(name="tel_service")
    private String tel_service;
	
	@Column(name="fax_service")
    private String fax_service;
	
	@Column(name="email_service")
    private String email_service;
	
	@Column(name="type_service")
    private String type_service;
	
	@Column(name="result_channel1")
    private String result_channel1;
	
	@Column(name="result_channel2")
    private String result_channel2;
	
	@Column(name="date_approval_document")
    private String date_approval_document;
	
	@Column(name="createdate")
    private String createdate;
	
	@Column(name="id_status_document")
    private int id_status_document;
	
	@Column(name="deleted")
    private int deleted;


	public String getName_member() {
		return name_member;
	}
	public String getAge() {
		return age;
	}
	public String getIdcard() {
		return idcard;
	}
	public String getNumberhome() {
		return numberhome;
	}
	public String getMoo() {
		return moo;
	}
	public String getSoi() {
		return soi;
	}
	public String getRoad() {
		return road;
	}
	public String getTumbon() {
		return tumbon;
	}
	public String getAumphur() {
		return aumphur;
	}
	public String getProvince() {
		return province;
	}
	public String getZipcode() {
		return zipcode;
	}
	public String getTel() {
		return tel;
	}
	public String getFax() {
		return fax;
	}
	public String getEmail() {
		return email;
	}
	public String getFile_purpose2() {
		return file_purpose2;
	}
	public String getFile_purpose3() {
		return file_purpose3;
	}
	public String getFile_purpose4() {
		return file_purpose4;
	}
	public String getFile_purpose5() {
		return file_purpose5;
	}
	public String getFile_purpose6() {
		return file_purpose6;
	}
	public String getFile_purpose7() {
		return file_purpose7;
	}
	public String getFile_purpose8() {
		return file_purpose8;
	}
	public String getFile_purpose9() {
		return file_purpose9;
	}
	public String getFile_purpose10() {
		return file_purpose10;
	}
	public String getPlace_service() {
		return place_service;
	}
	public String getNumhome_service() {
		return numhome_service;
	}
	public String getMoo_service() {
		return moo_service;
	}
	public String getSoi_service() {
		return soi_service;
	}
	public String getRoad_service() {
		return road_service;
	}
	public String getTumbon_service() {
		return tumbon_service;
	}
	public String getAumphur_service() {
		return aumphur_service;
	}
	public String getProvince_service() {
		return province_service;
	}
	public String getZipcode_service() {
		return zipcode_service;
	}
	public String getTel_service() {
		return tel_service;
	}
	public String getFax_service() {
		return fax_service;
	}
	public String getEmail_service() {
		return email_service;
	}
	public String getType_service() {
		return type_service;
	}
	public void setName_member(String name_member) {
		this.name_member = name_member;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public void setNumberhome(String numberhome) {
		this.numberhome = numberhome;
	}
	public void setMoo(String moo) {
		this.moo = moo;
	}
	public void setSoi(String soi) {
		this.soi = soi;
	}
	public void setRoad(String road) {
		this.road = road;
	}
	public void setTumbon(String tumbon) {
		this.tumbon = tumbon;
	}
	public void setAumphur(String aumphur) {
		this.aumphur = aumphur;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setFile_purpose2(String file_purpose2) {
		this.file_purpose2 = file_purpose2;
	}
	public void setFile_purpose3(String file_purpose3) {
		this.file_purpose3 = file_purpose3;
	}
	public void setFile_purpose4(String file_purpose4) {
		this.file_purpose4 = file_purpose4;
	}
	public void setFile_purpose5(String file_purpose5) {
		this.file_purpose5 = file_purpose5;
	}
	public void setFile_purpose6(String file_purpose6) {
		this.file_purpose6 = file_purpose6;
	}
	public void setFile_purpose7(String file_purpose7) {
		this.file_purpose7 = file_purpose7;
	}
	public void setFile_purpose8(String file_purpose8) {
		this.file_purpose8 = file_purpose8;
	}
	public void setFile_purpose9(String file_purpose9) {
		this.file_purpose9 = file_purpose9;
	}
	public void setFile_purpose10(String file_purpose10) {
		this.file_purpose10 = file_purpose10;
	}
	public void setPlace_service(String place_service) {
		this.place_service = place_service;
	}
	public void setNumhome_service(String numhome_service) {
		this.numhome_service = numhome_service;
	}
	public void setMoo_service(String moo_service) {
		this.moo_service = moo_service;
	}
	public void setSoi_service(String soi_service) {
		this.soi_service = soi_service;
	}
	public void setRoad_service(String road_service) {
		this.road_service = road_service;
	}
	public void setTumbon_service(String tumbon_service) {
		this.tumbon_service = tumbon_service;
	}
	public void setAumphur_service(String aumphur_service) {
		this.aumphur_service = aumphur_service;
	}
	public void setProvince_service(String province_service) {
		this.province_service = province_service;
	}
	public void setZipcode_service(String zipcode_service) {
		this.zipcode_service = zipcode_service;
	}
	public void setTel_service(String tel_service) {
		this.tel_service = tel_service;
	}
	public void setFax_service(String fax_service) {
		this.fax_service = fax_service;
	}
	public void setEmail_service(String email_service) {
		this.email_service = email_service;
	}
	public void setType_service(String type_service) {
		this.type_service = type_service;
	}
	public String getFile_purpose1() {
		return file_purpose1;
	}
	public void setFile_purpose1(String file_purpose1) {
		this.file_purpose1 = file_purpose1;
	}



}

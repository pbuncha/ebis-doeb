package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="member")	
public class MemBean {
	@Id
	@Column(name="id_member")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	
	@Column(name="user_name")
	String 	user;

	@Column(name="password")
	String 	pass;
	
	

	@Column(name="name_member")
	String 	Name;
	
	@Column(name="lastname_member")
	String 	Lname;
	
	@Column(name="birthday_member")
	String 	Age;
	
	@Column(name="id_card_member")
	String 	Idnum;
	
	@Column(name="no_address_member")
	String 	Noadd;
	
	@Column(name="	moo_address_member	")
	String 	Madd;
	
	@Column(name="soi_address_member")
	String 	Sadd;
	
	@Column(name="road_address_member")
	String 	Radd;
	
	@Column(name="id_district")
	String 	IdDT;
	
	@Column(name="phone_no_member")
	String 	Phone;
	
	@Column(name="fax_no_member")
	String 	Fax;
	
	@Column(name="email_member")
	String 	Email;
	
	@Column(name="createdate")
	String 	Cdate;
	
	@Column(name="updatedate")
	String 	Udate;

	public Long getId() {
		return id;
	}

	public String getName() {
		return Name;
	}

	public String getLname() {
		return Lname;
	}



	public String getIdnum() {
		return Idnum;
	}

	public String getNoadd() {
		return Noadd;
	}

	public String getMadd() {
		return Madd;
	}

	public String getSadd() {
		return Sadd;
	}

	public String getRadd() {
		return Radd;
	}

	public String getIdDT() {
		return IdDT;
	}

	public String getPhone() {
		return Phone;
	}

	public String getFax() {
		return Fax;
	}

	public String getEmail() {
		return Email;
	}

	public String getCdate() {
		return Cdate;
	}

	public String getUdate() {
		return Udate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public void setName(String name) {
		Name = name;
	}

	public void setLname(String lname) {
		Lname = lname;
	}



	public String getAge() {
		return Age;
	}

	public void setAge(String age) {
		Age = age;
	}

	public void setIdnum(String idnum) {
		this.Idnum = idnum;
	}

	public void setNoadd(String noadd) {
		Noadd = noadd;
	}

	public void setMadd(String madd) {
		Madd = madd;
	}

	public void setSadd(String sadd) {
		Sadd = sadd;
	}

	public void setRadd(String radd) {
		Radd = radd;
	}

	public void setIdDT(String idDT) {
		this.IdDT = idDT;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public void setCdate(String cdate) {
		Cdate = cdate;
	}

	public void setUdate(String udate) {
		Udate = udate;
	}

	public String getUser() {
		return user;
	}

	public String getPass() {
		return pass;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public void setuser(String user2) {
		// TODO Auto-generated method stub
		
	}

	public void setpass(String pass2) {
		// TODO Auto-generated method stub
		
	}
	
	
}

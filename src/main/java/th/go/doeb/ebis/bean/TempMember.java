package th.go.doeb.ebis.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="TEMP_MEMBER")	
public class TempMember {
	
	@Id
	@Column(name="MEMBER_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE9")
	@SequenceGenerator(name="SEQUENCE9", sequenceName="SEQUENCE9", allocationSize=1)
	private Long memberId;

	@Column(name="MEMBER_USERNAME")
	private String memberUsername;
	
	@Column(name="MEMBER_PASSWORD")
	private String memberPassword;
	
	@Column(name="MEMBER_CREATED")
	@Temporal(TemporalType.TIMESTAMP)
	private Date memberCreated;
	
	@Column(name="MEMBER_IP")
	private String memberIp;
	
	public Long getMemberId() {
		return memberId;
	}
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	public String getMemberUsername() {
		return memberUsername;
	}
	public void setMemberUsername(String memberUsername) {
		this.memberUsername = memberUsername;
	}
	public String getMemberPassword() {
		return memberPassword;
	}
	public void setMemberPassword(String memberPassword) {
		this.memberPassword = memberPassword;
	}
	public Date getMemberCreated() {
		return memberCreated;
	}
	public void setMemberCreated(Date memberCreated) {
		this.memberCreated = memberCreated;
	}
	public String getMemberIp() {
		return memberIp;
	}
	public void setMemberIp(String memberIp) {
		this.memberIp = memberIp;
	}
	
}

package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="COMPANY")
public class CompanyBean {
	
	@Id
	@Column(name="DBD_ID")
	private String dbdId;
	
	@Column(name="COMPANY_NAME")
	private String companyName;
	
	@Column(name="ADDRESS_NO")
	private String addressNo;
	
	@Column(name="ROOM")
	private String room;
	
	@Column(name="FLOOR")
	private String floor;
	
	@Column(name="BUILDING")
	private String building;
	
	@Column(name="MOO")
	private String moo;
	
	@Column(name="SOI")
	private String soi;
	
	@Column(name="STREET")
	private String street;
	
	@Column(name="DISTRICT_ID")
	private String districtId;
	
	@Column(name="AMPHUR_ID")
	private String amphurId;
	
	@Column(name="PROVINCE_ID")
	private String provinceId;
	
	@Column(name="ZIPCODE")
	private String zipcode;
	
	@Column(name="DOC")
	private String doc;
	
	@Column(name="MEMBER_ID")
	private String memberId;
	
	public String getDbdId() {
		return dbdId;
	}
	public void setDbdId(String dbdId) {
		this.dbdId = dbdId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getAddressNo() {
		return addressNo;
	}
	public void setAddressNo(String addressNo) {
		this.addressNo = addressNo;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public String getMoo() {
		return moo;
	}
	public void setMoo(String moo) {
		this.moo = moo;
	}
	public String getSoi() {
		return soi;
	}
	public void setSoi(String soi) {
		this.soi = soi;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getDistrictId() {
		return districtId;
	}
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}
	public String getAmphurId() {
		return amphurId;
	}
	public void setAmphurId(String amphurId) {
		this.amphurId = amphurId;
	}
	public String getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getDoc() {
		return doc;
	}
	public void setDoc(String doc) {
		this.doc = doc;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
}

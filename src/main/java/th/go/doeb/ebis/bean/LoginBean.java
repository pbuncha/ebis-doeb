package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_login")	
public class LoginBean {
	@Id
	@Column(name="	id_user_login")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	
	@Column(name="user_name")
	String 	user;
	
	@Column(name="password")
	String 	pass;

	public Long getId() {
		return id;
	}

	public String getUser() {
		return user;
	}

	public String getPass() {
		return pass;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
}

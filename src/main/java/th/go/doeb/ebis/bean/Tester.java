package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="tester_form")
public class Tester {
	
	@Id
	@Column(name="id_tester_form")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE7")
	@SequenceGenerator(name="SEQUENCE7", sequenceName="SEQUENCE7", allocationSize=1)
	Long id;
	
	@Column(name="write_at_tester")
    private String write_at_tester;
	


	@Column(name="name_tester")
    private String name_tester;

	@Column(name="namesend_tester")
    private String namesend_tester;

	@Column(name="type__tester")
    private String type__tester;

	@Column(name="datetester")
    private String datetester;

	@Column(name="namecheck_tester")
    private String namecheck_tester;

	@Column(name="roman_tester")
    private String roman_tester;

	@Column(name="china_tester")
    private String china_tester;

	@Column(name="register_tester")
    private String register_tester;

	@Column(name="placeregister_tester")
    private String placeregister_tester;
	
	@Column(name="num_tester")
    private String num_tester;
	
	@Column(name="datenum_tester")
    private String datenum_tester;
	
	@Column(name="office_tester")
    private String office_tester;
	
	@Column(name="numofficer_tester")
    private String numofficer_tester;
	
	@Column(name="date_office_tester")
    private String date_office_tester;
	
	@Column(name="wishing_tester")
    private String wishing_tester;
	
	@Column(name="amount_register")
    private String amount_register;
	
	@Column(name="payment_regiter")
    private String payment_regiter;
	
	@Column(name="anount_person")
    private String anount_person;
	
	@Column(name="amount_accident")
    private String amount_accident;
	
	@Column(name="amount_asset")
    private String amount_asset;
	
	@Column(name="office_numhome")
    private String office_numhome;
	
	@Column(name="office_soi")
    private String office_soi;
	
	@Column(name="office_road")
    private String office_road;
	
	@Column(name="office_tumbon")
    private String office_tumbon;
	
	@Column(name="office_auphur")
    private String office_auphur;
	
	@Column(name="office_province")
    private String office_province;
	
	@Column(name="office_tel")
    private String office_tel;
	
	@Column(name="systemsend_num")
    private String systemsend_num;
	
	@Column(name="systemsum_num")
    private String systemsum_num;
	
	@Column(name="admin_num")
    private String admin_num;
	
	@Column(name="check_num")
    private String check_num;
	
	@Column(name="detail_tester")
    private String detail_tester;
	
	@Column(name="file_tester1")
    private String file_tester1;
	
	@Column(name="file_tester2")
    private String file_tester2;
	
	@Column(name="file_tester3")
    private String file_tester3;
	
	@Column(name="file_tester4")
    private String file_tester4;
	
	@Column(name="file_tester5")
    private String file_tester5;
	
	@Column(name="file_tester6")
    private String file_tester6;
	
	@Column(name="file_tester7")
    private String file_tester7;
	
	@Column(name="file_tester8")
    private String file_tester8;
	
	@Column(name="file_tester9")
    private String file_tester9;
	
	@Column(name="file_tester10")
    private String file_tester10;
	
	@Column(name="file_tester11")
    private String file_tester11;
	
	@Column(name="file_tester12")
    private String file_tester12;
	
	@Column(name="file_tester13")
    private String file_tester13;
	
	@Column(name="file_tester14")
    private String file_tester14;
	
	@Column(name="file_tester15")
    private String file_tester15;
	
	@Column(name="file_tester16")
    private String file_tester16;
	
	public String getFile_tester14() {
		return file_tester14;
	}

	public String getFile_tester15() {
		return file_tester15;
	}

	public String getFile_tester16() {
		return file_tester16;
	}

	public void setFile_tester14(String file_tester14) {
		this.file_tester14 = file_tester14;
	}

	public void setFile_tester15(String file_tester15) {
		this.file_tester15 = file_tester15;
	}

	public void setFile_tester16(String file_tester16) {
		this.file_tester16 = file_tester16;
	}

	@Column(name="name_guarantee")
    private String name_guarantee;
	
	@Column(name="name_petitioner")
    private String name_petitioner;
	
	@Column(name="name_write")
    private String name_write;
	
	@Column(name="builddate_tester")
    private String builddate_tester;
	
	@Column(name="updatedate_tester")
    private String updatedate_tester;
	
	@Column(name="deleted")
    private int deleted;

	public Long getId() {
		return id;
	}

	public String getWrite_at_tester() {
		return write_at_tester;
	}

	

	public String getName_tester() {
		return name_tester;
	}

	public String getNamesend_tester() {
		return namesend_tester;
	}

	public String getType__tester() {
		return type__tester;
	}

	public String getDatetester() {
		return datetester;
	}

	public String getNamecheck_tester() {
		return namecheck_tester;
	}

	public String getRoman_tester() {
		return roman_tester;
	}

	public String getChina_tester() {
		return china_tester;
	}

	public String getRegister_tester() {
		return register_tester;
	}

	public String getPlaceregister_tester() {
		return placeregister_tester;
	}

	public String getNum_tester() {
		return num_tester;
	}

	public String getDatenum_tester() {
		return datenum_tester;
	}

	public String getOffice_tester() {
		return office_tester;
	}

	public String getNumofficer_tester() {
		return numofficer_tester;
	}

	public String getDate_office_tester() {
		return date_office_tester;
	}

	public String getWishing_tester() {
		return wishing_tester;
	}

	public String getAmount_register() {
		return amount_register;
	}

	public String getPayment_regiter() {
		return payment_regiter;
	}

	public String getAnount_person() {
		return anount_person;
	}

	public String getAmount_accident() {
		return amount_accident;
	}

	public String getAmount_asset() {
		return amount_asset;
	}

	public String getOffice_numhome() {
		return office_numhome;
	}

	public String getOffice_soi() {
		return office_soi;
	}

	public String getOffice_road() {
		return office_road;
	}

	public String getOffice_tumbon() {
		return office_tumbon;
	}

	public String getOffice_auphur() {
		return office_auphur;
	}

	public String getOffice_province() {
		return office_province;
	}

	public String getOffice_tel() {
		return office_tel;
	}

	public String getSystemsend_num() {
		return systemsend_num;
	}

	public String getSystemsum_num() {
		return systemsum_num;
	}

	public String getAdmin_num() {
		return admin_num;
	}

	public String getCheck_num() {
		return check_num;
	}

	public String getDetail_tester() {
		return detail_tester;
	}

	public String getFile_tester1() {
		return file_tester1;
	}

	public String getFile_tester2() {
		return file_tester2;
	}

	public String getFile_tester3() {
		return file_tester3;
	}

	public String getFile_tester4() {
		return file_tester4;
	}

	public String getFile_tester5() {
		return file_tester5;
	}

	public String getFile_tester6() {
		return file_tester6;
	}

	public String getFile_tester7() {
		return file_tester7;
	}

	public String getFile_tester8() {
		return file_tester8;
	}

	public String getFile_tester9() {
		return file_tester9;
	}

	public String getFile_tester10() {
		return file_tester10;
	}

	public String getFile_tester11() {
		return file_tester11;
	}

	public String getFile_tester12() {
		return file_tester12;
	}

	public String getFile_tester13() {
		return file_tester13;
	}

	public String getName_guarantee() {
		return name_guarantee;
	}

	public String getName_petitioner() {
		return name_petitioner;
	}

	public String getName_write() {
		return name_write;
	}

	public String getBuilddate_tester() {
		return builddate_tester;
	}

	public String getUpdatedate_tester() {
		return updatedate_tester;
	}

	public int getDeleted() {
		return deleted;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setWrite_at_tester(String write_at_tester) {
		this.write_at_tester = write_at_tester;
	}



	public void setName_tester(String name_tester) {
		this.name_tester = name_tester;
	}

	public void setNamesend_tester(String namesend_tester) {
		this.namesend_tester = namesend_tester;
	}

	public void setType__tester(String type__tester) {
		this.type__tester = type__tester;
	}

	public void setDatetester(String datetester) {
		this.datetester = datetester;
	}

	public void setNamecheck_tester(String namecheck_tester) {
		this.namecheck_tester = namecheck_tester;
	}

	public void setRoman_tester(String roman_tester) {
		this.roman_tester = roman_tester;
	}

	public void setChina_tester(String china_tester) {
		this.china_tester = china_tester;
	}

	public void setRegister_tester(String register_tester) {
		this.register_tester = register_tester;
	}

	public void setPlaceregister_tester(String placeregister_tester) {
		this.placeregister_tester = placeregister_tester;
	}

	public void setNum_tester(String num_tester) {
		this.num_tester = num_tester;
	}

	public void setDatenum_tester(String datenum_tester) {
		this.datenum_tester = datenum_tester;
	}

	public void setOffice_tester(String office_tester) {
		this.office_tester = office_tester;
	}

	public void setNumofficer_tester(String numofficer_tester) {
		this.numofficer_tester = numofficer_tester;
	}

	public void setDate_office_tester(String date_office_tester) {
		this.date_office_tester = date_office_tester;
	}

	public void setWishing_tester(String wishing_tester) {
		this.wishing_tester = wishing_tester;
	}

	public void setAmount_register(String amount_register) {
		this.amount_register = amount_register;
	}

	public void setPayment_regiter(String payment_regiter) {
		this.payment_regiter = payment_regiter;
	}

	public void setAnount_person(String anount_person) {
		this.anount_person = anount_person;
	}

	public void setAmount_accident(String amount_accident) {
		this.amount_accident = amount_accident;
	}

	public void setAmount_asset(String amount_asset) {
		this.amount_asset = amount_asset;
	}

	public void setOffice_numhome(String office_numhome) {
		this.office_numhome = office_numhome;
	}

	public void setOffice_soi(String office_soi) {
		this.office_soi = office_soi;
	}

	public void setOffice_road(String office_road) {
		this.office_road = office_road;
	}

	public void setOffice_tumbon(String office_tumbon) {
		this.office_tumbon = office_tumbon;
	}

	public void setOffice_auphur(String office_auphur) {
		this.office_auphur = office_auphur;
	}

	public void setOffice_province(String office_province) {
		this.office_province = office_province;
	}

	public void setOffice_tel(String office_tel) {
		this.office_tel = office_tel;
	}

	public void setSystemsend_num(String systemsend_num) {
		this.systemsend_num = systemsend_num;
	}

	public void setSystemsum_num(String systemsum_num) {
		this.systemsum_num = systemsum_num;
	}

	public void setAdmin_num(String admin_num) {
		this.admin_num = admin_num;
	}

	public void setCheck_num(String check_num) {
		this.check_num = check_num;
	}

	public void setDetail_tester(String detail_tester) {
		this.detail_tester = detail_tester;
	}

	public void setFile_tester1(String file_tester1) {
		this.file_tester1 = file_tester1;
	}

	public void setFile_tester2(String file_tester2) {
		this.file_tester2 = file_tester2;
	}

	public void setFile_tester3(String file_tester3) {
		this.file_tester3 = file_tester3;
	}

	public void setFile_tester4(String file_tester4) {
		this.file_tester4 = file_tester4;
	}

	public void setFile_tester5(String file_tester5) {
		this.file_tester5 = file_tester5;
	}

	public void setFile_tester6(String file_tester6) {
		this.file_tester6 = file_tester6;
	}

	public void setFile_tester7(String file_tester7) {
		this.file_tester7 = file_tester7;
	}

	public void setFile_tester8(String file_tester8) {
		this.file_tester8 = file_tester8;
	}

	public void setFile_tester9(String file_tester9) {
		this.file_tester9 = file_tester9;
	}

	public void setFile_tester10(String file_tester10) {
		this.file_tester10 = file_tester10;
	}

	public void setFile_tester11(String file_tester11) {
		this.file_tester11 = file_tester11;
	}

	public void setFile_tester12(String file_tester12) {
		this.file_tester12 = file_tester12;
	}

	public void setFile_tester13(String file_tester13) {
		this.file_tester13 = file_tester13;
	}

	public void setName_guarantee(String name_guarantee) {
		this.name_guarantee = name_guarantee;
	}

	public void setName_petitioner(String name_petitioner) {
		this.name_petitioner = name_petitioner;
	}

	public void setName_write(String name_write) {
		this.name_write = name_write;
	}

	public void setBuilddate_tester(String builddate_tester) {
		this.builddate_tester = builddate_tester;
	}

	public void setUpdatedate_tester(String updatedate_tester) {
		this.updatedate_tester = updatedate_tester;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}




}

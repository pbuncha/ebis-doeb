package th.go.doeb.ebis.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TPP_TBL")
public class tpp3pModel {

	@Id
	@Column(name="id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE5")
	@SequenceGenerator(name="SEQUENCE5", sequenceName="SEQUENCE5", allocationSize=1)
    private long id;

	@Column(name="request1")
	private String request1;
	
	@Column(name="Wa")
	private String Wa;
	
	@Column(name="dateuser")
	private String dateuser;
	
	@Column(name="uid_career")
	private String uid_career;
	
	@Column(name="end_date_career")
	private String end_date_career;
	
	@Column(name="t1")
	private String t1;
	
	@Column(name="prefix1")
	private String prefix1;
	
	@Column(name="fnameth1")
	private String fnameth1;
	
	@Column(name="uid")
	private String uid;
	
	@Column(name="comname")
	private String comname;
	
	@Column(name="license_id")
	private String license_id;
	
	@Column(name="license_type")
	private String license_type;
	
	@Column(name="numadd1")
	private String numadd1;
	
	@Column(name="room1")
	private String room1;
	
	@Column(name="class1")
	private String class1;
	
	@Column(name="building1")
	private String building1;
	
	@Column(name="nummoo1")
	private String nummoo1;
	
	@Column(name="alley1")
	private String alley1;
	
	@Column(name="road1")
	private String road1;
	
	@Column(name="district1")
	private String district1;
	
	@Column(name="districts1")
	private String districts1;
	
	@Column(name="province1")
	private String province1;
	
	@Column(name="post1")
	private String post1;
	
	@Column(name="phone1")
	private String phone1;
	
	@Column(name="fax1")
	private String fax1;
	
	@Column(name="phones1")
	private String phones1;
	
	@Column(name="email1")
	private String email1;
	
	@Column(name="web1")
	private String web1;
	
	@Column(name="t2")
	private String t2;
	
	@Column(name="prefix2")
	private String prefix2;
	
	@Column(name="fnameth2")
	private String fnameth2;
	
	@Column(name="numadd2")
	private String numadd2;
	
	@Column(name="room2")
	private String room2;
	
	@Column(name="class2")
	private String class2;
	
	@Column(name="building2")
	private String building2;
	
	@Column(name="nummoo2")
	private String nummoo2;
	
	@Column(name="alley2")
	private String alley2;
	
	@Column(name="road2")
	private String road2;
	
	@Column(name="district2")
	private String district2;
	
	@Column(name="districts2")
	private String districts2;
	
	@Column(name="province2")
	private String province2;
	
	@Column(name="post2")
	private String post2;
	
	@Column(name="phone2")
	private String phone2;
	
	@Column(name="fax2")
	private String fax2;
	
	@Column(name="phones2")
	private String phones2;
	
	@Column(name="email2")
	private String email2;
	
	@Column(name="web2")
	private String web2;
	
	@Column(name="t3")
	private String t3;
	
	@Column(name="prefix3")
	private String prefix3;
	
	@Column(name="fnameth3")
	private String fnameth3;
	
	@Column(name="phone3")
	private String phone3;
	
	@Column(name="fax3")
	private String fax3;
	
	@Column(name="phones3")
	private String phones3;
	
	@Column(name="email3")
	private String email3;
	
	@Column(name="web3")
	private String web3;
	
	@Column(name="Fa1")
	private String Fa1;
	
	@Column(name="Fa2")
	private String Fa2;
	
	@Column(name="Fa3")
	private String Fa3;
	
	@Column(name="Fa4")
	private String Fa4;
	
	@Column(name="Fa5")
	private String Fa5;
	
	@Column(name="Fa6")
	private String Fa6;
	
	@Column(name="Fa7")
	private String Fa7;
	
	@Column(name="Fa8")
	private String Fa8;
	
	@Column(name="pic_name1")
	private String pic_name1;
	
	@Column(name="pic_name2")
	private String pic_name2;
	
	@Column(name="pic_name3")
	private String pic_name3;
	
	@Column(name="pic_name4")
	private String pic_name4;
	
	@Column(name="pic_name5")
	private String pic_name5;
	
	@Column(name="pic_name6")
	private String pic_name6;
	
	@Column(name="pic_name7")
	private String pic_name7;
	
	@Column(name="pic_name8")
	private String pic_name8;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRequest1() {
		return request1;
	}

	public void setRequest1(String request1) {
		this.request1 = request1;
	}

	public String getWa() {
		return Wa;
	}

	public void setWa(String wa) {
		Wa = wa;
	}

	public String getDateuser() {
		return dateuser;
	}

	public void setDateuser(String dateuser) {
		this.dateuser = dateuser;
	}

	public String getUid_career() {
		return uid_career;
	}

	public void setUid_career(String uid_career) {
		this.uid_career = uid_career;
	}

	public String getEnd_date_career() {
		return end_date_career;
	}

	public void setEnd_date_career(String end_date_career) {
		this.end_date_career = end_date_career;
	}

	public String getT1() {
		return t1;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public String getPrefix1() {
		return prefix1;
	}

	public void setPrefix1(String prefix1) {
		this.prefix1 = prefix1;
	}

	public String getFnameth1() {
		return fnameth1;
	}

	public void setFnameth1(String fnameth1) {
		this.fnameth1 = fnameth1;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getComname() {
		return comname;
	}

	public void setComname(String comname) {
		this.comname = comname;
	}

	public String getLicense_id() {
		return license_id;
	}

	public void setLicense_id(String license_id) {
		this.license_id = license_id;
	}

	public String getLicense_type() {
		return license_type;
	}

	public void setLicense_type(String license_type) {
		this.license_type = license_type;
	}

	public String getNumadd1() {
		return numadd1;
	}

	public void setNumadd1(String numadd1) {
		this.numadd1 = numadd1;
	}

	public String getRoom1() {
		return room1;
	}

	public void setRoom1(String room1) {
		this.room1 = room1;
	}

	public String getClass1() {
		return class1;
	}

	public void setClass1(String class1) {
		this.class1 = class1;
	}

	public String getBuilding1() {
		return building1;
	}

	public void setBuilding1(String building1) {
		this.building1 = building1;
	}

	public String getNummoo1() {
		return nummoo1;
	}

	public void setNummoo1(String nummoo1) {
		this.nummoo1 = nummoo1;
	}

	public String getAlley1() {
		return alley1;
	}

	public void setAlley1(String alley1) {
		this.alley1 = alley1;
	}

	public String getRoad1() {
		return road1;
	}

	public void setRoad1(String road1) {
		this.road1 = road1;
	}

	public String getDistrict1() {
		return district1;
	}

	public void setDistrict1(String district1) {
		this.district1 = district1;
	}

	public String getDistricts1() {
		return districts1;
	}

	public void setDistricts1(String districts1) {
		this.districts1 = districts1;
	}

	public String getProvince1() {
		return province1;
	}

	public void setProvince1(String province1) {
		this.province1 = province1;
	}

	public String getPost1() {
		return post1;
	}

	public void setPost1(String post1) {
		this.post1 = post1;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getFax1() {
		return fax1;
	}

	public void setFax1(String fax1) {
		this.fax1 = fax1;
	}

	public String getPhones1() {
		return phones1;
	}

	public void setPhones1(String phones1) {
		this.phones1 = phones1;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getWeb1() {
		return web1;
	}

	public void setWeb1(String web1) {
		this.web1 = web1;
	}

	public String getT2() {
		return t2;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public String getPrefix2() {
		return prefix2;
	}

	public void setPrefix2(String prefix2) {
		this.prefix2 = prefix2;
	}

	public String getFnameth2() {
		return fnameth2;
	}

	public void setFnameth2(String fnameth2) {
		this.fnameth2 = fnameth2;
	}

	public String getNumadd2() {
		return numadd2;
	}

	public void setNumadd2(String numadd2) {
		this.numadd2 = numadd2;
	}

	public String getRoom2() {
		return room2;
	}

	public void setRoom2(String room2) {
		this.room2 = room2;
	}

	public String getClass2() {
		return class2;
	}

	public void setClass2(String class2) {
		this.class2 = class2;
	}

	public String getBuilding2() {
		return building2;
	}

	public void setBuilding2(String building2) {
		this.building2 = building2;
	}

	public String getNummoo2() {
		return nummoo2;
	}

	public void setNummoo2(String nummoo2) {
		this.nummoo2 = nummoo2;
	}

	public String getAlley2() {
		return alley2;
	}

	public void setAlley2(String alley2) {
		this.alley2 = alley2;
	}

	public String getRoad2() {
		return road2;
	}

	public void setRoad2(String road2) {
		this.road2 = road2;
	}

	public String getDistrict2() {
		return district2;
	}

	public void setDistrict2(String district2) {
		this.district2 = district2;
	}

	public String getDistricts2() {
		return districts2;
	}

	public void setDistricts2(String districts2) {
		this.districts2 = districts2;
	}

	public String getProvince2() {
		return province2;
	}

	public void setProvince2(String province2) {
		this.province2 = province2;
	}

	public String getPost2() {
		return post2;
	}

	public void setPost2(String post2) {
		this.post2 = post2;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getFax2() {
		return fax2;
	}

	public void setFax2(String fax2) {
		this.fax2 = fax2;
	}

	public String getPhones2() {
		return phones2;
	}

	public void setPhones2(String phones2) {
		this.phones2 = phones2;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getWeb2() {
		return web2;
	}

	public void setWeb2(String web2) {
		this.web2 = web2;
	}

	public String getT3() {
		return t3;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public String getPrefix3() {
		return prefix3;
	}

	public void setPrefix3(String prefix3) {
		this.prefix3 = prefix3;
	}

	public String getFnameth3() {
		return fnameth3;
	}

	public void setFnameth3(String fnameth3) {
		this.fnameth3 = fnameth3;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getFax3() {
		return fax3;
	}

	public void setFax3(String fax3) {
		this.fax3 = fax3;
	}

	public String getPhones3() {
		return phones3;
	}

	public void setPhones3(String phones3) {
		this.phones3 = phones3;
	}

	public String getEmail3() {
		return email3;
	}

	public void setEmail3(String email3) {
		this.email3 = email3;
	}

	public String getWeb3() {
		return web3;
	}

	public void setWeb3(String web3) {
		this.web3 = web3;
	}

	public String getFa1() {
		return Fa1;
	}

	public void setFa1(String fa1) {
		Fa1 = fa1;
	}

	public String getFa2() {
		return Fa2;
	}

	public void setFa2(String fa2) {
		Fa2 = fa2;
	}

	public String getFa3() {
		return Fa3;
	}

	public void setFa3(String fa3) {
		Fa3 = fa3;
	}

	public String getFa4() {
		return Fa4;
	}

	public void setFa4(String fa4) {
		Fa4 = fa4;
	}

	public String getFa5() {
		return Fa5;
	}

	public void setFa5(String fa5) {
		Fa5 = fa5;
	}

	public String getFa6() {
		return Fa6;
	}

	public void setFa6(String fa6) {
		Fa6 = fa6;
	}

	public String getFa7() {
		return Fa7;
	}

	public void setFa7(String fa7) {
		Fa7 = fa7;
	}

	public String getFa8() {
		return Fa8;
	}

	public void setFa8(String fa8) {
		Fa8 = fa8;
	}

	public String getPic_name1() {
		return pic_name1;
	}

	public void setPic_name1(String pic_name1) {
		this.pic_name1 = pic_name1;
	}

	public String getPic_name2() {
		return pic_name2;
	}

	public void setPic_name2(String pic_name2) {
		this.pic_name2 = pic_name2;
	}

	public String getPic_name3() {
		return pic_name3;
	}

	public void setPic_name3(String pic_name3) {
		this.pic_name3 = pic_name3;
	}

	public String getPic_name4() {
		return pic_name4;
	}

	public void setPic_name4(String pic_name4) {
		this.pic_name4 = pic_name4;
	}

	public String getPic_name5() {
		return pic_name5;
	}

	public void setPic_name5(String pic_name5) {
		this.pic_name5 = pic_name5;
	}

	public String getPic_name6() {
		return pic_name6;
	}

	public void setPic_name6(String pic_name6) {
		this.pic_name6 = pic_name6;
	}

	public String getPic_name7() {
		return pic_name7;
	}

	public void setPic_name7(String pic_name7) {
		this.pic_name7 = pic_name7;
	}

	public String getPic_name8() {
		return pic_name8;
	}

	public void setPic_name8(String pic_name8) {
		this.pic_name8 = pic_name8;
	}
	

	
}

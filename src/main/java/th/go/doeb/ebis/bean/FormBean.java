package th.go.doeb.ebis.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name="be_corporate_electric_form")	
public class FormBean {
	@Id
	@Column(name="id_corporate_electric_form")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE4")
	@SequenceGenerator(name="SEQUENCE4", sequenceName="SEQUENCE4", allocationSize=1)
	Long id;

	
	@Column(name="write_at")
	String 	write_at;
	
	@Column(name="dateat")
	Date 	dateat;
	
	@Column(name="fname")
	String 	fname;

	@Column(name="age")
	String 	age;

	@Column(name="Purpose")
	String 	Purpose;
	
	@Column(name="Typee")
	String 	Typee;
	
	@Column(name="peopleid")
	String 	peopleid;
	
	@Column(name="homeid")
	String 	homeid;
	
	@Column(name="moo")
	String 	moo;
	
	@Column(name="trock")
	String 	trock;
	
	@Column(name="road")
	String 	road;
	
	@Column(name="tombon")
	String 	tombon;
	
	@Column(name="amper")
	String 	amper;
	
	@Column(name="province")
	String 	province;
	
	@Column(name="postid")
	String 	postid;
	
	@Column(name="phone")
	String 	phone;
	
	@Column(name="fax")
	String 	fax;
	
	@Column(name="email")
	String 	email;
	
	@Column(name="nameid11")
	String 	nameid11;
	
	@Column(name="nameid12")
	String 	nameid12;
	
	@Column(name="doc_attached1_1")
	String 	doc_attached1_1;
	
	@Column(name="doc_attached1_2")
	String 	doc_attached1_2;
	
	@Column(name="doc_attached1_3")
	String 	doc_attached1_3;
	
	@Column(name="doc_attached1_4")
	String 	doc_attached1_4;
	
	@Column(name="doc_attached1_5")
	String 	doc_attached1_5;
	
	@Column(name="doc_attached1_6")
	String 	doc_attached1_6;
	
	@Column(name="doc_attached1_7")
	String 	doc_attached1_7;
	
	@Column(name="doc_attached1_8")
	String 	doc_attached1_8;

	@Column(name="doc_attached2_1")
	String 	doc_attached2_1;
	
	@Column(name="doc_attached2_2")
	String 	doc_attached2_2;
	
	@Column(name="doc_attached2_3")
	String 	doc_attached2_3;
	
	@Column(name="doc_attached2_4")
	String 	doc_attached2_4;
	
	@Column(name="doc_attached2_5")
	String 	doc_attached2_5;
	
	@Column(name="doc_attached2_6")
	String 	doc_attached2_6;
	
	@Column(name="doc_attached2_7")
	String 	doc_attached2_7;

	@Column(name="doc_attached2_11")
	String 	doc_attached2_11;
	
	@Column(name="doc_attached2_11_txt")
	String 	doc_attached2_11_txt;
		
	
	@Column(name="nameid21")
	String 	nameid21;
	
	@Column(name="nameid22")
	String 	nameid22;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWrite_at() {
		return write_at;
	}

	public void setWrite_at(String write_at) {
		this.write_at = write_at;
	}

	public Date getDateat() {
		return dateat;
	}

	public void setDateat(Date dateat) {
		this.dateat = dateat;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	

	
	public String getPeopleid() {
		return peopleid;
	}

	public void setPeopleid(String peopleid) {
		this.peopleid = peopleid;
	}

	public String getHomeid() {
		return homeid;
	}

	public void setHomeid(String homeid) {
		this.homeid = homeid;
	}

	public String getMoo() {
		return moo;
	}

	public void setMoo(String moo) {
		this.moo = moo;
	}

	public String getTrock() {
		return trock;
	}

	public void setTrock(String trock) {
		this.trock = trock;
	}

	public String getRoad() {
		return road;
	}

	public void setRoad(String road) {
		this.road = road;
	}

	public String getTombon() {
		return tombon;
	}

	public void setTombon(String tombon) {
		this.tombon = tombon;
	}

	public String getAmper() {
		return amper;
	}

	public void setAmper(String amper) {
		this.amper = amper;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getPostid() {
		return postid;
	}

	public void setPostid(String postid) {
		this.postid = postid;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNameid11() {
		return nameid11;
	}

	public void setNameid11(String nameid11) {
		this.nameid11 = nameid11;
	}

	public String getNameid12() {
		return nameid12;
	}

	public void setNameid12(String nameid12) {
		this.nameid12 = nameid12;
	}

	public String getDoc_attached1_1() {
		return doc_attached1_1;
	}

	public void setDoc_attached1_1(String doc_attached1_1) {
		this.doc_attached1_1 = doc_attached1_1;
	}

	public String getDoc_attached1_2() {
		return doc_attached1_2;
	}

	public void setDoc_attached1_2(String doc_attached1_2) {
		this.doc_attached1_2 = doc_attached1_2;
	}

	public String getDoc_attached1_3() {
		return doc_attached1_3;
	}

	public void setDoc_attached1_3(String doc_attached1_3) {
		this.doc_attached1_3 = doc_attached1_3;
	}

	public String getDoc_attached1_4() {
		return doc_attached1_4;
	}

	public void setDoc_attached1_4(String doc_attached1_4) {
		this.doc_attached1_4 = doc_attached1_4;
	}

	public String getDoc_attached1_5() {
		return doc_attached1_5;
	}

	public void setDoc_attached1_5(String doc_attached1_5) {
		this.doc_attached1_5 = doc_attached1_5;
	}

	public String getDoc_attached1_6() {
		return doc_attached1_6;
	}

	public void setDoc_attached1_6(String doc_attached1_6) {
		this.doc_attached1_6 = doc_attached1_6;
	}

	public String getDoc_attached1_7() {
		return doc_attached1_7;
	}

	public void setDoc_attached1_7(String doc_attached1_7) {
		this.doc_attached1_7 = doc_attached1_7;
	}

	public String getDoc_attached1_8() {
		return doc_attached1_8;
	}

	public void setDoc_attached1_8(String doc_attached1_8) {
		this.doc_attached1_8 = doc_attached1_8;
	}

	public String getDoc_attached2_1() {
		return doc_attached2_1;
	}

	public void setDoc_attached2_1(String doc_attached2_1) {
		this.doc_attached2_1 = doc_attached2_1;
	}

	public String getDoc_attached2_2() {
		return doc_attached2_2;
	}

	public void setDoc_attached2_2(String doc_attached2_2) {
		this.doc_attached2_2 = doc_attached2_2;
	}

	public String getDoc_attached2_3() {
		return doc_attached2_3;
	}

	public void setDoc_attached2_3(String doc_attached2_3) {
		this.doc_attached2_3 = doc_attached2_3;
	}

	public String getDoc_attached2_4() {
		return doc_attached2_4;
	}

	public void setDoc_attached2_4(String doc_attached2_4) {
		this.doc_attached2_4 = doc_attached2_4;
	}

	public String getDoc_attached2_5() {
		return doc_attached2_5;
	}

	public void setDoc_attached2_5(String doc_attached2_5) {
		this.doc_attached2_5 = doc_attached2_5;
	}

	public String getDoc_attached2_6() {
		return doc_attached2_6;
	}

	public void setDoc_attached2_6(String doc_attached2_6) {
		this.doc_attached2_6 = doc_attached2_6;
	}

	public String getDoc_attached2_7() {
		return doc_attached2_7;
	}

	public void setDoc_attached2_7(String doc_attached2_7) {
		this.doc_attached2_7 = doc_attached2_7;
	}

	public String getDoc_attached2_11() {
		return doc_attached2_11;
	}

	public void setDoc_attached2_11(String doc_attached2_11) {
		this.doc_attached2_11 = doc_attached2_11;
	}

	public String getNameid21() {
		return nameid21;
	}

	public void setNameid21(String nameid21) {
		this.nameid21 = nameid21;
	}

	public String getNameid22() {
		return nameid22;
	}

	public void setNameid22(String nameid22) {
		this.nameid22 = nameid22;
	}

	public String getPurpose() {
		return Purpose;
	}

	public void setPurpose(String purpose) {
		Purpose = purpose;
	}

	public String getTypee() {
		return Typee;
	}

	public void setTypee(String typee) {
		Typee = typee;
	}

	public String getDoc_attached2_11_txt() {
		return doc_attached2_11_txt;
	}

	public void setDoc_attached2_11_txt(String doc_attached2_11_txt) {
		this.doc_attached2_11_txt = doc_attached2_11_txt;
	}
	
	
}

package th.go.doeb.ebis.bean;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the AMPHUR database table.
 * 
 */
@Entity
@Table(name="AMPHUR")
@NamedQuery(name="AmphurBean.findAll", query="SELECT a FROM AmphurBean a")
public class AmphurBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="AMPHUR_ID")
	private long amphurId;

	@Column(name="AMPHUR_CODE")
	private String amphurCode;

	@Column(name="AMPHUR_NAME")
	private String amphurName;

	@Column(name="GEO_ID")
	private BigDecimal geoId;

	@Column(name="PROVINCE_ID")
	private BigDecimal provinceId;

	public AmphurBean() {
	}

	public long getAmphurId() {
		return this.amphurId;
	}

	public void setAmphurId(long amphurId) {
		this.amphurId = amphurId;
	}

	public String getAmphurCode() {
		return this.amphurCode;
	}

	public void setAmphurCode(String amphurCode) {
		this.amphurCode = amphurCode;
	}

	public String getAmphurName() {
		return this.amphurName;
	}

	public void setAmphurName(String amphurName) {
		this.amphurName = amphurName;
	}

	public BigDecimal getGeoId() {
		return this.geoId;
	}

	public void setGeoId(BigDecimal geoId) {
		this.geoId = geoId;
	}

	public BigDecimal getProvinceId() {
		return this.provinceId;
	}

	public void setProvinceId(BigDecimal provinceId) {
		this.provinceId = provinceId;
	}

}
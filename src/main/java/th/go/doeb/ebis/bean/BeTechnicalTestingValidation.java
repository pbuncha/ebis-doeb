package th.go.doeb.ebis.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="be_technical_testing_validation")
public class BeTechnicalTestingValidation {
	@Id
	@Column(name="id_technical_testing_validation")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
	
	@Column(name="write_at")
	private String write_at;
	
	@Column(name="date_write_at")
	private String date_write_at;
	
	@Column(name="docs_name")
	private String docs_name;
	
	@Column(name="name")
	private String name;
	
	@Column(name="lastname")
	private String lastname;
	
	@Column(name="name_en")
	private String name_en;
	
	@Column(name="birthday")
	private Date birthday;
	
	@Column(name="nationality")
	private String nationality;
	
	@Column(name="amphur_birthday")
	private String amphur_birthday;
	
	@Column(name="province_birthday")
	private String province_birthday;
	
	@Column(name="year_start_in_thai")
	private String year_start_in_thai;
	
	@Column(name="no_address")
	private String no_address;
	
	@Column(name="moo_address")
	private String moo_address;
	
	@Column(name="soi_address")
	private String soi_address;
	
	@Column(name="road_address")
	private String road_address;
	
	@Column(name="district")
	private String district;
	
	@Column(name="amphur")
	private String amphur;
	
	@Column(name="province")
	private String province;
	
	@Column(name="phone")
	private String phone;
	
	@Column(name="highest_study")
	private String highest_study;
	
	@Column(name="city_study")
	private String city_study;
	
	@Column(name="school_name")
	private String school_name;
	
	@Column(name="certificate_name")
	private String certificate_name;
	
	@Column(name="university_name")
	private String university_name;
	
	@Column(name="deegree_name")
	private String deegree_name;
	
	@Column(name="major_name")
	private String major_name;
	
	@Column(name="year_graduation")
	private String year_graduation;
	
	@Column(name="type_professional_certificate")
	private String type_professional_certificate;
	
	@Column(name="no_professional_certificate")
	private String no_professional_certificate;
	
	@Column(name="type_professional_certificate_date_start")
	private Date type_professional_certificate_date_start;
	
	@Column(name="type_professional_certificate_date_end")
	private Date type_professional_certificate_date_end;
	
	@Column(name="cause_leave")
	private String cause_leave;
	
	@Column(name="certificate1_name")
	private String certificate1_name;
	
	@Column(name="certificate1_level")
	private String certificate1_level;
	
	@Column(name="certificate1_no")
	private String certificate1_no;
	
	@Column(name="certificate1_issue_by")
	private String certificate1_issue_by;
	
	@Column(name="certificate1_issue_date")
	private Date certificate1_issue_date;
	
	@Column(name="certificate1_issue_expire")
	private Date certificate1_issue_expire;
	
	@Column(name="certificate2_name")
	private String certificate2_name;
	
	@Column(name="certificate2_level")
	private String certificate2_level;

	@Column(name="certificate2_no")
	private String certificate2_no;
	
	@Column(name="certificate2_issue_by")
	private String certificate2_issue_by;
	
	@Column(name="certificate2_issue_date")
	private Date certificate2_issue_date;
	
	@Column(name="certificate2_issue_expire")
	private Date certificate2_issue_expire;
	
	@Column(name="certificate3_name")
	private String certificate3_name;
	
	@Column(name="certificate3_level")
	private String certificate3_level;
	
	@Column(name="certificate3_no")
	private String certificate3_no;
	
	@Column(name="certificate3_issue_by")
	private String certificate3_issue_by;
	
	@Column(name="certificate3_issue_date")
	private Date certificate3_issue_date;
	
	@Column(name="certificate3_issue_expire")
	private Date certificate3_issue_expire;
	
	@Column(name="current_work")
	private String current_work;
	
	@Column(name="time_to_work")
	private String time_to_work;
	
	@Column(name="description_work")
	private String description_work;
	
	@Column(name="copy_registion")
	private String copy_registion;
	
	@Column(name="study_history")
	private String study_history;
	
	@Column(name="professional_license")
	private String professional_license;
	
	@Column(name="work_history")
	private String work_history;
	
	@Column(name="my_pic")
	private String my_pic;
	
	@Column(name="createdate")
	private String createdate;
	
	@Column(name="updatedate")
	private Date updatedate;
	
	@Column(name="deleted")
	private int deleted;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getWrite_at() {
		return write_at;
	}

	public void setWrite_at(String write_at) {
		this.write_at = write_at;
	}

	public String getDate_write_at() {
		return date_write_at;
	}

	public void setDate_write_at(String date_write_at) {
		this.date_write_at = date_write_at;
	}

	public String getDocs_name() {
		return docs_name;
	}

	public void setDocs_name(String docs_name) {
		this.docs_name = docs_name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getName_en() {
		return name_en;
	}

	public void setName_en(String name_en) {
		this.name_en = name_en;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getAmphur_birthday() {
		return amphur_birthday;
	}

	public void setAmphur_birthday(String amphur_birthday) {
		this.amphur_birthday = amphur_birthday;
	}

	public String getProvince_birthday() {
		return province_birthday;
	}

	public void setProvince_birthday(String province_birthday) {
		this.province_birthday = province_birthday;
	}

	public String getYear_start_in_thai() {
		return year_start_in_thai;
	}

	public void setYear_start_in_thai(String year_start_in_thai) {
		this.year_start_in_thai = year_start_in_thai;
	}

	public String getNo_address() {
		return no_address;
	}

	public void setNo_address(String no_address) {
		this.no_address = no_address;
	}

	public String getMoo_address() {
		return moo_address;
	}

	public void setMoo_address(String moo_address) {
		this.moo_address = moo_address;
	}

	public String getSoi_address() {
		return soi_address;
	}

	public void setSoi_address(String soi_address) {
		this.soi_address = soi_address;
	}

	public String getRoad_address() {
		return road_address;
	}

	public void setRoad_address(String road_address) {
		this.road_address = road_address;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getAmphur() {
		return amphur;
	}

	public void setAmphur(String amphur) {
		this.amphur = amphur;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getHighest_study() {
		return highest_study;
	}

	public void setHighest_study(String highest_study) {
		this.highest_study = highest_study;
	}

	public String getCity_study() {
		return city_study;
	}

	public void setCity_study(String city_study) {
		this.city_study = city_study;
	}

	public String getSchool_name() {
		return school_name;
	}

	public void setSchool_name(String school_name) {
		this.school_name = school_name;
	}

	public String getCertificate_name() {
		return certificate_name;
	}

	public void setCertificate_name(String certificate_name) {
		this.certificate_name = certificate_name;
	}

	public String getUniversity_name() {
		return university_name;
	}

	public void setUniversity_name(String university_name) {
		this.university_name = university_name;
	}

	public String getDeegree_name() {
		return deegree_name;
	}

	public void setDeegree_name(String deegree_name) {
		this.deegree_name = deegree_name;
	}

	public String getMajor_name() {
		return major_name;
	}

	public void setMajor_name(String major_name) {
		this.major_name = major_name;
	}

	public String getYear_graduation() {
		return year_graduation;
	}

	public void setYear_graduation(String year_graduation) {
		this.year_graduation = year_graduation;
	}

	public String getType_professional_certificate() {
		return type_professional_certificate;
	}

	public void setType_professional_certificate(String type_professional_certificate) {
		this.type_professional_certificate = type_professional_certificate;
	}

	public String getNo_professional_certificate() {
		return no_professional_certificate;
	}

	public void setNo_professional_certificate(String no_professional_certificate) {
		this.no_professional_certificate = no_professional_certificate;
	}

	public Date getType_professional_certificate_date_start() {
		return type_professional_certificate_date_start;
	}

	public void setType_professional_certificate_date_start(Date type_professional_certificate_date_start) {
		this.type_professional_certificate_date_start = type_professional_certificate_date_start;
	}

	public Date getType_professional_certificate_date_end() {
		return type_professional_certificate_date_end;
	}

	public void setType_professional_certificate_date_end(Date type_professional_certificate_date_end) {
		this.type_professional_certificate_date_end = type_professional_certificate_date_end;
	}

	public String getCause_leave() {
		return cause_leave;
	}

	public void setCause_leave(String cause_leave) {
		this.cause_leave = cause_leave;
	}

	public String getCertificate1_name() {
		return certificate1_name;
	}

	public void setCertificate1_name(String certificate1_name) {
		this.certificate1_name = certificate1_name;
	}

	public String getCertificate1_level() {
		return certificate1_level;
	}

	public void setCertificate1_level(String certificate1_level) {
		this.certificate1_level = certificate1_level;
	}

	public String getCertificate1_no() {
		return certificate1_no;
	}

	public void setCertificate1_no(String certificate1_no) {
		this.certificate1_no = certificate1_no;
	}

	public String getCertificate1_issue_by() {
		return certificate1_issue_by;
	}

	public void setCertificate1_issue_by(String certificate1_issue_by) {
		this.certificate1_issue_by = certificate1_issue_by;
	}

	public Date getCertificate1_issue_date() {
		return certificate1_issue_date;
	}

	public void setCertificate1_issue_date(Date certificate1_issue_date) {
		this.certificate1_issue_date = certificate1_issue_date;
	}

	public Date getCertificate1_issue_expire() {
		return certificate1_issue_expire;
	}

	public void setCertificate1_issue_expire(Date certificate1_issue_expire) {
		this.certificate1_issue_expire = certificate1_issue_expire;
	}

	public String getCertificate2_name() {
		return certificate2_name;
	}

	public void setCertificate2_name(String certificate2_name) {
		this.certificate2_name = certificate2_name;
	}

	public String getCertificate2_level() {
		return certificate2_level;
	}

	public void setCertificate2_level(String certificate2_level) {
		this.certificate2_level = certificate2_level;
	}

	public String getCertificate2_no() {
		return certificate2_no;
	}

	public void setCertificate2_no(String certificate2_no) {
		this.certificate2_no = certificate2_no;
	}

	public String getCertificate2_issue_by() {
		return certificate2_issue_by;
	}

	public void setCertificate2_issue_by(String certificate2_issue_by) {
		this.certificate2_issue_by = certificate2_issue_by;
	}

	public Date getCertificate2_issue_date() {
		return certificate2_issue_date;
	}

	public void setCertificate2_issue_date(Date certificate2_issue_date) {
		this.certificate2_issue_date = certificate2_issue_date;
	}

	public Date getCertificate2_issue_expire() {
		return certificate2_issue_expire;
	}

	public void setCertificate2_issue_expire(Date certificate2_issue_expire) {
		this.certificate2_issue_expire = certificate2_issue_expire;
	}

	public String getCertificate3_name() {
		return certificate3_name;
	}

	public void setCertificate3_name(String certificate3_name) {
		this.certificate3_name = certificate3_name;
	}

	public String getCertificate3_level() {
		return certificate3_level;
	}

	public void setCertificate3_level(String certificate3_level) {
		this.certificate3_level = certificate3_level;
	}

	public String getCertificate3_no() {
		return certificate3_no;
	}

	public void setCertificate3_no(String certificate3_no) {
		this.certificate3_no = certificate3_no;
	}

	public String getCertificate3_issue_by() {
		return certificate3_issue_by;
	}

	public void setCertificate3_issue_by(String certificate3_issue_by) {
		this.certificate3_issue_by = certificate3_issue_by;
	}

	public Date getCertificate3_issue_date() {
		return certificate3_issue_date;
	}

	public void setCertificate3_issue_date(Date certificate3_issue_date) {
		this.certificate3_issue_date = certificate3_issue_date;
	}

	public Date getCertificate3_issue_expire() {
		return certificate3_issue_expire;
	}

	public void setCertificate3_issue_expire(Date certificate3_issue_expire) {
		this.certificate3_issue_expire = certificate3_issue_expire;
	}

	public String getCurrent_work() {
		return current_work;
	}

	public void setCurrent_work(String current_work) {
		this.current_work = current_work;
	}

	public String getTime_to_work() {
		return time_to_work;
	}

	public void setTime_to_work(String time_to_work) {
		this.time_to_work = time_to_work;
	}

	public String getDescription_work() {
		return description_work;
	}

	public void setDescription_work(String description_work) {
		this.description_work = description_work;
	}

	public String getCopy_registion() {
		return copy_registion;
	}

	public void setCopy_registion(String copy_registion) {
		this.copy_registion = copy_registion;
	}

	public String getStudy_history() {
		return study_history;
	}

	public void setStudy_history(String study_history) {
		this.study_history = study_history;
	}

	public String getProfessional_license() {
		return professional_license;
	}

	public void setProfessional_license(String professional_license) {
		this.professional_license = professional_license;
	}

	public String getWork_history() {
		return work_history;
	}

	public void setWork_history(String work_history) {
		this.work_history = work_history;
	}

	public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

	public Date getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public int getDeleted() {
		return deleted;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}

	public String getMy_pic() {
		return my_pic;
	}

	public void setMy_pic(String my_pic) {
		this.my_pic = my_pic;
	}
	
	
}

package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ZIPCODE")
public class ZipcodeBean {

	@Id
	@Column(name="ZIPCODE_ID")
	private Long zipcodeId;

	@Column(name="DISTRICT_CODE")
	private String districtCode;

	@Column(name="PROVINCE_ID")
	private String provinceId;

	@Column(name="AMPHUR_ID")
	private String amphurId;

	@Column(name="DISTRICT_ID")
	private String districtId;
	
	@Column(name="ZIPCODE")
	private String zipcode;

	public Long getZipcodeId() {
		return zipcodeId;
	}

	public void setZipcodeId(Long zipcodeId) {
		this.zipcodeId = zipcodeId;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getAmphurId() {
		return amphurId;
	}

	public void setAmphurId(String amphurId) {
		this.amphurId = amphurId;
	}

	public String getDistrictId() {
		return districtId;
	}

	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}

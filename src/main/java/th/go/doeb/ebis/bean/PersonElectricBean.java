package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="be_person_electric_form")	
public class PersonElectricBean {
	@Id
	@Column(name="id_person_electric_form")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE6")
	@SequenceGenerator(name="SEQUENCE6", sequenceName="SEQUENCE6", allocationSize=1)
	Long id;
	
	@Column(name="id_member")
	String 	id_member;
	
	@Column(name="id_company")
	String 	id_company;
	
	@Column(name="name_member")
	String 	name_member;
	
	@Column(name="identity_no")
	String 	identity_no;
	
	@Column(name="no_address_company")
	String 	no_address_company;
	
	@Column(name="soi_address_company")
	String 	soi_address_company;
	
	@Column(name="road_address_company")
	String 	road_address_company;
	
	@Column(name="moo_address_company")
	String 	moo_address_company;
	
	@Column(name="district_address_company")
	String 	district_address_company;
	
	@Column(name="amphur_address_company")
	String 	amphur_address_company;
	
	@Column(name="province_address_company")
	String 	province_address_company;
	
	@Column(name="zipcode_address_company")
	String 	zipcode_address_company;
	
	@Column(name="phone_company")
	String 	phone_company;
	
	@Column(name="fax_company")
	String 	fax_company;
	
	@Column(name="email_company")
	String 	email_company;
	
	@Column(name="no_address")
	String 	no_address;
	
	@Column(name="soi_address")
	String 	soi_address;
	
	@Column(name="road_address")
	String 	road_address;
	
	@Column(name="moo_address")
	String 	moo_address;
	
	@Column(name="district_address")
	String 	district_address;
	
	@Column(name="amphur_address")
	String 	amphur_address;
	
	@Column(name="province_address")
	String 	province_address;
	
	@Column(name="zipcode_address")
	String 	zipcode_address;
	
	@Column(name="phone")
	String 	phone;
	
	@Column(name="fax")
	String 	fax;
	
	@Column(name="email")
	String 	email;
	
	@Column(name="no_certificate_engineer")
	String 	no_certificate_engineer;
	
	@Column(name="purpose1")
	String 	purpose1;
	
	@Column(name="purpose2")
	String 	purpose2;
	
	@Column(name="doc_attached7_1")
	String 	doc_attached7_1;
	
	@Column(name="doc_attached7_1_txt")
	String 	doc_attached7_1_txt;
	

	@Column(name="doc_attached7_2")
	String 	doc_attached7_2;
	
	@Column(name="doc_attached7_3")
	String 	doc_attached7_3;
	
	@Column(name="doc_attached7_4")
	String 	doc_attached7_4;
	
	@Column(name="doc_attached7_5")
	String 	doc_attached7_5;
	
	@Column(name="doc_attached7_6")
	String 	doc_attached7_6;
	
	@Column(name="doc_attached7_6_txt")
	String 	doc_attached7_6_txt;
	
	@Column(name="doc_attached7_7")
	String 	doc_attached7_7;
	
	@Column(name="doc_attached7_8")
	String 	doc_attached7_8;
	
	@Column(name="doc_attached7_8_txt")
	String 	doc_attached7_8_txt;
	
	public String getDoc_attached7_1_txt() {
		return doc_attached7_1_txt;
	}

	public void setDoc_attached7_1_txt(String doc_attached7_1_txt) {
		this.doc_attached7_1_txt = doc_attached7_1_txt;
	}

	public String getDoc_attached7_6_txt() {
		return doc_attached7_6_txt;
	}

	public void setDoc_attached7_6_txt(String doc_attached7_6_txt) {
		this.doc_attached7_6_txt = doc_attached7_6_txt;
	}

	public String getDoc_attached7_8_txt() {
		return doc_attached7_8_txt;
	}

	public void setDoc_attached7_8_txt(String doc_attached7_8_txt) {
		this.doc_attached7_8_txt = doc_attached7_8_txt;
	}

	@Column(name="createdate")
	String 	createdate;
	
	@Column(name="updatedate")
	String 	updatedate;
	
	@Column(name="id_status_document")
	String 	id_status_document;
	
	@Column(name="deleted")
	String 	deleted;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getId_member() {
		return id_member;
	}

	public void setId_member(String id_member) {
		this.id_member = id_member;
	}

	public String getId_company() {
		return id_company;
	}

	public void setId_company(String id_company) {
		this.id_company = id_company;
	}

	public String getName_member() {
		return name_member;
	}

	public void setName_member(String name_member) {
		this.name_member = name_member;
	}

	public String getIdentity_no() {
		return identity_no;
	}

	public void setIdentity_no(String identity_no) {
		this.identity_no = identity_no;
	}

	public String getNo_address_company() {
		return no_address_company;
	}

	public void setNo_address_company(String no_address_company) {
		this.no_address_company = no_address_company;
	}

	public String getSoi_address_company() {
		return soi_address_company;
	}

	public void setSoi_address_company(String soi_address_company) {
		this.soi_address_company = soi_address_company;
	}

	public String getRoad_address_company() {
		return road_address_company;
	}

	public void setRoad_address_company(String road_address_company) {
		this.road_address_company = road_address_company;
	}

	public String getMoo_address_company() {
		return moo_address_company;
	}

	public void setMoo_address_company(String moo_address_company) {
		this.moo_address_company = moo_address_company;
	}

	public String getDistrict_address_company() {
		return district_address_company;
	}

	public void setDistrict_address_company(String district_address_company) {
		this.district_address_company = district_address_company;
	}

	public String getAmphur_address_company() {
		return amphur_address_company;
	}

	public void setAmphur_address_company(String amphur_address_company) {
		this.amphur_address_company = amphur_address_company;
	}

	public String getProvince_address_company() {
		return province_address_company;
	}

	public void setProvince_address_company(String province_address_company) {
		this.province_address_company = province_address_company;
	}

	public String getZipcode_address_company() {
		return zipcode_address_company;
	}

	public void setZipcode_address_company(String zipcode_address_company) {
		this.zipcode_address_company = zipcode_address_company;
	}

	public String getPhone_company() {
		return phone_company;
	}

	public void setPhone_company(String phone_company) {
		this.phone_company = phone_company;
	}

	public String getFax_company() {
		return fax_company;
	}

	public void setFax_company(String fax_company) {
		this.fax_company = fax_company;
	}

	public String getEmail_company() {
		return email_company;
	}

	public void setEmail_company(String email_company) {
		this.email_company = email_company;
	}

	public String getNo_address() {
		return no_address;
	}

	public void setNo_address(String no_address) {
		this.no_address = no_address;
	}

	public String getSoi_address() {
		return soi_address;
	}

	public void setSoi_address(String soi_address) {
		this.soi_address = soi_address;
	}

	public String getRoad_address() {
		return road_address;
	}

	public void setRoad_address(String road_address) {
		this.road_address = road_address;
	}

	public String getMoo_address() {
		return moo_address;
	}

	public void setMoo_address(String moo_address) {
		this.moo_address = moo_address;
	}

	public String getDistrict_address() {
		return district_address;
	}

	public void setDistrict_address(String district_address) {
		this.district_address = district_address;
	}

	public String getAmphur_address() {
		return amphur_address;
	}

	public void setAmphur_address(String amphur_address) {
		this.amphur_address = amphur_address;
	}

	public String getProvince_address() {
		return province_address;
	}

	public void setProvince_address(String province_address) {
		this.province_address = province_address;
	}

	public String getZipcode_address() {
		return zipcode_address;
	}

	public void setZipcode_address(String zipcode_address) {
		this.zipcode_address = zipcode_address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNo_certificate_engineer() {
		return no_certificate_engineer;
	}

	public void setNo_certificate_engineer(String no_certificate_engineer) {
		this.no_certificate_engineer = no_certificate_engineer;
	}

	public String getPurpose1() {
		return purpose1;
	}

	public void setPurpose1(String purpose1) {
		this.purpose1 = purpose1;
	}

	public String getPurpose2() {
		return purpose2;
	}

	public void setPurpose2(String purpose2) {
		this.purpose2 = purpose2;
	}

	public String getDoc_attached7_1() {
		return doc_attached7_1;
	}

	public void setDoc_attached7_1(String doc_attached7_1) {
		this.doc_attached7_1 = doc_attached7_1;
	}

	public String getDoc_attached7_2() {
		return doc_attached7_2;
	}

	public void setDoc_attached7_2(String doc_attached7_2) {
		this.doc_attached7_2 = doc_attached7_2;
	}

	public String getDoc_attached7_3() {
		return doc_attached7_3;
	}

	public void setDoc_attached7_3(String doc_attached7_3) {
		this.doc_attached7_3 = doc_attached7_3;
	}

	public String getDoc_attached7_4() {
		return doc_attached7_4;
	}

	public void setDoc_attached7_4(String doc_attached7_4) {
		this.doc_attached7_4 = doc_attached7_4;
	}

	public String getDoc_attached7_5() {
		return doc_attached7_5;
	}

	public void setDoc_attached7_5(String doc_attached7_5) {
		this.doc_attached7_5 = doc_attached7_5;
	}

	public String getDoc_attached7_6() {
		return doc_attached7_6;
	}

	public void setDoc_attached7_6(String doc_attached7_6) {
		this.doc_attached7_6 = doc_attached7_6;
	}

	public String getDoc_attached7_7() {
		return doc_attached7_7;
	}

	public void setDoc_attached7_7(String doc_attached7_7) {
		this.doc_attached7_7 = doc_attached7_7;
	}

	public String getDoc_attached7_8() {
		return doc_attached7_8;
	}

	public void setDoc_attached7_8(String doc_attached7_8) {
		this.doc_attached7_8 = doc_attached7_8;
	}

	public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

	public String getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}

	public String getId_status_document() {
		return id_status_document;
	}

	public void setId_status_document(String id_status_document) {
		this.id_status_document = id_status_document;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	
	
}

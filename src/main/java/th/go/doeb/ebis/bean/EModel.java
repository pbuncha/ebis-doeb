package th.go.doeb.ebis.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="be_engineer_form")
public class EModel {
	
	@Id
	@Column(name="id_engineer_form")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE3")
	@SequenceGenerator(name="SEQUENCE3", sequenceName="SEQUENCE3", allocationSize=1)
    private long id;
	
	@Column(name="id_member")
	private int id_member;
	
	@Column(name="id_company")
	private int id_company;
	
	@Column(name="write_engineer")
	private String write_engineer;
	
	
	@Column(name="id_status_document")
	private int id_status_document;
	
	@Column(name="name_enginee")
	private String name_enginee;
	
	@Column(name="age_engineer")
	private String age_engineer;
	
	@Column(name="namecheck_engineer")
	private String namecheck_engineer;
	
	@Column(name="place_engineer")
	private String place_engineer;
	
	@Column(name="homenum_engineer")
	private String homenum_engineer;

	@Column(name="moo_engineer")
	private String moo_engineer;
	
	@Column(name="soi_engineer")
	private String soi_engineer;
	
	@Column(name="road_engineer")
	private String road_engineer;
	
	@Column(name="tumbon_engineer")
	private String tumbon_engineer;
	
	@Column(name="aumphur_engineer")
	private String aumphur_engineer;
	
	@Column(name="province_engineer")
	private String province_engineer;
	
	@Column(name="zipcode_engineer")
	private String zipcode_engineer;
	
	@Column(name="tel_engineer")
	private String tel_engineer;
	
	@Column(name="fax_engineer")
	private String fax_engineer;
	
	@Column(name="email_engineer")
	private String email_engineer;
	
	@Column(name="check_engineer1")
	private String check_engineer1;
	
	@Column(name="check_engineer2")
	private String check_engineer2;
	
	@Column(name="check_engineer3")
	private String check_engineer3;
	
	@Column(name="check_engineer4")
	private String check_engineer4;
	
	@Column(name="check_engineer5")
	private String check_engineer5;
	
	@Column(name="check_engineer6")
	private String check_engineer6;
	
	@Column(name="check_engineer7")
	private String check_engineer7;
	
	@Column(name="check_engineer8")
	private String check_engineer8;
	
	@Column(name="check_engineer9")
	private String check_engineer9;
	
	@Column(name="check_engineer10")
	private String check_engineer10;
	
	@Column(name="check_engineer11_1")
	private String check_engineer11_1;
	
	@Column(name="check_engineer11_2")
	private String check_engineer11_2;
	
	@Column(name="check_engineer11_3")
	private String check_engineer11_3;
	
	@Column(name="check_engineer12_1")
	private String check_engineer12_1;
	
	@Column(name="check_engineer12_2")
	private String check_engineer12_2;
	
	@Column(name="check_engineer12_3")
	private String check_engineer12_3;
	
	@Column(name="file_engineer1")
	private String file_engineer1;
	
	@Column(name="file_engineer2")
	private String file_engineer2;
	
	@Column(name="file_engineer3")
	private String file_engineer3;
	
	@Column(name="file_engineer4")
	private String file_engineer4;
	
	@Column(name="file_engineer5")
	private String file_engineer5;
	
	@Column(name="file_engineer6")
	private String file_engineer6;
	
	@Column(name="file_engineer7")
	private String file_engineer7;
	
	@Column(name="file_engineer8_1")
	private String file_engineer8_1;
	
	@Column(name="file_engineer8_2")
	private String file_engineer8_2;
	
	@Column(name="file_engineer8_3")
	private String file_engineer8_3;
	
	@Column(name="file_engineer9")
	private String file_engineer9;
	
	@Column(name="file_engineer10")
	private String file_engineer10;
	
	@Column(name="file_engineer11_1")
	private String file_engineer11_1;
	
	@Column(name="file_engineer11_2")
	private String file_engineer11_2;
	
	@Column(name="file_engineer11_3")
	private String file_engineer11_3;
	
	@Column(name="file_engineer12_1")
	private String file_engineer12_1;
	
	@Column(name="file_engineer12_2")
	private String file_engineer12_2;
	
	@Column(name="file_engineer12_3")
	private String file_engineer12_3;
	
	@Column(name="createdate")
	private String createdate;
	
	@Column(name="updatedate")
	private String updatedate;
	
	@Column(name="deleted")
	private String deleted;

	public String getWrite_engineer() {
		return write_engineer;
	}

	public void setWrite_engineer(String write_engineer) {
		this.write_engineer = write_engineer;
	}

	public long getId() {
		return id;
	}

	public int getId_member() {
		return id_member;
	}

	public int getId_company() {
		return id_company;
	}

	public int getId_status_document() {
		return id_status_document;
	}

	public String getName_enginee() {
		return name_enginee;
	}

	public String getAge_engineer() {
		return age_engineer;
	}

	public String getNamecheck_engineer() {
		return namecheck_engineer;
	}

	public String getPlace_engineer() {
		return place_engineer;
	}

	public String getHomenum_engineer() {
		return homenum_engineer;
	}

	public String getMoo_engineer() {
		return moo_engineer;
	}

	public String getSoi_engineer() {
		return soi_engineer;
	}

	public String getRoad_engineer() {
		return road_engineer;
	}

	public String getTumbon_engineer() {
		return tumbon_engineer;
	}

	public String getAumphur_engineer() {
		return aumphur_engineer;
	}

	public String getProvince_engineer() {
		return province_engineer;
	}

	public String getZipcode_engineer() {
		return zipcode_engineer;
	}

	public String getTel_engineer() {
		return tel_engineer;
	}

	public String getFax_engineer() {
		return fax_engineer;
	}

	public String getEmail_engineer() {
		return email_engineer;
	}

	public String getCheck_engineer1() {
		return check_engineer1;
	}

	public String getCheck_engineer2() {
		return check_engineer2;
	}

	public String getCheck_engineer3() {
		return check_engineer3;
	}

	public String getCheck_engineer4() {
		return check_engineer4;
	}

	public String getCheck_engineer5() {
		return check_engineer5;
	}

	public String getCheck_engineer6() {
		return check_engineer6;
	}

	public String getCheck_engineer7() {
		return check_engineer7;
	}

	public String getCheck_engineer8() {
		return check_engineer8;
	}

	public String getCheck_engineer9() {
		return check_engineer9;
	}

	public String getCheck_engineer10() {
		return check_engineer10;
	}

	public String getCheck_engineer11_1() {
		return check_engineer11_1;
	}

	public String getCheck_engineer11_2() {
		return check_engineer11_2;
	}

	public String getCheck_engineer11_3() {
		return check_engineer11_3;
	}

	public String getCheck_engineer12_1() {
		return check_engineer12_1;
	}

	public String getCheck_engineer12_2() {
		return check_engineer12_2;
	}

	public String getCheck_engineer12_3() {
		return check_engineer12_3;
	}

	public String getFile_engineer1() {
		return file_engineer1;
	}

	public String getFile_engineer2() {
		return file_engineer2;
	}

	public String getFile_engineer3() {
		return file_engineer3;
	}

	public String getFile_engineer4() {
		return file_engineer4;
	}

	public String getFile_engineer5() {
		return file_engineer5;
	}

	public String getFile_engineer6() {
		return file_engineer6;
	}

	public String getFile_engineer7() {
		return file_engineer7;
	}

	public String getFile_engineer8_1() {
		return file_engineer8_1;
	}

	public String getFile_engineer8_2() {
		return file_engineer8_2;
	}

	public String getFile_engineer8_3() {
		return file_engineer8_3;
	}

	public String getFile_engineer9() {
		return file_engineer9;
	}

	public String getFile_engineer10() {
		return file_engineer10;
	}

	public String getFile_engineer11_1() {
		return file_engineer11_1;
	}

	public String getFile_engineer11_2() {
		return file_engineer11_2;
	}

	public String getFile_engineer11_3() {
		return file_engineer11_3;
	}

	public String getFile_engineer12_1() {
		return file_engineer12_1;
	}

	public String getFile_engineer12_2() {
		return file_engineer12_2;
	}

	public String getFile_engineer12_3() {
		return file_engineer12_3;
	}

	public String getCreatedate() {
		return createdate;
	}

	public String getUpdatedate() {
		return updatedate;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setId_member(int id_member) {
		this.id_member = id_member;
	}

	public void setId_company(int id_company) {
		this.id_company = id_company;
	}

	public void setId_status_document(int id_status_document) {
		this.id_status_document = id_status_document;
	}

	public void setName_enginee(String name_enginee) {
		this.name_enginee = name_enginee;
	}

	public void setAge_engineer(String age_engineer) {
		this.age_engineer = age_engineer;
	}

	public void setNamecheck_engineer(String namecheck_engineer) {
		this.namecheck_engineer = namecheck_engineer;
	}

	public void setPlace_engineer(String place_engineer) {
		this.place_engineer = place_engineer;
	}

	public void setHomenum_engineer(String homenum_engineer) {
		this.homenum_engineer = homenum_engineer;
	}

	public void setMoo_engineer(String moo_engineer) {
		this.moo_engineer = moo_engineer;
	}

	public void setSoi_engineer(String soi_engineer) {
		this.soi_engineer = soi_engineer;
	}

	public void setRoad_engineer(String road_engineer) {
		this.road_engineer = road_engineer;
	}

	public void setTumbon_engineer(String tumbon_engineer) {
		this.tumbon_engineer = tumbon_engineer;
	}

	public void setAumphur_engineer(String aumphur_engineer) {
		this.aumphur_engineer = aumphur_engineer;
	}

	public void setProvince_engineer(String province_engineer) {
		this.province_engineer = province_engineer;
	}

	public void setZipcode_engineer(String zipcode_engineer) {
		this.zipcode_engineer = zipcode_engineer;
	}

	public void setTel_engineer(String tel_engineer) {
		this.tel_engineer = tel_engineer;
	}

	public void setFax_engineer(String fax_engineer) {
		this.fax_engineer = fax_engineer;
	}

	public void setEmail_engineer(String email_engineer) {
		this.email_engineer = email_engineer;
	}

	public void setCheck_engineer1(String check_engineer1) {
		this.check_engineer1 = check_engineer1;
	}

	public void setCheck_engineer2(String check_engineer2) {
		this.check_engineer2 = check_engineer2;
	}

	public void setCheck_engineer3(String check_engineer3) {
		this.check_engineer3 = check_engineer3;
	}

	public void setCheck_engineer4(String check_engineer4) {
		this.check_engineer4 = check_engineer4;
	}

	public void setCheck_engineer5(String check_engineer5) {
		this.check_engineer5 = check_engineer5;
	}

	public void setCheck_engineer6(String check_engineer6) {
		this.check_engineer6 = check_engineer6;
	}

	public void setCheck_engineer7(String check_engineer7) {
		this.check_engineer7 = check_engineer7;
	}

	public void setCheck_engineer8(String check_engineer8) {
		this.check_engineer8 = check_engineer8;
	}

	public void setCheck_engineer9(String check_engineer9) {
		this.check_engineer9 = check_engineer9;
	}

	public void setCheck_engineer10(String check_engineer10) {
		this.check_engineer10 = check_engineer10;
	}

	public void setCheck_engineer11_1(String check_engineer11_1) {
		this.check_engineer11_1 = check_engineer11_1;
	}

	public void setCheck_engineer11_2(String check_engineer11_2) {
		this.check_engineer11_2 = check_engineer11_2;
	}

	public void setCheck_engineer11_3(String check_engineer11_3) {
		this.check_engineer11_3 = check_engineer11_3;
	}

	public void setCheck_engineer12_1(String check_engineer12_1) {
		this.check_engineer12_1 = check_engineer12_1;
	}

	public void setCheck_engineer12_2(String check_engineer12_2) {
		this.check_engineer12_2 = check_engineer12_2;
	}

	public void setCheck_engineer12_3(String check_engineer12_3) {
		this.check_engineer12_3 = check_engineer12_3;
	}

	public void setFile_engineer1(String file_engineer1) {
		this.file_engineer1 = file_engineer1;
	}

	public void setFile_engineer2(String file_engineer2) {
		this.file_engineer2 = file_engineer2;
	}

	public void setFile_engineer3(String file_engineer3) {
		this.file_engineer3 = file_engineer3;
	}

	public void setFile_engineer4(String file_engineer4) {
		this.file_engineer4 = file_engineer4;
	}

	public void setFile_engineer5(String file_engineer5) {
		this.file_engineer5 = file_engineer5;
	}

	public void setFile_engineer6(String file_engineer6) {
		this.file_engineer6 = file_engineer6;
	}

	public void setFile_engineer7(String file_engineer7) {
		this.file_engineer7 = file_engineer7;
	}

	public void setFile_engineer8_1(String file_engineer8_1) {
		this.file_engineer8_1 = file_engineer8_1;
	}

	public void setFile_engineer8_2(String file_engineer8_2) {
		this.file_engineer8_2 = file_engineer8_2;
	}

	public void setFile_engineer8_3(String file_engineer8_3) {
		this.file_engineer8_3 = file_engineer8_3;
	}

	public void setFile_engineer9(String file_engineer9) {
		this.file_engineer9 = file_engineer9;
	}

	public void setFile_engineer10(String file_engineer10) {
		this.file_engineer10 = file_engineer10;
	}

	public void setFile_engineer11_1(String file_engineer11_1) {
		this.file_engineer11_1 = file_engineer11_1;
	}

	public void setFile_engineer11_2(String file_engineer11_2) {
		this.file_engineer11_2 = file_engineer11_2;
	}

	public void setFile_engineer11_3(String file_engineer11_3) {
		this.file_engineer11_3 = file_engineer11_3;
	}

	public void setFile_engineer12_1(String file_engineer12_1) {
		this.file_engineer12_1 = file_engineer12_1;
	}

	public void setFile_engineer12_2(String file_engineer12_2) {
		this.file_engineer12_2 = file_engineer12_2;
	}

	public void setFile_engineer12_3(String file_engineer12_3) {
		this.file_engineer12_3 = file_engineer12_3;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	


}

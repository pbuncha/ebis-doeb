package th.go.doeb.ebis.bean;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="formtestandquality")
public class formTestAndQuality2 {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	
	@Column(name="files")
	String 	files;
	
	@Column(name="fname1")
	String 	fname1;
	
	@Column(name="typetest")
	String 	typetest;
	
	@Column(name="fnameth")
	String 	fnameth;
	
	@Column(name="lnameth")
	String 	lnameth;
	
	@Column(name="fnameeng")
	String 	fnameeng;
	
	@Column(name="lnameeng")
	String 	lnameeng;
	
	@Column(name="company")
	String 	company;
	
	@Column(name="registration")
	String 	registration;
	
	@Column(name="registration_number")
	String 	registration_number;
	
	@Column(name="dateregis")
	String 	dateregis;
	
	@Column(name="commercial")
	String 	commercial;
	
	@Column(name="commercial_number")
	String 	commercial_number;
	
	@Column(name="datecom")
	String 	datecom;
	
	@Column(name="money_unit")
	String 	money_unit;
	
	@Column(name="pay_unit")
	String 	pay_unit;
	
	@Column(name="insurance_money")
	String 	insurance_money;
	
	@Column(name="insurance_money_unit")
	String 	insurance_money_unit;
	
	@Column(name="insurance_money_asset")
	String 	insurance_money_asset;
	
	@Column(name="numadd")
	String 	numadd;
	
	@Column(name="nummoo")
	String 	nummoo;
	
	@Column(name="alley")
	String 	alley;
	
	@Column(name="road")
	String 	road;
	
	@Column(name="district")
	String 	district;
	
	@Column(name="district1")
	String 	district1;
	
	@Column(name="province")
	String 	province;
	
	@Column(name="phone")
	String 	phone;
	
	@Column(name="total_unit")
	String 	total_unit;
	
	@Column(name="total_unit1")
	String 	total_unit1;
	
	@Column(name="people_unit1")
	String 	people_unit1;
	
	@Column(name="people_unit2")
	String 	people_unit2;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFiles() {
		return files;
	}

	public void setFiles(String files) {
		this.files = files;
	}

	public String getFname1() {
		return fname1;
	}

	public void setFname1(String fname1) {
		this.fname1 = fname1;
	}

	public String getTypetest() {
		return typetest;
	}

	public void setTypetest(String typetest) {
		this.typetest = typetest;
	}

	public String getFnameth() {
		return fnameth;
	}

	public void setFnameth(String fnameth) {
		this.fnameth = fnameth;
	}

	public String getLnameth() {
		return lnameth;
	}

	public void setLnameth(String lnameth) {
		this.lnameth = lnameth;
	}

	public String getFnameeng() {
		return fnameeng;
	}

	public void setFnameeng(String fnameeng) {
		this.fnameeng = fnameeng;
	}

	public String getLnameeng() {
		return lnameeng;
	}

	public void setLnameeng(String lnameeng) {
		this.lnameeng = lnameeng;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public String getRegistration_number() {
		return registration_number;
	}

	public void setRegistration_number(String registration_number) {
		this.registration_number = registration_number;
	}

	public String getCommercial() {
		return commercial;
	}

	public void setCommercial(String commercial) {
		this.commercial = commercial;
	}

	public String getCommercial_number() {
		return commercial_number;
	}

	public void setCommercial_number(String commercial_number) {
		this.commercial_number = commercial_number;
	}

	

	public String getMoney_unit() {
		return money_unit;
	}

	public void setMoney_unit(String money_unit) {
		this.money_unit = money_unit;
	}

	public String getPay_unit() {
		return pay_unit;
	}

	public void setPay_unit(String pay_unit) {
		this.pay_unit = pay_unit;
	}

	public String getInsurance_money() {
		return insurance_money;
	}

	public void setInsurance_money(String insurance_money) {
		this.insurance_money = insurance_money;
	}

	public String getInsurance_money_unit() {
		return insurance_money_unit;
	}

	public void setInsurance_money_unit(String insurance_money_unit) {
		this.insurance_money_unit = insurance_money_unit;
	}

	public String getInsurance_money_asset() {
		return insurance_money_asset;
	}

	public void setInsurance_money_asset(String insurance_money_asset) {
		this.insurance_money_asset = insurance_money_asset;
	}

	public String getNumadd() {
		return numadd;
	}

	public void setNumadd(String numadd) {
		this.numadd = numadd;
	}

	public String getNummoo() {
		return nummoo;
	}

	public void setNummoo(String nummoo) {
		this.nummoo = nummoo;
	}

	public String getAlley() {
		return alley;
	}

	public void setAlley(String alley) {
		this.alley = alley;
	}

	public String getRoad() {
		return road;
	}

	public void setRoad(String road) {
		this.road = road;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getDistrict1() {
		return district1;
	}

	public void setDistrict1(String district1) {
		this.district1 = district1;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTotal_unit() {
		return total_unit;
	}

	public void setTotal_unit(String total_unit) {
		this.total_unit = total_unit;
	}

	public String getTotal_unit1() {
		return total_unit1;
	}

	public void setTotal_unit1(String total_unit1) {
		this.total_unit1 = total_unit1;
	}

	public String getPeople_unit1() {
		return people_unit1;
	}

	public void setPeople_unit1(String people_unit1) {
		this.people_unit1 = people_unit1;
	}

	public String getPeople_unit2() {
		return people_unit2;
	}

	public void setPeople_unit2(String people_unit2) {
		this.people_unit2 = people_unit2;
	}

	public String getDateregis() {
		return dateregis;
	}

	public void setDateregis(String dateregis) {
		this.dateregis = dateregis;
	}

	public String getDatecom() {
		return datecom;
	}

	public void setDatecom(String datecom) {
		this.datecom = datecom;
	}
	
	
}

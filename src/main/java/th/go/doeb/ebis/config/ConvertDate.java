package th.go.doeb.ebis.config;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConvertDate {
	public static Date dateThai(String strDate)
	{
		/*String string = strDate;
		String[] parts = string.split("-");
		String monthString;
        switch (parts[1]) {
            case "01":  monthString = "JAN";
                     break;
            case "02":  monthString = "FEB";
                     break;
            case "03":  monthString = "MAR";
                     break;
            case "04":  monthString = "APR";
                     break;
            case "05":  monthString = "MAY";
                     break;
            case "06":  monthString = "JUN";
                     break;
            case "07":  monthString = "JUL";
                     break;
            case "08":  monthString = "AUG";
                     break;
            case "09":  monthString = "SEP";
                     break;
            case "10": monthString = "OCT";
                     break;
            case "11": monthString = "NOV";
                     break;
            case "12": monthString = "DEC";
                     break;
            default: monthString = "Invalid month";
                     break;
        }
		String cvDate = parts[2] + '-' + monthString + '-' +parts[0].substring(2);
		//System.out.println(cvDate);*/
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
		//DateFormat df = new SimpleDateFormat("dd-MMM-YY"); 
		Date date = null;
		try {
			date = df.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
}

package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "ZONE" database table.
 * 
 */
@Entity
@NamedQuery(name="Zone.findAll", query="SELECT z FROM Zone z")
public class Zone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ZONE_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_ZONE")
	@SequenceGenerator(name="SEQUENCE_ZONE", sequenceName="SEQUENCE_ZONE", allocationSize=1)
	private long zoneId;

	@Column(name="ZONE_NAME")
	private String zoneName;

	public Zone() {
	}

	public long getZoneId() {
		return this.zoneId;
	}

	public void setZoneId(long zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return this.zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

}
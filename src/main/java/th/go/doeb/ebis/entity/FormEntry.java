package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.math.BigDecimal;
import java.sql.Timestamp;

//comment test push
/**
 * The persistent class for the FORM_ENTRY database table.
 * 
 */
@Entity
@Table(name="FORM_ENTRY")
@NamedQuery(name="FormEntry.findAll", query="SELECT f FROM FormEntry f")
public class FormEntry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ENTRY_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_FORM_ENTRY")
    @SequenceGenerator(name="SEQUENCE_FORM_ENTRY", sequenceName="SEQUENCE_FORM_ENTRY", allocationSize=1)
	private long entryId;

	@Column(name="CURRENT_STATE")
	private BigDecimal currentState;

	private BigDecimal dst;

	@Column(name="ENTRY_TEXT")
	private String entryText;

	private Timestamp modified;

	private BigDecimal src;

	
	@ManyToOne
	@JoinColumn(name="STATUS")
	@JsonBackReference
	private RequestStatus requestStatus;

	//bi-directional many-to-one association to RequestForm
	@ManyToOne
	@JoinColumn(name="RF_ID")
	@JsonBackReference
	private RequestForm requestForm;

	//bi-directional many-to-one association to RequestForm
	@ManyToOne
	@JoinColumn(name="RT_ID")
	@JsonBackReference
	private RequestTransaction requestTransaction;	

	@Column(name="IDENGINEER")
	private String idEngineer;

	// @Column(name="ENGINEER_ID")
	// private long idEngineer;

	// @Column(name="FORM_ID")
	// private long idForm;
	
	public FormEntry() {
	}

	public long getEntryId() {
		return this.entryId;
	}

	public String getIDEngineer(){
		
		return this.idEngineer;
	}

	public void setIDEngineer(String idEngineer){
		this.idEngineer = idEngineer;
	}

	// public long getIDEngineer(){
		
	// 	return this.idEngineer;
	// }

	// public void setIDEngineer(long idEngineer){
	// 	this.idEngineer = idEngineer;
	// }

	// public long getIDForm(){
	// 	return this.idForm;
	// }

	// public void setIDForm(long idForm){
	// 	this.idForm = idForm;
	// }

	public void setEntryId(long entryId) {
		this.entryId = entryId;
	}

	public BigDecimal getCurrentState() {
		return this.currentState;
	}

	public void setCurrentState(BigDecimal currentState) {
		this.currentState = currentState;
	}

	public BigDecimal getDst() {
		return this.dst;
	}

	public void setDst(BigDecimal dst) {
		this.dst = dst;
	}

	public String getEntryText() {
		return this.entryText;
	}

	public void setEntryText(String entryText) {
		this.entryText = entryText;
	}

	public Timestamp getModified() {
		return this.modified;
	}

	public void setModified(Timestamp modified) {
		this.modified = modified;
	}

	public BigDecimal getSrc() {
		return this.src;
	}

	public void setSrc(BigDecimal src) {
		this.src = src;
	}

	public RequestForm getRequestForm() {
		return this.requestForm;
	}

	public void setRequestForm(RequestForm requestForm) {
		this.requestForm = requestForm;
	}

	public RequestTransaction getRequestTransaction() {
		return requestTransaction;
	}

	public void setRequestTransaction(RequestTransaction requestTransaction) {
		this.requestTransaction = requestTransaction;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}


}
package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.math.BigDecimal;
import java.util.List;

//comment test push
/**
 * The persistent class for the FORM database table.
 * 
 */
@Entity
@Table(name="FORM")
@NamedQuery(name="Form.findAll", query="SELECT f FROM Form f")
public class Form implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="FORM_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_FORM")
    @SequenceGenerator(name="SEQUENCE_FORM", sequenceName="SEQUENCE_FORM", allocationSize=1)
	private long formId;

	@Column(name="FORM_ACTIVE")
	private BigDecimal formActive;

	@Column(name="FORM_DESCRIPTION")
	private String formDescription;

	@Column(name="FORM_TEMPLATE")
	private String formTemplate;

	@Column(name="FORM_TITLE")
	private String formTitle;

	@Column(name="STATE_1")
	private BigDecimal state1;

	@Column(name="STATE_2")
	private BigDecimal state2;

	@Column(name="STATE_3")
	private BigDecimal state3;

	@Column(name="STATE_4")
	private BigDecimal state4;

	@Column(name="STATE_5")
	private BigDecimal state5;

	@Column(name="STATE_6")
	private BigDecimal state6;

	@Column(name="STATE_7")
	private BigDecimal state7;

	//bi-directional many-to-one association to RequestForm
	@OneToMany(mappedBy="form")
	@JsonManagedReference
	private List<RequestForm> requestForms;

	public Form() {
	}

	public long getFormId() {
		return this.formId;
	}

	public void setFormId(long formId) {
		this.formId = formId;
	}

	public BigDecimal getFormActive() {
		return this.formActive;
	}

	public void setFormActive(BigDecimal formActive) {
		this.formActive = formActive;
	}

	public String getFormDescription() {
		return this.formDescription;
	}

	public void setFormDescription(String formDescription) {
		this.formDescription = formDescription;
	}

	public String getFormTemplate() {
		return this.formTemplate;
	}

	public void setFormTemplate(String formTemplate) {
		this.formTemplate = formTemplate;
	}

	public String getFormTitle() {
		return this.formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public BigDecimal getState1() {
		return this.state1;
	}

	public void setState1(BigDecimal state1) {
		this.state1 = state1;
	}

	public BigDecimal getState2() {
		return this.state2;
	}

	public void setState2(BigDecimal state2) {
		this.state2 = state2;
	}

	public BigDecimal getState3() {
		return this.state3;
	}

	public void setState3(BigDecimal state3) {
		this.state3 = state3;
	}

	public BigDecimal getState4() {
		return this.state4;
	}

	public void setState4(BigDecimal state4) {
		this.state4 = state4;
	}

	public BigDecimal getState5() {
		return this.state5;
	}

	public void setState5(BigDecimal state5) {
		this.state5 = state5;
	}

	public BigDecimal getState6() {
		return this.state6;
	}

	public void setState6(BigDecimal state6) {
		this.state6 = state6;
	}

	public BigDecimal getState7() {
		return this.state7;
	}

	public void setState7(BigDecimal state7) {
		this.state7 = state7;
	}

	public List<RequestForm> getRequestForms() {
		return this.requestForms;
	}

	public void setRequestForms(List<RequestForm> requestForms) {
		this.requestForms = requestForms;
	}

	public RequestForm addRequestForm(RequestForm requestForm) {
		getRequestForms().add(requestForm);
		requestForm.setForm(this);

		return requestForm;
	}

	public RequestForm removeRequestForm(RequestForm requestForm) {
		getRequestForms().remove(requestForm);
		requestForm.setForm(null);

		return requestForm;
	}

}
package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.List;

//comment test push
/**
 * The persistent class for the REQUEST_CATEGORY database table.
 * 
 */
@Entity
@Table(name="REQUEST_CATEGORY")
@NamedQuery(name="RequestCategory.findAll", query="SELECT r FROM RequestCategory r")
public class RequestCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="RQ_CATEGORY_ID")
	private long rqCategoryId;

	@Column(name="RQ_CATEGORY_TYPE")
	private String rqCategoryType;

	@Column(name="RQ_CATEGORY_NAME")
	private String rqCategoryName;
	
	//bi-directional many-to-one association to Request
	@OneToMany(mappedBy="requestCategory")
	@JsonManagedReference
	private List<Request> requests;

	public RequestCategory() {
	}

	public long getRqCategoryId() {
		return this.rqCategoryId;
	}

	public void setRqCategoryId(long rqCategoryId) {
		this.rqCategoryId = rqCategoryId;
	}

	public String getRqCategoryName() {
		return this.rqCategoryName;
	}

	public void setRqCategoryName(String rqCategoryName) {
		this.rqCategoryName = rqCategoryName;
	}

	public List<Request> getRequests() {
		return this.requests;
	}

	public void setRequests(List<Request> requests) {
		this.requests = requests;
	}

	public Request addRequest(Request request) {
		getRequests().add(request);
		request.setRequestCategory(this);

		return request;
	}

	public Request removeRequest(Request request) {
		getRequests().remove(request);
		request.setRequestCategory(null);

		return request;
	}

	public String getRqCategoryType() {
		return rqCategoryType;
	}

	public void setRqCategoryType(String rqCategoryType) {
		this.rqCategoryType = rqCategoryType;
	}

}
package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the COUNTRY database table.
 * 
 */
@Entity
@NamedQuery(name="Country.findAll", query="SELECT c FROM Country c")
public class Country implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COUNTRY_CODE")
	private String countryCode;

	@Column(name="COUNTRY_NAME")
	private String countryName;

	@Column(name="ZONE_ID")
	private java.math.BigDecimal zoneId;

	public Country() {
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public java.math.BigDecimal getZoneId() {
		return this.zoneId;
	}

	public void setZoneId(java.math.BigDecimal zoneId) {
		this.zoneId = zoneId;
	}

}
package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the COMPANY_ATTORNEY database table.
 * 
 */
@Entity
@Table(name="COMPANY_ATTORNEY")
@NamedQuery(name="CompanyAttorney.findAll", query="SELECT c FROM CompanyAttorney c")
public class CompanyAttorney implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ATTORNEY_ID")
	private long attorneyId;

	@Column(name="ADDRESS_NO")
	private String addressNo;

	@Column(name="AMPHUR_ID")
	private String amphurId;

	private String building;

	@Column(name="DISTRICT_ID")
	private String districtId;

	private String email;

	private String fax;

	private String firstname;

	@Column(name="\"FLOOR\"")
	private String floor;

	private String lastname;

	private String mobile;

	private String moo;

	@Column(name="PROVINCE_ID")
	private String provinceId;

	private String room;

	private String soi;

	private String street;

	private String telephone;

	private String title;

	private String zipcode;

	@Column(name="BIRTHDAY")
	private String birthDay;
	
	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	@Column(name="IDENTIFICATION_NUMBER")
	private String identificationNumber;
	
	//bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name="DBD_ID")
	private Company company;

	public CompanyAttorney() {
	}

	public long getAttorneyId() {
		return this.attorneyId;
	}

	public void setAttorneyId(long attorneyId) {
		this.attorneyId = attorneyId;
	}

	public String getAddressNo() {
		return this.addressNo;
	}

	public void setAddressNo(String addressNo) {
		this.addressNo = addressNo;
	}

	public String getAmphurId() {
		return this.amphurId;
	}

	public void setAmphurId(String amphurId) {
		this.amphurId = amphurId;
	}

	public String getBuilding() {
		return this.building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getFloor() {
		return this.floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMoo() {
		return this.moo;
	}

	public void setMoo(String moo) {
		this.moo = moo;
	}

	public String getProvinceId() {
		return this.provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getRoom() {
		return this.room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getSoi() {
		return this.soi;
	}

	public void setSoi(String soi) {
		this.soi = soi;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}
package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the EMPLOYEE database table.
 * 
 */
@Entity
@NamedQuery(name="Employee.findAll", query="SELECT e FROM Employee e")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PERSONAL_ID")
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_EMPLOYEE")
//	@SequenceGenerator(name="SEQUENCE_EMPLOYEE", sequenceName="SEQUENCE_EMPLOYEE", allocationSize=1)
	private String personalId;

	private String firstname;

	private String lastname;

	//bi-directional many-to-one association to Department
	@ManyToOne
	@JoinColumn(name="DEPARTMENT_ID", referencedColumnName="DEPARTMENT_ID")
	private Department department;

	public Employee() {
	}

	public String getPersonalId() {
		return this.personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

}
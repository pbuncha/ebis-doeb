package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the EMPLOYEE_DOEB database table.
 * 
 */
@Entity
@Table(name="EMPLOYEE_DOEB")
@NamedQuery(name="EmployeeDoeb.findAll", query="SELECT e FROM EmployeeDoeb e")
public class EmployeeDoeb implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="EMPLOYEE_UID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_EMPLOYEE_DOEB")
	@SequenceGenerator(name="SEQUENCE_EMPLOYEE_DOEB", sequenceName="SEQUENCE_EMPLOYEE_DOEB", allocationSize=1)
	private long employeeUid;

	@Column(name="EMPLOYEE_CN")
	private String employeeCn;

	@Column(name="EMPLOYEE_USERNAME")
	private String employeeUsername;

	public EmployeeDoeb() {
	}

	public long getEmployeeUid() {
		return this.employeeUid;
	}

	public void setEmployeeUid(long employeeUid) {
		this.employeeUid = employeeUid;
	}

	public String getEmployeeCn() {
		return this.employeeCn;
	}

	public void setEmployeeCn(String employeeCn) {
		this.employeeCn = employeeCn;
	}

	public String getEmployeeUsername() {
		return this.employeeUsername;
	}

	public void setEmployeeUsername(String employeeUsername) {
		this.employeeUsername = employeeUsername;
	}

}
package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the UROLE database table.
 * 
 */
@Entity
@NamedQuery(name="Urole.findAll", query="SELECT u FROM Urole u")
public class Urole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ROLE_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_ROLE")
	@SequenceGenerator(name="SEQUENCE_ROLE", sequenceName="SEQUENCE_ROLE", allocationSize=1)
	private long roleId;

	@Column(name="ROLE_NAME")
	private String roleName;

	public Urole() {
	}

	public long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
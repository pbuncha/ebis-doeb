package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.math.BigDecimal;
import java.util.List;

//comment test push
/**
 * The persistent class for the REQUEST database table.
 * 
 */
@Entity
@NamedQuery(name="Request.findAll", query="SELECT r FROM Request r")
public class Request implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REQUEST_ID")
	private long requestId;

	@Column(name="REQUEST_NAME")
	private String requestName;
	
	@Column(name="GAURANTEE_DAY")
	private long gauranteeDay;
	
	//bi-directional many-to-one association to RequestCategory
	@ManyToOne
	@JoinColumn(name="RQ_CATEGORY_ID")
	@JsonBackReference
	private RequestCategory requestCategory;

	//bi-directional many-to-one association to RequestForm
	@OneToMany(mappedBy="request")
	@JsonManagedReference
	private List<RequestForm> requestForms;

	public Request() {
	}

	public long getRequestId() {
		return this.requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	public String getRequestName() {
		return this.requestName;
	}

	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}

	public RequestCategory getRequestCategory() {
		return this.requestCategory;
	}

	public void setRequestCategory(RequestCategory requestCategory) {
		this.requestCategory = requestCategory;
	}

	public List<RequestForm> getRequestForms() {
		return this.requestForms;
	}

	public void setRequestForms(List<RequestForm> requestForms) {
		this.requestForms = requestForms;
	}

	public RequestForm addRequestForm(RequestForm requestForm) {
		getRequestForms().add(requestForm);
		requestForm.setRequest(this);

		return requestForm;
	}

	public RequestForm removeRequestForm(RequestForm requestForm) {
		getRequestForms().remove(requestForm);
		requestForm.setRequest(null);

		return requestForm;
	}

	public long getGauranteeDay() {
		return gauranteeDay;
	}

	public void setGauranteeDay(long gauranteeDay) {
		this.gauranteeDay = gauranteeDay;
	}

}
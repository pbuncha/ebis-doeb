package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TOOL database table.
 * 
 */
@Entity
@NamedQuery(name="Tool.findAll", query="SELECT t FROM Tool t")
public class Tool implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="TOOL_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_TOOL")
	@SequenceGenerator(name="SEQUENCE_TOOL", sequenceName="SEQUENCE_TOOL", allocationSize=1)
	private long toolId;

	@Column(name="TOOL_BRAND")
	private String toolBrand;

	@Column(name="TOOL_MODEL")
	private String toolModel;

	@Column(name="TOOL_NAME")
	private String toolName;

	@Column(name="TOOL_PHOTO")
	private String toolPhoto;

	@Column(name="TOOL_SERIAL")
	private String toolSerial;
	
	@Column(name="DELETED")
	private int deleted;
	
	@Column(name="TOOL_NAME_CATAGORY")
	private String toolNameCatagory;
	
	@Column(name="MANUAL")
	private String manual;
	
	@Column(name="FIELD_INSPECTION")
	private String fieldInspection;
	
	@Column(name="PROPRIETARY_DOCUMENT")
	private String proprietaryDocument;
	
	

	@Column(name="CALIBRATION_TOOL")
	private String calibrationTool;
	
	
	@Column(name="CALIBRATION_TOOL_EXPIRE")
	private String calibrationToolExprie;
	
	
	//bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name="DBD_ID")
	private Company company;

	public Tool() {
	}

	public long getToolId() {
		return this.toolId;
	}

	public void setToolId(long toolId) {
		this.toolId = toolId;
	}

	public String getToolBrand() {
		return this.toolBrand;
	}

	public void setToolBrand(String toolBrand) {
		this.toolBrand = toolBrand;
	}

	public String getToolModel() {
		return this.toolModel;
	}

	public void setToolModel(String toolModel) {
		this.toolModel = toolModel;
	}

	public String getToolName() {
		return this.toolName;
	}

	public void setToolName(String toolName) {
		this.toolName = toolName;
	}

	public String getToolPhoto() {
		return this.toolPhoto;
	}

	public void setToolPhoto(String toolPhoto) {
		this.toolPhoto = toolPhoto;
	}

	public String getToolSerial() {
		return this.toolSerial;
	}

	public void setToolSerial(String toolSerial) {
		this.toolSerial = toolSerial;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public int getDeleted() {
		return deleted;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}

	public String getToolNameCatagory() {
		return toolNameCatagory;
	}

	public void setToolNameCatagory(String toolNameCatagory) {
		this.toolNameCatagory = toolNameCatagory;
	}

	public String getManual() {
		return manual;
	}

	public void setManual(String manual) {
		this.manual = manual;
	}

	public String getFieldInspection() {
		return fieldInspection;
	}

	public void setFieldInspection(String fieldInspection) {
		this.fieldInspection = fieldInspection;
	}

	public String getProprietaryDocument() {
		return proprietaryDocument;
	}

	public void setProprietaryDocument(String proprietaryDocument) {
		this.proprietaryDocument = proprietaryDocument;
	}

	public String getCalibrationTool() {
		return calibrationTool;
	}

	public void setCalibrationTool(String calibrationTool) {
		this.calibrationTool = calibrationTool;
	}
	public String getCalibrationToolExprie() {
		return calibrationToolExprie;
	}

	public void setCalibrationToolExprie(String calibrationToolExprie) {
		this.calibrationToolExprie = calibrationToolExprie;
	}
}
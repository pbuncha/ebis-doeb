package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the COMPANY database table.
 * 
 */
@Entity
@NamedQuery(name="Company.findAll", query="SELECT c FROM Company c")
public class Company implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DBD_ID")
	private String dbdId;

	@Column(name="ADDRESS_NO")
	private String addressNo;

	@Column(name="AMPHUR_ID")
	private String amphurId;

	private String building;

	private String capital;

	@Column(name="COMPANY_NAME")
	private String companyName;

	@Column(name="COMPANY_NAME_EN")
	private String companyNameEn;

	@Column(name="DISTRICT_ID")
	private String districtId;

	private String doc;

	private String email;

	private String fax;

	@Column(name="FLOOR")
	private String floor;

	@Column(name="MEMBER_ID")
	private String memberId;

	private String mobile;

	private String moo;

	@Column(name="PROVINCE_ID")
	private String provinceId;

	private String room;

	private String soi;

	private String street;

	private String telephone;

	private String web;

	private String zipcode;

	//bi-directional many-to-one association to Engineer
	@OneToMany(mappedBy="company")
	private List<Engineer> engineers;

	public Company() {
	}

	public String getDbdId() {
		return this.dbdId;
	}

	public void setDbdId(String dbdId) {
		this.dbdId = dbdId;
	}

	public String getAddressNo() {
		return this.addressNo;
	}

	public void setAddressNo(String addressNo) {
		this.addressNo = addressNo;
	}

	public String getAmphurId() {
		return this.amphurId;
	}

	public void setAmphurId(String amphurId) {
		this.amphurId = amphurId;
	}

	public String getBuilding() {
		return this.building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyNameEn() {
		return this.companyNameEn;
	}

	public void setCompanyNameEn(String companyNameEn) {
		this.companyNameEn = companyNameEn;
	}

	public String getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}

	public String getDoc() {
		return this.doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getFloor() {
		return this.floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getMemberId() {
		return this.memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMoo() {
		return this.moo;
	}

	public void setMoo(String moo) {
		this.moo = moo;
	}

	public String getProvinceId() {
		return this.provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getRoom() {
		return this.room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getSoi() {
		return this.soi;
	}

	public void setSoi(String soi) {
		this.soi = soi;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getWeb() {
		return this.web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public List<Engineer> getEngineers() {
		return this.engineers;
	}

	public void setEngineers(List<Engineer> engineers) {
		this.engineers = engineers;
	}

	public Engineer addEngineer(Engineer engineer) {
		getEngineers().add(engineer);
		engineer.setCompany(this);

		return engineer;
	}

	public Engineer removeEngineer(Engineer engineer) {
		getEngineers().remove(engineer);
		engineer.setCompany(null);

		return engineer;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

}
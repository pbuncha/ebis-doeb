package th.go.doeb.ebis.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
//comment test push
@Entity
@Table(name="FORM_META")
public class FormMeta {
	
	@Id
	@Column(name="META_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_FORM_META")
	@SequenceGenerator(name="SEQUENCE_FORM_META", sequenceName="SEQUENCE_FORM_META", allocationSize=1)
	private Long metaId;
	
	@Column(name="META_LABEL")
	private String metaLabel;
	
	@Column(name="META_NAME")
	private String metaName;
	
	@Column(name="META_TYPE")
	private String metaType;
	
	@Column(name="META_ORDER")
	private int metaOrder;
	
	@Column(name="FORM_ID")
	private BigDecimal formId;

	public Long getMetaId() {
		return metaId;
	}

	public void setMetaId(Long metaId) {
		this.metaId = metaId;
	}

	public String getMetaLabel() {
		return metaLabel;
	}

	public void setMetaLabel(String metaLabel) {
		this.metaLabel = metaLabel;
	}

	public String getMetaName() {
		return metaName;
	}

	public void setMetaName(String metaName) {
		this.metaName = metaName;
	}

	public String getMetaType() {
		return metaType;
	}

	public void setMetaType(String metaType) {
		this.metaType = metaType;
	}

	public int getMetaOrder() {
		return metaOrder;
	}

	public void setMetaOrder(int metaOrder) {
		this.metaOrder = metaOrder;
	}

	public BigDecimal getFormId() {
		return formId;
	}

	public void setFormId(BigDecimal formId) {
		this.formId = formId;
	}
	
	
}

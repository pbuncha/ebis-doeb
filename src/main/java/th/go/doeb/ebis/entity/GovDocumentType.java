package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the GOV_DOCUMENT_TYPE database table.
 * 
 */
@Entity
@Table(name="GOV_DOCUMENT_TYPE")
@NamedQuery(name="GovDocumentType.findAll", query="SELECT g FROM GovDocumentType g")
public class GovDocumentType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DOCTYPE_ID")
	private long doctypeId;

	@Column(name="DOCTYPE_NAME")
	private String doctypeName;

	//bi-directional many-to-one association to GovDocument
	@OneToMany(mappedBy="govDocumentType")
	private List<GovDocument> govDocuments;

	public GovDocumentType() {
	}

	public long getDoctypeId() {
		return this.doctypeId;
	}

	public void setDoctypeId(long doctypeId) {
		this.doctypeId = doctypeId;
	}

	public String getDoctypeName() {
		return this.doctypeName;
	}

	public void setDoctypeName(String doctypeName) {
		this.doctypeName = doctypeName;
	}

	public List<GovDocument> getGovDocuments() {
		return this.govDocuments;
	}

	public void setGovDocuments(List<GovDocument> govDocuments) {
		this.govDocuments = govDocuments;
	}

	public GovDocument addGovDocument(GovDocument govDocument) {
		getGovDocuments().add(govDocument);
		govDocument.setGovDocumentType(this);

		return govDocument;
	}

	public GovDocument removeGovDocument(GovDocument govDocument) {
		getGovDocuments().remove(govDocument);
		govDocument.setGovDocumentType(null);

		return govDocument;
	}

}
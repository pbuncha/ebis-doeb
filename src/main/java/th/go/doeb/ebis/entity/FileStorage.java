package th.go.doeb.ebis.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FILE_STORAGE database table.
 * 
 */
@Entity
@Table(name="FILE_STORAGE")
@NamedQuery(name="FileStorage.findAll", query="SELECT f FROM FileStorage f")
public class FileStorage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="FILE_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_FILE_ID")
    @SequenceGenerator(name="SEQUENCE_FILE_ID", sequenceName="SEQUENCE_FILE_ID", allocationSize=1)
	private long fileId;
	public BigDecimal getDeleted() {
		return deleted;
	}

	public void setDeleted(BigDecimal deleted) {
		this.deleted = deleted;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private BigDecimal deleted;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FILE_DATE")
	private Date fileDate;

	@Column(name="FILE_DOC_NUMBER")
	private String fileDocNumber;

	@Column(name="FILE_DOC_TITLE")
	private String fileDocTitle;

	@Column(name="FILE_ORIGINAL_NAME")
	private String fileOriginalName;

	@Column(name="FILE_PRINTABLE")
	private String filePrintable;

	@Column(name="FILE_STORE_NAME")
	private String fileStoreName;
	
	@Column(name="FILE_DOC_NAMEGENARAL")
	private String namegeneral;

	//bi-directional many-to-one association to GovDocumentType
	@ManyToOne
	@JoinColumn(name="FILE_DOC_TYPE")
	private GovDocumentType govDocumentType;

	//bi-directional many-to-one association to RequestTransaction
	@ManyToOne
	@JoinColumn(name="RT_ID")
	private RequestTransaction requestTransaction;

	public FileStorage() {
	}

	public long getFileId() {
		return this.fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public Date getFileDate() {
		return this.fileDate;
	}

	public void setFileDate(Date fileDate) {
		this.fileDate = fileDate;
	}

	public String getFileDocNumber() {
		return this.fileDocNumber;
	}

	public void setFileDocNumber(String fileDocNumber) {
		this.fileDocNumber = fileDocNumber;
	}

	public String getFileDocTitle() {
		return this.fileDocTitle;
	}

	public void setFileDocTitle(String fileDocTitle) {
		this.fileDocTitle = fileDocTitle;
	}

	public String getFileOriginalName() {
		return this.fileOriginalName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public String getFilePrintable() {
		return this.filePrintable;
	}

	public void setFilePrintable(String filePrintable) {
		this.filePrintable = filePrintable;
	}

	public String getFileStoreName() {
		return this.fileStoreName;
	}

	public void setFileStoreName(String fileStoreName) {
		this.fileStoreName = fileStoreName;
	}

	public String getNamegeneral() {
		return namegeneral;
	}

	public void setNamegeneral(String namegeneral) {
		this.namegeneral = namegeneral;
	}

	public GovDocumentType getGovDocumentType() {
		return this.govDocumentType;
	}

	public void setGovDocumentType(GovDocumentType govDocumentType) {
		this.govDocumentType = govDocumentType;
	}

	public RequestTransaction getRequestTransaction() {
		return this.requestTransaction;
	}

	public void setRequestTransaction(RequestTransaction requestTransaction) {
		this.requestTransaction = requestTransaction;
	}

}
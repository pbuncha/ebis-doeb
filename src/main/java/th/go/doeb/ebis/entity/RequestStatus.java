package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;

//comment test push
/**
 * The persistent class for the REQUEST_STATUS database table.
 * 
 */
@Entity
@Table(name="REQUEST_STATUS")
@NamedQuery(name="RequestStatus.findAll", query="SELECT r FROM RequestStatus r")
public class RequestStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String status;

	private String text;

	public RequestStatus() {
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
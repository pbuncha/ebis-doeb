package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.math.BigDecimal;
import java.util.List;

//comment test push
/**
 * The persistent class for the REQUEST_FORM database table.
 * 
 */
@Entity
@Table(name="REQUEST_FORM")
@NamedQuery(name="RequestForm.findAll", query="SELECT r FROM RequestForm r")
public class RequestForm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="RF_ID")
	private long rfId;

	//bi-directional many-to-one association to FormEntry
	@OneToMany(mappedBy="requestForm")
	@JsonManagedReference
	private List<FormEntry> formEntries;

	//bi-directional many-to-one association to FormFormEntry
	@ManyToOne
	@JoinColumn(name="FORM_ID")
	@JsonBackReference
	private Form form;

	//bi-directional many-to-one association to Request
	@ManyToOne
	@JoinColumn(name="REQUEST_ID")
	@JsonBackReference
	private Request request;
	
	@ManyToOne
	@JoinColumn(name="REQUEST_TYPE")
	private RequestType requestType;

	public RequestForm() {
	}

	public long getRfId() {
		return this.rfId;
	}

	public void setRfId(long rfId) {
		this.rfId = rfId;
	}

	public List<FormEntry> getFormEntries() {
		return this.formEntries;
	}

	public void setFormEntries(List<FormEntry> formEntries) {
		this.formEntries = formEntries;
	}

	public FormEntry addFormEntry(FormEntry formEntry) {
		getFormEntries().add(formEntry);
		formEntry.setRequestForm(this);

		return formEntry;
	}

	public FormEntry removeFormEntry(FormEntry formEntry) {
		getFormEntries().remove(formEntry);
		formEntry.setRequestForm(null);

		return formEntry;
	}

	public Form getForm() {
		return this.form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

	public Request getRequest() {
		return this.request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

}
package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the GOV_DOCUMENT_CREATE database table.
 * 
 */
@Entity
@Table(name="GOV_DOCUMENT_CREATE")
@NamedQuery(name="GovDocumentCreate.findAll", query="SELECT g FROM GovDocumentCreate g")
public class GovDocumentCreate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	@Column(name="DOC_CREATE_ID")
	private BigDecimal docCreateId;

	@Column(name="DOC_CREATE_TITLE")
	private String docCreateTitle;

	//bi-directional many-to-one association to Request
	@ManyToOne
	@JoinColumn(name="REQUEST_ID")
	private Request request;

	public GovDocumentCreate() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getDocCreateId() {
		return this.docCreateId;
	}

	public void setDocCreateId(BigDecimal docCreateId) {
		this.docCreateId = docCreateId;
	}

	public String getDocCreateTitle() {
		return this.docCreateTitle;
	}

	public void setDocCreateTitle(String docCreateTitle) {
		this.docCreateTitle = docCreateTitle;
	}

	public Request getRequest() {
		return this.request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

}
package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.math.BigDecimal;


/**
 * The persistent class for the EVALUATE database table.
 * 
 */
@Entity
@NamedQuery(name="Evaluate.findAll", query="SELECT e FROM Evaluate e")
public class Evaluate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	@Column(name="EVALUATE_ID")
	private BigDecimal evaluateId;


	@ManyToOne
	@JoinColumn(name="REQUEST_TYPE")
	@JsonBackReference
	private RequestType requestType;
	
	@Column(name="EVALUATE_TITLE")
	private String evaluateTitle;

	//bi-directional many-to-one association to Request
	@ManyToOne
	@JoinColumn(name="REQUEST_ID")
	private Request request;

	public Evaluate() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getEvaluateId() {
		return this.evaluateId;
	}

	public void setEvaluateId(BigDecimal evaluateId) {
		this.evaluateId = evaluateId;
	}

	public String getEvaluateTitle() {
		return this.evaluateTitle;
	}

	public void setEvaluateTitle(String evaluateTitle) {
		this.evaluateTitle = evaluateTitle;
	}

	public Request getRequest() {
		return this.request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

}
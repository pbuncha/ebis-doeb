package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the EVENT_LOG database table.
 * 
 */
@Entity
@Table(name="EVENT_LOG")
@NamedQuery(name="EventLog.findAll", query="SELECT e FROM EventLog e")
public class EventLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="LOG_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="EVENT_LOG_SEQ")
	@SequenceGenerator(name="EVENT_LOG_SEQ", sequenceName="EVENT_LOG_SEQ", allocationSize=1)
	private long logId;

	@Column(name="LOG_IP")
	private String logIp;

	@Column(name="LOG_REFER")
	private String logRefer;

	@Column(name="LOG_TIMESTAMP")
	private Timestamp logTimestamp;

	@Column(name="LOG_TYPE")
	private String logType;

	@Column(name="LOG_USER")
	private String logUser;

	public EventLog() {
	}

	public long getLogId() {
		return this.logId;
	}

	public void setLogId(long logId) {
		this.logId = logId;
	}

	public String getLogIp() {
		return this.logIp;
	}

	public void setLogIp(String logIp) {
		this.logIp = logIp;
	}

	public String getLogRefer() {
		return this.logRefer;
	}

	public void setLogRefer(String logRefer) {
		this.logRefer = logRefer;
	}

	public Timestamp getLogTimestamp() {
		return this.logTimestamp;
	}

	public void setLogTimestamp(Timestamp logTimestamp) {
		this.logTimestamp = logTimestamp;
	}

	public String getLogType() {
		return this.logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public String getLogUser() {
		return this.logUser;
	}

	public void setLogUser(String logUser) {
		this.logUser = logUser;
	}

}

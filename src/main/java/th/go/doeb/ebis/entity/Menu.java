package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the MENU database table.
 * 
 */
@Entity
@NamedQuery(name="Menu.findAll", query="SELECT m FROM Menu m")
public class Menu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MENU_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_MENU")
	@SequenceGenerator(name="SEQUENCE_MENU", sequenceName="SEQUENCE_MENU", allocationSize=1)
	private long menuId;

	@Column(name="MENU_RELATIVE_PATH")
	private String menuRelativePath;

	@Column(name="MENU_TITLE")
	private String menuTitle;

	public Menu() {
	}

	public long getMenuId() {
		return this.menuId;
	}

	public void setMenuId(long menuId) {
		this.menuId = menuId;
	}

	public String getMenuRelativePath() {
		return this.menuRelativePath;
	}

	public void setMenuRelativePath(String menuRelativePath) {
		this.menuRelativePath = menuRelativePath;
	}

	public String getMenuTitle() {
		return this.menuTitle;
	}

	public void setMenuTitle(String menuTitle) {
		this.menuTitle = menuTitle;
	}

}
package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;

import th.go.doeb.ebis.bean.CompanyBean;

import java.math.BigDecimal;


/**
 * The persistent class for the ENGINEER_TRAINING database table.
 * 
 */
@Entity
@Table(name="ENGINEER_TRAINING")
@NamedQuery(name="EngineerTraining.findAll", query="SELECT e FROM EngineerTraining e")
public class EngineerTraining implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="COURSE_ENGINEERTRAINING")
	private long courseEngineertraining;


	private int deleted;

	@Column(name="ENGINEER_ID")
	private String engineerId;

	private String firstname;

	@Id
	@Column(name="ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_ENGINEER_TRAINING")
	@SequenceGenerator(name="SEQUENCE_ENGINEER_TRAINING", sequenceName="SEQUENCE_ENGINEER_TRAINING", allocationSize=1)
	private Long id;

	private String lastname;

	private Long rtid;

	@Column(name="TYPE_ENGINEER_TRAINING")
	private long typeEngineerTraining;

	//bi-directional many-to-one association to TitleName
	@ManyToOne
	@JoinColumn(name="TITLE")
	private TitleName titleName;

	//bi-directional many-to-one association to Company
		@ManyToOne
		@JoinColumn(name="DBD_ID")
		private CompanyBean company;
		
	public EngineerTraining() {
	}

	public long getCourseEngineertraining() {
		return this.courseEngineertraining;
	}

	public void setCourseEngineertraining(long courseEngineertraining) {
		this.courseEngineertraining = courseEngineertraining;
	}

	public int getDeleted() {
		return this.deleted;
	}

	public void setDeleted(int i) {
		this.deleted = i;
	}

	public String getEngineerId() {
		return this.engineerId;
	}

	public void setEngineerId(String engineerId) {
		this.engineerId = engineerId;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Long getRtid() {
		return this.rtid;
	}

	public void setRtid(Long rtid) {
		this.rtid = rtid;
	}

	public long getTypeEngineerTraining() {
		return this.typeEngineerTraining;
	}

	public void setTypeEngineerTraining(long tYPE_ENGINEER_TRAINING) {
		this.typeEngineerTraining = tYPE_ENGINEER_TRAINING;
	}

	public TitleName getTitleName() {
		return this.titleName;
	}

	public void setTitleName(TitleName titleName) {
		this.titleName = titleName;
	}

	public CompanyBean getCompany() {
		return company;
	}

	public void setCompany(CompanyBean companyBean) {
		this.company = companyBean;
	}

}
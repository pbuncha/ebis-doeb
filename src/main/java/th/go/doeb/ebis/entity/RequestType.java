package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;

//comment test push
/**
 * The persistent class for the REQUEST_TYPE database table.
 * 
 */
@Entity
@Table(name="REQUEST_TYPE")
@NamedQuery(name="RequestType.findAll", query="SELECT r FROM RequestType r")
public class RequestType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REQUEST_TYPE_ID")
	private long requestTypeId;

	@Column(name="REQUEST_TYPE_NAME")
	private String requestTypeName;

	public RequestType() {
	}

	public long getRequestTypeId() {
		return this.requestTypeId;
	}

	public void setRequestTypeId(long requestTypeId) {
		this.requestTypeId = requestTypeId;
	}

	public String getRequestTypeName() {
		return this.requestTypeName;
	}

	public void setRequestTypeName(String requestTypeName) {
		this.requestTypeName = requestTypeName;
	}

}
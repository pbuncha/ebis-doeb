package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the REQUEST_CHECKLIST database table.
 * 
 */
@Entity
@Table(name="REQUEST_CHECKLIST")
@NamedQuery(name="RequestChecklist.findAll", query="SELECT r FROM RequestChecklist r")
public class RequestChecklist implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CHECKLIST_TEXT")
	private String checklistText;

	@Id
	@Column(name="ID")
	private Long id;

	//bi-directional many-to-one association to Request
	@ManyToOne
	@JoinColumn(name="REQUEST_ID")
	private Request request;

	public RequestChecklist() {
	}

	public String getChecklistText() {
		return this.checklistText;
	}

	public void setChecklistText(String checklistText) {
		this.checklistText = checklistText;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Request getRequest() {
		return this.request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

}
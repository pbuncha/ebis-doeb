package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the GOV_DOCUMENT database table.
 * 
 */
@Entity
@Table(name="GOV_DOCUMENT")
@NamedQuery(name="GovDocument.findAll", query="SELECT g FROM GovDocument g")
public class GovDocument implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DOC_ID")
	private long docId;

	@Column(name="DOC_ATTACH")
	private String docAttach;

	@Column(name="DOC_DETAIL")
	private String docDetail;

	@Column(name="DOC_NUMBER")
	private String docNumber;

	@Column(name="DOC_TITLE")
	private String docTitle;

	@Column(name="EMPLOYEE_PERSONAL_ID")
	private String employeePersonalId;

	//bi-directional many-to-one association to GovDocumentType
	@ManyToOne
	@JoinColumn(name="DOCTYPE_ID")
	private GovDocumentType govDocumentType;

	public GovDocument() {
	}

	public long getDocId() {
		return this.docId;
	}

	public void setDocId(long docId) {
		this.docId = docId;
	}

	public String getDocAttach() {
		return this.docAttach;
	}

	public void setDocAttach(String docAttach) {
		this.docAttach = docAttach;
	}

	public String getDocDetail() {
		return this.docDetail;
	}

	public void setDocDetail(String docDetail) {
		this.docDetail = docDetail;
	}

	public String getDocNumber() {
		return this.docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getDocTitle() {
		return this.docTitle;
	}

	public void setDocTitle(String docTitle) {
		this.docTitle = docTitle;
	}

	public String getEmployeePersonalId() {
		return this.employeePersonalId;
	}

	public void setEmployeePersonalId(String employeePersonalId) {
		this.employeePersonalId = employeePersonalId;
	}

	public GovDocumentType getGovDocumentType() {
		return this.govDocumentType;
	}

	public void setGovDocumentType(GovDocumentType govDocumentType) {
		this.govDocumentType = govDocumentType;
	}

}
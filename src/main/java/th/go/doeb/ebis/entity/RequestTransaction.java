package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.sql.Timestamp;
import java.util.Date;

//comment test push
/**
 * The persistent class for the REQUEST_TRANSACTION database table.
 * 
 */
@Entity
@Table(name="REQUEST_TRANSACTION")
@NamedQuery(name="RequestTransaction.findAll", query="SELECT r FROM RequestTransaction r")
public class RequestTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="RT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_REQUEST_TRANSACTION")
	@SequenceGenerator(name="SEQUENCE_REQUEST_TRANSACTION", sequenceName="SEQUENCE_REQUEST_TRANSACTION", allocationSize=1)
	private long rtId;
	
	@Column(name="ACTUAL_GAURANTEE_DAY")
	private long actualGaranteeDay;
	
	private Timestamp created;
	
	@ManyToOne
	@JoinColumn(name="DBD_ID")
	@JsonBackReference
	private Company Company;

	@ManyToOne
	@JoinColumn(name="REQUEST_ID")
	@JsonBackReference
	private Request request;
	
	


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Column(name="START_TIMESTAMP")
	private Timestamp startTimestamp;

	@Column(name="END_TIMESTAMP")
	private Timestamp endTimestamp;
	
	@Column(name="EVALUATE_RESULT")
	private String evaluateResult;
	
	@Column(name="DOCUMENT_RECEIPT_NO")
	private String documentReceiptNo;
	
	@Column(name="DATE_DOCUMENT_RECEIPT_NO")
	private Date dateDocumentReceiptNo;
	
	@Column(name="EVALUATE_REMARK")
	private String evaluateRemark;
	
	@Column(name="EVALUATE_1")
	private String evaluate1;
	@Column(name="EVALUATE_2")
	private String evaluate2;
	@Column(name="EVALUATE_3")
	private String evaluate3;
	@Column(name="EVALUATE_4")
	private String evaluate4;
	
	@Column(name="EVALUATE_REMARK_NO_1")
	private String evaluateRemarkNo1;
	@Column(name="EVALUATE_REMARK_NO_2")
	private String evaluateRemarkNo2;
	@Column(name="EVALUATE_REMARK_NO_3")
	private String evaluateRemarkNo3;
	@Column(name="EVALUATE_REMARK_NO_4")
	private String evaluateRemarkNo4;
	
	@ManyToOne
	@JoinColumn(name="STATUS")
	@JsonBackReference
	private RequestStatus requestStatus;

	@ManyToOne
	@JoinColumn(name="REQUEST_TYPE_ID")
	@JsonBackReference
	private RequestType requestType;
	
	public RequestTransaction() {
	}

	public long getRtId() {
		return this.rtId;
	}

	public void setRtId(long rtId) {
		this.rtId = rtId;
	}

	public Timestamp getCreated() {
		return this.created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	

	public Timestamp getStartTimestamp() {
		return this.startTimestamp;
	}

	public void setStartTimestamp(Timestamp startTimestamp) {
		this.startTimestamp = startTimestamp;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getEvaluateResult() {
		return evaluateResult;
	}

	public void setEvaluateResult(String evaluateResult) {
		this.evaluateResult = evaluateResult;
	}

	public String getEvaluateRemark() {
		return evaluateRemark;
	}

	public void setEvaluateRemark(String evaluateRemark) {
		this.evaluateRemark = evaluateRemark;
	}

	public String getEvaluate1() {
		return evaluate1;
	}

	public void setEvaluate1(String evaluate1) {
		this.evaluate1 = evaluate1;
	}

	public String getEvaluate2() {
		return evaluate2;
	}

	public void setEvaluate2(String evaluate2) {
		this.evaluate2 = evaluate2;
	}

	public String getEvaluate3() {
		return evaluate3;
	}

	public void setEvaluate3(String evaluate3) {
		this.evaluate3 = evaluate3;
	}

	public String getEvaluate4() {
		return evaluate4;
	}

	public void setEvaluate4(String evaluate4) {
		this.evaluate4 = evaluate4;
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

	public Company getCompany() {
		return Company;
	}

	public void setCompany(Company company) {
		Company = company;
	}

	public long getActualGaranteeDay() {
		return actualGaranteeDay;
	}

	public void setActualGaranteeDay(long actualGaranteeDay) {
		this.actualGaranteeDay = actualGaranteeDay;
	}

	public String getDocumentReceiptNo() {
		return documentReceiptNo;
	}

	public void setDocumentReceiptNo(String documentReceiptNo) {
		this.documentReceiptNo = documentReceiptNo;
	}

	public Date getDateDocumentReceiptNo() {
		return dateDocumentReceiptNo;
	}

	public void setDateDocumentReceiptNo(Date dateDocumentReceiptNo) {
		this.dateDocumentReceiptNo = dateDocumentReceiptNo;
	}

	public String getEvaluateRemarkNo1() {
		return evaluateRemarkNo1;
	}

	public void setEvaluateRemarkNo1(String evaluateRemarkNo1) {
		this.evaluateRemarkNo1 = evaluateRemarkNo1;
	}

	public String getEvaluateRemarkNo2() {
		return evaluateRemarkNo2;
	}

	public void setEvaluateRemarkNo2(String evaluateRemarkNo2) {
		this.evaluateRemarkNo2 = evaluateRemarkNo2;
	}

	public String getEvaluateRemarkNo3() {
		return evaluateRemarkNo3;
	}

	public void setEvaluateRemarkNo3(String evaluateRemarkNo3) {
		this.evaluateRemarkNo3 = evaluateRemarkNo3;
	}

	public String getEvaluateRemarkNo4() {
		return evaluateRemarkNo4;
	}

	public void setEvaluateRemarkNo4(String evaluateRemarkNo4) {
		this.evaluateRemarkNo4 = evaluateRemarkNo4;
	}

	public Timestamp getEndTimestamp() {
		return endTimestamp;
	}

	public void setEndTimestamp(Timestamp endTimestamp) {
		this.endTimestamp = endTimestamp;
	}

}
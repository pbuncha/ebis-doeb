package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the EVALUATE_ENTRY database table.
 * 
 */
@Entity
@Table(name="EVALUATE_ENTRY")
@NamedQuery(name="EvaluateEntry.findAll", query="SELECT e FROM EvaluateEntry e")
public class EvaluateEntry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="EE_ID")
	private long eeId;

	@Column(name="\"VALUE\"")
	private String value;

	//bi-directional many-to-one association to Evaluate
	@ManyToOne
	@JoinColumn(name="ID")
	private Evaluate evaluate;

	//bi-directional many-to-one association to RequestTransaction
	@ManyToOne
	@JoinColumn(name="RT_ID")
	private RequestTransaction requestTransaction;

	public EvaluateEntry() {
	}

	public long getEeId() {
		return this.eeId;
	}

	public void setEeId(long eeId) {
		this.eeId = eeId;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Evaluate getEvaluate() {
		return this.evaluate;
	}

	public void setEvaluate(Evaluate evaluate) {
		this.evaluate = evaluate;
	}

	public RequestTransaction getRequestTransaction() {
		return this.requestTransaction;
	}

	public void setRequestTransaction(RequestTransaction requestTransaction) {
		this.requestTransaction = requestTransaction;
	}

}
package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the COMPANY_CONTACT database table.
 * 
 */
@Entity
@Table(name="COMPANY_CONTACT")
@NamedQuery(name="CompanyContact.findAll", query="SELECT c FROM CompanyContact c")
public class CompanyContact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CONTACT_ID")
	private long contactId;

	private String email;

	private String fax;

	private String firstname;

	private String lastname;

	private String mobile;

	private String telephone;

	private String title;

	//bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name="DBD_ID")
	private Company company;

	public CompanyContact() {
	}

	public long getContactId() {
		return this.contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}
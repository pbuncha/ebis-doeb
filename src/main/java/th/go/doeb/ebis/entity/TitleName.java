package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TITLE_NAME database table.
 * 
 */
@Entity
@Table(name="TITLE_NAME")
@NamedQuery(name="TitleName.findAll", query="SELECT t FROM TitleName t")
public class TitleName implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String code;

	private String abbreviation;

	private BigDecimal active;

	@Column(name="GOV_CODE")
	private String govCode;

	private String info;

	public TitleName() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAbbreviation() {
		return this.abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public BigDecimal getActive() {
		return this.active;
	}

	public void setActive(BigDecimal active) {
		this.active = active;
	}

	public String getGovCode() {
		return this.govCode;
	}

	public void setGovCode(String govCode) {
		this.govCode = govCode;
	}

	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

}
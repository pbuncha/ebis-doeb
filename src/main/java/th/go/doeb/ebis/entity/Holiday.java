package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the HOLIDAY database table.
 * 
 */
@Entity
@NamedQuery(name="Holiday.findAll", query="SELECT h FROM Holiday h WHERE h.deleted != '1' ")
public class Holiday implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_HOLIDAY")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_HOLIDAY")
	@SequenceGenerator(name="SEQUENCE_HOLIDAY", sequenceName="SEQUENCE_HOLIDAY", allocationSize=1)
	private long idHoliday;

	private BigDecimal deleted;

	@Temporal(TemporalType.DATE)
	@Column(name="HOLIDAY_DATE")
	private Date holidayDate;

	@Column(name="HOLIDAY_NAME")
	private String holidayName;

	public Holiday() {
	}

	public long getIdHoliday() {
		return this.idHoliday;
	}

	public void setIdHoliday(long idHoliday) {
		this.idHoliday = idHoliday;
	}

	public BigDecimal getDeleted() {
		return this.deleted;
	}

	public void setDeleted(BigDecimal deleted) {
		this.deleted = deleted;
	}

	public Date getHolidayDate() {
		return this.holidayDate;
	}

	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}

	public String getHolidayName() {
		return this.holidayName;
	}

	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}

}
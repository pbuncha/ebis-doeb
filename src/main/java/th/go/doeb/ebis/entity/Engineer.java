package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ENGINEER database table.
 * 
 */
@Entity
@NamedQuery(name="Engineer.findAll", query="SELECT e FROM Engineer e")
public class Engineer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_ENGINEER_ID")
    @SequenceGenerator(name="SEQUENCE_ENGINEER_ID", sequenceName="SEQUENCE_ENGINEER_ID", allocationSize=1)
	private long id;
	
	@Column(name="ENGINEER_ID")
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_ENGINEER")
   // @SequenceGenerator(name="SEQUENCE_ENGINEER", sequenceName="SEQUENCE_ENGINEER", allocationSize=1)
	private String engineerId;

	private String firstname;

	private String lastname;

	@ManyToOne
	@JoinColumn(name="TITLE")
	private TitleName titleName;

	@Column(name="TYPE")
	private String type;

	@Column(name="FACULTY")
	private String faculty;

	@Column(name="RT_ID")
	private String rt_id;
	
	@Column(name="ISSUE_DATE")
	private String issueDate;
	
	@Column(name="EXPIRE_DATE")
	private String expireDate;
	
	@Column(name="POSITION")
	private String position;
	
	@Column(name="SKILL")
	private String skill;
	
	@Column(name="PERSONAL_ID")
	private String personalId;
	
	@Column(name="ADDRESS")
	private String address;
	
	@Column(name="COPY_ID")
	private String copyId;
	
	@Column(name="FILE_1")
	private String file1;
	
	@Column(name="FILE_2")
	private String file2;
	
	@Column(name="FILE_3")
	private String file3;
	
	@Column(name="FILE_4")
	private String file4;
	
	@Column(name="FILE_5")
	private String file5;
	
	@Column(name="COPY_PASSPORT")
	private String copyPassport;
	
	@Column(name="COPY_REGISTRATION")
	private String copyRegistration;
	
	@Column(name="COPY_LICENSED_ENGINEERING")
	private String copyLicensedEngineering;
	
	@Column(name="COPY_CERTIFICATE_1")
	private String copyCertificate1;
	
	@Column(name="COPY_CERTIFICATE_2")
	private String copyCertificate2;
	
	@Column(name="COPY_CERTIFICATE_3")
	private String copyCertificate3;
	
	@Column(name="DELETED")
	private int deleted;

	@Column(name="TITLEENGINEER")
	private String titleEngi;
	


	//bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name="DBD_ID")
	
	private Company company;

	public Engineer() {
	}

	public String getEngineerId() {
		return this.engineerId;
	}

	public void setEngineerId(String engineerId) {
		this.engineerId = engineerId;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getFaculty() {
		return this.faculty;
	}

	public void setRTID(String rt_id) {
		this.rt_id = rt_id;
	}

	public String getRTID() {
		return this.rt_id;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public int getDeleted() {
		return deleted;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}
 
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TitleName getTitleName() {
		return titleName;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCopyId() {
		return copyId;
	}

	public void setCopyId(String copyId) {
		this.copyId = copyId;
	}

	public String getCopyPassport() {
		return copyPassport;
	}

	public void setCopyPassport(String copyPassport) {
		this.copyPassport = copyPassport;
	}

	public String getCopyRegistration() {
		return copyRegistration;
	}

	public void setCopyRegistration(String copyRegistration) {
		this.copyRegistration = copyRegistration;
	}

	public String getCopyLicensedEngineering() {
		return copyLicensedEngineering;
	}

	public void setCopyLicensedEngineering(String copyLicensedEngineering) {
		this.copyLicensedEngineering = copyLicensedEngineering;
	}

	public String getCopyCertificate1() {
		return copyCertificate1;
	}

	public void setCopyCertificate1(String copyCertificate1) {
		this.copyCertificate1 = copyCertificate1;
	}

	public String getCopyCertificate2() {
		return copyCertificate2;
	}

	public void setCopyCertificate2(String copyCertificate2) {
		this.copyCertificate2 = copyCertificate2;
	}

	public String getCopyCertificate3() {
		return copyCertificate3;
	}

	public void setCopyCertificate3(String copyCertificate3) {
		this.copyCertificate3 = copyCertificate3;
	}

	public void setTitleName(TitleName titleName) {
		this.titleName = titleName;
	}

	//----------------------START SORNRAM----------------------

	

	public String getFile1() {
		return file1;
	}

	public String getFile2() {
		return file2;
	}

	public String getFile3() {
		return file3;
	}

	public String getFile4() {
		return file4;
	}

	public String getFile5() {
		return file5;
	}
	
	public void setFile1(String file1) {
		this.file1 = file1;
	}
	
	public void setFile2(String file2) {
		this.file2 = file2;
	}
	
	public void setFile3(String file3) {
		this.file3 = file3;
	}
	
	public void setFile4(String file4) {
		this.file4 = file4;
	}
	
	public void setFile5(String file5) {
		this.file5 = file5;
	}

	public String getTiTleEnGiNeer(){
		return this.titleEngi;
	}

	public void setTiTleEnGiNeer(String titleEngi) {
		this.titleEngi = titleEngi;
	}

	//------------------------------------------------------

	@Column(name="NUMHOME_SERVICE")
	private String numhome_service;

	public String getNumhome_service() {
		return numhome_service;
	}

	public void setNumhome_service(String numhome_service) {
		this.numhome_service = numhome_service;
	}

	@Column(name="MOO_SERVICE")
	private String moo_service;

	public String getMoo_service() {
		return moo_service;
	}

	public void setMoo_service(String moo_service) {
		this.moo_service = moo_service;
	}

	@Column(name="SOI_SERVICE")
	private String soi_service;

	public String getSoi_service() {
		return soi_service;
	}

	public void setSoi_service(String soi_service) {
		this.soi_service = soi_service;
	}

	@Column(name="ROAD_SERVICE")
	private String road_service;

	public String getRoad_service() {
		return road_service;
	}

	public void setRoad_service(String road_service) {
		this.road_service = road_service;
	}

	@Column(name="TUMBON_SERVICE")
	private String tumbon_service;

	public String getTumbon_service() {
		return tumbon_service;
	}

	public void setTumbon_service(String tumbon_service) {
		this.tumbon_service = tumbon_service;
	}

	@Column(name="AUMPHUR_SERVICE")
	private String aumphur_service;

	public String getAumphur_service() {
		return aumphur_service;
	}

	public void setAumphur_service(String aumphur_service) {
		this.aumphur_service = aumphur_service;
	}

	@Column(name="PROVINCE_SERVICE")
	private String province_service;

	public String getProvince_service() {
		return province_service;
	}

	public void setProvince_service(String province_service) {
		this.province_service = province_service;
	}

	@Column(name="ZIPCODE_SERVICE")
	private String zipcode_service;

	public String getZipcode_service() {
		return zipcode_service;
	}

	public void setZipcode_service(String zipcode_service) {
		this.zipcode_service = zipcode_service;
	}

	@Column(name="TEL_SERVICE")
	private String tel_service;

	public String getTel_service() {
		return tel_service;
	}

	public void setTel_service(String tel_service) {
		this.tel_service = tel_service;
	}

	@Column(name="EMAIL_SERVICE")
	private String email_service;

	public String getEmail_service() {
		return email_service;
	}

	public void setEmail_service(String email_service) {
		this.email_service = email_service;
	}
	
	// @Column(name="TITLE_NAME")
	// private String title_name;

	// public String getTitle_name() {
	// 	return title_name;
	// }

	// public void setTitle_name(String title_name) {
	// 	this.title_name = title_name;
	// }

	// //-----------------------FILE Name------------------------

	// @Column(name="FILE_1")
	// private String file_1;

	// public String getFile_1() {
	// 	return file_1;
	// }

	// public void setFile_1(String file_1) {
	// 	this.file_1 = file_1;
	// }

	// @Column(name="FILE_2")
	// private String file_2;

	// public String getFile_2() {
	// 	return file_2;
	// }

	// public void setFile_2(String file_2) {
	// 	this.file_2 = file_2;
	// }

	// @Column(name="FILE_3")
	// private String file_3;

	// public String getFile_3() {
	// 	return file_3;
	// }

	// public void setFile_3(String file_3) {
	// 	this.file_3 = file_3;
	// }

	// @Column(name="FILE_4")
	// private String file_4;

	// public String getFile_4() {
	// 	return file_4;
	// }

	// public void setFile_4(String file_4) {
	// 	this.file_4 = file_4;
	// }

	// @Column(name="FILE_5")
	// private String file_5;

	// public String getFile_5() {
	// 	return file_5;
	// }

	// public void setFile_5(String file_5) {
	// 	this.file_5 = file_5;
	// }

	// //-----------------------END SORNRAM----------------------

}
package th.go.doeb.ebis.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FUEL_OPERATOR database table.
 * 
 */
@Entity
@Table(name="FUEL_OPERATOR")
@NamedQuery(name="FuelOperator.findAll", query="SELECT f FROM FuelOperator f")
public class FuelOperator implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="FO_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQUENCE_FUEL_OPERATOR")
	@SequenceGenerator(name="SEQUENCE_FUEL_OPERATOR", sequenceName="SEQUENCE_FUEL_OPERATOR", allocationSize=1)
	private long foId;

	@Column(name="FO_CARD_NO")
	private String foCardNo;

	@Temporal(TemporalType.DATE)
	@Column(name="FO_EXPIRE_DATE")
	private Date foExpireDate;

	@Temporal(TemporalType.DATE)
	@Column(name="FO_INIT_DATE")
	private Date foInitDate;

	@Column(name="FO_NAME")
	private String foName;

	@Column(name="FO_PERSONAL_ID")
	private String foPersonalId;

	@Column(name="FO_SURNAME")
	private String foSurname;

	//bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name="DBD_ID")
	private Company company;

	public FuelOperator() {
	}

	public long getFoId() {
		return this.foId;
	}

	public void setFoId(long foId) {
		this.foId = foId;
	}

	public String getFoCardNo() {
		return this.foCardNo;
	}

	public void setFoCardNo(String foCardNo) {
		this.foCardNo = foCardNo;
	}

	public Date getFoExpireDate() {
		return this.foExpireDate;
	}

	public void setFoExpireDate(Date foExpireDate) {
		this.foExpireDate = foExpireDate;
	}

	public Date getFoInitDate() {
		return this.foInitDate;
	}

	public void setFoInitDate(Date foInitDate) {
		this.foInitDate = foInitDate;
	}

	public String getFoName() {
		return this.foName;
	}

	public void setFoName(String foName) {
		this.foName = foName;
	}

	public String getFoPersonalId() {
		return this.foPersonalId;
	}

	public void setFoPersonalId(String foPersonalId) {
		this.foPersonalId = foPersonalId;
	}

	public String getFoSurname() {
		return this.foSurname;
	}

	public void setFoSurname(String foSurname) {
		this.foSurname = foSurname;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}
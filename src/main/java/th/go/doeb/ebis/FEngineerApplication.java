package th.go.doeb.ebis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FEngineerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FEngineerApplication.class, args);
	}
}

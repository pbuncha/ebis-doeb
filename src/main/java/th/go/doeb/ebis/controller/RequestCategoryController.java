package th.go.doeb.ebis.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestCategory;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.repository.RequestCategoryRepository;
import th.go.doeb.ebis.repository.RequestFormRepository;
import th.go.doeb.ebis.repository.RequestRepository;

@Controller
public class RequestCategoryController {
	
	@Autowired(required=true)
	private RequestCategoryRepository requestCategory;
	
	
	
	@RequestMapping(value="/admin/manage_requestcategory")
	public String manage_request(Model model)
	{
		
		Iterable<RequestCategory> eForm = this.requestCategory.findAll();
		List<RequestCategory> lRequestCategory = new ArrayList<>();
		
		eForm.forEach(lRequestCategory::add);
		model.addAttribute("lRequestCategory", lRequestCategory);
		
		return "admin/form_requestcategory";
	}

}

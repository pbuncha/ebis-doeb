package th.go.doeb.ebis.controller;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.AuthenticationException;
import javax.naming.AuthenticationNotSupportedException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.doeb.ebis.bean.AmphurBean;
import th.go.doeb.ebis.bean.ProvinceBean;
import th.go.doeb.ebis.bean.TambonBean;
import th.go.doeb.ebis.entity.Country;
import th.go.doeb.ebis.entity.EmployeeDoeb;
import th.go.doeb.ebis.entity.Form;
import th.go.doeb.ebis.entity.Menu;
import th.go.doeb.ebis.entity.Urole;
import th.go.doeb.ebis.entity.Zone;
import th.go.doeb.ebis.repository.AmphurRepository;
import th.go.doeb.ebis.repository.CountryRepository;
import th.go.doeb.ebis.repository.EmployeeDoebRepository;
import th.go.doeb.ebis.repository.FormRepository;
import th.go.doeb.ebis.repository.MenuRepository;
import th.go.doeb.ebis.repository.ProvinceRepository;
import th.go.doeb.ebis.repository.TambonRepository;
import th.go.doeb.ebis.repository.UroleRepository;
import th.go.doeb.ebis.repository.ZoneRepository;

@Controller
public class AdminController {
	
	@Autowired(required=true)
	private EmployeeDoebRepository employeeDoebRepository;
	
	@Autowired(required=true)
	private UroleRepository uRoleRepository;
	
	@Autowired(required=true)
	private MenuRepository menuRepository;

	@Autowired(required=true)
	private ZoneRepository zoneRepository;

	@Autowired(required=true)
	private CountryRepository countryRepository;	
	
	@Autowired(required=true)
	private ProvinceRepository provinceRepository;
	
	@Autowired(required=true)
	private AmphurRepository amphurRepository;
	
	@Autowired(required=true)
	private TambonRepository tambonRepository;
	
	@Autowired(required=true)
	private FormRepository formRepository;
	
	@RequestMapping(value="/admin")
	public String index(HttpSession session)
	{

		if (session.getAttribute("isAdminLogon") != null)
		{
			if (session.getAttribute("isAdminLogon").toString().equalsIgnoreCase("1")) {
				return "redirect:/admin/dashboard";
			}
		}
		return "admin/index";
	}
	
//	@RequestMapping(value="/admin/login")
//	@ResponseBody
//	public String login(String ausername, String apassword)
//	{		

//	if ((fusername.equalsIgnoreCase("admin"))&&(fpassword.equalsIgnoreCase("doeb2018")))
//	{
//		request.getSession().setAttribute("isUserLogon", "0");
//		request.getSession().setAttribute("isAdminLogon", "1");
//		request.getSession().setAttribute("memberId", "1");
//		return "redirect:/admin/dashboard";
//	}	
	
	//		return "admin/dashboard"+ausername+apassword;
//	}

	@RequestMapping(value="/admin/login")
	//@ResponseBody
	public String authen(String ausername, String apassword, HttpServletRequest request)
	{
		boolean isValid = false;
		request.getSession().setAttribute("isAdminLogon", "0");
		
		String output = "";
		Hashtable<String, String> environment = new Hashtable<String, String>();

        environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        //environment.put(Context.PROVIDER_URL, "ldap://ldap.doeb.go.th:389");
        environment.put(Context.PROVIDER_URL, "ldap://167.99.77.6:389");
        environment.put(Context.SECURITY_AUTHENTICATION, "simple");
        environment.put(Context.SECURITY_PRINCIPAL, "UID="+ausername+",OU=People,DC=doeb,DC=go,DC=th");
        environment.put(Context.SECURITY_CREDENTIALS, apassword);
        
        try 
        {
            DirContext context = new InitialDirContext(environment);
            output = "Connected..";
            output += "<br/>\n"+context.getEnvironment();
            
            SearchControls ctrls = new SearchControls();
            ctrls.setReturningAttributes(new String[]{"givenName", "sn", "cn", "mail"});
            //ctrls.setReturningAttributes(null);
            ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            NamingEnumeration<SearchResult> answers = context.search("OU=People,DC=doeb,DC=go,DC=th", "(UID=" + ausername + ")", ctrls);
            
            SearchResult result = answers.next();

            output += "<br/>\n"+result.getNameInNamespace();
            
            		try {
            	        for (NamingEnumeration ae = result.getAttributes().getAll(); ae.hasMore();) {
            	          Attribute attr = (Attribute) ae.next();
            	          output += "<br/>\nattribute: " + attr.getID();

            	          /* print each value */
            	          for (NamingEnumeration e = attr.getAll(); e.hasMore();
            	        		  output += "<br/>\nvalue: " + e.next())
            	            ;
            	        }
            	        isValid = true;
            	        request.getSession().setAttribute("isAdminLogon", "1");
            	      } catch (NamingException e) {
            	        //e.printStackTrace();
            	      }
            		
        } 
        catch (AuthenticationNotSupportedException exception) 
        {
            output = "The authentication is not supported by the server";
        }

        catch (AuthenticationException exception)
        {
            output = exception.getMessage()+exception.getExplanation();
        }

        catch (NamingException exception)
        {
            output = exception.getMessage()+exception.getExplanation();
        }		
		
		//return output;
        if (isValid)
        {
        		return "redirect:/admin/dashboard";
        } else {
        		return "redirect:/admin/authen_error";
        }
        //return output;
        
	}

	@RequestMapping(value="/admin/logout")
	public String logout(HttpServletRequest request)
	{
		request.getSession().setAttribute("isAdminLogon", "0");
		return "redirect:/";
	}
	
	@RequestMapping(value="/admin/dashboard")
	public String dashboard(HttpSession session)
	{
		if (session.getAttribute("isAdminLogon") != null)
		{
			if (session.getAttribute("isAdminLogon").toString().equalsIgnoreCase("1")) {
				return "admin/dashboard";
			}
		}
		return "redirect:/admin";
	}

	@RequestMapping(value="/admin/authen_error")
	public String authenError()
	{
		return "admin/authen_error";
	}	
	
	@RequestMapping(value="/admin/manage_user")
	public String manageUser(Model model)
	{
		Iterable<EmployeeDoeb> employeeDoebs = this.employeeDoebRepository.findAll();
		List<EmployeeDoeb> lEmployeeDoeb = new ArrayList<>();
		employeeDoebs.forEach(lEmployeeDoeb::add);
		model.addAttribute("lEmployeeDoeb", lEmployeeDoeb);
		return "admin/manage_user";
	}	

	@RequestMapping(value="/admin/add_user")
	public String addUser()
	{
		return "admin/form_user";
	}		
	
	@RequestMapping(value="/admin/manage_role")
	public String manageRole(Model model)
	{
		Iterable<Urole> roles = this.uRoleRepository.findAll();
		List<Urole> lRole = new ArrayList<>();
		roles.forEach(lRole::add);
		model.addAttribute("lRole", lRole);		
		return "admin/manage_role";
	}		

	@RequestMapping(value="/admin/add_role")
	public String addRole()
	{
		return "admin/form_role";
	}			
	
	@RequestMapping(value="/admin/saveUser")
	@ResponseBody
	public String saveUser(@ModelAttribute EmployeeDoeb employeeDoeb)
	{
		String resp = "0";
		if (this.employeeDoebRepository.findByEmployeeUsername(employeeDoeb.getEmployeeUsername()) == null) {
			this.employeeDoebRepository.save(employeeDoeb);
			resp = "1";
		}
		//return "admin/added_success";
		return resp;
	}
	
	@RequestMapping(value="/admin/saveRole")
	@ResponseBody
	public String saveUser(@ModelAttribute Urole urole)
	{
		String resp = "0";
		if (this.uRoleRepository.findByRoleName(urole.getRoleName()) == null) {
			this.uRoleRepository.save(urole);
			resp = "1";
		}
		return resp;
	}	

	@RequestMapping(value="/admin/manage_menu")
	public String manageMenu(Model model)
	{
		Iterable<Menu> menus = this.menuRepository.findAll();
		List<Menu> lMenu = new ArrayList<>();
		menus.forEach(lMenu::add);
		model.addAttribute("lMenu", lMenu);		
		return "admin/manage_menu";
	}	

	@RequestMapping(value="/admin/add_menu")
	public String addMenu()
	{
		return "admin/form_menu";
	}				

	@RequestMapping(value="/admin/saveMenu")
	@ResponseBody
	public String saveMenu(@ModelAttribute Menu menu)
	{
		String resp = "0";
		if (this.menuRepository.findByMenuTitle(menu.getMenuTitle()) == null) {
			this.menuRepository.save(menu);
			resp = "1";
		}
		return resp;
	}		
	
	//
	@RequestMapping(value="/admin/manage_zone")
	public String manageZone(Model model)
	{
		Iterable<Zone> zones = this.zoneRepository.findAll();
		List<Zone> lZone = new ArrayList<>();
		zones.forEach(lZone::add);
		model.addAttribute("lZone", lZone);		
		return "admin/manage_zone";
	}	

	@RequestMapping(value="/admin/add_zone")
	public String addZone()
	{
		return "admin/form_zone";
	}					

	@RequestMapping(value="/admin/saveZone")
	public String saveZone(@ModelAttribute Zone zone)
	{
		this.zoneRepository.save(zone);
		return "redirect:/admin/manage_zone";
	}			
	
	@RequestMapping(value="/admin/manage_country")
	public String manageCountry(Model model)
	{
		Iterable<Country> countries = this.countryRepository.findAll();
		List<Country> lCountry = new ArrayList<>();
		countries.forEach(lCountry::add);
		model.addAttribute("lCountry", lCountry);		
		return "admin/manage_country";
	}		

	@RequestMapping(value="/admin/add_country")
	public String addCountry()
	{
		return "admin/form_country";
	}				
	
	@RequestMapping(value="/admin/saveCountry")
	public String saveCountry(@ModelAttribute Country country)
	{
		this.countryRepository.save(country);
		return "redirect:/admin/manage_country";
	}		
	
	@RequestMapping(value="/admin/manage_province")
	public String manageProvince(Model model)
	{
		Iterable<ProvinceBean> menus = this.provinceRepository.findAll();
		List<ProvinceBean> lProvince = new ArrayList<>();
		menus.forEach(lProvince::add);
		model.addAttribute("lProvince", lProvince);		
		return "admin/manage_province";
	}	

	@RequestMapping(value="/admin/manage_amphur")
	public String manageAmphur(Model model)
	{
		Iterable<AmphurBean> menus = this.amphurRepository.findAll();
		List<AmphurBean> lAmphur = new ArrayList<>();
		menus.forEach(lAmphur::add);
		model.addAttribute("lAmphur", lAmphur);		
		return "admin/manage_amphur";
	}	

	@RequestMapping(value="/admin/manage_tambon")
	public String manageTambon(Model model)
	{
		Iterable<TambonBean> menus = this.tambonRepository.findAll();
		List<TambonBean> lTambon = new ArrayList<>();
		menus.forEach(lTambon::add);
		model.addAttribute("lTambon", lTambon);		
		return "admin/manage_tambon";
	}		

	@RequestMapping(value="/admin/manage_form")
	public String manageForm(Model model)
	{
		Iterable<Form> menus = this.formRepository.findAll();
		List<Form> lForm = new ArrayList<>();
		menus.forEach(lForm::add);
		model.addAttribute("lForm", lForm);		
		return "admin/manage_form";
	}
	
	//
	
	//
	
}

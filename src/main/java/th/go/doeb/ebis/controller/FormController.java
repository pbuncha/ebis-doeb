package th.go.doeb.ebis.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import th.go.doeb.ebis.bean.ProvinceBean;
import th.go.doeb.ebis.entity.Form;
import th.go.doeb.ebis.entity.FormEntry;
import th.go.doeb.ebis.entity.FormMeta;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.entity.RequestTransaction;
import th.go.doeb.ebis.entity.TitleName;
import th.go.doeb.ebis.repository.CompanyRepository;
import th.go.doeb.ebis.repository.FormEntryRepository;
import th.go.doeb.ebis.repository.FormMetaRepository;
import th.go.doeb.ebis.repository.FormRepository;
import th.go.doeb.ebis.repository.ProvinceRepository;
import th.go.doeb.ebis.repository.RequestFormRepository;
import th.go.doeb.ebis.repository.RequestTransactionRepository;
import th.go.doeb.ebis.repository.TitleNameRepository;
//comment test push
@Controller
public class FormController {
	
	private static String UPLOAD_FOLDER = "/data/test/uploads/etdi/";

	@Autowired(required=true)
	private ProvinceRepository provinceRepository;	

	@Autowired(required=true)
	private FormRepository formRepository;		

	@Autowired(required=true)
	private FormMetaRepository formMetaRepository;

	@Autowired(required=true)
	private FormEntryRepository formEntryRepository;	
	
	@Autowired(required=true)
	private RequestFormRepository requestFormRepository;
	
	@Autowired(required=true)
	private CompanyRepository companyRepository;
	
	@Autowired(required=true)
	private RequestTransactionRepository requestTransactionRepository;

	@Autowired(required=true)
	private TitleNameRepository titleNameRepository;	
	
	@RequestMapping(value="/form")
	public String form()
	{
		return "redirect:/";
	}

	@RequestMapping(value="/form/list")
	public String formList(Model model)
	{
		Iterable<Form> iForm = this.formRepository.findAll();
		List<Form> lForm = new ArrayList<>();
		iForm.forEach(lForm::add);
		model.addAttribute("lForm", lForm);
		
		return "form/list";
	}

	@RequestMapping(value="/form/add")
	public String formAdd(Model model)
	{
		return "form/add";
	}	

	@RequestMapping(value="/form/save")
	public String formSave(@ModelAttribute Form form)
	{
		this.formRepository.save(form);
		return "redirect:/form/list";
	}		

	@RequestMapping(value="/form/view/{formId}")
	public String formView(@PathVariable("formId") Long formId, Model model)
	{
		Form f = this.formRepository.findByFormId(formId);
		
        Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);
		model.addAttribute("lProvinceBean", lProvinceBean);
		model.addAttribute("formId", formId);

		Iterable<TitleName> eTitleName = this.titleNameRepository.findTitleNameActive();
		List<TitleName> lTitleName = new ArrayList<>();
		eTitleName.forEach(lTitleName::add);
		model.addAttribute("lTitleName", lTitleName);		
		
		return "form/" + f.getFormTemplate();
	}			
	
	@RequestMapping(value="/form/meta/{formId}")
	public String formMeta(@PathVariable("formId") Long formId, Model model)
	{
		Form f = this.formRepository.findByFormId(formId);
		
        Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);
		model.addAttribute("lProvinceBean", lProvinceBean);
		
		return "form/meta";
	}

	@RequestMapping(value="/form/workflow/{formId}")
	public String formWorkflow(@PathVariable("formId") Long formId, Model model)
	{
		Form f = this.formRepository.findByFormId(formId);
		
        Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);
		model.addAttribute("lProvinceBean", lProvinceBean);
		
		return "form/workflow";
	}	
	
	@RequestMapping(value="/form/submission")
	public String formSubmission(WebRequest request, HttpSession session)
	{
		String out = "";
		Map<String, Object> mapParams = new HashMap<String, Object>();
		String memberId = "1";
		BigDecimal formId = new BigDecimal(request.getParameter("formId"));
		RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(Long.valueOf(request.getParameter("rtId")));
		RequestForm requestForm = this.requestFormRepository.findByRfId(Long.valueOf(request.getParameter("rfId")));
		
		Map<String, String[]> parameters = request.getParameterMap();
		MultiValueMap<String, MultipartFile> uploadFiles = ((StandardMultipartHttpServletRequest) ((ServletWebRequest) request).getRequest()).getMultiFileMap();
		
		for (Map.Entry<String, String[]> parameter : parameters.entrySet()) {
			String key = parameter.getKey();
			String[] value = parameter.getValue();
			
			this.manageFormMeta(key.trim(), formId);
			
			if (value.length == 1)
			{
				//out += "Item : " + key + " Count : " + value[0] + "<br/>\n";
				mapParams.put(key, value[0]);
			} else {
//				for (int i = 0; i < value.length; i++) {
//                    pw.println("<li>" + value[i].toString() + "</li><br>");
//                }
				//out += "Item : " + key + " Count : " + parameter.getValue() + "<br/>\n";				
			}

		}

		//out += "#######";
		
		for (MultiValueMap.Entry<String, List<MultipartFile>> uploadFile : uploadFiles.entrySet()) {

			List<MultipartFile> files = uploadFile.getValue();
			
            for (MultipartFile multipartFile : files) {
            	 
                String fileName = multipartFile.getOriginalFilename();
                String fileParamName = multipartFile.getName();
                String currentSaveFileName = "";

                this.manageFormMeta(fileParamName.trim(), formId);
                
                //start upload
                
                if (multipartFile.getSize() > 0)
                {
                	
		        	long millis = System.currentTimeMillis() ;
		            
		            String type = multipartFile.getContentType();
		            type = "." + type.substring(type.lastIndexOf("/") + 1);
		            
		            currentSaveFileName = millis + type;
                	
                    try {
    	                String userAndFormFolder = UPLOAD_FOLDER + memberId + "/" + formId + "/";
    	                File directory = new File(String.valueOf(userAndFormFolder));
    	
    	                if (!directory.exists()) {
    	                    directory.mkdirs();
    	                }
    		            
    		            Path path = Paths.get(userAndFormFolder + currentSaveFileName);
    		            byte[] bytes = multipartFile.getBytes();
    		            Files.write(path, bytes);
    		            
                    } catch (Exception e) {
                    	e.printStackTrace();
                    }                	
                }
                

                //end upload
                
                //out += "Param. : " + fileParamName;
                //out += "File : " + fileName + "<br/>\n";
                mapParams.put(fileParamName, currentSaveFileName);
            }

		}
		
		ObjectMapper mapper = new ObjectMapper();
		try {

			String json = mapper.writeValueAsString(mapParams);
			
			FormEntry formEntry = new FormEntry();
			//formEntry.setForm(this.formRepository.findByFormId(formId.longValue()));
//			formEntry.setRequestForm(requestForm);
//			formEntry.setMemberId(memberId);
			formEntry.setEntryText(json);
			formEntry.setCurrentState(BigDecimal.valueOf(Double.valueOf("1")));
			formEntry.setModified(new Timestamp(System.currentTimeMillis()));
			
			if (this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm) == null)
			{
				//insert
				// don't set ENTRY_ID for insert!!
			} else {
				//update
				// set ENTRY_ID for update
				formEntry.setEntryId(this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm).getEntryId());
			}
			this.formEntryRepository.save(formEntry);
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "user/process.html";
	}

	private void manageFormMeta(String key, BigDecimal formId)
	{
		FormMeta fm = this.formMetaRepository.findByMetaNameAndFormId(key, formId);
		if (fm == null)
		{
			FormMeta newFormMeta = new FormMeta();
			newFormMeta.setFormId(formId);
			newFormMeta.setMetaLabel(key);
			newFormMeta.setMetaName(key);
			newFormMeta.setMetaOrder(0);
			newFormMeta.setMetaType("text");
			this.formMetaRepository.save(newFormMeta);
		}		
	}

	@RequestMapping(value="/form/review/{entryId}")
	//@ResponseBody
	//public Map<String,String> formReview(@PathVariable("entryId") Long entryId, Model model)
	public String formReview(@PathVariable("entryId") Long entryId, Model model)
	{
		FormEntry formEntry = this.formEntryRepository.findByEntryId(entryId);
		
		//Form form = formEntry.getForm();
		Form form = formEntry.getRequestForm().getForm();
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String,String> entryMap = new HashMap<String, String>();
		try {
			entryMap = objectMapper.readValue(formEntry.getEntryText(), new TypeReference<HashMap<String,String>>() {});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//return entryMap;
		model.addAttribute("entryMap", entryMap);
		return "form/" + form.getFormTemplate().toString();
	}
	
}

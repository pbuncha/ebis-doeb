package th.go.doeb.ebis.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import th.go.doeb.ebis.entity.Department;
import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestCategory;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.repository.RequestCategoryRepository;
import th.go.doeb.ebis.repository.RequestFormRepository;
import th.go.doeb.ebis.repository.RequestRepository;

@Controller
public class RequestController {
	
	@Autowired(required=true)
	private RequestRepository requestRepository;
	@Autowired(required=true)
	private RequestCategoryRepository requestCategory;
	@Autowired(required=true)
	private RequestFormRepository requestForm;
	
	
	@RequestMapping(value="/admin/manage_request")
	public String manage_request(Model model)
	{
		Iterable<Request> eForm = this.requestRepository.findAll();
		List<Request> lRequest = new ArrayList<>();
		
		Iterable<RequestCategory> iRequestCategory = this.requestCategory.findAll();
		List<RequestCategory> lRequestCategory = new ArrayList<>();
		
		Iterable<RequestForm> iRequestForm = this.requestForm.findAll();
		List<RequestForm> lRequestForm = new ArrayList<>();
		eForm.forEach(lRequest::add);
		iRequestCategory.forEach(lRequestCategory::add);
		iRequestForm.forEach(lRequestForm::add);
		model.addAttribute("lRequest", lRequest);
		model.addAttribute("lRequestCategory", lRequestCategory);
		model.addAttribute("lRequestForm", lRequestForm);
		
		return "admin/form_request";
	}
	
}

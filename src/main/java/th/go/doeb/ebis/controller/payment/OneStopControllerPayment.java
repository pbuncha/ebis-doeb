package th.go.doeb.ebis.controller.payment;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import th.go.doeb.ebis.bean.CompanyBean;
import th.go.doeb.ebis.bean.ProvinceBean;
import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.CompanyAttorney;
import th.go.doeb.ebis.entity.CompanyContact;
import th.go.doeb.ebis.entity.Engineer;
import th.go.doeb.ebis.entity.Form;
import th.go.doeb.ebis.entity.FormEntry;
import th.go.doeb.ebis.entity.FormMeta;
import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestCategory;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.entity.RequestStatus;
import th.go.doeb.ebis.entity.RequestTransaction;
import th.go.doeb.ebis.entity.RequestType;
import th.go.doeb.ebis.entity.TitleName;
import th.go.doeb.ebis.repository.CompanyAttorneyRepository;
import th.go.doeb.ebis.repository.CompanyContactRepository;
import th.go.doeb.ebis.repository.CompanyNewRepository;
import th.go.doeb.ebis.repository.EngineerRepository;
import th.go.doeb.ebis.repository.FormEntryRepository;
import th.go.doeb.ebis.repository.FormMetaRepository;
import th.go.doeb.ebis.repository.FormRepository;
import th.go.doeb.ebis.repository.ProvinceRepository;
import th.go.doeb.ebis.repository.RequestCategoryRepository;
import th.go.doeb.ebis.repository.RequestFormRepository;
import th.go.doeb.ebis.repository.RequestRepository;
import th.go.doeb.ebis.repository.RequestStatusRepository;
import th.go.doeb.ebis.repository.RequestTransactionRepository;
import th.go.doeb.ebis.repository.RequestTypeRepository;
import th.go.doeb.ebis.repository.TitleNameRepository;

@Controller
public class OneStopControllerPayment {

	private static String UPLOAD_FOLDER = "/data/test/uploads/etdi/";
	
	@Autowired(required=true)
	private CompanyNewRepository companyRepository;

	@Autowired(required=true)
	private ProvinceRepository provinceRepository;	

	@Autowired(required=true)
	private FormRepository formRepository;	

	@Autowired(required=true)
	private FormMetaRepository formMetaRepository;	
	
	@Autowired(required=true)
	private FormEntryRepository formEntryRepository;	
	
	@Autowired(required=true)
	private RequestCategoryRepository requestCategoryRepository;
	
	@Autowired(required=true)
	private RequestRepository requestRepository;
	
	@Autowired(required=true)
	private RequestFormRepository requestFormRepository;	
	
	@Autowired(required=true)
	private RequestTransactionRepository requestTransactionRepository;
	
	@Autowired(required=true)
	private RequestStatusRepository requestStatusRepository;
	
	@Autowired(required=true)
	private EngineerRepository engineerRepository;
	
	@Autowired(required=true)
	private CompanyContactRepository companyContactRepository;
	
	@Autowired(required=true)
	private CompanyAttorneyRepository companyAttorneyRepository;
	
	@Autowired(required=true)
	private RequestTypeRepository requestTypeRepository;
	
	@Autowired(required=true)
	private TitleNameRepository titleNameRepository;
	
	///onestop/company/1010101001111/requestFormOil
	//@RequestMapping(value="/onestop")
	@RequestMapping(value="/onestop/company/{dbdId}/requestFormPayment")
	public String requestForm(@PathVariable("dbdId") String dbdId, Model model)
	{
		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);
		
		Iterable<RequestCategory> iRequestCategory = this.requestCategoryRepository.findAllFilter7();
		List<RequestCategory> lRequestCategory = new ArrayList<>();
		iRequestCategory.forEach(lRequestCategory::add);
		
		//Iterable<Form> iForm = this.formRepository.findAll();
		Iterable<Form> iForm = this.formRepository.findByFormActive(new BigDecimal(1));
		List<Form> lForm = new ArrayList<>();
		iForm.forEach(lForm::add);
		model.addAttribute("lRequestCategory", lRequestCategory);
		model.addAttribute("lForm", lForm);
		model.addAttribute("dbdId", dbdId);
		
		return "onestopPayment/request_form";
	}
	
	
	
	
}

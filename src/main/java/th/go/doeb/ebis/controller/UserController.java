package th.go.doeb.ebis.controller;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import th.go.doeb.ebis.bean.ProvinceBean;
import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.Form;
import th.go.doeb.ebis.entity.FormEntry;
import th.go.doeb.ebis.entity.FormMeta;
import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestCategory;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.entity.RequestStatus;
import th.go.doeb.ebis.entity.RequestTransaction;
import th.go.doeb.ebis.entity.RequestType;
import th.go.doeb.ebis.repository.CompanyNewRepository;
import th.go.doeb.ebis.repository.FormEntryRepository;
import th.go.doeb.ebis.repository.FormMetaRepository;
import th.go.doeb.ebis.repository.FormRepository;
import th.go.doeb.ebis.repository.ProvinceRepository;
import th.go.doeb.ebis.repository.RequestCategoryRepository;
import th.go.doeb.ebis.repository.RequestFormRepository;
import th.go.doeb.ebis.repository.RequestRepository;
import th.go.doeb.ebis.repository.RequestStatusRepository;
import th.go.doeb.ebis.repository.RequestTransactionRepository;
import th.go.doeb.ebis.repository.RequestTypeRepository;

@Controller
public class UserController {
	
	@Autowired(required=true)
	private CompanyNewRepository companyRepository;
	
	@Autowired(required=true)
	private ProvinceRepository provinceRepository;

	@Autowired(required=true)
	private FormEntryRepository formEntryRepository;	

	@Autowired(required=true)
	private RequestRepository requestRepository;	
	
	@Autowired(required=true)
	private RequestFormRepository requestFormRepository;	
	
	@Autowired(required=true)
	private RequestCategoryRepository requestCategoryRepository;	

	@Autowired(required=true)
	private RequestTransactionRepository requestTransactionRepository;	

	@Autowired(required=true)
	private RequestStatusRepository requestStatusRepository;	

	@Autowired(required=true)
	private RequestTypeRepository requestTypeRepository;	
	
	@Autowired(required=true)
	private FormRepository formRepository;		

	@Autowired(required=true)
	private FormMetaRepository formMetaRepository;	
	
	private static String UPLOAD_FOLDER =".";
	private static String UPLOAD_FOLDER_WIN = "C:\\Windows\\Temp\\";
	private static String UPLOAD_FOLDER_NIX = "/tmp/";	
	
	@PostConstruct
	public void initialize() {
	   if (th.go.doeb.ebis.util.OSValidator.isWindows())
	   {
		   UPLOAD_FOLDER = UPLOAD_FOLDER_WIN;
	   } else {
		   UPLOAD_FOLDER = UPLOAD_FOLDER_NIX;
	   }
	}
	
	@RequestMapping(value="/user")
	public String index()
	{
		return "redirect:/user/dashboard";
	}



	
	@RequestMapping(value="/user/logout")
	public String logout(HttpServletRequest request)
	{
		request.getSession().setAttribute("textValid", "");
		request.getSession().setAttribute("isUserLogon", "0");
		return "redirect:/";
	}	
	
	@RequestMapping(value="/user/dashboard")
	public String dashboard(HttpSession session, Model model)
	{
		String memberId = "0107550000220";//String.valueOf(session.getAttribute("memberId"));
		model.addAttribute("memberId", memberId);
		
		Company company = this.companyRepository.findByDbdId(memberId);
		
		if (company == null) {
			return "redirect:/user/detail";
		}
		model.addAttribute("company", company);
		return "dashboard/index";
	}	

	@RequestMapping(value="/user/detail")
	public String detail(HttpSession session, Model model)
	{
		String memberId = String.valueOf(session.getAttribute("memberId"));
		model.addAttribute("memberId", memberId);
		
		Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);
		model.addAttribute("lProvinceBean", lProvinceBean);
		
		Company company = this.companyRepository.findByDbdId(memberId);
		if (company != null)
		{
			model.addAttribute("company", company);
			return "dashboard/user_edit_detail";
		} else {
			return "dashboard/user_detail";	
		}
	}
	
	@RequestMapping(value="/user/saveCompany")
	public String saveCompany(@ModelAttribute Company company, HttpSession session)
	{
		this.companyRepository.save(company);
		return "redirect:/user/detail";
	}
	
	@RequestMapping(value="/user/test")
	@ResponseBody
	public String test(String a)
	{
		
		return a;
	}	

	@RequestMapping(value="/user/result")
	public String result(HttpSession session, Model model)
	{
		String memberId = "0107550000220";//String.valueOf(session.getAttribute("memberId"));
		model.addAttribute("memberId", memberId);
		
		Company company = this.companyRepository.findByDbdId(memberId);
		model.addAttribute("company", company);
		
		Iterable<FormEntry> iFormEntry = this.formEntryRepository.findAll();
		List<FormEntry> lFormEntry = new ArrayList<>();
		iFormEntry.forEach(lFormEntry::add);
		model.addAttribute("lFormEntry", lFormEntry);	
		
		return "user/status";
	}	

	@RequestMapping(value="/user/requestForm")
	public String requestForm(Model model)
	{
		String dbdId = "0107550000220";
		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);
		
		Iterable<RequestCategory> iRequestCategory = this.requestCategoryRepository.findAllFilter1();
		List<RequestCategory> lRequestCategory = new ArrayList<>();
		iRequestCategory.forEach(lRequestCategory::add);
		
		Iterable<Form> iForm = this.formRepository.findByFormActive(new BigDecimal(1));
		List<Form> lForm = new ArrayList<>();
		iForm.forEach(lForm::add);
		model.addAttribute("lRequestCategory", lRequestCategory);
		model.addAttribute("lForm", lForm);
		model.addAttribute("dbdId", dbdId);
		
		return "eservice/request_form";
	}
	
	@RequestMapping(value="/user/department")
	public String department(Model model)
	{
		String dbdId = "0107550000220";
		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);
		
		model.addAttribute("dbdId", dbdId);
		
		return "eservice/department";
	}		

	@RequestMapping(value="/user/form/submission")
	public String formSubmission(WebRequest wrequest, HttpSession session)
	{
		String out = "";
		Map<String, Object> mapParams = new HashMap<String, Object>();
		String memberId = "1";
		BigDecimal formId = new BigDecimal(wrequest.getParameter("formId"));
		Long requestId = Long.valueOf(wrequest.getParameter("requestId"));
		Long requestTypeId = Long.valueOf(wrequest.getParameter("requestTypeId"));

		String dbdId = "0107550000220";
		Company company = this.companyRepository.findByDbdId(dbdId);
		Request request = this.requestRepository.findByRequestId(requestId);		
		RequestType requestType = this.requestTypeRepository.findByRequestTypeId(requestTypeId);		
		RequestStatus requestStatus = this.requestStatusRepository.findByStatus("0");

		RequestTransaction requestTransaction = new RequestTransaction();
		requestTransaction.setCompany(company);
		requestTransaction.setRequest(this.requestRepository.findByRequestId(requestId));
		requestTransaction.setCreated(new Timestamp(System.currentTimeMillis()));
		requestTransaction.setRequestStatus(requestStatus);
		requestTransaction.setRequestType(requestType);
		requestTransaction.setActualGaranteeDay(request.getGauranteeDay());
		this.requestTransactionRepository.save(requestTransaction);
		requestTransaction = this.requestTransactionRepository.findByDbdIdAndRequestAndRequestStatus(dbdId, request.getRequestId(), requestStatus.getStatus());
		
		//RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(Long.valueOf(request.getParameter("rtId")));
		RequestForm requestForm = this.requestFormRepository.findByRfId(Long.valueOf(wrequest.getParameter("rfId")));
		
		Map<String, String[]> parameters = wrequest.getParameterMap();
		MultiValueMap<String, MultipartFile> uploadFiles = ((StandardMultipartHttpServletRequest) ((ServletWebRequest) wrequest).getRequest()).getMultiFileMap();
		
		for (Map.Entry<String, String[]> parameter : parameters.entrySet()) {
			String key = parameter.getKey();
			String[] value = parameter.getValue();
			
			this.manageFormMeta(key.trim(), formId);
			
			if (value.length == 1)
			{
				//out += "Item : " + key + " Count : " + value[0] + "<br/>\n";
				mapParams.put(key, value[0]);
			} else {
//				for (int i = 0; i < value.length; i++) {
//                    pw.println("<li>" + value[i].toString() + "</li><br>");
//                }
				//out += "Item : " + key + " Count : " + parameter.getValue() + "<br/>\n";				
			}

		}

		//out += "#######";
		
		for (MultiValueMap.Entry<String, List<MultipartFile>> uploadFile : uploadFiles.entrySet()) {

			List<MultipartFile> files = uploadFile.getValue();
			
            for (MultipartFile multipartFile : files) {
            	 
                String fileName = multipartFile.getOriginalFilename();
                String fileParamName = multipartFile.getName();
                String currentSaveFileName = "";

                this.manageFormMeta(fileParamName.trim(), formId);
                
                //start upload
                
                if (multipartFile.getSize() > 0)
                {
                	
		        	long millis = System.currentTimeMillis() ;
		            
		            String type = multipartFile.getContentType();
		            type = "." + type.substring(type.lastIndexOf("/") + 1);
		            
		            currentSaveFileName = millis + type;
                	
                    try {
    	                String userAndFormFolder = UPLOAD_FOLDER + memberId + "/" + formId + "/";
    	                File directory = new File(String.valueOf(userAndFormFolder));
    	
    	                if (!directory.exists()) {
    	                    directory.mkdirs();
    	                }
    		            
    		            Path path = Paths.get(userAndFormFolder + currentSaveFileName);
    		            byte[] bytes = multipartFile.getBytes();
    		            Files.write(path, bytes);
    		            
                    } catch (Exception e) {
                    	e.printStackTrace();
                    }                	
                }
                

                //end upload
                
                //out += "Param. : " + fileParamName;
                //out += "File : " + fileName + "<br/>\n";
                mapParams.put(fileParamName, currentSaveFileName);
            }

		}
		
		ObjectMapper mapper = new ObjectMapper();
		try {

			String json = mapper.writeValueAsString(mapParams);
			
			FormEntry formEntry = new FormEntry();
			//formEntry.setForm(this.formRepository.findByFormId(formId.longValue()));
//			formEntry.setRequestForm(requestForm);
//			formEntry.setMemberId(memberId);
			formEntry.setEntryText(json);
			formEntry.setCurrentState(BigDecimal.valueOf(Double.valueOf("1")));
			formEntry.setModified(new Timestamp(System.currentTimeMillis()));
			
			if (this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm) == null)
			{
				//insert
				// don't set ENTRY_ID for insert!!
			} else {
				//update
				// set ENTRY_ID for update
				formEntry.setEntryId(this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm).getEntryId());
			}
			this.formEntryRepository.save(formEntry);
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "user/process.html";
	}	

	private void manageFormMeta(String key, BigDecimal formId)
	{
		FormMeta fm = this.formMetaRepository.findByMetaNameAndFormId(key, formId);
		if (fm == null)
		{
			FormMeta newFormMeta = new FormMeta();
			newFormMeta.setFormId(formId);
			newFormMeta.setMetaLabel(key);
			newFormMeta.setMetaName(key);
			newFormMeta.setMetaOrder(0);
			newFormMeta.setMetaType("text");
			this.formMetaRepository.save(newFormMeta);
		}		
	}	
	
}

package th.go.doeb.ebis.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import th.go.doeb.ebis.entity.GovDocument;

@Controller
public class SuperofficeController {
	
	@RequestMapping(value="/superofficer/dashboard")
	public String dashboard(Model model)
	{
		/*Iterable<GovDocument> eForm = this.documentRepository.findAll();
		List<GovDocument> lGovDocument = new ArrayList<>();
		eForm.forEach(lGovDocument::add);
		model.addAttribute("lGovDocument", lGovDocument);*/
		
		return "superofficer/dashboard";
	}
	
	@RequestMapping(value="/superofficer/processing")
	public String processing(Model model)
	{
		/*Iterable<GovDocument> eForm = this.documentRepository.findAll();
		List<GovDocument> lGovDocument = new ArrayList<>();
		eForm.forEach(lGovDocument::add);
		model.addAttribute("lGovDocument", lGovDocument);*/
		
		return "superofficer/processing";
	}
	
	@RequestMapping(value="/superofficer/document_storage")
	public String documentstorage(Model model)
	{
		/*Iterable<GovDocument> eForm = this.documentRepository.findAll();
		List<GovDocument> lGovDocument = new ArrayList<>();
		eForm.forEach(lGovDocument::add);
		model.addAttribute("lGovDocument", lGovDocument);*/
		
		return "superofficer/document_storage";
	}

}

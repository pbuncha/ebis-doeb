package th.go.doeb.ebis.controller;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import th.go.doeb.ebis.bean.CompanyBean;
import th.go.doeb.ebis.config.ConvertDate;
import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.Department;
import th.go.doeb.ebis.entity.Employee;
import th.go.doeb.ebis.entity.FuelOperator;
import th.go.doeb.ebis.repository.CompanyNewRepository;
import th.go.doeb.ebis.repository.CompanyRepository;
import th.go.doeb.ebis.repository.FuelOperatorRepository;

@Controller
public class FuelOperatorController {
	@Autowired(required=true)
	private FuelOperatorRepository fuelOperatorRepository;	
	
	@Autowired(required=true)
	private CompanyNewRepository companyNewRepository;	
	
	@RequestMapping(value="/fueloperator/list")
	public String formList(Model model)
	{
		Iterable<FuelOperator> eFuelOperator = this.fuelOperatorRepository.findAll();
		List<FuelOperator> lFuelOperator = new ArrayList<>();
		eFuelOperator.forEach(lFuelOperator::add);
		model.addAttribute("lFuelOperator", lFuelOperator);
		
		return "fueloperator/list";
	}
	
	@RequestMapping(value="/fueloperator/add")
	public String formAdd(Model model)
	{
		Iterable<Company> eCompany = this.companyNewRepository.findAll();
		List<Company> lCompany = new ArrayList<>();
		eCompany.forEach(lCompany::add);
		model.addAttribute("lCompany", lCompany);
		
		return "fueloperator/add";
	}
	
	@RequestMapping(value="/fueloperator/save")
	//@ResponseBody
	public String formSave(@ModelAttribute FuelOperator fuelOperator,WebRequest request)
	{
		//foInitDateStr
		String foInitDateStr = request.getParameter("foInitDateStr");
		String foExpireDateStr = request.getParameter("foExpireDateStr");
		//String cvDate = ConvertDate.dateThai(foInitDateStr);
		//System.out.println(cvDate);
		//DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
		//DateFormat df = new SimpleDateFormat("dd-MMM-YY"); 
		//Date date = null;
		//try {
		//	date = df.parse(foInitDateStr);
		//} catch (ParseException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
		fuelOperator.setFoInitDate(ConvertDate.dateThai(foInitDateStr));
		fuelOperator.setFoExpireDate(ConvertDate.dateThai(foExpireDateStr));
		//System.out.println(fuelOperator.getFoInitDate());
		//DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
		//DateFormat df = new SimpleDateFormat("dd-MMM-dd"); 
		/*try {
			Date date = df.parse(foInitDateStr);
		    System.out.println("Full Date : " + date);
		    //sDateconvert = date.toString();
		    
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//System.out.println(foInitDateStr);
		
		
		//BigDecimal companyId = new BigDecimal(request.getParameter("company"));
		//System.out.println(companyId);
		//fuelOperator.setCompany(this.companyNewRepository.findByDbdId(companyId.toString()));
		
		//System.out.println(fuelOperator.getFoCardNo());
		//System.out.println(fuelOperator.getFoName());
		//System.out.println(fuelOperator.getFoSurname());
		//System.out.println(fuelOperator.getFoPersonalId());
		//System.out.println(fuelOperator.getCompany().getDbdId());
		//System.out.println(fuelOperator.getFoPersonalId());
		//System.out.println(fuelOperator.getFoExpireDate());
		//BigDecimal  = new BigDecimal(request.getParameter("company"));
		String companyId = request.getParameter("company");
		fuelOperator.setCompany(this.companyNewRepository.findByDbdId(companyId));
		this.fuelOperatorRepository.save(fuelOperator);
		return "redirect:/fueloperator/list";
	}
	
	@RequestMapping(value="/fueloperator/view/{foId}")
	public String formView(@PathVariable("foId") Long foId,Model model)
	{
		Iterable<Company> eCompany = this.companyNewRepository.findAll();
		List<Company> lCompany = new ArrayList<>();
		eCompany.forEach(lCompany::add);
		model.addAttribute("lCompany", lCompany);
		FuelOperator fuelOperator = this.fuelOperatorRepository.findByFoId(foId);
		
			model.addAttribute("fuelOperator", fuelOperator);
		return "fueloperator/view";
	}	
	
	@RequestMapping(value="/fueloperator/update")
	//@ResponseBody
	public String formUpdate(@ModelAttribute FuelOperator fuelOperator,WebRequest request)
	{
		//foInitDateStr
		String foInitDateStr = request.getParameter("foInitDateStr");
		String foExpireDateStr = request.getParameter("foExpireDateStr");
		//String cvDate = ConvertDate.dateThai(foInitDateStr);
		//System.out.println(cvDate);
		//DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
		//DateFormat df = new SimpleDateFormat("dd-MMM-YY"); 
		//Date date = null;
		//try {
		//	date = df.parse(foInitDateStr);
		//} catch (ParseException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
		fuelOperator.setFoInitDate(ConvertDate.dateThai(foInitDateStr));
		fuelOperator.setFoExpireDate(ConvertDate.dateThai(foExpireDateStr));
		//System.out.println(fuelOperator.getFoInitDate());
		//DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
		//DateFormat df = new SimpleDateFormat("dd-MMM-dd"); 
		/*try {
			Date date = df.parse(foInitDateStr);
		    System.out.println("Full Date : " + date);
		    //sDateconvert = date.toString();
		    
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//System.out.println(foInitDateStr);
		
		
		//BigDecimal companyId = new BigDecimal(request.getParameter("company"));
		//System.out.println(companyId);
		//fuelOperator.setCompany(this.companyNewRepository.findByDbdId(companyId.toString()));
		
		//System.out.println(fuelOperator.getFoCardNo());
		//System.out.println(fuelOperator.getFoName());
		//System.out.println(fuelOperator.getFoSurname());
		//System.out.println(fuelOperator.getFoPersonalId());
		//System.out.println(fuelOperator.getCompany().getDbdId());
		//System.out.println(fuelOperator.getFoPersonalId());
		//System.out.println(fuelOperator.getFoExpireDate());
		//BigDecimal  = new BigDecimal(request.getParameter("company"));
		String companyId = request.getParameter("company");
		fuelOperator.setCompany(this.companyNewRepository.findByDbdId(companyId));
		this.fuelOperatorRepository.save(fuelOperator);
		return "redirect:/fueloperator/list";
	}
}

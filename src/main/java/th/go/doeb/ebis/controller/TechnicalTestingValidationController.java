package th.go.doeb.ebis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import th.go.doeb.ebis.bean.BeTechnicalTestingValidation;
import th.go.doeb.ebis.repository.BeTechnicalTestingValidationRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
@RequestMapping("/TechnicalTestingValidation")
public class TechnicalTestingValidationController {
	 //Save the uploaded file to this folder
   // private static String UPLOADED_FOLDER = "..//..//uploads//";
    private static String UPLOADED_FOLDER = System.getProperty("user.dir")+"\\src\\main\\resources\\uploads\\TechnicalTestingValidation\\";
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    
	@Autowired(required=true)
	private BeTechnicalTestingValidationRepository BeTechnicalTestingValidationRepository;
	
	@RequestMapping(value="/AddTechnicalTestingValidation",method = RequestMethod.POST)
	//@ResponseBody
	public String AddTechnicalTestingValidation(@ModelAttribute("BeTechnicalTestingValidation") BeTechnicalTestingValidation BeTechnicalTestingValidation,
		      BindingResult result,@RequestParam("copy_registion") MultipartFile file,@RequestParam("study_history") MultipartFile file2,
		      @RequestParam("professional_license") MultipartFile file3,@RequestParam("work_history") MultipartFile file4,
		      @RequestParam("my_pic") MultipartFile file5,
		      RedirectAttributes redirectAttributes, ModelMap model) 
	{
		  log.info("info level log:"+UPLOADED_FOLDER);
		  //file copy registion
		if (file.isEmpty()) {
            //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            //return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
        	long millis = System.currentTimeMillis() ;
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file.getOriginalFilename());
            Files.write(path, bytes);
            BeTechnicalTestingValidation.setCopy_registion(millis +"_"+ file.getOriginalFilename());
            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }
        //file study_history
        if (file2.isEmpty()) {
            //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            //return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
        	long millis = System.currentTimeMillis() ;
            byte[] bytes = file2.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file2.getOriginalFilename());
            Files.write(path, bytes);
            BeTechnicalTestingValidation.setStudy_history(millis +"_"+ file2.getOriginalFilename());
            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file2.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }
        //file professional_license
        if (file3.isEmpty()) {
            //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            //return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
        	long millis = System.currentTimeMillis() ;
            byte[] bytes = file3.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file3.getOriginalFilename());
            Files.write(path, bytes);
            BeTechnicalTestingValidation.setProfessional_license(millis +"_"+ file3.getOriginalFilename());
            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file2.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }
        //file work_history
        if (file4.isEmpty()) {
            //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            //return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
        	long millis = System.currentTimeMillis() ;
            byte[] bytes = file4.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file4.getOriginalFilename());
            Files.write(path, bytes);
            BeTechnicalTestingValidation.setWork_history(millis +"_"+ file4.getOriginalFilename());
            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file2.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }
        //file my_pic
        if (file5.isEmpty()) {
            //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            //return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
        	long millis = System.currentTimeMillis() ;
            byte[] bytes = file5.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file5.getOriginalFilename());
            Files.write(path, bytes);
            BeTechnicalTestingValidation.setMy_pic(millis +"_"+ file5.getOriginalFilename());
            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file2.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	Date date = new Date();
    	//System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
    	BeTechnicalTestingValidation.setCreatedate(dateFormat.format(date));
		this.BeTechnicalTestingValidationRepository.save(BeTechnicalTestingValidation);
		//return "Success";
		return "redirect:/FormTP2";
	}
}

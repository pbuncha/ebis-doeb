package th.go.doeb.ebis.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import th.go.doeb.ebis.bean.ProvinceBean;
import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.Engineer;
import th.go.doeb.ebis.entity.Form;
import th.go.doeb.ebis.entity.FormEntry;
import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.entity.RequestStatus;
import th.go.doeb.ebis.entity.RequestTransaction;
import th.go.doeb.ebis.entity.TitleName;
import th.go.doeb.ebis.repository.CompanyNewRepository;
import th.go.doeb.ebis.repository.EngineerRepository;
import th.go.doeb.ebis.repository.FormEntryRepository;
import th.go.doeb.ebis.repository.FormRepository;
import th.go.doeb.ebis.repository.ProvinceRepository;
import th.go.doeb.ebis.repository.RequestFormRepository;
import th.go.doeb.ebis.repository.RequestRepository;
import th.go.doeb.ebis.repository.RequestStatusRepository;
import th.go.doeb.ebis.repository.RequestTransactionRepository;
import th.go.doeb.ebis.repository.TitleNameRepository;

@Controller
public class DirectorController {
	@Autowired(required=true)
	private RequestTransactionRepository requestTransactionRepository;
	
	@Autowired(required=true)
	private RequestStatusRepository requestStatusRepository;
	
	@Autowired(required=true)
	private RequestRepository requestRepository;	

	@Autowired(required=true)
	private RequestFormRepository requestFormRepository;
	
	@Autowired(required=true)
	private ProvinceRepository provinceRepository;

	@Autowired(required=true)
	private CompanyNewRepository companyRepository;	

	@Autowired(required=true)
	private FormRepository formRepository;	

	@Autowired(required=true)
	private FormEntryRepository formEntryRepository;

	@Autowired(required=true)
	private EngineerRepository engineerRepository;
	
	@Autowired(required=true)
	private TitleNameRepository titleNameRepository;	
	
	@RequestMapping(value="/director")
	public String index(HttpSession session)
	{
		if (session.getAttribute("isUserLogon") == null)
		{
			if(session.getAttribute("isPosition") != "1" || session.getAttribute("isPosition") != "2" || session.getAttribute("isPosition") != "3"  ) {
				return "redirect:/index";
			}
				
		}
		//return "admin/index";
		return "redirect:/director/dashboard";
	}
	
	@RequestMapping(value="/director/dashboard")
	public String dashboard(Model model,HttpSession session,HttpServletRequest requestsession
)
	{
		if (session.getAttribute("isUserLogon") == null)
		{
			if(session.getAttribute("isPosition") != "1" || session.getAttribute("isPosition") != "2" || session.getAttribute("isPosition") != "3"  ) {
				return "redirect:/index";
			}
				
		}
	/*	Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllStatusallNew();
		List<RequestTransaction> lRequestTransaction = new ArrayList<>();
		iRequestTransaction.forEach(lRequestTransaction::add);
		model.addAttribute("lRequestTransaction", lRequestTransaction);
		model.addAttribute("CountlRequestTransaction", lRequestTransaction.size());
		
		Iterable<RequestTransaction> iRequestTransaction2 = this.requestTransactionRepository.findAllStatusallEdit();
		List<RequestTransaction> lRequestTransaction2 = new ArrayList<>();
		iRequestTransaction2.forEach(lRequestTransaction2::add);
		model.addAttribute("lRequestTransaction2", lRequestTransaction2);
		model.addAttribute("CountlRequestTransaction2", lRequestTransaction2.size());
		
		
		Iterable<RequestTransaction> iRequestTransaction3 = this.requestTransactionRepository.findAllStatusallChange();
		List<RequestTransaction> lRequestTransaction3 = new ArrayList<>();
		iRequestTransaction3.forEach(lRequestTransaction3::add);
		model.addAttribute("lRequestTransaction3", lRequestTransaction3);
		model.addAttribute("CountlRequestTransaction3", lRequestTransaction3.size());
		model.addAttribute("Sum1", lRequestTransaction.size()+lRequestTransaction2.size()+lRequestTransaction3.size()); */
		
		Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> lRequestTransaction = new ArrayList<>();
		iRequestTransaction.forEach(lRequestTransaction::add);
		model.addAttribute("lRequestTransaction", lRequestTransaction);
		model.addAttribute("CountlRequestTransaction", lRequestTransaction.size());
		
		Iterable<RequestTransaction> iRequestTransaction2 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> lRequestTransaction2 = new ArrayList<>();
		iRequestTransaction2.forEach(lRequestTransaction2::add);
		model.addAttribute("lRequestTransaction2", lRequestTransaction2);
		model.addAttribute("CountlRequestTransaction2", lRequestTransaction2.size());
		
		
		Iterable<RequestTransaction> iRequestTransaction3 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> lRequestTransaction3 = new ArrayList<>();
		iRequestTransaction3.forEach(lRequestTransaction3::add);
		model.addAttribute("lRequestTransaction3", lRequestTransaction3);
		model.addAttribute("CountlRequestTransaction3", lRequestTransaction3.size());
		model.addAttribute("Sum1", lRequestTransaction.size()+lRequestTransaction2.size()+lRequestTransaction3.size());
		
		Iterable<RequestTransaction> iRequestTransaction_2 = this.requestTransactionRepository.findAllStatus206New();
		List<RequestTransaction> lRequestTransaction_2 = new ArrayList<>();
		iRequestTransaction_2.forEach(lRequestTransaction_2::add);
		
		Iterable<RequestTransaction> iRequestTransaction2_2 = this.requestTransactionRepository.findAllStatus206Edit();
		List<RequestTransaction> lRequestTransaction2_2 = new ArrayList<>();
		iRequestTransaction2_2.forEach(lRequestTransaction2_2::add);
		
		Iterable<RequestTransaction> iRequestTransaction3_2 = this.requestTransactionRepository.findAllStatus206Change();
		List<RequestTransaction> lRequestTransaction3_2 = new ArrayList<>();
		iRequestTransaction3_2.forEach(lRequestTransaction3_2::add);
		model.addAttribute("Sum2", lRequestTransaction_2.size()+lRequestTransaction2_2.size()+lRequestTransaction3_2.size());
		
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
		model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
		return "director/dashboard";
	}

	@RequestMapping(value="/director/sign")
	public String sign(Model model,HttpSession session,HttpServletRequest requestsession)
	{
		if (session.getAttribute("isUserLogon") == null)
		{
			if(session.getAttribute("isPosition") != "1" || session.getAttribute("isPosition") != "2" || session.getAttribute("isPosition") != "3"  ) {
				return "redirect:/index";
			}
				
		}
		Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllStatus206New();
		List<RequestTransaction> lRequestTransaction = new ArrayList<>();
		iRequestTransaction.forEach(lRequestTransaction::add);
		model.addAttribute("lRequestTransaction", lRequestTransaction);
		model.addAttribute("CountlRequestTransaction", lRequestTransaction.size());
		
		Iterable<RequestTransaction> iRequestTransaction2 = this.requestTransactionRepository.findAllStatus206Edit();
		List<RequestTransaction> lRequestTransaction2 = new ArrayList<>();
		iRequestTransaction2.forEach(lRequestTransaction2::add);
		model.addAttribute("lRequestTransaction2", lRequestTransaction2);
		model.addAttribute("CountlRequestTransaction2", lRequestTransaction2.size());
		
		Iterable<RequestTransaction> iRequestTransaction3 = this.requestTransactionRepository.findAllStatus206Change();
		List<RequestTransaction> lRequestTransaction3 = new ArrayList<>();
		iRequestTransaction3.forEach(lRequestTransaction3::add);
		model.addAttribute("lRequestTransaction3", lRequestTransaction3);
		model.addAttribute("CountlRequestTransaction3", lRequestTransaction3.size());
		
		model.addAttribute("Sum2", lRequestTransaction.size()+lRequestTransaction2.size()+lRequestTransaction3.size());
		
		Iterable<RequestTransaction> iRequestTransaction_2 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> lRequestTransaction_2 = new ArrayList<>();
		iRequestTransaction_2.forEach(lRequestTransaction_2::add);
		
		Iterable<RequestTransaction> iRequestTransaction2_2 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> lRequestTransaction2_2 = new ArrayList<>();
		iRequestTransaction2_2.forEach(lRequestTransaction2_2::add);
		
		
		Iterable<RequestTransaction> iRequestTransaction3_2 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> lRequestTransaction3_2 = new ArrayList<>();
		iRequestTransaction3_2.forEach(lRequestTransaction3_2::add);
		model.addAttribute("Sum1", lRequestTransaction_2.size()+lRequestTransaction2_2.size()+lRequestTransaction3_2.size());
		
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
		model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
		model.addAttribute("isPosition", session.getAttribute("isPosition"));
		return "director/sign";
	}
	
	@RequestMapping(value="/director/sign-transaction/{rtId}")
	public String signTransaction(@PathVariable("rtId") Long rtId, Model model,HttpSession session)
	{
		
		if (session.getAttribute("isUserLogon") == null)
		{
			if(session.getAttribute("isPosition") != "1" || session.getAttribute("isPosition") != "2" || session.getAttribute("isPosition") != "3"  ) {
				return "redirect:/index";
			}
				
		}
		System.out.println("rtId:"+rtId);
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("207"));
		requestTransactionRepository.save(requestTransaction);
		return "redirect:/director/sign";
	}
	
	@RequestMapping(value="/director/sign-transaction2/{rtId}")
	public String signTransaction2(@PathVariable("rtId") Long rtId, Model model,HttpSession session)
	{
		if (session.getAttribute("isUserLogon") == null)
		{
			if(session.getAttribute("isPosition") != "1" || session.getAttribute("isPosition") != "2" || session.getAttribute("isPosition") != "3"  ) {
				return "redirect:/index";
			}
				
		}
		System.out.println("rtId:"+rtId);
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("208"));
		requestTransactionRepository.save(requestTransaction);
		return "redirect:/director/sign";
	}
	
	@RequestMapping(value="/director/sign-transaction3/{rtId}")
	public String signTransaction3(@PathVariable("rtId") Long rtId, Model model,HttpSession session)
	{
		if (session.getAttribute("isUserLogon") == null)
		{
			if(session.getAttribute("isPosition") != "1" || session.getAttribute("isPosition") != "2" || session.getAttribute("isPosition") != "3"  ) {
				return "redirect:/index";
			}
				
		}
		System.out.println("rtId:"+rtId);
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("303"));
		requestTransactionRepository.save(requestTransaction);
		return "redirect:/director/sign";
	}
	
	@RequestMapping(value="/director/deny-transaction/{rtId}")
	public String denyTransaction(@PathVariable("rtId") Long rtId, Model model,HttpSession session)
	{
		if (session.getAttribute("isUserLogon") == null)
		{
			if(session.getAttribute("isPosition") != "1" || session.getAttribute("isPosition") != "2" || session.getAttribute("isPosition") != "3"  ) {
				return "redirect:/index";
			}
				
		}
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("205"));
		requestTransactionRepository.save(requestTransaction);
		return "redirect:/director/sign";
	}

	@RequestMapping(value="/director/company/{dbdId}/formgroup/view/{requestId}/{requestTransactionId}")
	public String formgroupView(@PathVariable("dbdId") String dbdId, @PathVariable("requestId") Long requestId, @PathVariable("requestTransactionId") Long requestTransactionId, Model model  ,HttpServletRequest requestsession)
	{
		//Form f = this.formRepository.findByFormId(formId);
		RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(requestTransactionId);
		
		Request request = this.requestRepository.findByRequestId(requestId);
		Iterable<RequestForm> iRequestForm = this.requestFormRepository.findByRequestAndTypeId(request,requestTransaction.getRequestType().getRequestTypeId());
		List<RequestForm> lRequestForm = new ArrayList<>();
		iRequestForm.forEach(lRequestForm::add);
		
        Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);

		model.addAttribute("request", request);
		model.addAttribute("lRequestForm", lRequestForm);
		model.addAttribute("lProvinceBean", lProvinceBean);
		model.addAttribute("dbdId", dbdId);
		//model.addAttribute("formId", formId);
		
		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);
		
		RequestStatus requestStatus = this.requestStatusRepository.findByStatus("0");
		//RequestTransaction requestTransaction = this.requestTransactionRepository.findByDbdIdAndRequestAndRequestStatus(dbdId, request.getRequestId(), requestStatus.getStatus());
		//if (requestTransaction == null)
		//{
		//	requestTransaction = new RequestTransaction();
			//requestTransaction.setCompany(company);
		//	requestTransaction.setRequest(this.requestRepository.findByRequestId(requestId));
		//	requestTransaction.setCreated(new Timestamp(System.currentTimeMillis()));
		//	requestTransaction.setRequestStatus(requestStatus);
		//	this.requestTransactionRepository.save(requestTransaction);
		//	requestTransaction = this.requestTransactionRepository.findByDbdIdAndRequestAndRequestStatus(dbdId, request.getRequestId(), requestStatus.getStatus());
		//}
		model.addAttribute("rtId", requestTransaction.getRtId());
		model.addAttribute("requestTypeId", requestTransaction.getRequestType().getRequestTypeId());
		model.addAttribute("requestTransactionId", requestTransactionId);

model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
		return "director/formgroup";
	}	
	
	@RequestMapping(value="/director/company/{dbdId}/{rtId}/form/review/{formId}/{requestTypeId}")
	public String formView(@PathVariable("dbdId") String dbdId, @PathVariable("rtId") long rtId, @PathVariable("formId") Long formId, @PathVariable("requestTypeId") String requestTypeId, Model model,HttpServletRequest requestsession)
	{
		Iterable<TitleName> eTitleName = this.titleNameRepository.findTitleNameActive();
		List<TitleName> lTitleName = new ArrayList<>();
		eTitleName.forEach(lTitleName::add);
		model.addAttribute("lTitleName", lTitleName);
		
		RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(rtId);
		Request r = requestTransaction.getRequest();
		Form f = this.formRepository.findByFormId(formId);
		RequestForm requestForm = this.requestFormRepository.findByRequestAndFormAadTypeId(r, f, requestTransaction.getRequestType().getRequestTypeId());
		long rfId = requestForm.getRfId();
				
		FormEntry formEntry = this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm);		
		if (formEntry != null)
		{
			ObjectMapper objectMapper = new ObjectMapper();
			Map<String,String> entryMap = new HashMap<String, String>();
			try {
				entryMap = objectMapper.readValue(formEntry.getEntryText(), new TypeReference<HashMap<String,String>>() {});
				model.addAttribute("entryMap", entryMap);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		
        Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);
		model.addAttribute("lProvinceBean", lProvinceBean);
		model.addAttribute("dbdId", dbdId);
		model.addAttribute("rtId", rtId);
		model.addAttribute("formId", formId);
		model.addAttribute("rfId", rfId);
		
		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);
		
		Iterable<Engineer> eForm = this.engineerRepository.findAllbyDbdId(dbdId);
		List<Engineer> lEngineer = new ArrayList<>();
		eForm.forEach(lEngineer::add);
		model.addAttribute("lEngineer", lEngineer);
		System.out.println(f.getFormTemplate());
		String requestTypeName = "";
		if (requestTypeId.equalsIgnoreCase("1"))
		{
			requestTypeName = "ขอใหม่";
		} else if (requestTypeId.equalsIgnoreCase("2"))
		{
			requestTypeName = "ขอต่ออายุ";
		} else if (requestTypeId.equalsIgnoreCase("3"))
		{
			requestTypeName = "ขอแก้ไขเปลี่ยนแปลง";
		}
		Long requestId = r.getRequestId();
		model.addAttribute("requestId", requestId);
		String requestName = r.getRequestName();
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
		model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
		model.addAttribute("requestTypeId", requestTypeId);
		model.addAttribute("requestTypeName", requestTypeName);
		model.addAttribute("requestName", requestName);
		return "director/form/" + f.getFormTemplate();
	}

	@RequestMapping(value="/director/company/{dbdId}/{rtId}/form/review/{formId}/{requestTypeId}/{engiId}")
	public String formView2(HttpServletRequest requestsession,@PathVariable("engiId") String engiId,@PathVariable("dbdId") String dbdId, @PathVariable("rtId") long rtId, @PathVariable("formId") Long formId, @PathVariable("requestTypeId") String requestTypeId, Model model)
	{
		Iterable<TitleName> eTitleName = this.titleNameRepository.findTitleNameActive();

		List<Engineer> lEngineer = new ArrayList<>();
		long ideng = Long.valueOf(engiId);
		System.out.println("engiId : "+engiId);

		for(Engineer engineer : this.engineerRepository.findAllbyDbdId(dbdId)){
			if(engineer.getDeleted() == 0 && ideng == engineer.getId()){
				Engineer e2 = new Engineer();
				e2.setId(engineer.getId());
				e2.setFirstname(engineer.getFirstname());
				e2.setLastname(engineer.getLastname());
				e2.setTitleName(engineer.getTitleName());
				e2.setTiTleEnGiNeer(engineer.getTiTleEnGiNeer());
				e2.setType(engineer.getType());
				e2.setFaculty(engineer.getFaculty());
				e2.setNumhome_service(engineer.getNumhome_service());
				e2.setMoo_service(engineer.getMoo_service());
				e2.setSoi_service(engineer.getSoi_service());
				e2.setRoad_service(engineer.getRoad_service());
				e2.setTumbon_service(engineer.getTumbon_service());
				e2.setProvince_service(engineer.getProvince_service());
				e2.setZipcode_service(engineer.getZipcode_service());
				e2.setTel_service(engineer.getTel_service());
				e2.setEmail_service(engineer.getEmail_service());

				lEngineer.add(e2);

				model.addAttribute("id", engineer.getId());
				model.addAttribute("firstname", engineer.getFirstname());
				model.addAttribute("lastname", engineer.getLastname());
				model.addAttribute("titlename", engineer.getTitleName());
				model.addAttribute("stitlename", engineer.getTiTleEnGiNeer());
				model.addAttribute("type", engineer.getType());
				model.addAttribute("faculty", engineer.getFaculty());
				model.addAttribute("numhome", engineer.getNumhome_service());
				model.addAttribute("moo", engineer.getMoo_service());
				model.addAttribute("soi", engineer.getSoi_service());
				model.addAttribute("road", engineer.getRoad_service());
				model.addAttribute("tumbon", engineer.getTumbon_service());
				model.addAttribute("province", engineer.getProvince_service());
				model.addAttribute("zipcode", engineer.getZipcode_service());
				model.addAttribute("tel", engineer.getTel_service());
				model.addAttribute("mail", engineer.getEmail_service());

				System.out.println("id : "+engineer.getId());
				System.out.println("Firstname : "+engineer.getFirstname());
				System.out.println("Lastname : "+engineer.getLastname());	
				System.out.println("STITLE : "+engineer.getTiTleEnGiNeer());	
				System.out.println("province : "+engineer.getProvince_service());	
				System.out.println(e2.getTiTleEnGiNeer());
			}
		}
		
		model.addAttribute("lEngineer", lEngineer);
		model.addAttribute("engiId", engiId);

		// Engineer aDataEngi = this.engineerRepository.findAllbyID(engiId);

		// List<Engineer> aEngi= new ArrayList<>();

		// Engineer e2 = new Engineer();
		// e2.setId(aDataEngi.getId());
		// e2.setFirstname(aDataEngi.getFirstname());
		// e2.setLastname(aDataEngi.getLastname());
		// System.out.println("Firstname : "+aDataEngi.getFirstname());
		// System.out.println("Lastname : "+aDataEngi.getLastname());	

		// aEngi.add(e2);

		
		// aDataEngi.forEach(aEngi::add);
		// model.addAttribute("aEngi", aEngi);

		// System.out.println(aEngi);
		
		List<TitleName> lTitleName = new ArrayList<>();
		eTitleName.forEach(lTitleName::add);
		model.addAttribute("lTitleName", lTitleName);

		RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(rtId);
		// System.out.println("Testttt!!!");
		//System.out.println(requestTransaction.getRequest().getRequestId());
		Request r = requestTransaction.getRequest();
		Form f = this.formRepository.findByFormId(formId);
		RequestForm requestForm = this.requestFormRepository.findByRequestAndFormAadTypeId(r, f, requestTransaction.getRequestType().getRequestTypeId());
		// System.out.println("rId: "+r.getRequestId()+" | fId:  "+ f.getFormId() +" | getRequestTypeId: "+requestTransaction.getRequestType().getRequestTypeId());
		// System.out.println("Comment 3");
		//long rfId = requestForm.getRfId();

		// FormEntry formEntry = this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm);
		FormEntry formEntry = this.formEntryRepository.findByRequestTransactionAndRequestFormAndIdEngineer(requestTransaction, requestForm, engiId);
		//System.out.println("formEntry: "+formEntry.getEntryId());
		model.addAttribute("formMetaId", "0");
		if (formEntry != null)
		{
			model.addAttribute("formMetaId", formEntry.getEntryId());
			ObjectMapper objectMapper = new ObjectMapper();
			Map<String,String> entryMap = new HashMap<String, String>();
			try {
				entryMap = objectMapper.readValue(formEntry.getEntryText(), new TypeReference<HashMap<String,String>>() {});
				model.addAttribute("entryMap", entryMap);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				// System.out.println("Error1!!");
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				// System.out.println("Error2!!");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// System.out.println("Error3!!");
				e.printStackTrace();
			}
		}

      Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);
		model.addAttribute("lProvinceBean", lProvinceBean);
		model.addAttribute("dbdId", dbdId);
		model.addAttribute("rtId", rtId);
		model.addAttribute("formId", formId);
		model.addAttribute("rfId", requestForm.getRfId());
		String requestTypeName = "";
		if (requestTypeId.equalsIgnoreCase("1"))
		{
			requestTypeName = "ขอใหม่";
		} else if (requestTypeId.equalsIgnoreCase("2"))
		{
			requestTypeName = "ขอต่ออายุ";
		} else if (requestTypeId.equalsIgnoreCase("3"))
		{
			requestTypeName = "ขอแก้ไขเปลี่ยนแปลง";
		}

		String requestName = r.getRequestName();
		Long requestId = r.getRequestId();
		model.addAttribute("requestTypeId", requestTypeId);
		model.addAttribute("requestTypeName", requestTypeName);
		model.addAttribute("requestId", requestId);
		model.addAttribute("requestName", requestName);
		model.addAttribute("controllerPosition", "officerController");
		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);

		Iterable<Engineer> eForm = this.engineerRepository.findAllbyDbdId(dbdId);
		// List<Engineer> lEngineer = new ArrayList<>();
		// eForm.forEach(lEngineer::add);
		// model.addAttribute("lEngineer", lEngineer);
		// System.out.println(">>>"+f.getFormTemplate()+"<<<<");
		// System.out.println("utukjg"+requestsession.getSession().getAttribute("Position"));
		// System.out.println("utukjg22"+requestsession.getSession().getAttribute("nameMember"));
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
		model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
		model.addAttribute("role",requestsession.getSession().getAttribute("role"));
		//model.addAttribute("lRequestTransaction", lRequestTransaction);
		return "director/form/" + f.getFormTemplate();
	}

	@RequestMapping(value="/director/Detail/{dbdId}/{requestTransactionId}/{formId}/{requestTypeId}")
	public String formgroupViewDetail2(Model model,@ModelAttribute Engineer Engineer, @PathVariable("requestTypeId") String requestTypeId, @PathVariable("formId") String formId, @PathVariable("dbdId") String dbdId,@PathVariable("requestTransactionId") String requestTransactionId)
	{
		List<Engineer> lEngineer = new ArrayList<>();
		
		for(Engineer engineer : this.engineerRepository.findAllbyDbdId(dbdId)){
			if(engineer.getDeleted() == 0){
				Engineer e2 = new Engineer();
				e2.setId(engineer.getId());
				e2.setFirstname(engineer.getFirstname());
				e2.setLastname(engineer.getLastname());
				System.out.println("id : "+engineer.getId());
				System.out.println("Firstname : "+engineer.getFirstname());
				System.out.println("Lastname : "+engineer.getLastname());	

				lEngineer.add(e2);
			}
		}
		
		model.addAttribute("lEngineer", lEngineer);
		model.addAttribute("requestTypeId", requestTypeId);
		model.addAttribute("formId", formId);
		model.addAttribute("dbdId", dbdId);
		model.addAttribute("requestTransactionId", requestTransactionId);


		return "director/formgroupDetail";
	}
	
}

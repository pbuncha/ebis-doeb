package th.go.doeb.ebis.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.Department;
import th.go.doeb.ebis.entity.Employee;
import th.go.doeb.ebis.entity.Engineer;
import th.go.doeb.ebis.entity.GovDocument;
import th.go.doeb.ebis.entity.GovDocumentType;
import th.go.doeb.ebis.entity.TitleName;
import th.go.doeb.ebis.repository.CompanyNewRepository;
import th.go.doeb.ebis.repository.EngineerRepository;
import th.go.doeb.ebis.repository.TitleNameRepository;


//-------------Sornram----------------
import java.util.Map;
import th.go.doeb.ebis.entity.FormEntry;
import java.util.HashMap;
import com.fasterxml.jackson.databind.ObjectMapper;
import th.go.doeb.ebis.repository.FormMetaRepository;
import th.go.doeb.ebis.entity.FormMeta;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.ui.ModelMap;
//------------------------------------

@Controller
public class EngineerControlle {
	@Autowired(required=true)
	private EngineerRepository engineerRepository;

	@Autowired(required=true)
	private CompanyNewRepository companyNewRepository;

	@Autowired(required=true)
	private TitleNameRepository titleNameRepository;

	//-------------Sornram----------------
	@Autowired(required=true)
	private FormMetaRepository formMetaRepository;
	//------------------------------------


	private static String  workingDir = System.getProperty("user.dir");
	private static String UPLOAD_FOLDER = workingDir+"\\src\\data\\test\\uploads\\tool\\";
	//private static String UPLOAD_FOLDER_DOC = workingDir+"\\src\\data\\doc\\";
	private static String UPLOAD_FOLDER_DOC_WIN = "C:\\Windows\\Temp\\";
	private static String UPLOAD_FOLDER_DOC_NIX = "/tmp/";
	private static String UPLOAD_FOLDER_DOC = "";

	@PostConstruct
	public void initialize() {
	   if (th.go.doeb.ebis.util.OSValidator.isWindows())
	   {
		   UPLOAD_FOLDER_DOC = UPLOAD_FOLDER_DOC_WIN;
		   UPLOAD_FOLDER= UPLOAD_FOLDER_DOC_WIN;
	   } else {
		   UPLOAD_FOLDER_DOC = UPLOAD_FOLDER_DOC_NIX;
		   UPLOAD_FOLDER= UPLOAD_FOLDER_DOC_WIN;
	   }
	}

	@RequestMapping(value="/engineer/list")
	public String formList(Model model)
	{
		Iterable<Engineer> eForm = this.engineerRepository.findAll();
		List<Engineer> lEngineer = new ArrayList<>();
		eForm.forEach(lEngineer::add);
		model.addAttribute("lEngineer", lEngineer);

		return "engineer/list";
	}

	@RequestMapping(value="/engineer/add")
	public String formAdd(Model model)
	{
		 Iterable<Company> iCompany = this.companyNewRepository.findAll();
			List<Company> lCompany = new ArrayList<>();
			iCompany.forEach(lCompany::add);
			model.addAttribute("lCompany", lCompany);
		return "engineer/add";
	}

	@RequestMapping(value="/engineer/view/{engineerId}")
	public String formView(@PathVariable("engineerId") Long engineerId,Model model)
	{
		 Iterable<Company> iCompany = this.companyNewRepository.findAll();
			List<Company> lCompany = new ArrayList<>();
			iCompany.forEach(lCompany::add);
			Engineer Engineer = this.engineerRepository.findByengineerId(engineerId);
			model.addAttribute("Engineer", Engineer);
			model.addAttribute("lCompany", lCompany);

		return "engineer/view";
	}

	@RequestMapping(value="/engineer/update")
	//@ResponseBody
	public String formUpdate(@ModelAttribute Engineer Engineer,WebRequest request)
	{

		String dbdId = request.getParameter("dbdId");
		Engineer.setCompany(this.companyNewRepository.findByDbdId(dbdId));
		//System.out.println("Test Area");
		//System.out.println(Engineer.getCompany());
		this.engineerRepository.save(Engineer);
		return "redirect:/engineer/list";

		//System.out.println(departmentId);
		//return "AA";
		//return Employee.getDepartment().toString();
	}

	@RequestMapping(value="/engineer/save")
	//@ResponseBody
	public String formSave(@ModelAttribute Engineer Engineer,WebRequest request)
	{


		String dbdId = request.getParameter("dbdId");
		Engineer.setCompany(this.companyNewRepository.findByDbdId(dbdId.toString()));
		this.engineerRepository.save(Engineer);
		return "redirect:/engineer/list";
		//System.out.println(departmentId);
		//return "AA";
		//return Employee.getDepartment().toString();
	}

	@RequestMapping(value="/api-engineer/save")
	@ResponseBody
	public String apiFormSave(@ModelAttribute Engineer Engineer,WebRequest request)
	{


		String dbdId = request.getParameter("dbdId");
	//	MultipartFile file_1 = request.getParameter("file_1");
		//String title = request.getParameter("title");
		Engineer.setEngineerId(request.getParameter("engineerId"));
		System.out.println("title: "+request.getParameter("title"));
		MultiValueMap<String, MultipartFile> uploadFiles = ((StandardMultipartHttpServletRequest) ((ServletWebRequest) request).getRequest()).getMultiFileMap();
		for (MultiValueMap.Entry<String, List<MultipartFile>> uploadFile : uploadFiles.entrySet()) {

			List<MultipartFile> files = uploadFile.getValue();

            for (MultipartFile multipartFile : files) {

                String fileName = multipartFile.getOriginalFilename();
                String fileParamName = multipartFile.getName();
                String currentSaveFileName = "";

               // this.manageFormMeta(fileParamName.trim(), formId);

                //start upload

                if (multipartFile.getSize() > 0)
                {
                	System.out.println("System uploading...");
		        	long millis = System.currentTimeMillis() ;

		            String type = multipartFile.getContentType();
		            type = "." + type.substring(type.lastIndexOf("/") + 1);

		            currentSaveFileName = request.getParameter("firstname")+"_"+request.getParameter("lastname")+"_"+millis + type;

                    try {
                    	String tmpFormId = "12";
    	                String userAndFormFolder = UPLOAD_FOLDER + dbdId + "/engineer/";
    	                File directory = new File(String.valueOf(userAndFormFolder));

    	                if (!directory.exists()) {
    	                    directory.mkdirs();
    	                }

    		            Path path = Paths.get(userAndFormFolder + currentSaveFileName);
    		            byte[] bytes = multipartFile.getBytes();
    		            Files.write(path, bytes);
    		            System.out.println("Uploaded success");
    		            System.out.println(userAndFormFolder);
                    } catch (Exception e) {
                    	System.out.println("Uploaded error");
                    	e.printStackTrace();
                    }
                }


                //end upload

                //out += "Param. : " + fileParamName;
                //out += "File : " + fileName + "<br/>\n";
                System.out.println("Param. : " + fileParamName);
                System.out.println("File : " + fileName);

                switch(fileParamName) {
                   case "file_1":
                	   Engineer.setCopyId(currentSaveFileName);
			           break;
                   case "file_2":
                	   Engineer.setCopyPassport(currentSaveFileName);
			           break;
                   case "file_3":
                	   Engineer.setCopyRegistration(currentSaveFileName);
			           break;
                   case "file_4":
                	   Engineer.setCopyLicensedEngineering(currentSaveFileName);
			           break;
                   case "file_5":
                	   Engineer.setCopyCertificate1(currentSaveFileName);
			           break;
                   case "file_6":
                	   Engineer.setCopyCertificate2(currentSaveFileName);
			           break;
                   case "file_7":
                	   Engineer.setCopyCertificate3(currentSaveFileName);
			           break;
			       default:
			    	   //monthString = "Invalid month";
			           break;

                }


            }

		}
		System.out.println("rtId : " + request.getParameter("rtId"));
		Engineer.setTitleName(this.titleNameRepository.findTitleNameByCode(request.getParameter("title")));
		Engineer.setFirstname(request.getParameter("firstname"));
		Engineer.setLastname(request.getParameter("lastname"));
		Engineer.setType(request.getParameter("e-engineerType"));
		Engineer.setFaculty(request.getParameter("e-engineerFaculty"));
		Engineer.setRTID(request.getParameter("rtId"));
		Engineer.setIssueDate(request.getParameter("sDatePermit"));
		Engineer.setExpireDate(request.getParameter("sDatePermitExpire"));
		Engineer.setPersonalId(request.getParameter("e-idPerson").replace("-", ""));
		Engineer.setAddress(request.getParameter("e-address"));
		Engineer.setCompany(this.companyNewRepository.findByDbdId(dbdId.toString()));
		this.engineerRepository.save(Engineer);
		//return "redirect:/engineer/list";
		//System.out.println(departmentId);
		return "ok";
		//return Employee.getDepartment().toString();
	}

	@RequestMapping(value="/api-engineer/update")
	@ResponseBody
	public int apiFormUpdate(@ModelAttribute Engineer Engineer,WebRequest request)
	{


		String dbdId = request.getParameter("dbdId");
		Long engineerIdOriginal = Long.valueOf(request.getParameter("engineerIdOriginal"));
		String engineerId = request.getParameter("engineerId");
		String title = request.getParameter("title");
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String faculty = request.getParameter("faculty");
		String type = request.getParameter("type");
		/*
		engineerId:$('#edit-engineerId').val(),
		title:$('#edit-title').val(),
		firstname:$('#edit-firstname').val(),
		lastname:$('#edit-lastname').val(),
		type:$('#edit-type').val(),*/

		//return "redirect:/engineer/list";
		//System.out.println(departmentId);
		TitleName titleName = this.titleNameRepository.findTitleNameByCode(title);
		return this.engineerRepository.updateEngineerCustom(engineerId,titleName,firstname,lastname,type,faculty,engineerIdOriginal);
		//return Employee.getDepartment().toString();
	}
	@RequestMapping(value="/api-engineer/delete")
	@ResponseBody
	public int apiFormDelete(@ModelAttribute Engineer Engineer,WebRequest request)
	{


		Long id = Long.valueOf(request.getParameter("id"));

		return this.engineerRepository.deleteEngineerCustom(id);
		//return Employee.getDepartment().toString();
	}
	@RequestMapping(value="/api-engineer/list")
	@ResponseBody
	public Iterable<Engineer> apiFormList(@ModelAttribute Engineer Engineer,WebRequest request)
	{
		

		String dbdId = request.getParameter("dbdId");
		Iterable<Engineer> eForm = this.engineerRepository.findAllbyDbdId(dbdId);
		List<Engineer> lEngineer = new ArrayList<>();

		for(Engineer e : eForm)
		{
			Engineer e2 = new Engineer();
			e2.setId(e.getId());
			e2.setEngineerId(e.getEngineerId());
			e2.setTitleName(e.getTitleName());
			e2.setFirstname(e.getFirstname());
			e2.setLastname(e.getLastname());
			e2.setType(e.getType());
			e2.setFaculty(e.getFaculty());
			e2.setExpireDate(e.getExpireDate());
			//e2.setCompany(e.getCompany());
			lEngineer.add(e2);

		}
		//eForm.forEach(lEngineer::add);


		return lEngineer;
	}

	private void manageFormMeta(String key, BigDecimal formId)
	{
		FormMeta fm = this.formMetaRepository.findByMetaNameAndFormId(key, formId);
		if (fm == null)
		{
			FormMeta newFormMeta = new FormMeta();
			newFormMeta.setFormId(formId);
			newFormMeta.setMetaLabel(key);
			newFormMeta.setMetaName(key);
			newFormMeta.setMetaOrder(0);
			newFormMeta.setMetaType("text");
			this.formMetaRepository.save(newFormMeta);
		}
	}

	@RequestMapping(value="/api-engineer/saveTEST")
	@ResponseBody
	public String apiFormSave2(@ModelAttribute Engineer Engineer,WebRequest request)
	{


		String dbdId = request.getParameter("dbdId");
	//	MultipartFile file_1 = request.getParameter("file_1");
		//String title = request.getParameter("title");
		Engineer.setEngineerId(request.getParameter("engineerId"));
		System.out.println("title: "+request.getParameter("title"));
		MultiValueMap<String, MultipartFile> uploadFiles = ((StandardMultipartHttpServletRequest) ((ServletWebRequest) request).getRequest()).getMultiFileMap();
		for (MultiValueMap.Entry<String, List<MultipartFile>> uploadFile : uploadFiles.entrySet()) {

			List<MultipartFile> files = uploadFile.getValue();

            for (MultipartFile multipartFile : files) {

                String fileName = multipartFile.getOriginalFilename();
                String fileParamName = multipartFile.getName();
                String currentSaveFileName = "";

               // this.manageFormMeta(fileParamName.trim(), formId);

                //start upload

                if (multipartFile.getSize() > 0)
                {
                	System.out.println("System uploading...");
		        	long millis = System.currentTimeMillis() ;

		            String type = multipartFile.getContentType();
		            type = "." + type.substring(type.lastIndexOf("/") + 1);

		            currentSaveFileName = request.getParameter("firstname")+"_"+request.getParameter("lastname")+"_"+millis + type;

                    try {
						String tmpFormId = "12";
						String userAndFormFolder = UPLOAD_FOLDER+"/1/12/";
    	                // String userAndFormFolder = UPLOAD_FOLDER + dbdId + "/engineer/";
    	                File directory = new File(String.valueOf(userAndFormFolder));

    	                if (!directory.exists()) {
    	                    directory.mkdirs();
    	                }

    		            Path path = Paths.get(userAndFormFolder + currentSaveFileName);
    		            byte[] bytes = multipartFile.getBytes();
    		            Files.write(path, bytes);
    		            System.out.println("Uploaded success");
    		            System.out.println(userAndFormFolder);
                    } catch (Exception e) {
                    	System.out.println("Uploaded error");
                    	e.printStackTrace();
                    }
                }


                //end upload

                //out += "Param. : " + fileParamName;
                //out += "File : " + fileName + "<br/>\n";
                System.out.println("Param. : " + fileParamName);
                System.out.println("File : " + fileName);

                switch(fileParamName) {
                   case "file_1":
					   Engineer.setCopyId(currentSaveFileName);
					   Engineer.setFile1(currentSaveFileName);
			           break;
                   case "file_2":
					   Engineer.setCopyPassport(currentSaveFileName);
					   Engineer.setFile2(currentSaveFileName);
			           break;
                   case "file_3":
					   Engineer.setCopyRegistration(currentSaveFileName);
					   Engineer.setFile3(currentSaveFileName);
			           break;
                   case "file_4":
					   Engineer.setCopyLicensedEngineering(currentSaveFileName);
					   Engineer.setFile4(currentSaveFileName);
			           break;
                   case "file_5":
					   Engineer.setCopyCertificate1(currentSaveFileName);
					   Engineer.setFile5(currentSaveFileName);
			           break;
                   case "file_6":
                	   Engineer.setCopyCertificate2(currentSaveFileName);
			           break;
                   case "file_7":
                	   Engineer.setCopyCertificate3(currentSaveFileName);
			           break;
			       default:
			    	   //monthString = "Invalid month";
			           break;

                }


            }

		}
		System.out.println("rtId : " + request.getParameter("rtId"));
		Engineer.setTitleName(this.titleNameRepository.findTitleNameByCode(request.getParameter("title")));
		Engineer.setFirstname(request.getParameter("firstname"));
		Engineer.setLastname(request.getParameter("lastname"));
		Engineer.setType(request.getParameter("e-engineerType"));
		Engineer.setFaculty(request.getParameter("e-engineerFaculty"));
		Engineer.setRTID(request.getParameter("rtId"));
		Engineer.setIssueDate(request.getParameter("sDatePermit"));
		Engineer.setExpireDate(request.getParameter("sDatePermitExpire"));
		Engineer.setPersonalId(request.getParameter("e-idPerson").replace("-", ""));
		// Engineer.setAddress(request.getParameter("e-address"));

		Engineer.setTiTleEnGiNeer(request.getParameter("sTitle_Name"));

		Engineer.setNumhome_service(request.getParameter("e-numberhome"));
        Engineer.setMoo_service(request.getParameter("e-moo"));
        Engineer.setSoi_service(request.getParameter("e-soi"));
        Engineer.setRoad_service(request.getParameter("e-road"));
        Engineer.setTumbon_service(request.getParameter("e-tumbon"));
        Engineer.setAumphur_service(request.getParameter("e-aumphur"));
        Engineer.setProvince_service(request.getParameter("e-province"));
        Engineer.setZipcode_service(request.getParameter("e-zipcode"));
        Engineer.setTel_service(request.getParameter("e-tel").replace("-", ""));
        Engineer.setEmail_service(request.getParameter("e-email"));

        // Engineer.setEngineerId(request.getParameter("engineerId"));
	
		Engineer.setCompany(this.companyNewRepository.findByDbdId(dbdId.toString()));
		this.engineerRepository.save(Engineer);
		//return "redirect:/engineer/list";
		//System.out.println(departmentId);
		return "ok";
		//return Employee.getDepartment().toString();
	}


	

	@RequestMapping(value = "/upload/file")
	public ResponseEntity executeByPost(WebRequest webRequest)
	{
		MultiValueMap<String, MultipartFile> uploadFiles = ((StandardMultipartHttpServletRequest) ((ServletWebRequest) webRequest).getRequest()).getMultiFileMap();
		for (MultiValueMap.Entry<String, List<MultipartFile>> uploadFile : uploadFiles.entrySet()) {

			List<MultipartFile> files = uploadFile.getValue();
			
            for (MultipartFile multipartFile : files) {
            	 
                String fileName = multipartFile.getOriginalFilename();
				String fileParamName = multipartFile.getName();
				String currentSaveFileName = "";

				System.out.println(fileName+" - "+fileParamName);
				


                //end upload

                //out += "Param. : " + fileParamName;
                //out += "File : " + fileName + "<br/>\n";
                System.out.println("Param. : " + fileParamName);
                System.out.println("File : " + fileName);

			}
		}
		return ResponseEntity.ok(uploadFiles.keySet());
	}


	//--------------------SORNRAM-----------------------

	@RequestMapping(value="/api-engineer/listTest")
	@ResponseBody
	public Iterable<Engineer> apiFormListTest(@ModelAttribute Engineer Engineer,WebRequest request)
	{


		String dbdId = request.getParameter("dbdId");
		Iterable<Engineer> eForm = this.engineerRepository.findAllbyDbdId(dbdId);
		List<Engineer> lEngineer = new ArrayList<>();

		for(Engineer e : eForm)
		{
			Engineer e2 = new Engineer();
			// System.out.println(e.getSTile());
			e2.setId(e.getId());
			e2.setEngineerId(e.getEngineerId());
			e2.setTitleName(e.getTitleName());
			e2.setTiTleEnGiNeer(e.getTiTleEnGiNeer());
			e2.setFirstname(e.getFirstname());
			e2.setLastname(e.getLastname());
			e2.setType(e.getType());
			e2.setFaculty(e.getFaculty());
			e2.setPersonalId(e.getPersonalId());
			e2.setNumhome_service(e.getNumhome_service());
			e2.setMoo_service(e.getMoo_service());
			e2.setSoi_service(e.getSoi_service());
			e2.setRoad_service(e.getRoad_service());
			e2.setTumbon_service(e.getTumbon_service());
			e2.setAumphur_service(e.getAumphur_service());
			e2.setProvince_service(e.getProvince_service());
			e2.setZipcode_service(e.getZipcode_service());
			e2.setTel_service(e.getTel_service());
			e2.setEmail_service(e.getEmail_service());

			e2.setFile1(e.getFile1());
			e2.setFile2(e.getFile2());
			e2.setFile3(e.getFile3());
			e2.setFile4(e.getFile4());
			e2.setFile5(e.getFile5());

			e2.setExpireDate(e.getExpireDate());
			e2.setIssueDate(e.getIssueDate());


			//e2.setCompany(e.getCompany());
			lEngineer.add(e2);

		}
		//eForm.forEach(lEngineer::add);


		return lEngineer;
	}

	//--------------------------------------------------

}

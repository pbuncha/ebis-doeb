package th.go.doeb.ebis.controller.trade;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import th.go.doeb.ebis.bean.CompanyBean;
import th.go.doeb.ebis.bean.ProvinceBean;
import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.CompanyAttorney;
import th.go.doeb.ebis.entity.CompanyContact;
import th.go.doeb.ebis.entity.Engineer;
import th.go.doeb.ebis.entity.Form;
import th.go.doeb.ebis.entity.FormEntry;
import th.go.doeb.ebis.entity.FormMeta;
import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestCategory;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.entity.RequestStatus;
import th.go.doeb.ebis.entity.RequestTransaction;
import th.go.doeb.ebis.entity.RequestType;
import th.go.doeb.ebis.entity.TitleName;
import th.go.doeb.ebis.repository.CompanyAttorneyRepository;
import th.go.doeb.ebis.repository.CompanyContactRepository;
import th.go.doeb.ebis.repository.CompanyNewRepository;
import th.go.doeb.ebis.repository.EngineerRepository;
import th.go.doeb.ebis.repository.FormEntryRepository;
import th.go.doeb.ebis.repository.FormMetaRepository;
import th.go.doeb.ebis.repository.FormRepository;
import th.go.doeb.ebis.repository.ProvinceRepository;
import th.go.doeb.ebis.repository.RequestCategoryRepository;
import th.go.doeb.ebis.repository.RequestFormRepository;
import th.go.doeb.ebis.repository.RequestRepository;
import th.go.doeb.ebis.repository.RequestStatusRepository;
import th.go.doeb.ebis.repository.RequestTransactionRepository;
import th.go.doeb.ebis.repository.RequestTypeRepository;
import th.go.doeb.ebis.repository.TitleNameRepository;

@Controller
public class OneStopControllerTrade {

	private static String UPLOAD_FOLDER = "/data/test/uploads/etdi/";
	
	@Autowired(required=true)
	private CompanyNewRepository companyRepository;

	@Autowired(required=true)
	private ProvinceRepository provinceRepository;	

	@Autowired(required=true)
	private FormRepository formRepository;	

	@Autowired(required=true)
	private FormMetaRepository formMetaRepository;	
	
	@Autowired(required=true)
	private FormEntryRepository formEntryRepository;	
	
	@Autowired(required=true)
	private RequestCategoryRepository requestCategoryRepository;
	
	@Autowired(required=true)
	private RequestRepository requestRepository;
	
	@Autowired(required=true)
	private RequestFormRepository requestFormRepository;	
	
	@Autowired(required=true)
	private RequestTransactionRepository requestTransactionRepository;
	
	@Autowired(required=true)
	private RequestStatusRepository requestStatusRepository;
	
	@Autowired(required=true)
	private EngineerRepository engineerRepository;
	
	@Autowired(required=true)
	private CompanyContactRepository companyContactRepository;
	
	@Autowired(required=true)
	private CompanyAttorneyRepository companyAttorneyRepository;
	
	@Autowired(required=true)
	private RequestTypeRepository requestTypeRepository;
	
	@Autowired(required=true)
	private TitleNameRepository titleNameRepository;
	
	///onestop/company/1010101001111/requestFormOil
	//@RequestMapping(value="/onestop")
	@RequestMapping(value="/onestop/company/{dbdId}/requestFormTrade")
	public String requestForm(@PathVariable("dbdId") String dbdId, Model model)
	{
		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);
		
		Iterable<RequestCategory> iRequestCategory = this.requestCategoryRepository.findAllFilter5();
		List<RequestCategory> lRequestCategory = new ArrayList<>();
		iRequestCategory.forEach(lRequestCategory::add);
		
		//Iterable<Form> iForm = this.formRepository.findAll();
		Iterable<Form> iForm = this.formRepository.findByFormActive(new BigDecimal(1));
		List<Form> lForm = new ArrayList<>();
		iForm.forEach(lForm::add);
		model.addAttribute("lRequestCategory", lRequestCategory);
		model.addAttribute("lForm", lForm);
		model.addAttribute("dbdId", dbdId);
		
		return "onestopTrade/request_form";
	}
	
	@RequestMapping(value="/onestopTrade/company/{dbdId}/formgroup/view/{requestId}/{requestTypeId}")
	public String formgroupView(@PathVariable("dbdId") String dbdId, @PathVariable("requestId") Long requestId, @PathVariable("requestTypeId") long requestTypeId, Model model)
	{
		//Form f = this.formRepository.findByFormId(formId);
		Request request = this.requestRepository.findByRequestId(requestId);
		Iterable<RequestForm> iRequestForm = this.requestFormRepository.findByRequestAndTypeId(request,requestTypeId);
		List<RequestForm> lRequestForm = new ArrayList<>();
		iRequestForm.forEach(lRequestForm::add);
		
        Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);

		model.addAttribute("request", request);
		model.addAttribute("lRequestForm", lRequestForm);
		model.addAttribute("lProvinceBean", lProvinceBean);
		model.addAttribute("dbdId", dbdId);
		model.addAttribute("requestTypeId", requestTypeId);
		//model.addAttribute("formId", formId);
		
		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);
		
		RequestType requestType = requestTypeRepository.findByRequestTypeId(requestTypeId);
		
		RequestStatus requestStatus = this.requestStatusRepository.findByStatus("0");
		RequestTransaction requestTransaction = this.requestTransactionRepository.findByDbdIdAndRequestAndRequestStatus(dbdId, request.getRequestId(), requestStatus.getStatus());
		if (requestTransaction == null)
		{
			requestTransaction = new RequestTransaction();
			requestTransaction.setCompany(company);
			requestTransaction.setRequest(this.requestRepository.findByRequestId(requestId));
			requestTransaction.setCreated(new Timestamp(System.currentTimeMillis()));
			requestTransaction.setRequestStatus(requestStatus);
			requestTransaction.setRequestType(requestType);
			requestTransaction.setActualGaranteeDay(request.getGauranteeDay());
			this.requestTransactionRepository.save(requestTransaction);
			requestTransaction = this.requestTransactionRepository.findByDbdIdAndRequestAndRequestStatus(dbdId, request.getRequestId(), requestStatus.getStatus());
		}
		model.addAttribute("rtId", requestTransaction.getRtId());
		//this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction,);
		return "onestopTrade/formgroup";
	}
	
	@RequestMapping(value="/onestopTrade/company/{dbdId}/{rtId}/form/input/{formId}/{requestTypeId}")
	public String formView(@PathVariable("dbdId") String dbdId, @PathVariable("rtId") long rtId, @PathVariable("formId") Long formId, @PathVariable("requestTypeId") String requestTypeId, ModelMap  model)
	{
		RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(rtId);
		Request r = requestTransaction.getRequest();
		Form f = this.formRepository.findByFormId(formId);
		
		RequestForm requestForm = this.requestFormRepository.findByRequestAndFormAadTypeId(r, f,requestTransaction.getRequestType().getRequestTypeId());
		System.out.println("rId: "+r.getRequestId()+" | fId:  "+ f.getFormId() +" | getRequestTypeId: "+requestTransaction.getRequestType().getRequestTypeId());
		//long rfId = requestForm.getRfId();
				
		FormEntry formEntry = this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm);		
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String,String> entryMap = new HashMap<String, String>();
		model.addAttribute("entryMap", entryMap);
		model.addAttribute("formEntry", formEntry);
		model.addAttribute("formMetaId", "0");
		if (formEntry != null)
		{
			model.addAttribute("formMetaId", formEntry.getEntryId());
			try {
				
				entryMap = objectMapper.readValue(formEntry.getEntryText(), new TypeReference<HashMap<String,String>>() {});
				model.addAttribute("entryMap", entryMap);
				System.out.println(entryMap);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}

		String requestTypeName = "";
		if (requestTypeId.equalsIgnoreCase("1"))
		{
			requestTypeName = "ขอใหม่";
		} else if (requestTypeId.equalsIgnoreCase("2"))
		{
			requestTypeName = "ขอต่ออายุ";
		} else if (requestTypeId.equalsIgnoreCase("3"))
		{
			requestTypeName = "ขอแก้ไขเปลี่ยนแปลง";
		}
		
		String requestName = r.getRequestName();
		
        Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);
		
		//CompanyAttorney iCompanyAttorney = this.companyAttorneyRepository.findByDbdIdLimit1(dbdId);
		//List<CompanyAttorney> lCompanyAttorney = new ArrayList<>();
		//iCompanyAttorney.forEach(lCompanyAttorney::add);
		
		model.addAttribute("lProvinceBean", lProvinceBean);
		model.addAttribute("dbdId", dbdId);
		model.addAttribute("rtId", rtId);
		model.addAttribute("formId", formId);
		model.addAttribute("rfId", f.getFormId());
//<<<<<<< HEAD
		model.addAttribute("requestTypeId", requestTypeId);
		model.addAttribute("requestTypeName", requestTypeName);
		model.addAttribute("requestName", requestName);
//=======
		//model.addAttribute("iCompanyAttorney", iCompanyAttorney);
//>>>>>>> 0840dbd8796a58706776381b9fbb33e8c2284d7f

		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);
		
		Iterable<Engineer> eForm = this.engineerRepository.findAllbyDbdId(dbdId);
		List<Engineer> lEngineer = new ArrayList<>();
		eForm.forEach(lEngineer::add);
		model.addAttribute("lEngineer", lEngineer);
		
		Iterable<TitleName> eTitleName = this.titleNameRepository.findTitleNameActive();
		List<TitleName> lTitleName = new ArrayList<>();
		eTitleName.forEach(lTitleName::add);
		model.addAttribute("lTitleName", lTitleName);
		
		return "onestopTrade/form/" + f.getFormTemplate();
	}
	
	@RequestMapping(value="/onestopTrade/form/submission")
	public String formSubmission(WebRequest request, HttpSession session)
	{
		
		
		String out = "";
		Map<String, Object> mapParams = new HashMap<String, Object>();
		FormEntry formEntry = new FormEntry();
		String memberId = "1";
		//company.setDbdId(company.getDbdId().toString().replace("-", ""));
		String dbdId = request.getParameter("dbdId").toString().replace("-", "");
		long rtId = Long.valueOf(request.getParameter("rtId"));
		BigDecimal formId = new BigDecimal(request.getParameter("formId"));
		long rfId = Long.valueOf(request.getParameter("rfId"));
		long formMetaId = Long.valueOf(request.getParameter("formMetaId"));
		Map<String, String[]> parameters = request.getParameterMap();
		MultiValueMap<String, MultipartFile> uploadFiles = ((StandardMultipartHttpServletRequest) ((ServletWebRequest) request).getRequest()).getMultiFileMap();
		
		for (Map.Entry<String, String[]> parameter : parameters.entrySet()) {
			String key = parameter.getKey();
			String[] value = parameter.getValue();
			System.out.println(key+": "+value[0]);
			
			this.manageFormMeta(key.trim(), formId);
			
			//if (value.length == 1)
			//{
				//out += "Item : " + key + " Count : " + value[0] + "<br/>\n";
				mapParams.put(key, value[0]);
			//} else {
//				for (int i = 0; i < value.length; i++) {
//                    pw.println("<li>" + value[i].toString() + "</li><br>");
//                }
				//out += "Item : " + key + " Count : " + parameter.getValue() + "<br/>\n";				
			//}

		}

		//out += "#######";
		
		for (MultiValueMap.Entry<String, List<MultipartFile>> uploadFile : uploadFiles.entrySet()) {

			List<MultipartFile> files = uploadFile.getValue();
			
            for (MultipartFile multipartFile : files) {
            	 
                String fileName = multipartFile.getOriginalFilename();
                String fileParamName = multipartFile.getName();
                String currentSaveFileName = "";

                this.manageFormMeta(fileParamName.trim(), formId);
                
                //start upload
                
                if (multipartFile.getSize() > 0)
                {
                	
		        	long millis = System.currentTimeMillis() ;
		            
		            String type = multipartFile.getContentType();
		            type = "." + type.substring(type.lastIndexOf("/") + 1);
		            
		            currentSaveFileName = millis + type;
                	
                    try {
    	                String userAndFormFolder = UPLOAD_FOLDER + memberId + "/" + formId + "/";
    	                File directory = new File(String.valueOf(userAndFormFolder));
    	
    	                if (!directory.exists()) {
    	                    directory.mkdirs();
    	                }
    		            
    		            Path path = Paths.get(userAndFormFolder + currentSaveFileName);
    		            byte[] bytes = multipartFile.getBytes();
    		            Files.write(path, bytes);
    		            
                    } catch (Exception e) {
                    	e.printStackTrace();
                    }                	
                }
                

                //end upload
                
                //out += "Param. : " + fileParamName;
                //out += "File : " + fileName + "<br/>\n";
                mapParams.put(fileParamName, currentSaveFileName);
            }

		}
		
		ObjectMapper mapper = new ObjectMapper();
		RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(rtId);
		try {

			String json = mapper.writeValueAsString(mapParams);
			System.out.println(json);
			
			
			
			formEntry.setEntryText(json);
			formEntry.setCurrentState(BigDecimal.valueOf(Double.valueOf("1")));
			formEntry.setModified(new Timestamp(System.currentTimeMillis()));
			System.out.println("rfId: "+rfId);
			System.out.println("requestFormRepository: "+this.requestFormRepository.findByRfId(rfId));
			formEntry.setRequestForm(this.requestFormRepository.findByRfId(rfId));
			formEntry.setRequestTransaction(requestTransaction);
			formEntry.setRequestStatus(this.requestStatusRepository.findByStatus("101"));
			if(formMetaId != 0) {
				formEntry.setEntryId(formMetaId);
			}
			this.formEntryRepository.save(formEntry);

			//requestTransaction.setRequestStatus(this.requestStatusRepository.findByStatus("101"));
			//this.requestTransactionRepository.save(requestTransaction);
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//http://localhost:8080/onestop/company/0107550000220/formgroup/view/205/1/1#
		return "redirect:/onestopTrade/company/" + dbdId +"/formgroup/view/"+ requestTransaction.getRequest().getRequestId()+"/"+requestTransaction.getRequestType().getRequestTypeId();
		//return "onestop/dashboard";
		
	}
	
	private void manageFormMeta(String key, BigDecimal formId)
	{
		FormMeta fm = this.formMetaRepository.findByMetaNameAndFormId(key, formId);
		if (fm == null)
		{
			FormMeta newFormMeta = new FormMeta();
			newFormMeta.setFormId(formId);
			newFormMeta.setMetaLabel(key);
			newFormMeta.setMetaName(key);
			newFormMeta.setMetaOrder(0);
			newFormMeta.setMetaType("text");
			this.formMetaRepository.save(newFormMeta);
		}		
	}	
	
	 @RequestMapping(value="/onestop/company/{dbdId}/form_trade_010")
	 public String form_trade_009(@PathVariable("dbdId") String dbdId, Model model)
	 {
	  Company company = this.companyRepository.findByDbdId(dbdId);
	  model.addAttribute("company", company);
	  
	  Iterable<RequestCategory> iRequestCategory = this.requestCategoryRepository.findAllFilter3();
	  List<RequestCategory> lRequestCategory = new ArrayList<>();
	  iRequestCategory.forEach(lRequestCategory::add);
	  
	  //Iterable<Form> iForm = this.formRepository.findAll();
	  Iterable<Form> iForm = this.formRepository.findByFormActive(new BigDecimal(1));
	  List<Form> lForm = new ArrayList<>();
	  iForm.forEach(lForm::add);
	  model.addAttribute("lRequestCategory", lRequestCategory);
	  model.addAttribute("lForm", lForm);
	  model.addAttribute("dbdId", dbdId);
	  return "onestopTrade/form/form_trade_010";
	 }
	
	 @RequestMapping(value="/onestop/company/{dbdId}/form_trade_012")
	 public String form_oil_010(@PathVariable("dbdId") String dbdId, Model model)
	 {
		 
		 
	  Company company = this.companyRepository.findByDbdId(dbdId);
	  model.addAttribute("company", company);
	  
	  Iterable<RequestCategory> iRequestCategory = this.requestCategoryRepository.findAllFilter3();
	  List<RequestCategory> lRequestCategory = new ArrayList<>();
	  iRequestCategory.forEach(lRequestCategory::add);
	  
	  //Iterable<Form> iForm = this.formRepository.findAll();
	  Iterable<Form> iForm = this.formRepository.findByFormActive(new BigDecimal(1));
	  List<Form> lForm = new ArrayList<>();
	  iForm.forEach(lForm::add);
	  model.addAttribute("lRequestCategory", lRequestCategory);
	  model.addAttribute("lForm", lForm);
	  model.addAttribute("dbdId", dbdId);
	  return "onestopTrade/form/form_trade_012";
	 }

	
}

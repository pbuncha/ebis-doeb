package th.go.doeb.ebis.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import th.go.doeb.ebis.bean.EModel;
import th.go.doeb.ebis.bean.Notice;
import th.go.doeb.ebis.repository.NoticeRepository;
import th.go.doeb.ebis.repository.StcRepository;

@Controller
public class StcController {
	
	@Autowired(required=true)
	private StcRepository stcRepository;
	private static String UPLOADED_FOLDER = System.getProperty("user.dir")+"\\src\\main\\resources\\uploads\\TechnicalTestingValidation\\";
	
	@RequestMapping(value="/addstc",method = RequestMethod.POST)
	//@ResponseBody
	public String save(@ModelAttribute("EModel") EModel c,BindingResult result,
			@RequestParam("file_engineer1") MultipartFile file1,
			@RequestParam("file_engineer2") MultipartFile file2,
			@RequestParam("file_engineer3") MultipartFile file3,
			@RequestParam("file_engineer4") MultipartFile file4,
			@RequestParam("file_engineer5") MultipartFile file5,
			@RequestParam("file_engineer6") MultipartFile file6,
			@RequestParam("file_engineer7") MultipartFile file7,
			@RequestParam("file_engineer8_1") MultipartFile file8_1,
			@RequestParam("file_engineer8_2") MultipartFile file8_2,
			@RequestParam("file_engineer8_3") MultipartFile file8_3,
			@RequestParam("file_engineer9") MultipartFile file9,
			@RequestParam("file_engineer10") MultipartFile file10,
			@RequestParam("file_engineer11_1") MultipartFile file11_1,
			@RequestParam("file_engineer11_2") MultipartFile file11_2,
			@RequestParam("file_engineer11_3") MultipartFile file11_3,
			@RequestParam("file_engineer12_1") MultipartFile file12_1,
			@RequestParam("file_engineer12_2") MultipartFile file12_2,
			@RequestParam("file_engineer12_3") MultipartFile file12_3
			
			,RedirectAttributes redirectAttributes, ModelMap model)
	{
		//file_purpose1
		if (file1.isEmpty()) {
            //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            //return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
        	long millis = System.currentTimeMillis() ;
            byte[] bytes = file1.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file1.getOriginalFilename());
            Files.write(path, bytes);
            c.setFile_engineer1(millis +"_"+ file1.getOriginalFilename());
            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }
        
      //file_purpose2
      		if (file2.isEmpty()) {
                  //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                  //return "redirect:uploadStatus";
              }

              try {

                  // Get the file and save it somewhere
              	long millis = System.currentTimeMillis() ;
                  byte[] bytes = file2.getBytes();
                  Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file2.getOriginalFilename());
                  Files.write(path, bytes);
                  c.setFile_engineer2(millis +"_"+ file2.getOriginalFilename());
                  //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

              } catch (IOException e) {
                  e.printStackTrace();
              }
              
              //file_purpose3
        		if (file3.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file4.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file3.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer3(millis +"_"+ file3.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose4
        		if (file4.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                   // return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file4.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file4.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer4(millis +"_"+ file4.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                
                //file_purpose5
        		if (file5.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file5.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file5.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer5(millis +"_"+ file5.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose6
        		if (file6.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                   // return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file6.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file6.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer6(millis +"_"+ file6.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose7
        		if (file7.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                   // return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file7.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file7.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer7(millis +"_"+ file7.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose8_1
        		if (file8_1.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                   // return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file8_1.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file8_1.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer8_1(millis +"_"+ file8_1.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose8_2
        		if (file8_2.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file8_2.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file8_2.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer8_2(millis +"_"+ file8_2.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose8_3
        		if (file8_3.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file8_3.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file8_3.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer8_3(millis +"_"+ file8_3.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose9
        		if (file9.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                   // return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file9.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file9.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer9(millis +"_"+ file9.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                
                //file_purpose10
        		if (file10.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                   // return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file10.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file10.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer10(millis +"_"+ file10.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
              //file_purpose11_1
        		if (file11_1.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file11_1.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file11_1.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer11_1(millis +"_"+ file11_1.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
              //file_purpose11_2
        		if (file11_2.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file11_2.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file11_2.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer11_2(millis +"_"+ file11_2.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
              
                
                //file_purpose11_3
        		if (file11_3.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file11_3.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file11_3.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer11_3(millis +"_"+ file11_3.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                
                //file_purpose12_1
        		if (file12_1.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file12_1.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file12_1.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer12_1(millis +"_"+ file12_1.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose12_2
        		if (file12_2.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file12_2.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file12_2.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer12_2(millis +"_"+ file12_2.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                
                //file_purpose12_3
        		if (file12_2.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file12_3.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file12_3.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_engineer12_3(millis +"_"+ file12_3.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
              
              
              
              
              
              
              
        //---
		//ModelAttribute("BeTechnicalTestingValidation") BeTechnicalTestingValidation BeTechnicalTestingValidation,
	    //  BindingResult result,@RequestParam("copy_registion") MultipartFile file,@RequestParam("study_history") MultipartFile file2,
	    //  @RequestParam("professional_license") MultipartFile file3,@RequestParam("work_history") MultipartFile file4,
	   //   @RequestParam("my_pic") MultipartFile file5,
	  //    RedirectAttributes redirectAttributes, ModelMap model) 
		this.stcRepository.save(c);
		//return "Show";
		return "redirect:/stc2_1";
	}

}

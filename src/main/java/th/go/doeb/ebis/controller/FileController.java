package th.go.doeb.ebis.controller;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;

import javax.annotation.PostConstruct;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.doeb.ebis.entity.FileStorage;

@Controller
public class FileController {

	private static String UPLOAD_FOLDER_WIN = "C:\\Windows\\Temp\\";
	private static String UPLOAD_FOLDER_NIX = "/tmp/";
	private static String UPLOAD_FOLDER = "";

	private static String workingDir = System.getProperty("user.dir");
	private static String UPLOAD_FOLDER_TOOL = workingDir+"\\src\\main\\resources\\static\\images\\data\\uploads\\tool\\";

	@PostConstruct
	public void initialize() {
	   if (th.go.doeb.ebis.util.OSValidator.isWindows())
	   {
		   UPLOAD_FOLDER = UPLOAD_FOLDER_WIN;
	   } else {
		   UPLOAD_FOLDER = UPLOAD_FOLDER_NIX;
	   }
	}		
	
	@RequestMapping(value="/filemanager/{fileName}",produces = "application/octet-stream; charset=utf-8")
	@ResponseBody
	public ResponseEntity<InputStreamResource> filemanager(@PathVariable("fileName") String fileName) throws Exception
	{
			
		File file = new File(UPLOAD_FOLDER+"/1/12/"+fileName);
		
	        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
	        return ResponseEntity.ok()
	                // Content-Disposition
	                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName)
	                .contentLength(file.length()) //
	                .body(resource);

	}


	// @RequestMapping(value="/filemanager/{fileName}",produces = "application/octet-stream; charset=utf-8")
	// @ResponseBody
	// public ResponseEntity<InputStreamResource> filemanager(@PathVariable("fileName") String fileName) throws Exception
	// {
			
	// 	File file = new File(UPLOAD_FOLDER_TOOL+"/1/12/"+fileName);
		
	//         InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
	//         return ResponseEntity.ok()
	//                 // Content-Disposition
	//                 .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName)
	//                 .contentLength(file.length()) //
	//                 .body(resource);

	// }
	
}
/*
	@RequestMapping(value="/FileStorage/{fileId}",produces = "application/vnd.openxmlformats-officedocument.wordprocessingml.document; charset=utf-8")
	@ResponseBody
	public ResponseEntity<InputStreamResource> exportFileFileStorage(@PathVariable("fileId") Long fileId) throws Exception
	{
			
		FileStorage fileStorage = this.fileStorageRepository.findByfileId(fileId);
		System.out.println(UPLOAD_FOLDER_DOC+fileStorage.getRequestTransaction().getRtId()+"\\"+fileStorage.getFileStoreName());
		File file = new File(UPLOAD_FOLDER_DOC+fileStorage.getRequestTransaction().getRtId()+"\\"+fileStorage.getFileStoreName());
		
	        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
	        String Strfilename = URLEncoder.encode(fileStorage.getFileStoreName(),"UTF-8");
	        return ResponseEntity.ok()
	                // Content-Disposition
	                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + Strfilename)
	                // Content-Type
	               // .contentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
	                // Contet-Length
	                .contentLength(file.length()) //
	                .body(resource);
		//return str;
	}
*/
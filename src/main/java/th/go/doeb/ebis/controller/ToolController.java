package th.go.doeb.ebis.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import oracle.net.aso.e;
import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.Engineer;
import th.go.doeb.ebis.entity.FileStorage;
import th.go.doeb.ebis.entity.Tool;
import th.go.doeb.ebis.repository.CompanyNewRepository;
import th.go.doeb.ebis.repository.ToolRepository;

import java.net.URISyntaxException;
import java.net.URL;

@Controller
public class ToolController {
	@Autowired(required=true)
	private ToolRepository toolRepository;
	
	@Autowired(required=true)
	private CompanyNewRepository companyNewRepository;
	
	//private static String UPLOAD_FOLDER = "/data/test/uploads/tool/";
	private static String  workingDir = System.getProperty("user.dir");
	//src\main\resources\static\asset
	private static String UPLOAD_FOLDER = workingDir+"\\src\\main\\resources\\static\\images\\data\\uploads\\tool\\";
	@RequestMapping(value="/tool/list")
	public String formList(Model model)
	{
		Iterable<Tool> eTool = this.toolRepository.findAll();
		List<Tool> lTool = new ArrayList<>();
		eTool.forEach(lTool::add);
		model.addAttribute("lTool", lTool);
		return "tool/list";
	}
	
	@RequestMapping(value="/tool/add")
	public String formAdd(Model model)
	{
		Iterable<Company> eCompany = this.companyNewRepository.findAll();
		List<Company> lCompany = new ArrayList<>();
		eCompany.forEach(lCompany::add);
		model.addAttribute("lCompany", lCompany);
		return "tool/add";
	}
	
	@RequestMapping(value="/tool/save")
	public String formSave(@ModelAttribute Tool Tool,WebRequest request, HttpSession session)
	{
		String company = request.getParameter("companyIdstr");
		MultiValueMap<String, MultipartFile> uploadFiles = ((StandardMultipartHttpServletRequest) ((ServletWebRequest) request).getRequest()).getMultiFileMap();
		
		for (MultiValueMap.Entry<String, List<MultipartFile>> uploadFile : uploadFiles.entrySet()) {

			List<MultipartFile> files = uploadFile.getValue();
			
            for (MultipartFile multipartFile : files) {
            	 
                String fileName = multipartFile.getOriginalFilename();
                String fileParamName = multipartFile.getName();
                String currentSaveFileName = "";

                
                //start upload
                
                if (multipartFile.getSize() > 0)
                {
                	
		        	long millis = System.currentTimeMillis() ;
		            
		            String type = multipartFile.getContentType();
		            type = "." + type.substring(type.lastIndexOf("/") + 1);
		            
		            currentSaveFileName = millis + type;
                	
                    try {
    	               /* String ToolFloder = UPLOAD_FOLDER+"DbdId"+company+"/" ;
    	                File directory = new File(String.valueOf(ToolFloder));
    	               // System.out.println(directory.toString());
    	                if (!directory.exists()) {
    	                    directory.mkdirs();
    	                }
    		            */
            	         Path path2 = Paths.get(UPLOAD_FOLDER+"DbdId"+company+"\\");
            	         //if directory exists?
            	         if (!Files.exists(path2)) {
            	             try {
            	                 Files.createDirectories(path2);
            	             } catch (IOException e) {
            	                 //fail to create directory
            	            	 System.out.print("fail to create directory");
            	                 e.printStackTrace();
            	             }
            	         }
    		            Path path = Paths.get(UPLOAD_FOLDER+"DbdId"+company+"\\" + currentSaveFileName);
    		            byte[] bytes = multipartFile.getBytes();
    		            Files.write(path, bytes);
    		            
                    } catch (Exception e) {
                    	e.printStackTrace();
                    }                	
                }
                

                //end upload
                
                //out += "Param. : " + fileParamName;
                //out += "File : " + fileName + "<br/>\n";
                //System.out.println(fileParamName);
                //System.out.println(fileName);
                //System.out.println(currentSaveFileName);
                Tool.setToolPhoto(currentSaveFileName);
                //mapParams.put(fileParamName, currentSaveFileName);
            }

		}
		
		
		Tool.setCompany(this.companyNewRepository.findByDbdId(company));
		this.toolRepository.save(Tool);
		return "redirect:/tool/list";
	}
	@SuppressWarnings("unused")
	@RequestMapping(value="/tool/update")
	public String formUpdate(@ModelAttribute Tool Tool,WebRequest request, HttpSession session)
	{
		String company = request.getParameter("companyIdstr");
		MultiValueMap<String, MultipartFile> uploadFiles = ((StandardMultipartHttpServletRequest) ((ServletWebRequest) request).getRequest()).getMultiFileMap();
		
		for (MultiValueMap.Entry<String, List<MultipartFile>> uploadFile : uploadFiles.entrySet()) {

			List<MultipartFile> files = uploadFile.getValue();
			
            for (MultipartFile multipartFile : files) {
            	 
                String fileName = multipartFile.getOriginalFilename();
                String fileParamName = multipartFile.getName();
                String currentSaveFileName = "";

                
                //start upload
                
                if (multipartFile.getSize() > 0)
                {
                	
		        	long millis = System.currentTimeMillis() ;
		            
		            String type = multipartFile.getContentType();
		            type = "." + type.substring(type.lastIndexOf("/") + 1);
		            
		            currentSaveFileName = millis + type;
                	
                    try {
    	                String ToolFloder = UPLOAD_FOLDER+"DbdId"+company+"/" ;
    	                File directory = new File(String.valueOf(ToolFloder));
    	               // System.out.println(directory.toString());
    	                if (!directory.exists()) {
    	                    directory.mkdirs();
    	                }
    		            
    		            Path path = Paths.get(ToolFloder + currentSaveFileName);
    		            byte[] bytes = multipartFile.getBytes();
    		            Files.write(path, bytes);
    		            
                    } catch (Exception e) {
                    	e.printStackTrace();
                    }                	
                }
                

                //end upload
                
                //out += "Param. : " + fileParamName;
                //out += "File : " + fileName + "<br/>\n";
               // System.out.println("Debug1");
               // System.out.println(fileParamName);
               // System.out.println("Debug2");
               // System.out.println(fileName);
               // System.out.println("Debug3");
               // System.out.println(currentSaveFileName);
                if(currentSaveFileName != null && currentSaveFileName != "") {
                	// System.out.println("Into if");
                	  Tool.setToolPhoto(currentSaveFileName);
                }else{
                	Tool eTool = this.toolRepository.findByToolId(Tool.getToolId());
                	// System.out.println("into else");
                	Tool.setToolPhoto(eTool.getToolPhoto());
                }
              
                //mapParams.put(fileParamName, currentSaveFileName);
            }

		}
		
		
		Tool.setCompany(this.companyNewRepository.findByDbdId(company));
		this.toolRepository.save(Tool);
		return "redirect:/tool/list";
	}
	@RequestMapping(value="/tool/img/{toolId}")
	public String formImg(Model model,@PathVariable("toolId") Long toolId)
	{
		Tool eTool = this.toolRepository.findByToolId(toolId);

		model.addAttribute("ETool", eTool);
		 URL u = getClass().getProtectionDomain().getCodeSource().getLocation();
		    File f = null;
			try {
				f = new File(u.toURI());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String string = f.getParent();
			String[] parts = string.split(Pattern.quote("\\"));
		    //System.out.println(parts[0]);
		    model.addAttribute("url_img", parts[0]+"/"+UPLOAD_FOLDER+"DbdId"+eTool.getCompany().getDbdId()+"/"+eTool.getToolPhoto());
		    //UPLOAD_FOLDER
		return "tool/img";
	}
	
	@RequestMapping(value="/tool/view/{toolId}")
	public String formView(Model model,@PathVariable("toolId") Long toolId)
	{
		Tool eTool = this.toolRepository.findByToolId(toolId);
		Iterable<Company> eCompany = this.companyNewRepository.findAll();
		List<Company> lCompany = new ArrayList<>();
		eCompany.forEach(lCompany::add);
		model.addAttribute("lCompany", lCompany);
		model.addAttribute("ETool", eTool);
		 URL u = getClass().getProtectionDomain().getCodeSource().getLocation();
		    File f = null;
			try {
				f = new File(u.toURI());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String string = f.getParent();
			String[] parts = string.split(Pattern.quote("\\"));
		    //System.out.println(parts[0]);
		    model.addAttribute("url_img", parts[0]+"/"+UPLOAD_FOLDER+"DbdId"+eTool.getCompany().getDbdId()+"/"+eTool.getToolPhoto());
		return "tool/view";
	}
	
	@RequestMapping(value="/api-tool/list")
	@ResponseBody
	public Iterable<Tool> apiToolList(@ModelAttribute Engineer Engineer,WebRequest request)
	{
		
		
		String dbdId = request.getParameter("dbdId");
		Iterable<Tool> eTool = this.toolRepository.findAllbyDbdId(dbdId);
		List<Tool> lTool = new ArrayList<>();
		
		for(Tool e : eTool)
		{
			Tool e2 = new Tool();
			e2.setToolId(e.getToolId());
			e2.setToolName(e.getToolName());
			e2.setToolSerial(e.getToolSerial());

			e2.setCalibrationToolExprie(e.getCalibrationToolExprie());

			e2.setToolBrand(e.getToolBrand());
			e2.setToolModel(e.getToolModel());
			e2.setToolPhoto(e.getToolPhoto());
			e2.setToolNameCatagory(e.getToolNameCatagory());
			//e2.setCompany(e.getCompany());
			lTool.add(e2);
		    
		}
		//eForm.forEach(lEngineer::add);
		
		
		return lTool;
	}
	
	@RequestMapping(value="/api-tool/save")
	@ResponseBody
	public String apiToolSave(@ModelAttribute Tool tool,WebRequest request,BindingResult result
			,@RequestParam("toolPhotoImg") MultipartFile toolPhotoImg,
			RedirectAttributes redirectAttributes,ModelMap model)
	{
		
		System.out.println(request.getParameter("username"));
		String dbdId = request.getParameter("dbdId");
		MultiValueMap<String, MultipartFile> uploadFiles = ((StandardMultipartHttpServletRequest) ((ServletWebRequest) request).getRequest()).getMultiFileMap();
		for (MultiValueMap.Entry<String, List<MultipartFile>> uploadFile : uploadFiles.entrySet()) {

			List<MultipartFile> files = uploadFile.getValue();
			
            for (MultipartFile multipartFile : files) {
            	 
                String fileName = multipartFile.getOriginalFilename();
                String fileParamName = multipartFile.getName();
                String currentSaveFileName = "";

               // this.manageFormMeta(fileParamName.trim(), formId);
                
                //start upload
                
                if (multipartFile.getSize() > 0)
                {
                	System.out.println("System uploading...");
		        	long millis = System.currentTimeMillis() ;
		            
		            String type = multipartFile.getContentType();
		            type = "." + type.substring(type.lastIndexOf("/") + 1);
		            
		            currentSaveFileName = request.getParameter("toolSerial")+"_"+millis + type;
                	
                    try {
                    	String tmpFormId = "12";
    	                String userAndFormFolder = UPLOAD_FOLDER + dbdId + "/tool/";
    	                File directory = new File(String.valueOf(userAndFormFolder));
    	
    	                if (!directory.exists()) {
    	                    directory.mkdirs();
    	                }
    		            
    		            Path path = Paths.get(userAndFormFolder + currentSaveFileName);
    		            byte[] bytes = multipartFile.getBytes();
    		            Files.write(path, bytes);
    		            System.out.println("Uploaded success");
    		            System.out.println(userAndFormFolder);
                    } catch (Exception e) {
                    	System.out.println("Uploaded error");
                    	e.printStackTrace();
                    }             	
                }
                

                //end upload
                
                //out += "Param. : " + fileParamName;
                //out += "File : " + fileName + "<br/>\n";
                System.out.println("Param. : " + fileParamName);
                System.out.println("File : " + fileName);
                
                switch(fileParamName) {
                   case "toolPhotoImg":  
                	   tool.setToolPhoto(currentSaveFileName);
			           break;
                   case "proprietary_document":  
                	   tool.setProprietaryDocument(currentSaveFileName);
			           break;
                   case "calibration_tool":  
                	   tool.setCalibrationTool(currentSaveFileName);
			           break;
                 
			       default: 
			    	   //monthString = "Invalid month";
			           break;
                
                }
                
                
            }

		}
		//String title = request.getParameter("title");
		tool.setCompany(this.companyNewRepository.findByDbdId(dbdId.toString()));
		this.toolRepository.save(tool);
		//return "redirect:/engineer/list";
		//System.out.println(departmentId);
		return "ok";
		//return Employee.getDepartment().toString();
	}
	
	@RequestMapping(value="/api-tool/update")
	@ResponseBody
	public String apiToolUpdate(@ModelAttribute Tool tool,WebRequest request,BindingResult result
			,@RequestParam("toolphoto") MultipartFile toolPhotoImg,
			RedirectAttributes redirectAttributes,ModelMap model)
	{
		
		System.out.println(request.getParameter("username"));
		String dbdId = request.getParameter("dbdId");
		 try {
			 String ToolFloder = UPLOAD_FOLDER+"DbdId"+dbdId+"/" ;
             File directory = new File(String.valueOf(ToolFloder));
            // System.out.println(directory.toString());
             if (!directory.exists()) {
                 directory.mkdirs();
             }
	            
	            // Get the file and save it somewhere
	        	long millis = System.currentTimeMillis() ;
	            byte[] bytes = toolPhotoImg.getBytes();
	            //UPLOAD_FOLDER+"DbdId"+company+"/" ;
				Path path = Paths.get(UPLOAD_FOLDER +"/"+dbdId+"/"+"/tool/"+millis +"_"+ toolPhotoImg.getOriginalFilename());
				System.out.println(path);
	            Files.write(path, bytes);
	            if (!toolPhotoImg.isEmpty()) {
					tool.setToolPhoto(millis +"_"+ toolPhotoImg.getOriginalFilename());
		        }
	            
	            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	        } catch (IOException e) {
	        	System.out.println("Error!!!!!!!!!!!!!!!!!");
	            e.printStackTrace();
	        }
		//String title = request.getParameter("title");
		tool.setCompany(this.companyNewRepository.findByDbdId(dbdId.toString()));
		this.toolRepository.save(tool);
		//return "redirect:/engineer/list";
		//System.out.println(departmentId);
		return "ok";
		//return Employee.getDepartment().toString();
	}
	
	@RequestMapping(value="/api-tool/delete")
	@ResponseBody
	public int apiFormDelete(@ModelAttribute Tool tool,WebRequest request)
	{
		
		
		Long id = Long.valueOf(request.getParameter("id"));
		
		return this.toolRepository.deleteToolCustom(id);
		//return Employee.getDepartment().toString();
	}
	
}

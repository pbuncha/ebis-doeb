package th.go.doeb.ebis.controller;

import java.io.Console;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.docx4j.dml.wordprocessingDrawing.Inline;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPartAbstractImage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.CTBorder;
import org.docx4j.wml.CTShd;
import org.docx4j.wml.CTTblPrBase.TblStyle;
import org.docx4j.wml.CTVerticalJc;
import org.docx4j.wml.Color;
import org.docx4j.wml.Drawing;
import org.docx4j.wml.HpsMeasure;
import org.docx4j.wml.Jc;
import org.docx4j.wml.JcEnumeration;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.PPr;
import org.docx4j.wml.R;
import org.docx4j.wml.RFonts;
import org.docx4j.wml.RPr;
import org.docx4j.wml.STBorder;
import org.docx4j.wml.STVerticalJc;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.TblPr;
import org.docx4j.wml.TblWidth;
import org.docx4j.wml.Tc;
import org.docx4j.wml.TcMar;
import org.docx4j.wml.TcPr;
import org.docx4j.wml.TcPrInner.GridSpan;
import org.docx4j.wml.TcPrInner.TcBorders;
import org.docx4j.wml.TcPrInner.VMerge;
import org.docx4j.wml.Text;
import org.docx4j.wml.Tr;
import org.docx4j.wml.U;
import org.docx4j.wml.UnderlineEnumeration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.jsolve.templ4docx.core.Docx;
import pl.jsolve.templ4docx.core.VariablePattern;
import pl.jsolve.templ4docx.variable.TextVariable;
import pl.jsolve.templ4docx.variable.Variables;
import th.go.doeb.ebis.bean.AmphurBean;
import th.go.doeb.ebis.bean.DocxStyle;
import th.go.doeb.ebis.bean.ProvinceBean;
import th.go.doeb.ebis.bean.TambonBean;
import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.Engineer;
import th.go.doeb.ebis.entity.Evaluate;
import th.go.doeb.ebis.entity.EventLog;
import th.go.doeb.ebis.entity.FileStorage;
import th.go.doeb.ebis.entity.Form;
import th.go.doeb.ebis.entity.FormEntry;
import th.go.doeb.ebis.entity.GovDocumentCreate;
import th.go.doeb.ebis.entity.GovDocumentType;
import th.go.doeb.ebis.entity.Holiday;
import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.entity.RequestStatus;
import th.go.doeb.ebis.entity.RequestTransaction;
import th.go.doeb.ebis.entity.TitleName;
import th.go.doeb.ebis.repository.AmphurRepository;
import th.go.doeb.ebis.repository.CompanyNewRepository;
import th.go.doeb.ebis.repository.EngineerRepository;
import th.go.doeb.ebis.repository.EvaluateRepository;
import th.go.doeb.ebis.repository.EventLogRepository;
import th.go.doeb.ebis.repository.FileStorageRepository;
import th.go.doeb.ebis.repository.FormEntryRepository;
import th.go.doeb.ebis.repository.FormRepository;
import th.go.doeb.ebis.repository.GovDocumentCreateRepository;
import th.go.doeb.ebis.repository.GovDocumentTypeRepository;
import th.go.doeb.ebis.repository.ProvinceRepository;
import th.go.doeb.ebis.repository.RequestFormRepository;
import th.go.doeb.ebis.repository.RequestRepository;
import th.go.doeb.ebis.repository.RequestStatusRepository;
import th.go.doeb.ebis.repository.RequestTransactionRepository;
import th.go.doeb.ebis.repository.TambonRepository;
import th.go.doeb.ebis.repository.TitleNameRepository;

import java.io.FileInputStream;
import java.io.FileWriter;

//import org.o7planning.sbdownload.utils.MediaTypeUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
@Controller
public class OfficerController {

	@Autowired(required=true)
	private FormRepository formRepository;

	@Autowired(required=true)
	private FormEntryRepository formEntryRepository;

	@Autowired(required=true)
	private EvaluateRepository evaluateRepository;

	@Autowired(required=true)
	private RequestFormRepository requestFormRepository;

	@Autowired(required=true)
	private RequestTransactionRepository requestTransactionRepository;

	@Autowired(required=true)
	private RequestStatusRepository requestStatusRepository;

	@Autowired(required=true)
	private FileStorageRepository fileStorageRepository;

	@Autowired(required=true)
	private GovDocumentTypeRepository govDocumentTypeRepository;

	@Autowired(required=true)
	private GovDocumentCreateRepository govDocumentCreateRepository;

	@Autowired(required=true)
	private CompanyNewRepository companyNewRepository;

	@Autowired(required=true)
	private CompanyNewRepository companyRepository;

	@Autowired(required=true)
	private RequestRepository requestRepository;

	@Autowired(required=true)
	private AmphurRepository amphurRepository;

	@Autowired(required=true)
	private TambonRepository tambonRepository;

	@Autowired(required=true)
	private ProvinceRepository provinceRepository;

	@Autowired(required=true)
	private EngineerRepository engineerRepository;


	@Autowired(required=true)
	private TitleNameRepository titleNameRepository;

	@Autowired(required=true)
	private EventLogRepository eventLogRepository;


	private static String  workingDir = System.getProperty("user.dir");
	private static String UPLOAD_FOLDER = workingDir+"\\src\\data\\test\\uploads\\tool\\";
	//private static String UPLOAD_FOLDER_DOC = workingDir+"\\src\\data\\doc\\";
	private static String UPLOAD_FOLDER_DOC_WIN = "C:\\Windows\\Temp\\";
	private static String UPLOAD_FOLDER_DOC_NIX = "/tmp/";
	private static String UPLOAD_FOLDER_DOC = "";

	@PostConstruct
	public void initialize() {
	   if (th.go.doeb.ebis.util.OSValidator.isWindows())
	   {
		   UPLOAD_FOLDER_DOC = UPLOAD_FOLDER_DOC_WIN;
	   } else {
		   UPLOAD_FOLDER_DOC = UPLOAD_FOLDER_DOC_NIX;
	   }
	}

	@RequestMapping(value="/officer")
	public String index()
	{
		return "redirect:/officer/dashboard";
	}

	@RequestMapping(value="/officer/dashboard")
	public String dashboard(Model model ,HttpServletRequest requestsession)
	{
		//Iterable<FormEntry> iFormEntry = this.formEntryRepository.findAll();
		//List<FormEntry> lFormEntry = new ArrayList<>();
		//iFormEntry.forEach(lFormEntry::add);
		//model.addAttribute("lFormEntry", lFormEntry);
		Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> lRequestTransaction = new ArrayList<>();
		iRequestTransaction.forEach(lRequestTransaction::add);
		model.addAttribute("lRequestTransaction", lRequestTransaction);
		model.addAttribute("CountlRequestTransaction", lRequestTransaction.size());
		Iterable<RequestTransaction> iRequestTransaction2 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> lRequestTransaction2 = new ArrayList<>();
		iRequestTransaction2.forEach(lRequestTransaction2::add);
		model.addAttribute("lRequestTransaction2", lRequestTransaction2);
		model.addAttribute("CountlRequestTransaction2", lRequestTransaction2.size());
		Iterable<RequestTransaction> iRequestTransaction3 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> lRequestTransaction3 = new ArrayList<>();
		iRequestTransaction3.forEach(lRequestTransaction3::add);
		model.addAttribute("lRequestTransaction3", lRequestTransaction3);
		model.addAttribute("CountlRequestTransaction3", lRequestTransaction3.size());
		model.addAttribute("Sum1", lRequestTransaction.size()+lRequestTransaction2.size()+lRequestTransaction3.size());

		Iterable<RequestTransaction> iRequestTransaction_2 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> lRequestTransaction_2 = new ArrayList<>();
		iRequestTransaction_2.forEach(lRequestTransaction_2::add);

		Iterable<RequestTransaction> iRequestTransaction2_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> lRequestTransaction2_2 = new ArrayList<>();
		iRequestTransaction2_2.forEach(lRequestTransaction2_2::add);

		Iterable<RequestTransaction> iRequestTransaction3_2 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> lRequestTransaction3_2 = new ArrayList<>();
		iRequestTransaction3_2.forEach(lRequestTransaction3_2::add);
		model.addAttribute("Sum2", lRequestTransaction_2.size()+lRequestTransaction2_2.size()+lRequestTransaction3_2.size());

		Iterable<RequestTransaction> iRequestTransaction_3 = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> lRequestTransaction_3 = new ArrayList<>();
		iRequestTransaction_3.forEach(lRequestTransaction_3::add);

		Iterable<RequestTransaction> iRequestTransaction2_3 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> lRequestTransaction2_3 = new ArrayList<>();
		iRequestTransaction2_3.forEach(lRequestTransaction2_3::add);

		Iterable<RequestTransaction> iRequestTransaction3_3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> lRequestTransaction3_3 = new ArrayList<>();
		iRequestTransaction3_3.forEach(lRequestTransaction3_3::add);
		model.addAttribute("Sum3", lRequestTransaction_3.size()+lRequestTransaction2_3.size()+lRequestTransaction3_3.size());


		Iterable<RequestTransaction> iRequestTransaction_4 = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> lRequestTransaction_4 = new ArrayList<>();
		iRequestTransaction_4.forEach(lRequestTransaction_4::add);

		Iterable<RequestTransaction> iRequestTransaction2_4 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> lRequestTransaction2_4 = new ArrayList<>();
		iRequestTransaction2_4.forEach(lRequestTransaction2_4::add);

		Iterable<RequestTransaction> iRequestTransaction3_4 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> lRequestTransaction3_4 = new ArrayList<>();
		iRequestTransaction3_4.forEach(lRequestTransaction3_4::add);
		model.addAttribute("Sum4", lRequestTransaction_4.size()+lRequestTransaction2_4.size()+lRequestTransaction3_4.size());


		Iterable<RequestTransaction> iRequestTransaction_5 = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> lRequestTransaction_5 = new ArrayList<>();
		iRequestTransaction_5.forEach(lRequestTransaction_5::add);

		Iterable<RequestTransaction> iRequestTransaction2_5 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> lRequestTransaction2_5 = new ArrayList<>();
		iRequestTransaction2_5.forEach(lRequestTransaction2_5::add);

		Iterable<RequestTransaction> iRequestTransaction3_5 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> lRequestTransaction3_5 = new ArrayList<>();
		iRequestTransaction3_5.forEach(lRequestTransaction3_5::add);
		model.addAttribute("Sum5", lRequestTransaction_5.size()+lRequestTransaction2_5.size()+lRequestTransaction3_5.size());

		Iterable<RequestTransaction> das_1_1 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> das1_1 = new ArrayList<>();
		das_1_1.forEach(das1_1::add);
		Iterable<RequestTransaction> das_1_2 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> das1_2 = new ArrayList<>();
		das_1_2.forEach(das1_2::add);
		Iterable<RequestTransaction> das_1_3 = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> das1_3 = new ArrayList<>();
		das_1_3.forEach(das1_3::add);
		Iterable<RequestTransaction> das_1_4 = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> das1_4 = new ArrayList<>();
		das_1_4.forEach(das1_4::add);
		Iterable<RequestTransaction> das_1_5 = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> das1_5 = new ArrayList<>();
		das_1_5.forEach(das1_5::add);
		model.addAttribute("SumDas1", das1_1.size()+das1_2.size()+das1_3.size()+das1_4.size()+das1_5.size());

		Iterable<RequestTransaction> das_2_1 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> das2_1 = new ArrayList<>();
		das_2_1.forEach(das2_1::add);
		Iterable<RequestTransaction> das_2_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> das2_2 = new ArrayList<>();
		das_2_2.forEach(das2_2::add);
		Iterable<RequestTransaction> das_2_3 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> das2_3 = new ArrayList<>();
		das_2_3.forEach(das2_3::add);
		Iterable<RequestTransaction> das_2_4 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> das2_4 = new ArrayList<>();
		das_2_4.forEach(das2_4::add);
		Iterable<RequestTransaction> das_2_5 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> das2_5 = new ArrayList<>();
		das_2_5.forEach(das2_5::add);
		model.addAttribute("SumDas2", das2_1.size()+das2_2.size()+das2_3.size()+das2_4.size()+das2_5.size());

		Iterable<RequestTransaction> das_3_1 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> das3_1 = new ArrayList<>();
		das_3_1.forEach(das3_1::add);
		Iterable<RequestTransaction> das_3_2 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> das3_2 = new ArrayList<>();
		das_3_2.forEach(das3_2::add);
		Iterable<RequestTransaction> das_3_3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> das3_3 = new ArrayList<>();
		das_3_3.forEach(das3_3::add);
		Iterable<RequestTransaction> das_3_4 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> das3_4 = new ArrayList<>();
		das_3_4.forEach(das3_4::add);
		Iterable<RequestTransaction> das_3_5 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> das3_5 = new ArrayList<>();
		das_3_5.forEach(das3_5::add);
		model.addAttribute("SumDas3", das3_1.size()+das3_2.size()+das3_3.size()+das3_4.size()+das3_5.size());


		Iterable<RequestTransaction> das4_1 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> das_4_1 = new ArrayList<>();
		das4_1.forEach(das_4_1::add);
		Iterable<RequestTransaction> das4_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> das_4_2 = new ArrayList<>();
		das4_2.forEach(das_4_2::add);
		Iterable<RequestTransaction> das4_3 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> das_4_3 = new ArrayList<>();
		das4_3.forEach(das_4_3::add);
		model.addAttribute("SumDas4", das_4_1.size()+das_4_2.size()+das_4_3.size());




		model.addAttribute("pagename", "dashboard");
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
		model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
		model.addAttribute("role",requestsession.getSession().getAttribute("role"));
		return "officer/dashboard";
	}

	@RequestMapping(value="/officer/processing")
	public String processing(Model model ,HttpServletRequest requestsession)
	{
		Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> lRequestTransaction = new ArrayList<>();
		iRequestTransaction.forEach(lRequestTransaction::add);
		model.addAttribute("lRequestTransaction", lRequestTransaction);
		model.addAttribute("CountlRequestTransaction", lRequestTransaction.size());
		Iterable<RequestTransaction> iRequestTransaction2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> lRequestTransaction2 = new ArrayList<>();
		iRequestTransaction2.forEach(lRequestTransaction2::add);
		model.addAttribute("lRequestTransaction2", lRequestTransaction2);
		model.addAttribute("CountlRequestTransaction2", lRequestTransaction2.size());
		Iterable<RequestTransaction> iRequestTransaction3 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> lRequestTransaction3 = new ArrayList<>();
		iRequestTransaction3.forEach(lRequestTransaction3::add);
		model.addAttribute("lRequestTransaction3", lRequestTransaction3);
		model.addAttribute("CountlRequestTransaction3", lRequestTransaction3.size());
		model.addAttribute("Sum2", lRequestTransaction.size()+lRequestTransaction2.size()+lRequestTransaction3.size());


		Iterable<RequestTransaction> iRequestTransaction_2 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> lRequestTransaction_2 = new ArrayList<>();
		iRequestTransaction_2.forEach(lRequestTransaction_2::add);

		Iterable<RequestTransaction> iRequestTransaction2_2 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> lRequestTransaction2_2 = new ArrayList<>();
		iRequestTransaction2_2.forEach(lRequestTransaction2_2::add);

		Iterable<RequestTransaction> iRequestTransaction3_2 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> lRequestTransaction3_2 = new ArrayList<>();
		iRequestTransaction3_2.forEach(lRequestTransaction3_2::add);

		model.addAttribute("Sum1", lRequestTransaction_2.size()+lRequestTransaction2_2.size()+lRequestTransaction3_2.size());

		Iterable<RequestTransaction> iRequestTransaction_3 = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> lRequestTransaction_3 = new ArrayList<>();
		iRequestTransaction_3.forEach(lRequestTransaction_3::add);

		Iterable<RequestTransaction> iRequestTransaction2_3 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> lRequestTransaction2_3 = new ArrayList<>();
		iRequestTransaction2_3.forEach(lRequestTransaction2_3::add);

		Iterable<RequestTransaction> iRequestTransaction3_3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> lRequestTransaction3_3 = new ArrayList<>();
		iRequestTransaction3_3.forEach(lRequestTransaction3_3::add);
		model.addAttribute("Sum3", lRequestTransaction_3.size()+lRequestTransaction2_3.size()+lRequestTransaction3_3.size());

		Iterable<RequestTransaction> iRequestTransaction_4 = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> lRequestTransaction_4 = new ArrayList<>();
		iRequestTransaction_4.forEach(lRequestTransaction_4::add);

		Iterable<RequestTransaction> iRequestTransaction2_4 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> lRequestTransaction2_4 = new ArrayList<>();
		iRequestTransaction2_4.forEach(lRequestTransaction2_4::add);

		Iterable<RequestTransaction> iRequestTransaction3_4 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> lRequestTransaction3_4 = new ArrayList<>();
		iRequestTransaction3_4.forEach(lRequestTransaction3_4::add);
		model.addAttribute("Sum4", lRequestTransaction_4.size()+lRequestTransaction2_4.size()+lRequestTransaction3_4.size());


		Iterable<RequestTransaction> iRequestTransaction_5 = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> lRequestTransaction_5 = new ArrayList<>();
		iRequestTransaction_5.forEach(lRequestTransaction_5::add);

		Iterable<RequestTransaction> iRequestTransaction2_5 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> lRequestTransaction2_5 = new ArrayList<>();
		iRequestTransaction2_5.forEach(lRequestTransaction2_5::add);

		Iterable<RequestTransaction> iRequestTransaction3_5 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> lRequestTransaction3_5 = new ArrayList<>();
		iRequestTransaction3_5.forEach(lRequestTransaction3_5::add);
		model.addAttribute("Sum5", lRequestTransaction_5.size()+lRequestTransaction2_5.size()+lRequestTransaction3_5.size());


		Iterable<RequestTransaction> das_1_1 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> das1_1 = new ArrayList<>();
		das_1_1.forEach(das1_1::add);
		Iterable<RequestTransaction> das_1_2 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> das1_2 = new ArrayList<>();
		das_1_2.forEach(das1_2::add);
		Iterable<RequestTransaction> das_1_3 = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> das1_3 = new ArrayList<>();
		das_1_3.forEach(das1_3::add);
		Iterable<RequestTransaction> das_1_4 = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> das1_4 = new ArrayList<>();
		das_1_4.forEach(das1_4::add);
		Iterable<RequestTransaction> das_1_5 = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> das1_5 = new ArrayList<>();
		das_1_5.forEach(das1_5::add);
		model.addAttribute("SumDas1", das1_1.size()+das1_2.size()+das1_3.size()+das1_4.size()+das1_5.size());

		Iterable<RequestTransaction> das_2_1 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> das2_1 = new ArrayList<>();
		das_2_1.forEach(das2_1::add);
		Iterable<RequestTransaction> das_2_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> das2_2 = new ArrayList<>();
		das_2_2.forEach(das2_2::add);
		Iterable<RequestTransaction> das_2_3 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> das2_3 = new ArrayList<>();
		das_2_3.forEach(das2_3::add);
		Iterable<RequestTransaction> das_2_4 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> das2_4 = new ArrayList<>();
		das_2_4.forEach(das2_4::add);
		Iterable<RequestTransaction> das_2_5 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> das2_5 = new ArrayList<>();
		das_2_5.forEach(das2_5::add);
		model.addAttribute("SumDas2", das2_1.size()+das2_2.size()+das2_3.size()+das2_4.size()+das2_5.size());

		Iterable<RequestTransaction> das_3_1 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> das3_1 = new ArrayList<>();
		das_3_1.forEach(das3_1::add);
		Iterable<RequestTransaction> das_3_2 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> das3_2 = new ArrayList<>();
		das_3_2.forEach(das3_2::add);
		Iterable<RequestTransaction> das_3_3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> das3_3 = new ArrayList<>();
		das_3_3.forEach(das3_3::add);
		Iterable<RequestTransaction> das_3_4 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> das3_4 = new ArrayList<>();
		das_3_4.forEach(das3_4::add);
		Iterable<RequestTransaction> das_3_5 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> das3_5 = new ArrayList<>();
		das_3_5.forEach(das3_5::add);
		model.addAttribute("SumDas3", das3_1.size()+das3_2.size()+das3_3.size()+das3_4.size()+das3_5.size());


		Iterable<RequestTransaction> das4_1 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> das_4_1 = new ArrayList<>();
		das4_1.forEach(das_4_1::add);
		Iterable<RequestTransaction> das4_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> das_4_2 = new ArrayList<>();
		das4_2.forEach(das_4_2::add);
		Iterable<RequestTransaction> das4_3 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> das_4_3 = new ArrayList<>();
		das4_3.forEach(das_4_3::add);
		model.addAttribute("SumDas4", das_4_1.size()+das_4_2.size()+das_4_3.size());



		model.addAttribute("pagename", "processing");
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
model.addAttribute("role",requestsession.getSession().getAttribute("role"));
		return "officer/processing";
	}

	@RequestMapping(value="/officer/withdraw")
	public String withdraw(Model model ,HttpServletRequest requestsession)
	{
		Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> lRequestTransaction = new ArrayList<>();
		iRequestTransaction.forEach(lRequestTransaction::add);
		model.addAttribute("lRequestTransaction", lRequestTransaction);
		model.addAttribute("CountlRequestTransaction", lRequestTransaction.size());
		Iterable<RequestTransaction> iRequestTransaction2 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> lRequestTransaction2 = new ArrayList<>();
		iRequestTransaction2.forEach(lRequestTransaction2::add);
		model.addAttribute("lRequestTransaction2", lRequestTransaction2);
		model.addAttribute("CountlRequestTransaction2", lRequestTransaction2.size());
		Iterable<RequestTransaction> iRequestTransaction3 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> lRequestTransaction3 = new ArrayList<>();
		iRequestTransaction3.forEach(lRequestTransaction3::add);
		model.addAttribute("lRequestTransaction3", lRequestTransaction3);
		model.addAttribute("CountlRequestTransaction3", lRequestTransaction3.size());
		model.addAttribute("Sum4", lRequestTransaction.size()+lRequestTransaction2.size()+lRequestTransaction3.size());


		Iterable<RequestTransaction> iRequestTransaction_1 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> lRequestTransaction_1 = new ArrayList<>();
		iRequestTransaction_1.forEach(lRequestTransaction_1::add);

		Iterable<RequestTransaction> iRequestTransaction2_1 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> lRequestTransaction2_1 = new ArrayList<>();
		iRequestTransaction2_1.forEach(lRequestTransaction2_1::add);

		Iterable<RequestTransaction> iRequestTransaction3_1 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> lRequestTransaction3_1 = new ArrayList<>();
		iRequestTransaction3_1.forEach(lRequestTransaction3_1::add);

		model.addAttribute("Sum1", lRequestTransaction_1.size()+lRequestTransaction2_1.size()+lRequestTransaction3_1.size());


		Iterable<RequestTransaction> iRequestTransaction_2 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> lRequestTransaction_2 = new ArrayList<>();
		iRequestTransaction_2.forEach(lRequestTransaction_2::add);

		Iterable<RequestTransaction> iRequestTransaction2_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> lRequestTransaction2_2 = new ArrayList<>();
		iRequestTransaction2_2.forEach(lRequestTransaction2_2::add);

		Iterable<RequestTransaction> iRequestTransaction3_2 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> lRequestTransaction3_2 = new ArrayList<>();
		iRequestTransaction3_2.forEach(lRequestTransaction3_2::add);
		model.addAttribute("Sum2", lRequestTransaction_2.size()+lRequestTransaction2_2.size()+lRequestTransaction3_2.size());


		Iterable<RequestTransaction> iRequestTransaction_3 = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> lRequestTransaction_3 = new ArrayList<>();
		iRequestTransaction_3.forEach(lRequestTransaction_3::add);

		Iterable<RequestTransaction> iRequestTransaction2_3 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> lRequestTransaction2_3 = new ArrayList<>();
		iRequestTransaction2_3.forEach(lRequestTransaction2_3::add);

		Iterable<RequestTransaction> iRequestTransaction3_3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> lRequestTransaction3_3 = new ArrayList<>();
		iRequestTransaction3_3.forEach(lRequestTransaction3_3::add);
		model.addAttribute("Sum3", lRequestTransaction_3.size()+lRequestTransaction2_3.size()+lRequestTransaction3_3.size());

		Iterable<RequestTransaction> iRequestTransaction_5 = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> lRequestTransaction_5 = new ArrayList<>();
		iRequestTransaction_5.forEach(lRequestTransaction_5::add);

		Iterable<RequestTransaction> iRequestTransaction2_5 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> lRequestTransaction2_5 = new ArrayList<>();
		iRequestTransaction2_5.forEach(lRequestTransaction2_5::add);

		Iterable<RequestTransaction> iRequestTransaction3_5 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> lRequestTransaction3_5 = new ArrayList<>();
		iRequestTransaction3_5.forEach(lRequestTransaction3_5::add);
		model.addAttribute("Sum5", lRequestTransaction_5.size()+lRequestTransaction2_5.size()+lRequestTransaction3_5.size());



		Iterable<RequestTransaction> das_1_1 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> das1_1 = new ArrayList<>();
		das_1_1.forEach(das1_1::add);
		Iterable<RequestTransaction> das_1_2 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> das1_2 = new ArrayList<>();
		das_1_2.forEach(das1_2::add);
		Iterable<RequestTransaction> das_1_3 = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> das1_3 = new ArrayList<>();
		das_1_3.forEach(das1_3::add);
		Iterable<RequestTransaction> das_1_4 = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> das1_4 = new ArrayList<>();
		das_1_4.forEach(das1_4::add);
		Iterable<RequestTransaction> das_1_5 = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> das1_5 = new ArrayList<>();
		das_1_5.forEach(das1_5::add);
		model.addAttribute("SumDas1", das1_1.size()+das1_2.size()+das1_3.size()+das1_4.size()+das1_5.size());

		Iterable<RequestTransaction> das_2_1 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> das2_1 = new ArrayList<>();
		das_2_1.forEach(das2_1::add);
		Iterable<RequestTransaction> das_2_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> das2_2 = new ArrayList<>();
		das_2_2.forEach(das2_2::add);
		Iterable<RequestTransaction> das_2_3 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> das2_3 = new ArrayList<>();
		das_2_3.forEach(das2_3::add);
		Iterable<RequestTransaction> das_2_4 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> das2_4 = new ArrayList<>();
		das_2_4.forEach(das2_4::add);
		Iterable<RequestTransaction> das_2_5 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> das2_5 = new ArrayList<>();
		das_2_5.forEach(das2_5::add);
		model.addAttribute("SumDas2", das2_1.size()+das2_2.size()+das2_3.size()+das2_4.size()+das2_5.size());

		Iterable<RequestTransaction> das_3_1 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> das3_1 = new ArrayList<>();
		das_3_1.forEach(das3_1::add);
		Iterable<RequestTransaction> das_3_2 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> das3_2 = new ArrayList<>();
		das_3_2.forEach(das3_2::add);
		Iterable<RequestTransaction> das_3_3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> das3_3 = new ArrayList<>();
		das_3_3.forEach(das3_3::add);
		Iterable<RequestTransaction> das_3_4 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> das3_4 = new ArrayList<>();
		das_3_4.forEach(das3_4::add);
		Iterable<RequestTransaction> das_3_5 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> das3_5 = new ArrayList<>();
		das_3_5.forEach(das3_5::add);
		model.addAttribute("SumDas3", das3_1.size()+das3_2.size()+das3_3.size()+das3_4.size()+das3_5.size());


		Iterable<RequestTransaction> das4_1 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> das_4_1 = new ArrayList<>();
		das4_1.forEach(das_4_1::add);
		Iterable<RequestTransaction> das4_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> das_4_2 = new ArrayList<>();
		das4_2.forEach(das_4_2::add);
		Iterable<RequestTransaction> das4_3 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> das_4_3 = new ArrayList<>();
		das4_3.forEach(das_4_3::add);
		model.addAttribute("SumDas4", das_4_1.size()+das_4_2.size()+das_4_3.size());



		model.addAttribute("pagename", "withdraw");
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
model.addAttribute("role",requestsession.getSession().getAttribute("role"));
		return "officer/withdraw";
	}
	@RequestMapping(value="/officer/complete")
	public String complete(Model model ,HttpServletRequest requestsession)
	{
		Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> lRequestTransaction = new ArrayList<>();
		iRequestTransaction.forEach(lRequestTransaction::add);
		model.addAttribute("lRequestTransaction", lRequestTransaction);
		model.addAttribute("CountlRequestTransaction", lRequestTransaction.size());

		Iterable<RequestTransaction> iRequestTransaction2 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> lRequestTransaction2 = new ArrayList<>();
		iRequestTransaction2.forEach(lRequestTransaction2::add);
		model.addAttribute("lRequestTransaction2", lRequestTransaction2);
		model.addAttribute("CountlRequestTransaction2", lRequestTransaction2.size());

		Iterable<RequestTransaction> iRequestTransaction3 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> lRequestTransaction3 = new ArrayList<>();
		iRequestTransaction3.forEach(lRequestTransaction3::add);
		model.addAttribute("lRequestTransaction3", lRequestTransaction3);
		model.addAttribute("CountlRequestTransaction3", lRequestTransaction3.size());

		model.addAttribute("Sum5", lRequestTransaction.size()+lRequestTransaction2.size()+lRequestTransaction3.size());


		Iterable<RequestTransaction> iRequestTransaction_1 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> lRequestTransaction_1 = new ArrayList<>();
		iRequestTransaction_1.forEach(lRequestTransaction_1::add);

		Iterable<RequestTransaction> iRequestTransaction2_1 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> lRequestTransaction2_1 = new ArrayList<>();
		iRequestTransaction2_1.forEach(lRequestTransaction2_1::add);

		Iterable<RequestTransaction> iRequestTransaction3_1 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> lRequestTransaction3_1 = new ArrayList<>();
		iRequestTransaction3_1.forEach(lRequestTransaction3_1::add);

		model.addAttribute("Sum1", lRequestTransaction_1.size()+lRequestTransaction2_1.size()+lRequestTransaction3_1.size());


		Iterable<RequestTransaction> iRequestTransaction_2 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> lRequestTransaction_2 = new ArrayList<>();
		iRequestTransaction_2.forEach(lRequestTransaction_2::add);

		Iterable<RequestTransaction> iRequestTransaction2_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> lRequestTransaction2_2 = new ArrayList<>();
		iRequestTransaction2_2.forEach(lRequestTransaction2_2::add);

		Iterable<RequestTransaction> iRequestTransaction3_2 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> lRequestTransaction3_2 = new ArrayList<>();
		iRequestTransaction3_2.forEach(lRequestTransaction3_2::add);
		model.addAttribute("Sum2", lRequestTransaction_2.size()+lRequestTransaction2_2.size()+lRequestTransaction3_2.size());



		Iterable<RequestTransaction> iRequestTransaction_3 = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> lRequestTransaction_3 = new ArrayList<>();
		iRequestTransaction_3.forEach(lRequestTransaction_3::add);

		Iterable<RequestTransaction> iRequestTransaction2_3 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> lRequestTransaction2_3 = new ArrayList<>();
		iRequestTransaction2_3.forEach(lRequestTransaction2_3::add);

		Iterable<RequestTransaction> iRequestTransaction3_3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> lRequestTransaction3_3 = new ArrayList<>();
		iRequestTransaction3_3.forEach(lRequestTransaction3_3::add);
		model.addAttribute("Sum3", lRequestTransaction_3.size()+lRequestTransaction2_3.size()+lRequestTransaction3_3.size());


		Iterable<RequestTransaction> iRequestTransaction_4 = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> lRequestTransaction_4 = new ArrayList<>();
		iRequestTransaction_4.forEach(lRequestTransaction_4::add);

		Iterable<RequestTransaction> iRequestTransaction2_4 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> lRequestTransaction2_4 = new ArrayList<>();
		iRequestTransaction2_4.forEach(lRequestTransaction2_4::add);

		Iterable<RequestTransaction> iRequestTransaction3_4 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> lRequestTransaction3_4 = new ArrayList<>();
		iRequestTransaction3_4.forEach(lRequestTransaction3_4::add);
		model.addAttribute("Sum4", lRequestTransaction_4.size()+lRequestTransaction2_4.size()+lRequestTransaction3_4.size());


		Iterable<RequestTransaction> das_1_1 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> das1_1 = new ArrayList<>();
		das_1_1.forEach(das1_1::add);
		Iterable<RequestTransaction> das_1_2 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> das1_2 = new ArrayList<>();
		das_1_2.forEach(das1_2::add);
		Iterable<RequestTransaction> das_1_3 = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> das1_3 = new ArrayList<>();
		das_1_3.forEach(das1_3::add);
		Iterable<RequestTransaction> das_1_4 = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> das1_4 = new ArrayList<>();
		das_1_4.forEach(das1_4::add);
		Iterable<RequestTransaction> das_1_5 = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> das1_5 = new ArrayList<>();
		das_1_5.forEach(das1_5::add);
		model.addAttribute("SumDas1", das1_1.size()+das1_2.size()+das1_3.size()+das1_4.size()+das1_5.size());

		Iterable<RequestTransaction> das_2_1 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> das2_1 = new ArrayList<>();
		das_2_1.forEach(das2_1::add);
		Iterable<RequestTransaction> das_2_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> das2_2 = new ArrayList<>();
		das_2_2.forEach(das2_2::add);
		Iterable<RequestTransaction> das_2_3 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> das2_3 = new ArrayList<>();
		das_2_3.forEach(das2_3::add);
		Iterable<RequestTransaction> das_2_4 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> das2_4 = new ArrayList<>();
		das_2_4.forEach(das2_4::add);
		Iterable<RequestTransaction> das_2_5 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> das2_5 = new ArrayList<>();
		das_2_5.forEach(das2_5::add);
		model.addAttribute("SumDas2", das2_1.size()+das2_2.size()+das2_3.size()+das2_4.size()+das2_5.size());

		Iterable<RequestTransaction> das_3_1 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> das3_1 = new ArrayList<>();
		das_3_1.forEach(das3_1::add);
		Iterable<RequestTransaction> das_3_2 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> das3_2 = new ArrayList<>();
		das_3_2.forEach(das3_2::add);
		Iterable<RequestTransaction> das_3_3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> das3_3 = new ArrayList<>();
		das_3_3.forEach(das3_3::add);
		Iterable<RequestTransaction> das_3_4 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> das3_4 = new ArrayList<>();
		das_3_4.forEach(das3_4::add);
		Iterable<RequestTransaction> das_3_5 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> das3_5 = new ArrayList<>();
		das_3_5.forEach(das3_5::add);
		model.addAttribute("SumDas3", das3_1.size()+das3_2.size()+das3_3.size()+das3_4.size()+das3_5.size());


		Iterable<RequestTransaction> das4_1 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> das_4_1 = new ArrayList<>();
		das4_1.forEach(das_4_1::add);
		Iterable<RequestTransaction> das4_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> das_4_2 = new ArrayList<>();
		das4_2.forEach(das_4_2::add);
		Iterable<RequestTransaction> das4_3 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> das_4_3 = new ArrayList<>();
		das4_3.forEach(das_4_3::add);
		model.addAttribute("SumDas4", das_4_1.size()+das_4_2.size()+das_4_3.size());


		model.addAttribute("pagename", "complete");
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
model.addAttribute("role",requestsession.getSession().getAttribute("role"));
		return "officer/complete";
	}
	@RequestMapping(value="/officer/accept/{rtId}")
	public String accept(@PathVariable("rtId") Long rtId,Model model,HttpServletRequest requestsession)
	{
		//Iterable<FormEntry> iFormEntry = this.formEntryRepository.findAll();
		//List<FormEntry> lFormEntry = new ArrayList<>();
		//iFormEntry.forEach(lFormEntry::add);
		//model.addAttribute("lFormEntry", lFormEntry);
		RequestTransaction RTData = this.requestTransactionRepository.findByRtId(rtId);
		Iterable<Evaluate> ev = this.evaluateRepository.findEvaluateListByRtIdAndRequestType(RTData.getRequest().getRequestId(),RTData.getRequestType().getRequestTypeId());
				List<Evaluate> lEv = new ArrayList<>();
				ev.forEach(lEv::add);
			    model.addAttribute("lEv", lEv);
		model.addAttribute("rtId", rtId);
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
model.addAttribute("role",requestsession.getSession().getAttribute("role"));		return "officer/accept";
	}

	@RequestMapping(value="/officer/edit-doc/{rtId}")
	public String edit_doc(@PathVariable("rtId") Long rtId,Model model,HttpServletRequest requestsession)
	{
		//govDocumentCreateRepository
		RequestTransaction RTData = this.requestTransactionRepository.findByRtId(rtId);
		Iterable<GovDocumentCreate> gc = this.govDocumentCreateRepository.findGovDocumentCreateListByRtId(RTData.getRequest().getRequestId());
				List<GovDocumentCreate> lGc = new ArrayList<>();
				gc.forEach(lGc::add);
			    model.addAttribute("lGc", lGc);
		model.addAttribute("rtId", rtId);
		model.addAttribute("RTData", RTData);
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
model.addAttribute("role",requestsession.getSession().getAttribute("role"));		return "officer/edit_doc";
	}
	@RequestMapping(value="/officer/keep-file/{rtId}")
	public String keep_file(@PathVariable("rtId") Long rtId,Model model,HttpServletRequest requestsession)
	{

		Iterable<FileStorage> fileStorage = this.fileStorageRepository.findByrtId(rtId);
		List<FileStorage> ifileStorage = new ArrayList<>();
		fileStorage.forEach(ifileStorage::add);

		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
model.addAttribute("role",requestsession.getSession().getAttribute("role"));		model.addAttribute("iFileStorage", ifileStorage);
		model.addAttribute("rtId", rtId);

		return "officer/keep_file";

	}

	@RequestMapping(value="/officer/document-storage")
	public String document_storage(Model model ,HttpServletRequest requestsession)
	{
		Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> lRequestTransaction = new ArrayList<>();
		iRequestTransaction.forEach(lRequestTransaction::add);
		model.addAttribute("lRequestTransaction", lRequestTransaction);
		model.addAttribute("CountlRequestTransaction", lRequestTransaction.size());

		Iterable<RequestTransaction> iRequestTransaction2 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> lRequestTransaction2 = new ArrayList<>();
		iRequestTransaction2.forEach(lRequestTransaction2::add);
		model.addAttribute("lRequestTransaction2", lRequestTransaction2);
		model.addAttribute("CountlRequestTransaction2", lRequestTransaction2.size());

		Iterable<RequestTransaction> iRequestTransaction3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> lRequestTransaction3 = new ArrayList<>();
		iRequestTransaction3.forEach(lRequestTransaction3::add);
		model.addAttribute("lRequestTransaction3", lRequestTransaction3);
		model.addAttribute("CountlRequestTransaction3", lRequestTransaction3.size());

		model.addAttribute("Sum3", lRequestTransaction.size()+lRequestTransaction2.size()+lRequestTransaction3.size());

		Iterable<RequestTransaction> iRequestTransaction_2 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> lRequestTransaction_2 = new ArrayList<>();
		iRequestTransaction_2.forEach(lRequestTransaction_2::add);

		Iterable<RequestTransaction> iRequestTransaction2_2 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> lRequestTransaction2_2 = new ArrayList<>();
		iRequestTransaction2_2.forEach(lRequestTransaction2_2::add);

		Iterable<RequestTransaction> iRequestTransaction3_2 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> lRequestTransaction3_2 = new ArrayList<>();
		iRequestTransaction3_2.forEach(lRequestTransaction3_2::add);

		model.addAttribute("Sum1", lRequestTransaction_2.size()+lRequestTransaction2_2.size()+lRequestTransaction3_2.size());

		Iterable<RequestTransaction> iRequestTransaction_3 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> lRequestTransaction_3 = new ArrayList<>();
		iRequestTransaction_3.forEach(lRequestTransaction_3::add);

		Iterable<RequestTransaction> iRequestTransaction2_3 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> lRequestTransaction2_3 = new ArrayList<>();
		iRequestTransaction2_3.forEach(lRequestTransaction2_3::add);

		Iterable<RequestTransaction> iRequestTransaction3_3 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> lRequestTransaction3_3 = new ArrayList<>();
		iRequestTransaction3_3.forEach(lRequestTransaction3_3::add);
		model.addAttribute("Sum2", lRequestTransaction_3.size()+lRequestTransaction2_3.size()+lRequestTransaction3_3.size());

		Iterable<RequestTransaction> iRequestTransaction_4 = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> lRequestTransaction_4 = new ArrayList<>();
		iRequestTransaction_4.forEach(lRequestTransaction_4::add);

		Iterable<RequestTransaction> iRequestTransaction2_4 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> lRequestTransaction2_4 = new ArrayList<>();
		iRequestTransaction2_4.forEach(lRequestTransaction2_4::add);

		Iterable<RequestTransaction> iRequestTransaction3_4 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> lRequestTransaction3_4 = new ArrayList<>();
		iRequestTransaction3_4.forEach(lRequestTransaction3_4::add);
		model.addAttribute("Sum4", lRequestTransaction_4.size()+lRequestTransaction2_4.size()+lRequestTransaction3_4.size());


		Iterable<RequestTransaction> iRequestTransaction_5 = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> lRequestTransaction_5 = new ArrayList<>();
		iRequestTransaction_5.forEach(lRequestTransaction_5::add);

		Iterable<RequestTransaction> iRequestTransaction2_5 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> lRequestTransaction2_5 = new ArrayList<>();
		iRequestTransaction2_5.forEach(lRequestTransaction2_5::add);

		Iterable<RequestTransaction> iRequestTransaction3_5 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> lRequestTransaction3_5 = new ArrayList<>();
		iRequestTransaction3_5.forEach(lRequestTransaction3_5::add);
		model.addAttribute("Sum5", lRequestTransaction_5.size()+lRequestTransaction2_5.size()+lRequestTransaction3_5.size());

		Iterable<RequestTransaction> das_1_1 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> das1_1 = new ArrayList<>();
		das_1_1.forEach(das1_1::add);
		Iterable<RequestTransaction> das_1_2 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> das1_2 = new ArrayList<>();
		das_1_2.forEach(das1_2::add);
		Iterable<RequestTransaction> das_1_3 = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> das1_3 = new ArrayList<>();
		das_1_3.forEach(das1_3::add);
		Iterable<RequestTransaction> das_1_4 = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> das1_4 = new ArrayList<>();
		das_1_4.forEach(das1_4::add);
		Iterable<RequestTransaction> das_1_5 = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> das1_5 = new ArrayList<>();
		das_1_5.forEach(das1_5::add);
		model.addAttribute("SumDas1", das1_1.size()+das1_2.size()+das1_3.size()+das1_4.size()+das1_5.size());

		Iterable<RequestTransaction> das_2_1 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> das2_1 = new ArrayList<>();
		das_2_1.forEach(das2_1::add);
		Iterable<RequestTransaction> das_2_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> das2_2 = new ArrayList<>();
		das_2_2.forEach(das2_2::add);
		Iterable<RequestTransaction> das_2_3 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> das2_3 = new ArrayList<>();
		das_2_3.forEach(das2_3::add);
		Iterable<RequestTransaction> das_2_4 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> das2_4 = new ArrayList<>();
		das_2_4.forEach(das2_4::add);
		Iterable<RequestTransaction> das_2_5 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> das2_5 = new ArrayList<>();
		das_2_5.forEach(das2_5::add);
		model.addAttribute("SumDas2", das2_1.size()+das2_2.size()+das2_3.size()+das2_4.size()+das2_5.size());

		Iterable<RequestTransaction> das_3_1 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> das3_1 = new ArrayList<>();
		das_3_1.forEach(das3_1::add);
		Iterable<RequestTransaction> das_3_2 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> das3_2 = new ArrayList<>();
		das_3_2.forEach(das3_2::add);
		Iterable<RequestTransaction> das_3_3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> das3_3 = new ArrayList<>();
		das_3_3.forEach(das3_3::add);
		Iterable<RequestTransaction> das_3_4 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> das3_4 = new ArrayList<>();
		das_3_4.forEach(das3_4::add);
		Iterable<RequestTransaction> das_3_5 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> das3_5 = new ArrayList<>();
		das_3_5.forEach(das3_5::add);
		model.addAttribute("SumDas3", das3_1.size()+das3_2.size()+das3_3.size()+das3_4.size()+das3_5.size());


		Iterable<RequestTransaction> das4_1 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> das_4_1 = new ArrayList<>();
		das4_1.forEach(das_4_1::add);
		Iterable<RequestTransaction> das4_2 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> das_4_2 = new ArrayList<>();
		das4_2.forEach(das_4_2::add);
		Iterable<RequestTransaction> das4_3 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> das_4_3 = new ArrayList<>();
		das4_3.forEach(das_4_3::add);
		model.addAttribute("SumDas4", das_4_1.size()+das_4_2.size()+das_4_3.size());


		model.addAttribute("pagename", "documentstorage");
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
		model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
		model.addAttribute("role",requestsession.getSession().getAttribute("role"));
		return "officer/document_storage";
	}
	@RequestMapping(value="/officer/search")
	public String search(Model model ,HttpServletRequest requestsession)
	{
		Iterable<Company> Icompanylist = companyNewRepository.findAll();
		List<Company> lcompanylist = new ArrayList<>();
		Icompanylist.forEach(lcompanylist::add);
		model.addAttribute("lcompanylist", lcompanylist);

		Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> lRequestTransaction = new ArrayList<>();
		iRequestTransaction.forEach(lRequestTransaction::add);

		Iterable<RequestTransaction> iRequestTransaction2 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> lRequestTransaction2 = new ArrayList<>();
		iRequestTransaction2.forEach(lRequestTransaction2::add);

		Iterable<RequestTransaction> iRequestTransaction3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> lRequestTransaction3 = new ArrayList<>();
		iRequestTransaction3.forEach(lRequestTransaction3::add);

		model.addAttribute("Sum3", lRequestTransaction.size()+lRequestTransaction2.size()+lRequestTransaction3.size());

		Iterable<RequestTransaction> iRequestTransaction_2 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> lRequestTransaction_2 = new ArrayList<>();
		iRequestTransaction_2.forEach(lRequestTransaction_2::add);

		Iterable<RequestTransaction> iRequestTransaction2_2 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> lRequestTransaction2_2 = new ArrayList<>();
		iRequestTransaction2_2.forEach(lRequestTransaction2_2::add);

		Iterable<RequestTransaction> iRequestTransaction3_2 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> lRequestTransaction3_2 = new ArrayList<>();
		iRequestTransaction3_2.forEach(lRequestTransaction3_2::add);

		model.addAttribute("Sum1", lRequestTransaction_2.size()+lRequestTransaction2_2.size()+lRequestTransaction3_2.size());

		Iterable<RequestTransaction> iRequestTransaction_3 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> lRequestTransaction_3 = new ArrayList<>();
		iRequestTransaction_3.forEach(lRequestTransaction_3::add);

		Iterable<RequestTransaction> iRequestTransaction2_3 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> lRequestTransaction2_3 = new ArrayList<>();
		iRequestTransaction2_3.forEach(lRequestTransaction2_3::add);

		Iterable<RequestTransaction> iRequestTransaction3_3 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> lRequestTransaction3_3 = new ArrayList<>();
		iRequestTransaction3_3.forEach(lRequestTransaction3_3::add);
		model.addAttribute("Sum2", lRequestTransaction_3.size()+lRequestTransaction2_3.size()+lRequestTransaction3_3.size());

		Iterable<RequestTransaction> iRequestTransaction_4 = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> lRequestTransaction_4 = new ArrayList<>();
		iRequestTransaction_4.forEach(lRequestTransaction_4::add);

		Iterable<RequestTransaction> iRequestTransaction2_4 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> lRequestTransaction2_4 = new ArrayList<>();
		iRequestTransaction2_4.forEach(lRequestTransaction2_4::add);

		Iterable<RequestTransaction> iRequestTransaction3_4 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> lRequestTransaction3_4 = new ArrayList<>();
		iRequestTransaction3_4.forEach(lRequestTransaction3_4::add);
		model.addAttribute("Sum4", lRequestTransaction_4.size()+lRequestTransaction2_4.size()+lRequestTransaction3_4.size());


		Iterable<RequestTransaction> iRequestTransaction_5 = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> lRequestTransaction_5 = new ArrayList<>();
		iRequestTransaction_5.forEach(lRequestTransaction_5::add);

		Iterable<RequestTransaction> iRequestTransaction2_5 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> lRequestTransaction2_5 = new ArrayList<>();
		iRequestTransaction2_5.forEach(lRequestTransaction2_5::add);

		Iterable<RequestTransaction> iRequestTransaction3_5 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> lRequestTransaction3_5 = new ArrayList<>();
		iRequestTransaction3_5.forEach(lRequestTransaction3_5::add);
		model.addAttribute("Sum5", lRequestTransaction_5.size()+lRequestTransaction2_5.size()+lRequestTransaction3_5.size());


		model.addAttribute("pagename", "search");
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
model.addAttribute("role",requestsession.getSession().getAttribute("role"));		return "officer/search";
	}

	@RequestMapping(value="/officer/search-result")
	public String searchResult(@RequestParam("dbdId") String dbdId,
			@RequestParam("dbdIdTxt") String dbdIdTxt,
			@RequestParam("actualGaranteeDay") String actualGaranteeDay,Model model)
	{

		Iterable<RequestTransaction> iRequestTransaction1 = this.requestTransactionRepository.findAllStatus303New();
		List<RequestTransaction> lRequestTransaction1 = new ArrayList<>();
		iRequestTransaction1.forEach(lRequestTransaction1::add);

		Iterable<RequestTransaction> iRequestTransaction2 = this.requestTransactionRepository.findAllStatus303Edit();
		List<RequestTransaction> lRequestTransaction2 = new ArrayList<>();
		iRequestTransaction2.forEach(lRequestTransaction2::add);

		Iterable<RequestTransaction> iRequestTransaction3 = this.requestTransactionRepository.findAllStatus303Change();
		List<RequestTransaction> lRequestTransaction3 = new ArrayList<>();
		iRequestTransaction3.forEach(lRequestTransaction3::add);

		model.addAttribute("Sum3", lRequestTransaction1.size()+lRequestTransaction2.size()+lRequestTransaction3.size());

		Iterable<RequestTransaction> iRequestTransaction_2 = this.requestTransactionRepository.findAllStatus0New();
		List<RequestTransaction> lRequestTransaction_2 = new ArrayList<>();
		iRequestTransaction_2.forEach(lRequestTransaction_2::add);

		Iterable<RequestTransaction> iRequestTransaction2_2 = this.requestTransactionRepository.findAllStatus0Edit();
		List<RequestTransaction> lRequestTransaction2_2 = new ArrayList<>();
		iRequestTransaction2_2.forEach(lRequestTransaction2_2::add);

		Iterable<RequestTransaction> iRequestTransaction3_2 = this.requestTransactionRepository.findAllStatus0Change();
		List<RequestTransaction> lRequestTransaction3_2 = new ArrayList<>();
		iRequestTransaction3_2.forEach(lRequestTransaction3_2::add);

		model.addAttribute("Sum1", lRequestTransaction_2.size()+lRequestTransaction2_2.size()+lRequestTransaction3_2.size());

		Iterable<RequestTransaction> iRequestTransaction_3 = this.requestTransactionRepository.findAllStatus101New();
		List<RequestTransaction> lRequestTransaction_3 = new ArrayList<>();
		iRequestTransaction_3.forEach(lRequestTransaction_3::add);

		Iterable<RequestTransaction> iRequestTransaction2_3 = this.requestTransactionRepository.findAllStatus101Edit();
		List<RequestTransaction> lRequestTransaction2_3 = new ArrayList<>();
		iRequestTransaction2_3.forEach(lRequestTransaction2_3::add);

		Iterable<RequestTransaction> iRequestTransaction3_3 = this.requestTransactionRepository.findAllStatus101Change();
		List<RequestTransaction> lRequestTransaction3_3 = new ArrayList<>();
		iRequestTransaction3_3.forEach(lRequestTransaction3_3::add);
		model.addAttribute("Sum2", lRequestTransaction_3.size()+lRequestTransaction2_3.size()+lRequestTransaction3_3.size());

		Iterable<RequestTransaction> iRequestTransaction_4 = this.requestTransactionRepository.findAllWithdrawNew();
		List<RequestTransaction> lRequestTransaction_4 = new ArrayList<>();
		iRequestTransaction_4.forEach(lRequestTransaction_4::add);

		Iterable<RequestTransaction> iRequestTransaction2_4 = this.requestTransactionRepository.findAllWithdrawEdit();
		List<RequestTransaction> lRequestTransaction2_4 = new ArrayList<>();
		iRequestTransaction2_4.forEach(lRequestTransaction2_4::add);

		Iterable<RequestTransaction> iRequestTransaction3_4 = this.requestTransactionRepository.findAllWithdrawChange();
		List<RequestTransaction> lRequestTransaction3_4 = new ArrayList<>();
		iRequestTransaction3_4.forEach(lRequestTransaction3_4::add);
		model.addAttribute("Sum4", lRequestTransaction_4.size()+lRequestTransaction2_4.size()+lRequestTransaction3_4.size());


		Iterable<RequestTransaction> iRequestTransaction_5 = this.requestTransactionRepository.findAllCompleteNew();
		List<RequestTransaction> lRequestTransaction_5 = new ArrayList<>();
		iRequestTransaction_5.forEach(lRequestTransaction_5::add);

		Iterable<RequestTransaction> iRequestTransaction2_5 = this.requestTransactionRepository.findAllCompleteEdit();
		List<RequestTransaction> lRequestTransaction2_5 = new ArrayList<>();
		iRequestTransaction2_5.forEach(lRequestTransaction2_5::add);

		Iterable<RequestTransaction> iRequestTransaction3_5 = this.requestTransactionRepository.findAllCompleteChange();
		List<RequestTransaction> lRequestTransaction3_5 = new ArrayList<>();
		iRequestTransaction3_5.forEach(lRequestTransaction3_5::add);
		model.addAttribute("Sum5", lRequestTransaction_5.size()+lRequestTransaction2_5.size()+lRequestTransaction3_5.size());



		if(dbdId != null && dbdId != "") {
			Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllbydbdId(dbdId);
			List<RequestTransaction> lRequestTransaction = new ArrayList<>();
			iRequestTransaction.forEach(lRequestTransaction::add);
			model.addAttribute("lRequestTransaction", lRequestTransaction);
		}else if(dbdIdTxt != null && dbdIdTxt != "") {
			Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllbyLikedbdId("%"+dbdIdTxt+"%");
			List<RequestTransaction> lRequestTransaction = new ArrayList<>();
			iRequestTransaction.forEach(lRequestTransaction::add);
			model.addAttribute("lRequestTransaction", lRequestTransaction);
		}else if(actualGaranteeDay != null) {
			Iterable<RequestTransaction> iRequestTransaction = this.requestTransactionRepository.findAllbyDay("%"+actualGaranteeDay+"%");
			List<RequestTransaction> lRequestTransaction = new ArrayList<>();
			iRequestTransaction.forEach(lRequestTransaction::add);
			model.addAttribute("lRequestTransaction", lRequestTransaction);
		}


		System.out.println("actualGaranteeDay:"+actualGaranteeDay);
		//System.out.println(Long.valueOf(actualGaranteeDay));

		Iterable<Company> Icompanylist = companyNewRepository.findAll();
		List<Company> lcompanylist = new ArrayList<>();
		Icompanylist.forEach(lcompanylist::add);
		model.addAttribute("lcompanylist", lcompanylist);

		model.addAttribute("pagename", "search");
		return "officer/search";
	}

	@RequestMapping(value="/officer/accept-transaction/{rtId}")
	public String acceptTransaction(@PathVariable("rtId") Long rtId, Model model)
	{
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("102"));
		requestTransactionRepository.save(requestTransaction);
		this.logEventLog(requestTransaction.getRtId()+" "+requestTransaction.getRequest().getRequestName()+ " " + requestStatusRepository.findByStatus("102").getText());
		return "redirect:/officer/dashboard";
	}
	@RequestMapping(value="/officer/deny-transaction/{rtId}")
	public String denyTransaction(@PathVariable("rtId") Long rtId, Model model)
	{
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("103"));
		requestTransactionRepository.save(requestTransaction);
		this.logEventLog(requestTransaction.getRtId()+" "+requestTransaction.getRequest().getRequestName()+ " " + requestStatusRepository.findByStatus("103").getText());
		return "redirect:/officer/dashboard";
	}

	@RequestMapping(value="/officer/deny-withdraw/{rtId}")
	public String denyWithdraw(@PathVariable("rtId") Long rtId, Model model)
	{
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("103"));
		requestTransactionRepository.save(requestTransaction);
		this.logEventLog(requestTransaction.getRtId()+" "+requestTransaction.getRequest().getRequestName()+ " " + requestStatusRepository.findByStatus("103").getText());
		return "redirect:/officer/withdraw";
	}

	@RequestMapping(value="/officer/deny-transaction-processing/{rtId}")
	public String denyTransactionProcessing(@PathVariable("rtId") Long rtId, Model model)
	{
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("103"));
		requestTransactionRepository.save(requestTransaction);
		this.logEventLog(requestTransaction.getRtId()+" "+requestTransaction.getRequest().getRequestName()+ " " + requestStatusRepository.findByStatus("103").getText());
		return "redirect:/officer/processing";
	}

	@RequestMapping(value="/officer/notice-transaction-processing/{rtId}")
	public String noticeTransactionProcessing(@PathVariable("rtId") Long rtId, Model model)
	{
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("302"));
		requestTransactionRepository.save(requestTransaction);
		this.logEventLog(requestTransaction.getRtId()+" "+requestTransaction.getRequest().getRequestName()+ " " + requestStatusRepository.findByStatus("302").getText());
		return "redirect:/officer/processing";
	}

	@RequestMapping(value="/officer/cancel/{rtId}")
	public String cancel(@PathVariable("rtId") Long rtId, Model model)
	{
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("104"));
		requestTransactionRepository.save(requestTransaction);
		this.logEventLog(requestTransaction.getRtId()+" "+requestTransaction.getRequest().getRequestName()+ " " + requestStatusRepository.findByStatus("104").getText());
		return "redirect:/officer/processing";
	}

	@RequestMapping(value="/officer/accept-submission")
	//@ResponseBody
	public String acceptSubmission(int rtId, String c_status,@ModelAttribute RequestTransaction requestTransactionStr)
	{
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus(c_status));
		requestTransaction.setEvaluateResult(requestTransactionStr.getEvaluateResult());
		requestTransaction.setEvaluateRemark(requestTransactionStr.getEvaluateRemark());
		requestTransaction.setEvaluate1(requestTransactionStr.getEvaluate1());
		requestTransaction.setEvaluate2(requestTransactionStr.getEvaluate2());
		requestTransaction.setEvaluate3(requestTransactionStr.getEvaluate3());
		requestTransaction.setEvaluate4(requestTransactionStr.getEvaluate4());
		requestTransaction.setEvaluateRemarkNo1(requestTransactionStr.getEvaluateRemarkNo1());
		requestTransaction.setEvaluateRemarkNo2(requestTransactionStr.getEvaluateRemarkNo2());
		requestTransaction.setEvaluateRemarkNo3(requestTransactionStr.getEvaluateRemarkNo3());
		requestTransaction.setEvaluateRemarkNo4(requestTransactionStr.getEvaluateRemarkNo4());
		requestTransactionRepository.save(requestTransaction);
		this.logEventLog(requestTransaction.getRtId()+" "+requestTransaction.getRequest().getRequestName()+ " " + requestStatusRepository.findByStatus(c_status).getText());
		return "redirect:/officer/processing";
	}

	@RequestMapping(value="/officer/edit-doc-submission")
	public String editDocSubmission(int rtId)
	{
		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("206"));
		requestTransactionRepository.save(requestTransaction);
		this.logEventLog(requestTransaction.getRtId()+" "+requestTransaction.getRequest().getRequestName()+ " " + requestStatusRepository.findByStatus("206").getText());
		return "redirect:/officer/processing";
	}
	@RequestMapping(value="/officer/keep-file-submission")
	//@ResponseBody
	public String keepFileSubmission(int rtId,
			@RequestParam(value="fileDocNumber[]") String[] fileDocNumber,
			@RequestParam(value="govDocumentType[]") Long[] govDocumentType,
			@RequestParam(value="fileDate[]") String[] fileDate,
			@RequestParam(value="fileDocTitle[]") String[] fileDocTitle,
			@RequestParam(value="fileStoreName[]") MultipartFile[] fileStoreName,
			@RequestParam(value="filePrintableSetValue[]") String[] filePrintable_value,
			@RequestParam(value="namegeneral[]") String[] namegeneral)
//			,
//			@RequestParam(value="deleted[]") BigDecimal[] deleted)
	{

		RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(rtId);
		if(requestTransaction.getEndTimestamp() == null) {
			Date date= new Date();

			 long time = date.getTime();
			     System.out.println("Time in Milliseconds: " + time);

			 Timestamp ts = new Timestamp(time);
			 System.out.println("Current Time Stamp: " + ts);
			requestTransaction.setEndTimestamp(ts);
			requestTransactionRepository.save(requestTransaction);
		}


		// Print all the array elements
	      for (int i = 0; i < fileDocNumber.length; i++) {
	        // File theDir = new File(UPLOAD_FOLDER_DOC+rtId+"\\filename.txt");
	    	  FileStorage fileStorage = new FileStorage();
	         Path path2 = Paths.get(UPLOAD_FOLDER_DOC+rtId+"\\");
	         //if directory exists?
	         if (!Files.exists(path2)) {
	             try {
	                 Files.createDirectories(path2);
	             } catch (IOException e) {
	                 //fail to create directory
	            	 System.out.print("fail to create directory");
	                 e.printStackTrace();
	             }
	         }
	         	// Get the file and save it somewhere
	        	long millis = System.currentTimeMillis() ;
	            byte[] bytes = null;
				try {
					bytes = fileStoreName[i].getBytes();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("Error1");
					e.printStackTrace();
				}
	            Path path = Paths.get(UPLOAD_FOLDER_DOC+rtId+"\\" + millis +"_"+ fileStoreName[i].getOriginalFilename());
	            try {
					Files.write(path, bytes);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("Error2");
					e.printStackTrace();
				}
	            fileStorage.setFileOriginalName(fileStoreName[i].getOriginalFilename());
	            fileStorage.setFileStoreName(millis +"_"+ fileStoreName[i].getOriginalFilename());
	            fileStorage.setGovDocumentType(govDocumentTypeRepository.findByDoctypeId(govDocumentType[i]));
	            fileStorage.setFileDocNumber(fileDocNumber[i]);
	            System.out.println(fileDate[i]);
		        	String[] parts = fileDate[i].split("/");
					String year = String.valueOf((Integer.valueOf(parts[2]) + 543));
					String dateFull = parts[0] + "-" + parts[1] + "-" +  year;
					Date date1 = null;
					try {
						date1 = new SimpleDateFormat("dd-MM-yyyy").parse(dateFull);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            fileStorage.setFileDate(date1);
	            fileStorage.setFileDocTitle(fileDocTitle[i]);
	            fileStorage.setFilePrintable(filePrintable_value[i]);
	            System.out.println(filePrintable_value[i]);
	            fileStorage.setNamegeneral("-");
	            fileStorage.setDeleted(BigDecimal.valueOf(1));




	            fileStorage.setRequestTransaction(requestTransactionRepository.findByRtId(rtId));
	            fileStorageRepository.save(fileStorage);
	      }

	    	requestTransaction = requestTransactionRepository.findByRtId(rtId);
			requestTransaction.setRequestStatus(requestStatusRepository.findByStatus("209"));
			requestTransactionRepository.save(requestTransaction);
			this.logEventLog(requestTransaction.getRtId()+" "+requestTransaction.getRequest().getRequestName()+ " " + requestStatusRepository.findByStatus("209").getText());

	     // return UPLOAD_FOLDER_DOC+rtId;
		  return "redirect:/officer/document-storage";
	}
	public static String dateThai(String strDate)
	{
		String Months[] = {
			      "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน",
			      "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
			      "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		int year=0,month=0,day=0;
		try {
			Date date = df.parse(strDate);
			Calendar c = Calendar.getInstance();
			c.setTime(date);

			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DATE);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return String.format("%s %s %s", day,Months[month],year+543);
	}
	public static String dateThaiYearThai(String strDate)
	{
		String Months[] = {
			      "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน",
			      "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
			      "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		int year=0,month=0,day=0;
		try {
			Date date = df.parse(strDate);
			Calendar c = Calendar.getInstance();
			c.setTime(date);

			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DATE);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return String.format("%s %s พ.ศ.%s", day,Months[month],year+543);
	}

	public static String dateThaiMonthThaiYearThaiPlus(String strDate,int YearPlus)
	{
		String Months[] = {
			      "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน",
			      "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
			      "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		int year=0,month=0,day=0;
		try {
			Date date = df.parse(strDate);
			Calendar c = Calendar.getInstance();
			c.setTime(date);

			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DATE);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return String.format("เดือน	%s	   พ.ศ.    %s", Months[month],year+543+YearPlus);
	}
	public static String dateThaiM(String strDate)
	{
		String Months[] = {
			      "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน",
			      "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
			      "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		int year=0,month=0,day=0;
		try {
			Date date = df.parse(strDate);
			Calendar c = Calendar.getInstance();
			c.setTime(date);

			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DATE);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return String.format("%s %s", Months[month],year+543);
	}

	public static String dateThaiY(String strDate)
	{
		String Months[] = {
			      "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน",
			      "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
			      "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		int year=0,month=0,day=0;
		try {
			Date date = df.parse(strDate);
			Calendar c = Calendar.getInstance();
			c.setTime(date);

			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DATE);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return String.format("%s", year+543);
	}
	@RequestMapping(value="/officer/document/export-doc/{entryId}/{docCreateId}",produces = "application/vnd.openxmlformats-officedocument.wordprocessingml.document; charset=utf-8")
	@ResponseBody
	public ResponseEntity<InputStreamResource> exportDoc(@PathVariable("entryId") Long entryId,@PathVariable("docCreateId") Long docCreateId) throws Exception
	{
			Docx docx;
		 //System.out.println("Current working directory : " + workingDir);
				switch (docCreateId.intValue()) {
				case 1:
					//
					docx = new Docx(workingDir+"\\src\\main\\resources\\static\\asset\\doc1docx.docx");
					break;
				case 2:
					docx = new Docx(workingDir+"\\src\\main\\resources\\static\\asset\\doc2docx.docx");
					break;
				case 3:
					docx = new Docx(workingDir+"\\src\\main\\resources\\static\\asset\\doc3docx.docx");
					break;
				case 4:
					docx = new Docx(workingDir+"\\src\\main\\resources\\static\\asset\\doc4docx.docx");
					break;

				default:
					docx = new Docx(workingDir+"\\src\\main\\resources\\static\\asset\\doc1docx.docx");
					break;
				}


				// set the variable pattern. In this example the pattern is as follows: #{variableName}
				docx.setVariablePattern(new VariablePattern("#{", "}"));

				// read docx content as simple text
				String content = docx.readTextContent();

				// and display it
				//System.out.println(content);

				// find all variables satisfying the pattern #{...}
				//List<String> findVariables = docx.findVariables();

				// and display it
				//for (String var : findVariables) {
				       // System.out.println("VARIABLE => " + var);
				//}

				// prepare map of variables for template

				RequestTransaction requestTransaction = requestTransactionRepository.findByRtId(entryId);
				Company company = companyNewRepository.findByDbdId(requestTransaction.getCompany().getDbdId());
				Request request = requestRepository.findByRequestId(requestTransaction.getRequest().getRequestId());

				FormEntry requestFormEntry = formEntryRepository.findFirstByRequestTransaction(requestTransaction);

				Long rfID = requestFormEntry.getRequestForm().getRfId();
				System.out.println(rfID);

			//	RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(rtId);
				Request r = requestTransaction.getRequest();
				//RequestForm formData = requestFormRepository.findByRfId(r.getRequestId());
				RequestForm formData = requestFormRepository.findByRfId(rfID);

				Form f = this.formRepository.findByFormId(formData.getForm().getFormId());
				RequestForm requestForm = this.requestFormRepository.findByRequestAndFormAadTypeId(r, f, requestTransaction.getRequestType().getRequestTypeId());
				//long rfId = requestForm.getRfId();

				FormEntry formEntry = this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm);
				ObjectMapper objectMapper = new ObjectMapper();
				Map<String,String> entryMap = new HashMap<String, String>();
				String firstnameStr = "";

				if(entryMap.get("firstname") != null ) {
					firstnameStr = entryMap.get("firstname")+" "+entryMap.get("lastname");
				}else {
					firstnameStr = " ____________________________ ";
				}
				//model.addAttribute("entryMap", entryMap);

				Variables var = new Variables();
				var.addTextVariable(new TextVariable("#{company}", company.getCompanyName()));
				var.addTextVariable(new TextVariable("#{DbdId}", company.getDbdId()));
				var.addTextVariable(new TextVariable("#{Capital}", company.getCapital()));
				var.addTextVariable(new TextVariable("#{type-Request}", request.getRequestName()));
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				   LocalDateTime now = LocalDateTime.now();
				String date_th =    dateThai(dtf.format(now));//วัน-เดือน-ปี
				String date_th_M =    dateThaiM(dtf.format(now));//เดือน-ปี
				String date_th_Y =    dateThaiY(dtf.format(now));//ปี
				String date_th_YearThai = dateThaiYearThai(dtf.format(now)); //มี พศ
				String date_th_MonthThaiYearThaip3 = dateThaiMonthThaiYearThaiPlus(dtf.format(now),3); // มีเดือน มีพศ บวกสามปี
				String date_th_MonthThaiYearThai = dateThaiMonthThaiYearThaiPlus(dtf.format(now),0);
				var.addTextVariable(new TextVariable("#{date}", date_th));
				var.addTextVariable(new TextVariable("#{date-m}", date_th_M));
				var.addTextVariable(new TextVariable("#{year}", date_th_Y));
				var.addTextVariable(new TextVariable("#{date-t}", date_th_YearThai));
				var.addTextVariable(new TextVariable("#{date-untill}", date_th_MonthThaiYearThaip3));
				var.addTextVariable(new TextVariable("#{date_th_MonthThaiYearThai}", date_th_MonthThaiYearThai));
				var.addTextVariable(new TextVariable("#{contract}", firstnameStr));
				String com_address = " ";
				if(company.getAddressNo() != null) {
					com_address += company.getAddressNo();
				}
				if(company.getRoom() != null) {
					com_address += " ห้อง "+company.getRoom();
				}
				if(company.getFloor() != null) {
					com_address += " ชั้น "+company.getFloor();
				}

				if(company.getBuilding() != null) {
					com_address += " ตึก "+company.getBuilding();
				}
				if(company.getMoo() != null) {
					com_address += " หมู่ "+company.getMoo();
				}
				if(company.getSoi() != null) {
					com_address += " ซอย"+company.getSoi();
				}
				if(company.getStreet() != null) {
					com_address += " ถนน"+company.getStreet();
				}
				if(company.getDistrictId() != null) {
					TambonBean tambonBean = tambonRepository.findByIdTambon(Long.parseLong(company.getDistrictId()));
					com_address += " ตำบล"+tambonBean.getDistrictName();
				}
				if(company.getAmphurId() != null) {
					AmphurBean amphurBean = amphurRepository.findByIdAmphur(Long.parseLong(company.getAmphurId()));
					com_address += " อำเภอ"+amphurBean.getAmphurName();
				}
				if(company.getProvinceId() != null) {
					ProvinceBean provinceBean = provinceRepository.findByIdProvince(Integer.valueOf(company.getProvinceId()));
					com_address += " จังหวัด"+provinceBean.getProvinceName();
				}
				var.addTextVariable(new TextVariable("#{address_company}", com_address));
				// fill template by given map of variables
				docx.fillTemplate(var);

				// save filled document
				Path path2 = Paths.get(UPLOAD_FOLDER_DOC+entryId+"\\");
		         //if directory exists?
		         if (!Files.exists(path2)) {
		             try {
		                 Files.createDirectories(path2);
		             } catch (IOException e) {
		                 //fail to create directory
		            	 System.out.print("fail to create directory");
		                 e.printStackTrace();
		             }
		         }
		         String Strfilename;

		         switch (docCreateId.intValue()) {
					case 1:
						//
						Strfilename = "บันทึกข้อความถึง_อธิบดี_(ภายใน)_"+requestTransaction.getRequestType().getRequestTypeName().replace(" ", "_")+"_"+requestTransaction.getRequest().getRequestName().replace(" ", "_")+"_"+company.getCompanyName().replace(" ", "_")+".docx";
						docx.save(UPLOAD_FOLDER_DOC+entryId+"\\"+Strfilename);
						break;
					case 2:
						Strfilename = "หนังสือภายนอกถึงผู้ประกอบการ (ภายนอก)_"+requestTransaction.getRequestType().getRequestTypeName().replace(" ", "_")+"_"+requestTransaction.getRequest().getRequestName().replace(" ", "_")+"_"+company.getCompanyName().replace(" ", "_")+".docx";
						docx.save(UPLOAD_FOLDER_DOC+entryId+"\\"+Strfilename);
						break;
					case 3:
						Strfilename = "หนังสือรับรองบริษัท_"+requestTransaction.getRequestType().getRequestTypeName().replace(" ", "_")+"_"+requestTransaction.getRequest().getRequestName().replace(" ", "_")+"_"+company.getCompanyName().replace(" ", "_")+".docx";
						docx.save(UPLOAD_FOLDER_DOC+entryId+"\\"+Strfilename);
						break;
					case 4:
						Strfilename = "หนังสือรับรองตัวบุคคล_"+requestTransaction.getRequestType().getRequestTypeName().replace(" ", "_")+"_"+requestTransaction.getRequest().getRequestName().replace(" ", "_")+"_"+company.getCompanyName().replace(" ", "_")+".docx";
						docx.save(UPLOAD_FOLDER_DOC+entryId+"\\"+Strfilename);
						break;

					default:
						Strfilename = "doc1.docx";
						docx.save(UPLOAD_FOLDER_DOC+entryId+"\\"+Strfilename);
						break;
					}


		//File exportFile = new File(UPLOAD_FOLDER+"welcome.docx");
		//wordPackage.save(exportFile);
		//String fileName = UPLOAD_FOLDER+"welcome.docx";
		/* MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, fileName);
	        System.out.println("fileName: " + fileName);
	        System.out.println("mediaType: " + mediaType);*/

				File file;

				switch (docCreateId.intValue()) {
				case 1:
					//
					file = new File(UPLOAD_FOLDER_DOC+entryId+"\\"+Strfilename);
					break;
				case 2:
					file = new File(UPLOAD_FOLDER_DOC+entryId+"\\"+Strfilename);

					break;
				case 3:
					file = new File(UPLOAD_FOLDER_DOC+entryId+"\\"+Strfilename);

					break;
				case 4:
					file = new File(UPLOAD_FOLDER_DOC+entryId+"\\"+Strfilename);

					break;

				default:
					file = new File(UPLOAD_FOLDER_DOC+entryId+"\\"+Strfilename);

					break;
				}

				//Strfilename = "ไทย.docx";
	        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
	        Strfilename = URLEncoder.encode(Strfilename,"UTF-8");
	        return ResponseEntity.ok()
	                // Content-Disposition
	                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + Strfilename)
	                // Content-Type
	               // .contentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
	                // Contet-Length
	                .contentLength(file.length()) //
	                .body(resource);
		//return str;
	}
	@RequestMapping(value="/FileStorage/{fileId}",produces = "application/vnd.openxmlformats-officedocument.wordprocessingml.document; charset=utf-8")
	@ResponseBody
	public ResponseEntity<InputStreamResource> exportFileFileStorage(@PathVariable("fileId") Long fileId) throws Exception
	{

		FileStorage fileStorage = this.fileStorageRepository.findByfileId(fileId);
		System.out.println(UPLOAD_FOLDER_DOC+fileStorage.getRequestTransaction().getRtId()+"\\"+fileStorage.getFileStoreName());
		File file = new File(UPLOAD_FOLDER_DOC+fileStorage.getRequestTransaction().getRtId()+"\\"+fileStorage.getFileStoreName());

	        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
	        String Strfilename = URLEncoder.encode(fileStorage.getFileStoreName(),"UTF-8");
	        return ResponseEntity.ok()
	                // Content-Disposition
	                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + Strfilename)
	                // Content-Type
	               // .contentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
	                // Contet-Length
	                .contentLength(file.length()) //
	                .body(resource);
		//return str;
	}

	@RequestMapping(value="/officer/form/review/{entryId}")
	//@ResponseBody
	//public Map<String,String> formReview(@PathVariable("entryId") Long entryId, Model model)
	public String formReview(@PathVariable("entryId") Long entryId, Model model)
	{
		FormEntry formEntry = this.formEntryRepository.findByEntryId(entryId);

		Form form = formEntry.getRequestForm().getForm();

		ObjectMapper objectMapper = new ObjectMapper();
		Map<String,String> entryMap = new HashMap<String, String>();
		try {
			entryMap = objectMapper.readValue(formEntry.getEntryText(), new TypeReference<HashMap<String,String>>() {});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("entryMap", entryMap);

//		Company company = companyRepository.findByDbdId(formEntry.getDbdId());
//		model.addAttribute("company", company);

		//return entryMap;
		return "onestop/form/" + form.getFormTemplate();
	}

	@RequestMapping(value="/officer/company/{dbdId}/formgroup/view/{requestId}/{requestTransactionId}")
	public String formgroupView(HttpServletRequest requestsession,@PathVariable("dbdId") String dbdId, @PathVariable("requestId") Long requestId, @PathVariable("requestTransactionId") Long requestTransactionId, Model model)
	{	
		Long requestTransactionId1 = requestTransactionId;
		RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(requestTransactionId);
		RequestForm requestSubForm = null;
		Request request = this.requestRepository.findByRequestId(requestId);
		Long requesId = request.getRequestId();
		if(requesId == 18 || requesId == 19 || requesId == 20) {
			requestSubForm = this.requestFormRepository.findByRfId(41);
			model.addAttribute("requestSubForm", requestSubForm);
			System.out.println("requestTransaction.getRtId(): "+requestTransaction.getRtId()+"/requestSubForm.getRfId(): "+requestSubForm.getRfId());
			Iterable<FormEntry> listFormEntry = this.formEntryRepository.findAllByRequestTransactionAndRequestForm(requestTransaction.getRtId(), requestSubForm.getRfId());
			List<FormEntry> llistFormEntry = new ArrayList<>();
			listFormEntry.forEach(llistFormEntry::add);
			System.out.println("Size llistFormEntry "+llistFormEntry.size());
			model.addAttribute("LooprequestSubForm", 3-llistFormEntry.size());
			// 41 เป็นฟอร์มคำขอ ธพ.พ.1ว
		}else {
			requestSubForm = null;
		}
		
		System.out.println(requestTransaction.getRequest());
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
		model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
		model.addAttribute("role",requestsession.getSession().getAttribute("role"));		//model.addAttribute("lRequestTransaction", lRequestTransaction);

//		Iterable<RequestForm> iRequestFormID = this.requestFormRepository.findByRequestAndTypeId(request,requestTransaction.getRequestType().getRequestTypeId());
		Iterable<RequestForm> iRequestForm = this.requestFormRepository.findByRequestAndTypeId(request,requestTransaction.getRequestType().getRequestTypeId());
		Company company = this.companyRepository.findByDbdId(dbdId);
		RequestTransaction requestTransactionID = this.requestTransactionRepository.findByRtId(requestTransactionId);
//		//Form f = this.formRepository.findByFormId(formId);
//		//System.out.println("rId: "+request.getRequestId()+" | getRequestTypeId: "+requestTransaction.getRequestType().getRequestTypeId());
//
//		Iterable<RequestForm> iRequestFormType = this.requestFormRepository.findByRequestAndTypeId(request,requestTransaction.getRequestType().getRequestTypeId());
////=======
	//	RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(requestTransactionId);
		//Form f = this.formRepository.findByFormId(formId);
		
	//	Iterable<RequestForm> iRequestForm = this.requestFormRepository.findByRequestAndTypeId(request,requestTransaction.getRequestType().getRequestTypeId());
//>>>>>>> 18dbc2a63b954b00aa22973b250e8ab49b734531

		List<RequestForm> lRequestForm = new ArrayList<>();
		iRequestForm.forEach(lRequestForm::add);

        Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);

		model.addAttribute("request", request);
		model.addAttribute("lRequestForm", lRequestForm);
		model.addAttribute("lProvinceBean", lProvinceBean);
		model.addAttribute("dbdId", dbdId);
		//model.addAttribute("formId", formId);


		model.addAttribute("company", company);
		model.addAttribute("requestTypeId", requestTransaction.getRequestType().getRequestTypeId());

		model.addAttribute("rtId", requestTransaction.getRtId());
		model.addAttribute("requestTransactionId1", requestTransactionId1);
		model.addAttribute("requestId", requestId);
		return "officer/formgroup";
	}


	@RequestMapping(value="/officer/company/{dbdId}/{rtId}/form/review/{formId}/{requestTypeId}")
	public String formView(HttpServletRequest requestsession,@PathVariable("dbdId") String dbdId, @PathVariable("rtId") long rtId, @PathVariable("formId") Long formId, @PathVariable("requestTypeId") String requestTypeId, Model model)
	{
		Iterable<TitleName> eTitleName = this.titleNameRepository.findTitleNameActive();
		List<TitleName> lTitleName = new ArrayList<>();
		eTitleName.forEach(lTitleName::add);
		model.addAttribute("lTitleName", lTitleName);

		RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(rtId);
		System.out.println("Testttt!!!");
		//System.out.println(requestTransaction.getRequest().getRequestId());
		Request r = requestTransaction.getRequest();
		Form f = this.formRepository.findByFormId(formId);
		RequestForm requestForm = this.requestFormRepository.findByRequestAndFormAadTypeId(r, f, requestTransaction.getRequestType().getRequestTypeId());
		System.out.println("rId: "+r.getRequestId()+" | fId:  "+ f.getFormId() +" | getRequestTypeId: "+requestTransaction.getRequestType().getRequestTypeId());
		System.out.println("Comment 3");
		//long rfId = requestForm.getRfId();

		FormEntry formEntry = this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm);
		//System.out.println("formEntry: "+formEntry.getEntryId());
		model.addAttribute("formMetaId", "0");
		if (formEntry != null)
		{
			model.addAttribute("formMetaId", formEntry.getEntryId());
			ObjectMapper objectMapper = new ObjectMapper();
			Map<String,String> entryMap = new HashMap<String, String>();
			try {
				entryMap = objectMapper.readValue(formEntry.getEntryText(), new TypeReference<HashMap<String,String>>() {});
				model.addAttribute("entryMap", entryMap);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				System.out.println("Error1!!");
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				System.out.println("Error2!!");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Error3!!");
				e.printStackTrace();
			}
		}

        Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);
		model.addAttribute("lProvinceBean", lProvinceBean);
		model.addAttribute("dbdId", dbdId);
		model.addAttribute("rtId", rtId);
		model.addAttribute("formId", formId);
		model.addAttribute("rfId", requestForm.getRfId());
		String requestTypeName = "";
		if (requestTypeId.equalsIgnoreCase("1"))
		{
			requestTypeName = "ขอใหม่";
		} else if (requestTypeId.equalsIgnoreCase("2"))
		{
			requestTypeName = "ขอต่ออายุ";
		} else if (requestTypeId.equalsIgnoreCase("3"))
		{
			requestTypeName = "ขอแก้ไขเปลี่ยนแปลง";
		}

		String requestName = r.getRequestName();
		Long requestId = r.getRequestId();
		model.addAttribute("requestTypeId", requestTypeId);
		model.addAttribute("requestTypeName", requestTypeName);
		model.addAttribute("requestId", requestId);
		model.addAttribute("requestName", requestName);
		model.addAttribute("controllerPosition", "officerController");
		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);

		Iterable<Engineer> eForm = this.engineerRepository.findAllbyDbdId(dbdId);
		List<Engineer> lEngineer = new ArrayList<>();
		eForm.forEach(lEngineer::add);
		model.addAttribute("lEngineer", lEngineer);
		System.out.println(">>>"+f.getFormTemplate()+"<<<<");
		System.out.println("utukjg"+requestsession.getSession().getAttribute("Position"));
		System.out.println("utukjg22"+requestsession.getSession().getAttribute("nameMember"));
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
model.addAttribute("role",requestsession.getSession().getAttribute("role"));
		//model.addAttribute("lRequestTransaction", lRequestTransaction);
		return "onestop/form/" + f.getFormTemplate();
	}

	@RequestMapping(value="/officer/company/{dbdId}/{rtId}/form/review/{formId}/{requestTypeId}/{engiId}")
	public String formView2(HttpServletRequest requestsession,@PathVariable("engiId") String engiId,@PathVariable("dbdId") String dbdId, @PathVariable("rtId") long rtId, @PathVariable("formId") Long formId, @PathVariable("requestTypeId") String requestTypeId, Model model)
	{
		Iterable<TitleName> eTitleName = this.titleNameRepository.findTitleNameActive();

		List<Engineer> lEngineer = new ArrayList<>();
		long ideng = Long.valueOf(engiId);
		System.out.println("engiId : "+engiId);

		for(Engineer engineer : this.engineerRepository.findAllbyDbdId(dbdId)){
			if(engineer.getDeleted() == 0 && ideng == engineer.getId()){
				Engineer e2 = new Engineer();
				e2.setId(engineer.getId());
				e2.setFirstname(engineer.getFirstname());
				e2.setLastname(engineer.getLastname());
				e2.setTitleName(engineer.getTitleName());
				e2.setTiTleEnGiNeer(engineer.getTiTleEnGiNeer());
				e2.setType(engineer.getType());
				e2.setFaculty(engineer.getFaculty());
				e2.setNumhome_service(engineer.getNumhome_service());
				e2.setMoo_service(engineer.getMoo_service());
				e2.setSoi_service(engineer.getSoi_service());
				e2.setRoad_service(engineer.getRoad_service());
				e2.setTumbon_service(engineer.getTumbon_service());
				e2.setProvince_service(engineer.getProvince_service());
				e2.setZipcode_service(engineer.getZipcode_service());
				e2.setTel_service(engineer.getTel_service());
				e2.setEmail_service(engineer.getEmail_service());

				lEngineer.add(e2);

				model.addAttribute("id", engineer.getId());
				model.addAttribute("firstname", engineer.getFirstname());
				model.addAttribute("lastname", engineer.getLastname());
				model.addAttribute("titlename", engineer.getTitleName());
				model.addAttribute("stitlename", engineer.getTiTleEnGiNeer());
				model.addAttribute("type", engineer.getType());
				model.addAttribute("faculty", engineer.getFaculty());
				model.addAttribute("numhome", engineer.getNumhome_service());
				model.addAttribute("moo", engineer.getMoo_service());
				model.addAttribute("soi", engineer.getSoi_service());
				model.addAttribute("road", engineer.getRoad_service());
				model.addAttribute("tumbon", engineer.getTumbon_service());
				model.addAttribute("province", engineer.getProvince_service());
				model.addAttribute("zipcode", engineer.getZipcode_service());
				model.addAttribute("tel", engineer.getTel_service());
				model.addAttribute("mail", engineer.getEmail_service());

				System.out.println("id : "+engineer.getId());
				System.out.println("Firstname : "+engineer.getFirstname());
				System.out.println("Lastname : "+engineer.getLastname());	
				System.out.println("STITLE : "+engineer.getTiTleEnGiNeer());	
				System.out.println("province : "+engineer.getProvince_service());	
				System.out.println(e2.getTiTleEnGiNeer());
			}
		}
		
		model.addAttribute("lEngineer", lEngineer);
		model.addAttribute("engiId", engiId);

		// Engineer aDataEngi = this.engineerRepository.findAllbyID(engiId);

		// List<Engineer> aEngi= new ArrayList<>();

		// Engineer e2 = new Engineer();
		// e2.setId(aDataEngi.getId());
		// e2.setFirstname(aDataEngi.getFirstname());
		// e2.setLastname(aDataEngi.getLastname());
		// System.out.println("Firstname : "+aDataEngi.getFirstname());
		// System.out.println("Lastname : "+aDataEngi.getLastname());	

		// aEngi.add(e2);

		
		// aDataEngi.forEach(aEngi::add);
		// model.addAttribute("aEngi", aEngi);

		// System.out.println(aEngi);
		
		List<TitleName> lTitleName = new ArrayList<>();
		eTitleName.forEach(lTitleName::add);
		model.addAttribute("lTitleName", lTitleName);

		RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(rtId);
		// System.out.println("Testttt!!!");
		//System.out.println(requestTransaction.getRequest().getRequestId());
		Request r = requestTransaction.getRequest();
		Form f = this.formRepository.findByFormId(formId);
		RequestForm requestForm = this.requestFormRepository.findByRequestAndFormAadTypeId(r, f, requestTransaction.getRequestType().getRequestTypeId());
		// System.out.println("rId: "+r.getRequestId()+" | fId:  "+ f.getFormId() +" | getRequestTypeId: "+requestTransaction.getRequestType().getRequestTypeId());
		// System.out.println("Comment 3");
		//long rfId = requestForm.getRfId();

		// FormEntry formEntry = this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm);
		FormEntry formEntry = this.formEntryRepository.findByRequestTransactionAndRequestFormAndIdEngineer(requestTransaction, requestForm, engiId);
		//System.out.println("formEntry: "+formEntry.getEntryId());
		model.addAttribute("formMetaId", "0");
		if (formEntry != null)
		{
			model.addAttribute("formMetaId", formEntry.getEntryId());
			ObjectMapper objectMapper = new ObjectMapper();
			Map<String,String> entryMap = new HashMap<String, String>();
			try {
				entryMap = objectMapper.readValue(formEntry.getEntryText(), new TypeReference<HashMap<String,String>>() {});
				model.addAttribute("entryMap", entryMap);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				// System.out.println("Error1!!");
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				// System.out.println("Error2!!");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// System.out.println("Error3!!");
				e.printStackTrace();
			}
		}

      Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);
		model.addAttribute("lProvinceBean", lProvinceBean);
		model.addAttribute("dbdId", dbdId);
		model.addAttribute("rtId", rtId);
		model.addAttribute("formId", formId);
		model.addAttribute("rfId", requestForm.getRfId());
		String requestTypeName = "";
		if (requestTypeId.equalsIgnoreCase("1"))
		{
			requestTypeName = "ขอใหม่";
		} else if (requestTypeId.equalsIgnoreCase("2"))
		{
			requestTypeName = "ขอต่ออายุ";
		} else if (requestTypeId.equalsIgnoreCase("3"))
		{
			requestTypeName = "ขอแก้ไขเปลี่ยนแปลง";
		}

		String requestName = r.getRequestName();
		Long requestId = r.getRequestId();
		model.addAttribute("requestTypeId", requestTypeId);
		model.addAttribute("requestTypeName", requestTypeName);
		model.addAttribute("requestId", requestId);
		model.addAttribute("requestName", requestName);
		model.addAttribute("controllerPosition", "officerController");
		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);

		Iterable<Engineer> eForm = this.engineerRepository.findAllbyDbdId(dbdId);
		// List<Engineer> lEngineer = new ArrayList<>();
		// eForm.forEach(lEngineer::add);
		// model.addAttribute("lEngineer", lEngineer);
		// System.out.println(">>>"+f.getFormTemplate()+"<<<<");
		// System.out.println("utukjg"+requestsession.getSession().getAttribute("Position"));
		// System.out.println("utukjg22"+requestsession.getSession().getAttribute("nameMember"));
		model.addAttribute("ps",requestsession.getSession().getAttribute("Position"));
		model.addAttribute("nm",requestsession.getSession().getAttribute("nameMember"));
		model.addAttribute("role",requestsession.getSession().getAttribute("role"));
		//model.addAttribute("lRequestTransaction", lRequestTransaction);
		return "onestop/form/" + f.getFormTemplate();
	}

	@RequestMapping(value="/officer/manager/delete/{fileId}")
	public String delete_manager(@PathVariable("fileId") Long fileId,@ModelAttribute FileStorage iFileStorage,HttpSession session)
	{
		iFileStorage = this.fileStorageRepository.findByfileId(fileId);
		BigDecimal valDouble = new BigDecimal(0);
		iFileStorage.setDeleted(valDouble);
		this.fileStorageRepository.save(iFileStorage);
				return "redirect:/officer/complete";





	}

	@RequestMapping(value="/officer/export_keepfile/{fileId}")
	public String exportkeepfile(@PathVariable("fileId") Long fileId,@ModelAttribute FileStorage iFileStorage,HttpSession session)
	{
		/*iFileStorage = this.fileStorageRepository.findByfileId(fileId);
		BigDecimal valDouble = new BigDecimal(0);
		iFileStorage.setDeleted(valDouble);
		this.fileStorageRepository.save(iFileStorage);*/
	    return "redirect:/officer/complete";

	}

	public void logEventLog(String status)
	{
		EventLog el = new EventLog();

		el.setLogIp("127.0.0.1");
		el.setLogRefer("http://ebis.redirectme.net:8080/");
		el.setLogTimestamp(new Timestamp(System.currentTimeMillis()));
		el.setLogType("officer workflow "+status);
		el.setLogUser("ofc001");

		this.eventLogRepository.save(el);
	}


	@RequestMapping(value="/officer/Detail/{dbdId}/{requestTransactionId}/{formId}/{requestTypeId}")
	public String formgroupViewDetail2(Model model, @PathVariable("requestTypeId") String requestTypeId, @PathVariable("formId") String formId, @PathVariable("dbdId") String dbdId,@PathVariable("requestTransactionId") String requestTransactionId,@ModelAttribute Engineer Engineer)
	{
		List<Engineer> lEngineer = new ArrayList<>();
		
		for(Engineer engineer : this.engineerRepository.findAllbyDbdId(dbdId)){
			if(engineer.getDeleted() == 0){
				Engineer e2 = new Engineer();
				e2.setId(engineer.getId());
				e2.setFirstname(engineer.getFirstname());
				e2.setLastname(engineer.getLastname());
				System.out.println("id : "+engineer.getId());
				System.out.println("Firstname : "+engineer.getFirstname());
				System.out.println("Lastname : "+engineer.getLastname());	

				lEngineer.add(e2);
			}
		}
		
		model.addAttribute("lEngineer", lEngineer);
		model.addAttribute("requestTypeId", requestTypeId);
		model.addAttribute("formId", formId);
		model.addAttribute("dbdId", dbdId);
		model.addAttribute("requestTransactionId", requestTransactionId);


		return "officer/formgroupDetail";
	}

	

}

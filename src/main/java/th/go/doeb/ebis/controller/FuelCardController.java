package th.go.doeb.ebis.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import net.minidev.json.JSONObject;

@Controller
public class FuelCardController {

	@RequestMapping(value="/fuelcard")
	@ResponseBody
	public String  employeeType()
	{
		
		
		String url="http://fuelcard.doeb.go.th/api/employee-type/";
		
//		String url="http://fuelcard.doeb.go.th/api/employee/search/?personal_no=3471500168077";
		
		
//		HttpHeaders headers = new HttpHeaders();
//		headers.add("Authorization", "Basic eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOjQsImlhdCI6MTUyNzY5OTYwMCwiZXhwIjoxNTM1NDc1NjAwLCJ0eXAiOjN9.S_rcs1FGvZ3ad3TjDTjmirvR4GhEQIX_AIme6yw3qdw");
//
//		
//		RestTemplate restTemplate = new RestTemplate();
////
////		  for (HttpMessageConverter<?> myConverter : restTemplate.getMessageConverters ()) {
////		     if (myConverter instanceof MappingJackson2HttpMessageConverter) {
////		        List<MediaType> myMediaTypes = new ArrayList<MediaType> ();
////		        myMediaTypes.addAll (myConverter.getSupportedMediaTypes ());
////		        myMediaTypes.add (MediaType.parseMediaType ("text/html; charset=utf-8"));
////		        ((MappingJackson2HttpMessageConverter) myConverter).setSupportedMediaTypes (myMediaTypes);
////		     }
////		  }		
//
//		HttpEntity<String> request = new HttpEntity<String>(headers);
//
//		ResponseEntity<List<EmployeeType>> allResponse =
//		        restTemplate.exchange(url,
//		                    HttpMethod.GET, request, new ParameterizedTypeReference<List<EmployeeType>>() {
//		            });
//		List<EmployeeType> all = allResponse.getBody();
//		//return allResponse.getHeaders().toString();
		
		String json = "";

		try {

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //Request header
        con.setRequestProperty("Authorization", "Basic eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOjQsImlhdCI6MTUyNzY5OTYwMCwiZXhwIjoxNTM1NDc1NjAwLCJ0eXAiOjN9.S_rcs1FGvZ3ad3TjDTjmirvR4GhEQIX_AIme6yw3qdw");

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //System.out.println(response.toString());
        json += response.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return json;
	}
	
	@RequestMapping(value="/employeeSearch")
	@ResponseBody
	public String employeeSearch()
	{
		

//		String url="http://fuelcard.doeb.go.th/api/employee-type/";		
		String url="http://fuelcard.doeb.go.th/api/employee/search/?personal_no=3471500168077";
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOjQsImlhdCI6MTUyNzY5OTYwMCwiZXhwIjoxNTM1NDc1NjAwLCJ0eXAiOjN9.S_rcs1FGvZ3ad3TjDTjmirvR4GhEQIX_AIme6yw3qdw");

		
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> request = new HttpEntity<String>(headers);

		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		return response.getBody();

	}	

	@RequestMapping(value="/workplaceSearch")
	@ResponseBody
	public String workplaceSearch()
	{
		

		String url="http://fuelcard.doeb.go.th/api/workplace/search/";
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOjQsImlhdCI6MTUyNzY5OTYwMCwiZXhwIjoxNTM1NDc1NjAwLCJ0eXAiOjN9.S_rcs1FGvZ3ad3TjDTjmirvR4GhEQIX_AIme6yw3qdw");

		
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> request = new HttpEntity<String>(headers);

		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		return response.getBody();

	}		

	@RequestMapping(value="/prefix")
	@ResponseBody
	public String prefix()
	{
		

		String url="http://fuelcard.doeb.go.th/api/prefix/";
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOjQsImlhdCI6MTUyNzY5OTYwMCwiZXhwIjoxNTM1NDc1NjAwLCJ0eXAiOjN9.S_rcs1FGvZ3ad3TjDTjmirvR4GhEQIX_AIme6yw3qdw");

		
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> request = new HttpEntity<String>(headers);

		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		return response.getBody();

	}			

	@RequestMapping(value="/businessType")
	@ResponseBody
	public String businessType()
	{
		

		String url="http://fuelcard.doeb.go.th/api/business-type/";
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOjQsImlhdCI6MTUyNzY5OTYwMCwiZXhwIjoxNTM1NDc1NjAwLCJ0eXAiOjN9.S_rcs1FGvZ3ad3TjDTjmirvR4GhEQIX_AIme6yw3qdw");

		
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> request = new HttpEntity<String>(headers);

		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		return response.getBody();

	}				

	@RequestMapping(value="/province")
	@ResponseBody
	public String province()
	{
		

		String url="http://fuelcard.doeb.go.th/api/province/";
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOjQsImlhdCI6MTUyNzY5OTYwMCwiZXhwIjoxNTM1NDc1NjAwLCJ0eXAiOjN9.S_rcs1FGvZ3ad3TjDTjmirvR4GhEQIX_AIme6yw3qdw");

		
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> request = new HttpEntity<String>(headers);

		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		return response.getBody();

	}					
	
}

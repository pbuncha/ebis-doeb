package th.go.doeb.ebis.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import th.go.doeb.ebis.bean.formTestAndQuality2;
import th.go.doeb.ebis.repository.formTestAndQuality3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class UploadController {
	@Autowired(required=true)
	private formTestAndQuality3 formTestAndQuality3;
    //Save the uploaded file to this folder
	 private static String UPLOADED_FOLDER = System.getProperty("user.dir")+"\\src\\main\\resources\\uploads\\formTestAndQuality\\";
	   

  
    @RequestMapping(value="/FormTP1")
	public String FormTP1() 
	{
		return "formTestAndQuality";
	}
	
	
	@RequestMapping(value = "/formTestAndQuality1", method = RequestMethod.POST)
	@ResponseBody
	public String formTestAndQuality1(@ModelAttribute("formTestAndQuality2") formTestAndQuality2 formTestAndQuality2,BindingResult result,@RequestParam("files") MultipartFile file,
			RedirectAttributes redirectAttributes, ModelMap model  ){
		 String message = "AAA";
	     if (file.isEmpty()) {
	            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	           // return "redirect:formTestAndQuality";
	            
	            message = "Please select a file to upload";
	           
	        }

	        try {
	        	long millis = System.currentTimeMillis() ;
	            // Get the file and save it somewhere
	            byte[] bytes = file.getBytes();
	            Path path = Paths.get(UPLOADED_FOLDER +millis+"_"+ file.getOriginalFilename());
	            Files.write(path, bytes);

	            //redirectAttributes.addFlashAttribute("message", "You successfully uploaded '" + file.getOriginalFilename() + "'");
	            message =  "You successfully uploaded '" +millis+"_"+ file.getOriginalFilename() + "'";
	            formTestAndQuality2.setFiles(millis+"_"+ file.getOriginalFilename());
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		this.formTestAndQuality3.save(formTestAndQuality2);
		model.addAttribute("formTestAndQuality2", formTestAndQuality2);
		//return "formTestAndQuality";
		return message ;
	}


}
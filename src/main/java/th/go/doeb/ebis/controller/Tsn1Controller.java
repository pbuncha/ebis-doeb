package th.go.doeb.ebis.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import th.go.doeb.ebis.bean.BeOilInspector;
import th.go.doeb.ebis.bean.MemBean;
import th.go.doeb.ebis.repository.Tsn1Repository;

@Controller
@RequestMapping(value="/BeOilInspector", method = RequestMethod.POST)
public class Tsn1Controller {
	
	@Autowired(required=true)
	private Tsn1Repository tsn1Repository;
	//private static String UPLOADED_FOLDER = System.getProperty("user.dir")+"\\src\\main\\resources\\uploads\\Tsc\\";
	private static String UPLOADED_FOLDER = "/data/test/uploads/etdi/";
	
	@RequestMapping(value="/save_tsn1")
	//@ResponseBody
	public String save_tsn1(@ModelAttribute("BeOilInspector") BeOilInspector c,BindingResult result,
			@RequestParam("file_ch3_1") MultipartFile file1,
			@RequestParam("file_ch3_2") MultipartFile file2,
			@RequestParam("file_ch3_3") MultipartFile file3,
			@RequestParam("file_ch3_4") MultipartFile file4,
			@RequestParam("file_ch3_5") MultipartFile file5,
			@RequestParam("file_ch3_6") MultipartFile file6,
			@RequestParam("file_ch3_7") MultipartFile file7,
			@RequestParam("file_ch3_8") MultipartFile file8,
			@RequestParam("file_ch3_9") MultipartFile file9,
			@RequestParam("file_ch4_1") MultipartFile file10,
			@RequestParam("file_ch4_2") MultipartFile file11,
			@RequestParam("file_ch4_3") MultipartFile file12,
			@RequestParam("file_ch4_4") MultipartFile file13,
			@RequestParam("file_ch4_5") MultipartFile file14,
			@RequestParam("file_ch4_6") MultipartFile file15,
			@RequestParam("file_ch4_7") MultipartFile file16,
			
			
			
			RedirectAttributes redirectAttributes,ModelMap model) 
	{
		
			//file_purpose1
				/*if (file1.isEmpty()) {
		            //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
		            return "redirect:uploadStatus";
		        }*/

		        try {

		            // Get the file and save it somewhere
		        	long millis = System.currentTimeMillis() ;
		            byte[] bytes = file1.getBytes();
		            Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file1.getOriginalFilename());
		            Files.write(path, bytes);
		            if (!file1.isEmpty()) {
		            	c.setFile_ch3_1(millis +"_"+ file1.getOriginalFilename());
			        }
		            
		            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		        
		      //file_purpose2
	      		/*if (file2.isEmpty()) {
	                  //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                  return "redirect:uploadStatus";
	              }*/

	              try {

	                  // Get the file and save it somewhere
	              	long millis = System.currentTimeMillis() ;
	                  byte[] bytes = file2.getBytes();
	                  Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file2.getOriginalFilename());
	                  Files.write(path, bytes);
	                  if (!file2.isEmpty()) {
	                	  c.setFile_ch3_2(millis +"_"+ file2.getOriginalFilename());
	 	              }
	                  //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	              } catch (IOException e) {
	                  e.printStackTrace();
	              }
	              
	            //file_purpose3
	        		/*if (file3.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file3.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file3.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file3.isEmpty()) {
	                    	c.setFile_ch3_3(millis +"_"+ file3.getOriginalFilename());
		                }
	                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	            //file_purpose4
	        		/*if (file4.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file4.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file4.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file4.isEmpty()) {
	                    	c.setFile_ch3_4(millis +"_"+ file4.getOriginalFilename());
		                }
	                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	              //file_purpose5
	        		/*if (file5.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file5.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file5.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file5.isEmpty()) {
	                    	c.setFile_ch3_5(millis +"_"+ file5.getOriginalFilename());
		                }
	                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	              //file_purpose6
	        		/*if (file6.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file6.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file6.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file6.isEmpty()) {
	                    	c.setFile_ch3_6(millis +"_"+ file6.getOriginalFilename());
		                }
	                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	                
	              //file_purpose7
	        		/*if (file7.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file7.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file7.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file7.isEmpty()) {
	                    	c.setFile_ch3_7(millis +"_"+ file7.getOriginalFilename());
		                }//redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	                
	              //file_purpose8
	        		/*if (file8.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file8.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file8.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file8.isEmpty()) {
	                    	c.setFile_ch3_8(millis +"_"+ file8.getOriginalFilename());
		                }//redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	                
	              //file_purpose9
	        		/*if (file9.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file9.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file9.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file9.isEmpty()) {
	                    	c.setFile_ch3_9(millis +"_"+ file9.getOriginalFilename());
	 	                }
	                     //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	              //file_purpose10
	        		/*if (file10.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file10.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file10.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file10.isEmpty()) {
	                    	c.setFile_ch4_1(millis +"_"+ file10.getOriginalFilename());
		                }//redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	              //file_purpose11
	        		/*if (file11.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file11.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file11.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file11.isEmpty()) {
	                    	c.setFile_ch4_2(millis +"_"+ file11.getOriginalFilename());
		                }//redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	              //file_purpose12
	        		/*if (file12.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file12.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file12.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file12.isEmpty()) {
	                    	c.setFile_ch4_3(millis +"_"+ file12.getOriginalFilename());
		                }//redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	              //file_purpose13
	        		/*if (file13.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file13.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file13.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file13.isEmpty()) {
	                    	c.setFile_ch4_4(millis +"_"+ file13.getOriginalFilename());
		                }//redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	              //file_purpose14
	        		/*if (file14.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }
*/
	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file14.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file14.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file14.isEmpty()) {
	                    	c.setFile_ch4_5(millis +"_"+ file14.getOriginalFilename());
		               }//redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	              //file_purpose15
	        		/*if (file15.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file15.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file15.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file15.isEmpty()) {
	                    	c.setFile_ch4_6(millis +"_"+ file15.getOriginalFilename());
		                }//redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                
	              //file_purpose16
	        		/*if (file16.isEmpty()) {
	                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	                    return "redirect:uploadStatus";
	                }*/

	                try {

	                    // Get the file and save it somewhere
	                	long millis = System.currentTimeMillis() ;
	                    byte[] bytes = file16.getBytes();
	                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file16.getOriginalFilename());
	                    Files.write(path, bytes);
	                    if (!file16.isEmpty()) {
	                    	c.setFile_ch4_7(millis +"_"+ file16.getOriginalFilename());
		                }//redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	              
	              
	              
	              
		
		
		//@ModelAttribute("BeTechnicalTestingValidation") BeTechnicalTestingValidation BeTechnicalTestingValidation,
	      //BindingResult result,@RequestParam("copy_registion") MultipartFile file,@RequestParam("study_history") MultipartFile file2,
	     // @RequestParam("professional_license") MultipartFile file3,@RequestParam("work_history") MultipartFile file4,
	     // @RequestParam("my_pic") MultipartFile file5,
	     // RedirectAttributes redirectAttributes, ModelMap model) 

		this.tsn1Repository.save(c);
		//public String AddTechnicalTestingValidation(@ModelAttribute("BeTechnicalTestingValidation") BeTechnicalTestingValidation BeTechnicalTestingValidation,
		//return "Success";
		return "redirect:/tsn1";
	}
	
}

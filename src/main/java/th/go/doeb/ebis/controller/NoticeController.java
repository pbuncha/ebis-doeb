package th.go.doeb.ebis.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import th.go.doeb.ebis.bean.Notice;
import th.go.doeb.ebis.repository.NoticeRepository;
@Controller
public class NoticeController {

	@Autowired(required=true)
	private NoticeRepository noticeRepository;
	//private static String UPLOADED_FOLDER = System.getProperty("user.dir")+"\\src\\main\\resources\\uploads\\Notice\\";
	private static String UPLOADED_FOLDER = "/data/test/uploads/etdi/";
	
	@RequestMapping(value="/greeting",method = RequestMethod.POST)
	@ResponseBody
	public String save(@ModelAttribute("Notice") Notice c,BindingResult result,
			@RequestParam("file_purpose1") MultipartFile file1,
			@RequestParam("file_purpose2") MultipartFile file2,
			@RequestParam("file_purpose3") MultipartFile file3,
			@RequestParam("file_purpose4") MultipartFile file4,
			@RequestParam("file_purpose5") MultipartFile file5,
			@RequestParam("file_purpose6") MultipartFile file6,
			@RequestParam("file_purpose7") MultipartFile file7,
			@RequestParam("file_purpose8") MultipartFile file8,
			@RequestParam("file_purpose9") MultipartFile file9,
			@RequestParam("file_purpose10") MultipartFile file10
			
			,RedirectAttributes redirectAttributes, ModelMap model)
	{
		//file_purpose1
		if (file1.isEmpty()) {
            //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            //return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
        	long millis = System.currentTimeMillis() ;
            byte[] bytes = file1.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file1.getOriginalFilename());
            Files.write(path, bytes);
            c.setFile_purpose1(millis +"_"+ file1.getOriginalFilename());
            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }
        
      //file_purpose2
      		if (file2.isEmpty()) {
                  //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                  //return "redirect:uploadStatus";
              }

              try {

                  // Get the file and save it somewhere
              	long millis = System.currentTimeMillis() ;
                  byte[] bytes = file2.getBytes();
                  Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file2.getOriginalFilename());
                  Files.write(path, bytes);
                  c.setFile_purpose2(millis +"_"+ file2.getOriginalFilename());
                  //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

              } catch (IOException e) {
                  e.printStackTrace();
              }
              
              //file_purpose3
        		if (file3.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file4.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file3.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_purpose3(millis +"_"+ file3.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose4
        		if (file4.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file4.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file4.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_purpose4(millis +"_"+ file4.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                
                //file_purpose5
        		if (file5.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file5.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file5.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_purpose5(millis +"_"+ file5.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose6
        		if (file6.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file6.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file6.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_purpose6(millis +"_"+ file6.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose7
        		if (file7.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file7.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file7.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_purpose7(millis +"_"+ file7.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose8
        		if (file8.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file8.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file8.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_purpose8(millis +"_"+ file8.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose9
        		if (file9.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file9.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file9.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_purpose9(millis +"_"+ file9.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose10
        		if (file10.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file10.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file10.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setFile_purpose10(millis +"_"+ file10.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
              
              
        //---
		//ModelAttribute("BeTechnicalTestingValidation") BeTechnicalTestingValidation BeTechnicalTestingValidation,
	    //  BindingResult result,@RequestParam("copy_registion") MultipartFile file,@RequestParam("study_history") MultipartFile file2,
	    //  @RequestParam("professional_license") MultipartFile file3,@RequestParam("work_history") MultipartFile file4,
	   //   @RequestParam("my_pic") MultipartFile file5,
	  //    RedirectAttributes redirectAttributes, ModelMap model) 
		this.noticeRepository.save(c);
		//return "Show";
		return "redirect:/spp";
	}
}

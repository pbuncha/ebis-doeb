package th.go.doeb.ebis.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;

import th.go.doeb.ebis.entity.Department;
import th.go.doeb.ebis.entity.Employee;
import th.go.doeb.ebis.repository.DepartmentRepository;
import th.go.doeb.ebis.repository.EmployeeRepository;

@Controller
public class EmployeeController {
	@Autowired(required=true)
	private EmployeeRepository employeeRepository;	
	
	@Autowired(required=true)
	private DepartmentRepository departmentRepository;	
	
	@RequestMapping(value="/employee/list")
	public String formList(Model model)
	{
		Iterable<Employee> eForm = this.employeeRepository.findAll();
		List<Employee> lEmployee = new ArrayList<>();
		eForm.forEach(lEmployee::add);
		model.addAttribute("lEmployee", lEmployee);
		
		return "employee/list";
	}
	
	@RequestMapping(value="/employee/add")
	public String formAdd(Model model)
	{
		 Iterable<Department> iDepartment = this.departmentRepository.findAll();
			List<Department> lDepartment = new ArrayList<>();
			iDepartment.forEach(lDepartment::add);
			model.addAttribute("lDepartment", lDepartment);
		return "employee/add";
	}	
	
	@RequestMapping(value="/employee/view/{personalId}")
	public String formView(@PathVariable("personalId") String personalId,Model model)
	{
		 Iterable<Department> iDepartment = this.departmentRepository.findAll();
			List<Department> lDepartment = new ArrayList<>();
			iDepartment.forEach(lDepartment::add);
			Employee employee = this.employeeRepository.findByPersonalId(personalId);
			model.addAttribute("employee", employee);
			model.addAttribute("lDepartment", lDepartment);
		return "employee/view";
	}	

	@RequestMapping(value="/employee/save")
	//@ResponseBody
	public String formSave(@ModelAttribute Employee employee,WebRequest request)
	{
		
		BigDecimal departmentId = new BigDecimal(request.getParameter("departmentId"));
		employee.setDepartment(this.departmentRepository.findByDepartmentId(departmentId.longValue()));
		this.employeeRepository.save(employee);
		return "redirect:/employee/list";
		//System.out.println(departmentId);
		//return "AA";
		//return Employee.getDepartment().toString();
	}		
	
	@RequestMapping(value="/employee/update")
	//@ResponseBody
	public String formUpdate(@ModelAttribute Employee employee,WebRequest request)
	{
		BigDecimal departmentId = new BigDecimal(request.getParameter("departmentId"));
		employee.setDepartment(this.departmentRepository.findByDepartmentId(departmentId.longValue()));
		
		this.employeeRepository.save(employee);
		return "redirect:/employee/list";
		//System.out.println(departmentId);
		//return "AA";
		//return Employee.getDepartment().toString();
	}
}

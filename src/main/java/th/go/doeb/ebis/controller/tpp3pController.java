package th.go.doeb.ebis.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import th.go.doeb.ebis.bean.tpp3pModel;
import th.go.doeb.ebis.repository.tpp3pRepository;
@Controller
public class tpp3pController {
	@Autowired(required=true)
	private tpp3pRepository tpp3pRepository;
//private static String UPLOADED_FOLDER = System.getProperty("user.dir")+"\\src\\main\\resources\\uploads\\TechnicalTestingValidation\\";
	private static String UPLOADED_FOLDER = "/data/test/uploads/etdi/";
	
	@RequestMapping(value="/tpp3pView4",method = RequestMethod.POST)
	//@ResponseBody
	public String save(@ModelAttribute("tpp3pRepository") tpp3pModel c,BindingResult result,
			@RequestParam("pic_name1") MultipartFile file1,
			@RequestParam("pic_name2") MultipartFile file2,
			@RequestParam("pic_name3") MultipartFile file3,
			@RequestParam("pic_name4") MultipartFile file4,
			@RequestParam("pic_name5") MultipartFile file5,
			@RequestParam("pic_name6") MultipartFile file6,
			@RequestParam("pic_name7") MultipartFile file7,
			@RequestParam("pic_name8") MultipartFile file8,

			RedirectAttributes redirectAttributes, ModelMap model)
	{
		//file_purpose1
		
        try {

            // Get the file and save it somewhere
        	long millis = System.currentTimeMillis() ;
            byte[] bytes = file1.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file1.getOriginalFilename());
            Files.write(path, bytes);
            c.setPic_name1(millis +"_"+ file1.getOriginalFilename());
            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }
        
      //file_purpose2
      		if (file2.isEmpty()) {
                  //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
        
              }

              try {

                  // Get the file and save it somewhere
              	long millis = System.currentTimeMillis() ;
                  byte[] bytes = file2.getBytes();
                  Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file2.getOriginalFilename());
                  Files.write(path, bytes);
                  c.setPic_name2(millis +"_"+ file2.getOriginalFilename());
                  //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

              } catch (IOException e) {
                  e.printStackTrace();
              }
              
              //file_purpose3
        		if (file3.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");

                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file4.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file3.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setPic_name3(millis +"_"+ file3.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose4
        		if (file4.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
 
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file4.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file4.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setPic_name4(millis +"_"+ file4.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                
                //file_purpose5
        		if (file5.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
   
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file5.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file5.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setPic_name5(millis +"_"+ file5.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose6
        		if (file6.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");

                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file6.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file6.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setPic_name6(millis +"_"+ file6.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose7
        		if (file7.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
     
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file7.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file7.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setPic_name7(millis +"_"+ file7.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
                //file_purpose8
        		if (file8.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
    
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file8.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file8.getOriginalFilename());
                    Files.write(path, bytes);
                    c.setPic_name8(millis +"_"+ file8.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
              
        //---
		//ModelAttribute("BeTechnicalTestingValidation") BeTechnicalTestingValidation BeTechnicalTestingValidation,
	    //  BindingResult result,@RequestParam("copy_registion") MultipartFile file,@RequestParam("study_history") MultipartFile file2,
	    //  @RequestParam("professional_license") MultipartFile file3,@RequestParam("work_history") MultipartFile file4,
	   //   @RequestParam("my_pic") MultipartFile file5,
	  //    RedirectAttributes redirectAttributes, ModelMap model) 
		this.tpp3pRepository.save(c);
		//return "Show";
		return "redirect:/tpp3pView";
	
	}
}

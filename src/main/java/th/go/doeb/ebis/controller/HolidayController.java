package th.go.doeb.ebis.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.doeb.ebis.entity.Department;
import th.go.doeb.ebis.entity.Employee;
import th.go.doeb.ebis.entity.Holiday;
import th.go.doeb.ebis.repository.HolidayRepository;

@Controller
public class HolidayController {
	@Autowired(required=true)
	private HolidayRepository holidayRepository;
	
	@RequestMapping(value="/admin/manage_holiday")
	public String manage_holiday(HttpSession session,Model model)
	{
		
		if (session.getAttribute("isAdminLogon") != null)
		{
			if (session.getAttribute("isAdminLogon").toString().equalsIgnoreCase("1")) {
				Iterable<Holiday> eHoliday = this.holidayRepository.findAll();
				List<Holiday> lHoliday = new ArrayList<>();
				eHoliday.forEach(lHoliday::add);
				model.addAttribute("lHoliday", lHoliday);
				return "admin/form_holiday";
			}
		}
		return "redirect:/index";
		
		
	
		
	}
	
	@RequestMapping(value="/admin/manage_holiday/add_holiday")
	public String add_holiday(HttpSession session)
	{
		if (session.getAttribute("isAdminLogon") != null)
		{
			if (session.getAttribute("isAdminLogon").toString().equalsIgnoreCase("1")) {
				return "admin/add_holiday";
			}
		}
		return "redirect:/index";

			
	
		
	}
	
	@RequestMapping(value="/admin/save_holiday")
	//@ResponseBody
	public String save_holiday(@ModelAttribute Holiday holiday,HttpSession session,String holidayDate)
	{
		if (session.getAttribute("isAdminLogon") != null)
		{
			if (session.getAttribute("isAdminLogon").toString().equalsIgnoreCase("1")) {
				
				String[] parts = holidayDate.split("/");
				String year = String.valueOf((Integer.valueOf(parts[2]) - 543));
				String dateFull = parts[0] + "-" + parts[1] + "-" +  year;
				Date date1 = null;
				try {
					date1 = new SimpleDateFormat("dd-MM-yyyy").parse(dateFull);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				holiday.setHolidayDate(date1);
				BigDecimal valDouble = new BigDecimal(0);
				holiday.setDeleted(valDouble);
				this.holidayRepository.save(holiday);
				return "redirect:/admin/manage_holiday";
			//return date1.toString();
			}
		}
		return "redirect:/index";
	
	
		
	}

	@RequestMapping(value="/admin/manage_holiday/view/{idHoliday}")
	public String view_holiday(@PathVariable("idHoliday") Long idHoliday,Model model,HttpSession session)
	{
	
		if (session.getAttribute("isAdminLogon") != null)
		{
			if (session.getAttribute("isAdminLogon").toString().equalsIgnoreCase("1")) {
				Holiday holiday = this.holidayRepository.findByIdHoliday(idHoliday);
				
				String[] parts = holiday.getHolidayDate().toString().split("-");
				String year = String.valueOf((Integer.valueOf(parts[0]) + 0));
				String dateFull = parts[2] + "/" + parts[1]+ "/"+ year ;
				Date date1 = null;
				try {
					date1 = new SimpleDateFormat("dd/MM/yyyy").parse(dateFull);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				holiday.setHolidayDate(date1);
				
				model.addAttribute("holiday", holiday);
				return "/admin/view_holiday";
			}
		}
		return "redirect:/index";
			
	
		
	}
	
	@RequestMapping(value="/admin/update_holiday")
	public String update_holiday(@ModelAttribute Holiday holiday,HttpSession session)
	{
		if (session.getAttribute("isAdminLogon") != null)
		{
			if (session.getAttribute("isAdminLogon").toString().equalsIgnoreCase("1")) {
				this.holidayRepository.save(holiday);
				return "redirect:/admin/manage_holiday";
			}
		}
		return "redirect:/index";
		
	
		
	}
	
	@RequestMapping(value="/admin/manage_holiday/delete/{idHoliday}")
	public String delete_holiday(@PathVariable("idHoliday") Long idHoliday,@ModelAttribute Holiday holiday,HttpSession session)
	{
		if (session.getAttribute("isAdminLogon") != null)
		{
			if (session.getAttribute("isAdminLogon").toString().equalsIgnoreCase("1")) {
				holiday = this.holidayRepository.findByIdHoliday(idHoliday);
				BigDecimal valDouble = new BigDecimal(1);
				holiday.setDeleted(valDouble);
				this.holidayRepository.save(holiday);
				return "redirect:/admin/manage_holiday";
			}
		}
		return "redirect:/index";
		
	
		
	}
}

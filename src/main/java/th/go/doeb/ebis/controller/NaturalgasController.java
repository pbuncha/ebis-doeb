/*package th.go.doeb.ebis.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import th.go.doeb.ebis.bean.ProvinceBean;
import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.Engineer;
import th.go.doeb.ebis.entity.Form;
import th.go.doeb.ebis.entity.FormEntry;
import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestCategory;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.entity.RequestTransaction;
import th.go.doeb.ebis.entity.TitleName;
import th.go.doeb.ebis.repository.CompanyAttorneyRepository;
import th.go.doeb.ebis.repository.CompanyContactRepository;
import th.go.doeb.ebis.repository.CompanyNewRepository;
import th.go.doeb.ebis.repository.CompanyRepository;
import th.go.doeb.ebis.repository.EngineerRepository;
import th.go.doeb.ebis.repository.FormEntryRepository;
import th.go.doeb.ebis.repository.FormMetaRepository;
import th.go.doeb.ebis.repository.FormRepository;
import th.go.doeb.ebis.repository.ProvinceRepository;
import th.go.doeb.ebis.repository.RequestCategoryRepository;
import th.go.doeb.ebis.repository.RequestFormRepository;
import th.go.doeb.ebis.repository.RequestRepository;
import th.go.doeb.ebis.repository.RequestStatusRepository;
import th.go.doeb.ebis.repository.RequestTransactionRepository;
import th.go.doeb.ebis.repository.RequestTypeRepository;
import th.go.doeb.ebis.repository.TitleNameRepository;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

@Controller
public class NaturalgasController {
private static String UPLOAD_FOLDER = "/data/test/uploads/etdi/";
	
	@Autowired(required=true)
	private CompanyNewRepository companyRepository;

	@Autowired(required=true)
	private ProvinceRepository provinceRepository;	

	@Autowired(required=true)
	private FormRepository formRepository;	

	@Autowired(required=true)
	private FormMetaRepository formMetaRepository;	
	
	@Autowired(required=true)
	private FormEntryRepository formEntryRepository;	
	
	@Autowired(required=true)
	private RequestCategoryRepository requestCategoryRepository;
	
	@Autowired(required=true)
	private RequestRepository requestRepository;
	
	@Autowired(required=true)
	private RequestFormRepository requestFormRepository;	
	
	@Autowired(required=true)
	private RequestTransactionRepository requestTransactionRepository;
	
	@Autowired(required=true)
	private RequestStatusRepository requestStatusRepository;
	
	@Autowired(required=true)
	private EngineerRepository engineerRepository;
	
	@Autowired(required=true)
	private CompanyContactRepository companyContactRepository;
	
	@Autowired(required=true)
	private CompanyAttorneyRepository companyAttorneyRepository;
	
	@Autowired(required=true)
	private RequestTypeRepository requestTypeRepository;
	
	@Autowired(required=true)
	private TitleNameRepository titleNameRepository;
	
	
	@RequestMapping(value="onestop/natural_gas/form_001")
	public String formSave(@ModelAttribute Form form)
	{
		this.formRepository.save(form);
		return "onestop/natural_gas/form_001.html";
	}
	
	@RequestMapping(value="/onestop/company/{dbdId}/request_naturalgas")
	public String requestForm(@PathVariable("dbdId") String dbdId, Model model)
	{
		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);
		
		Iterable<RequestCategory> iRequestCategory = this.requestCategoryRepository.findAllFilter2();
		List<RequestCategory> lRequestCategory = new ArrayList<>();
		iRequestCategory.forEach(lRequestCategory::add);
		
		//Iterable<Form> iForm = this.formRepository.findAll();
		Iterable<Form> iForm = this.formRepository.findByFormActive(new BigDecimal(1));
		List<Form> lForm = new ArrayList<>();
		iForm.forEach(lForm::add);
		model.addAttribute("lRequestCategory", lRequestCategory);
		model.addAttribute("lForm", lForm);
		model.addAttribute("dbdId", dbdId);
		
		return "onestop/natural_gas/request_naturalgas";
	}
	
	@RequestMapping(value="/onestop/company/{dbdId}/{rtId}/form/input/{formId}/{requestTypeId}")
	public String formViewgas(@PathVariable("dbdId") String dbdId, @PathVariable("rtId") long rtId, @PathVariable("formId") Long formId, @PathVariable("requestTypeId") String requestTypeId, ModelMap  model)
	{
		RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(rtId);
		Request r = requestTransaction.getRequest();
		Form f = this.formRepository.findByFormId(formId);
		
		RequestForm requestForm = this.requestFormRepository.findByRequestAndFormAadTypeId(r, f,requestTransaction.getRequestType().getRequestTypeId());
		System.out.println("rId: "+r.getRequestId()+" | fId:  "+ f.getFormId() +" | getRequestTypeId: "+requestTransaction.getRequestType().getRequestTypeId());
		//long rfId = requestForm.getRfId();
				
		FormEntry formEntry = this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm);		
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String,String> entryMap = new HashMap<String, String>();
		model.addAttribute("entryMap", entryMap);
		model.addAttribute("formEntry", formEntry);
		model.addAttribute("formMetaId", "0");
		if (formEntry != null)
		{
			model.addAttribute("formMetaId", formEntry.getEntryId());
			try {
				
				entryMap = objectMapper.readValue(formEntry.getEntryText(), new TypeReference<HashMap<String,String>>() {});
				model.addAttribute("entryMap", entryMap);
				System.out.println(entryMap);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}

		String requestTypeName = "";
		if (requestTypeId.equalsIgnoreCase("1"))
		{
			requestTypeName = "ขอใหม่";
		} else if (requestTypeId.equalsIgnoreCase("41"))
		{
			requestTypeName = "เปลี่ยนแปลง แก้ไขใบรับรอง";
		} else if (requestTypeId.equalsIgnoreCase("42"))
		{
			requestTypeName = "เปลี่ยนแปลง ไม่แก้ไขใบรับรอง";
		}
		
		String requestName = r.getRequestName();
		
        Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);
		
		//CompanyAttorney iCompanyAttorney = this.companyAttorneyRepository.findByDbdIdLimit1(dbdId);
		//List<CompanyAttorney> lCompanyAttorney = new ArrayList<>();
		//iCompanyAttorney.forEach(lCompanyAttorney::add);
		
		model.addAttribute("lProvinceBean", lProvinceBean);
		model.addAttribute("dbdId", dbdId);
		model.addAttribute("rtId", rtId);
		model.addAttribute("formId", formId);
		model.addAttribute("rfId", f.getFormId());
//<<<<<<< HEAD
		model.addAttribute("requestTypeId", requestTypeId);
		model.addAttribute("requestTypeName", requestTypeName);
		model.addAttribute("requestName", requestName);
//=======
		//model.addAttribute("iCompanyAttorney", iCompanyAttorney);
//>>>>>>> 0840dbd8796a58706776381b9fbb33e8c2284d7f

		Company company = this.companyRepository.findByDbdId(dbdId);
		model.addAttribute("company", company);
		
		Iterable<Engineer> eForm = this.engineerRepository.findAllbyDbdId(dbdId);
		List<Engineer> lEngineer = new ArrayList<>();
		eForm.forEach(lEngineer::add);
		model.addAttribute("lEngineer", lEngineer);
		
		Iterable<TitleName> eTitleName = this.titleNameRepository.findTitleNameActive();
		List<TitleName> lTitleName = new ArrayList<>();
		eTitleName.forEach(lTitleName::add);
		model.addAttribute("lTitleName", lTitleName);
		
		return "onestop/natural_gas/" + f.getFormTemplate();
	}


}
*/
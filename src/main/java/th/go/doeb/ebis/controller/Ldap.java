package th.go.doeb.ebis.controller;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.naming.AuthenticationException;
import javax.naming.AuthenticationNotSupportedException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/ldap")
public class Ldap {
	
	@RequestMapping(value="/authen")
	@ResponseBody
	public String authen()
	{
		String output = "";
		Hashtable<String, String> environment = new Hashtable<String, String>();

        environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        environment.put(Context.PROVIDER_URL, "ldap://ldap.doeb.go.th:389");
//        environment.put(Context.PROVIDER_URL, "ldap://ldap.forumsys.com:389");
//        environment.put(Context.SECURITY_AUTHENTICATION, "simple");
//        environment.put(Context.SECURITY_PRINCIPAL, "sarawut_a");
//        environment.put(Context.SECURITY_CREDENTIALS, "<password>");
//        environment.put(Context.SECURITY_AUTHENTICATION, "simple");
//        environment.put(Context.SECURITY_PRINCIPAL, "cn=read-only-admin,dc=example,dc=com");
//        environment.put(Context.SECURITY_CREDENTIALS, "password");
        
        try 
        {
            DirContext context = new InitialDirContext(environment);
            output = "Connected..";
            output += "<br/>\n"+context.getEnvironment();
            
            SearchControls ctrls = new SearchControls();
            //ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
            ctrls.setReturningAttributes(new String[]{"givenName", "sn", "cn", "mail"});
            //ctrls.setReturningAttributes(null);
            //ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            String username = "netchanok";//"effective";
            NamingEnumeration<SearchResult> answers = context.search("ou=People,dc=doeb,dc=go,dc=th", "(uid=" + username + ")", ctrls);
//            NamingEnumeration<SearchResult> answers = context.search("ou=scientists,dc=example,dc=com", "(uid=" + username + ")", ctrls);            
            
            SearchResult result = answers.next();

            output += "<br/>\n"+result.getNameInNamespace();
            
            		try {
            	        for (NamingEnumeration ae = result.getAttributes().getAll(); ae.hasMore();) {
            	          Attribute attr = (Attribute) ae.next();
            	          output += "<br/>\nattribute: " + attr.getID();

            	          /* print each value */
            	          for (NamingEnumeration e = attr.getAll(); e.hasMore();
            	        		  output += "<br/>\nvalue: " + e.next())
            	            ;
            	        }
            	      } catch (NamingException e) {
            	        //e.printStackTrace();
            	      }
            		
        } 
        catch (AuthenticationNotSupportedException exception) 
        {
            output = "The authentication is not supported by the server";
        }

        catch (AuthenticationException exception)
        {
            output = "Incorrect password or username";
        }

        catch (NamingException exception)
        {
            output = "Error when trying to create the context";
        }		
		
		return output;
	}

	@RequestMapping(value="/authen2")
	@ResponseBody
	public String authen2()
	{
		String output = "";
		Hashtable<String, String> environment = new Hashtable<String, String>();

        environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        environment.put(Context.PROVIDER_URL, "ldap://167.99.77.6:389");
//        environment.put(Context.PROVIDER_URL, "ldap://ldap.forumsys.com:389");
//        environment.put(Context.SECURITY_AUTHENTICATION, "simple");
//        environment.put(Context.SECURITY_PRINCIPAL, "sarawut_a");
//        environment.put(Context.SECURITY_CREDENTIALS, "<password>");
//        environment.put(Context.SECURITY_AUTHENTICATION, "simple");
//        environment.put(Context.SECURITY_PRINCIPAL, "cn=read-only-admin,dc=example,dc=com");
//        environment.put(Context.SECURITY_CREDENTIALS, "password");
        
        try 
        {
            DirContext context = new InitialDirContext(environment);
            output = "Connected..";
            output += "<br/>\n"+context.getEnvironment();
            
            SearchControls ctrls = new SearchControls();
            //ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
            ctrls.setReturningAttributes(new String[]{"givenName", "sn", "cn", "mail"});
            //ctrls.setReturningAttributes(null);
            //ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            String username = "nattapol";//"effective";
            NamingEnumeration<SearchResult> answers = context.search("ou=People,dc=doeb,dc=go,dc=th", "(uid=" + username + ")", ctrls);
//            NamingEnumeration<SearchResult> answers = context.search("ou=scientists,dc=example,dc=com", "(uid=" + username + ")", ctrls);            
            
            SearchResult result = answers.next();

            output += "<br/>\n"+result.getNameInNamespace();
            
            		try {
            	        for (NamingEnumeration ae = result.getAttributes().getAll(); ae.hasMore();) {
            	          Attribute attr = (Attribute) ae.next();
            	          output += "<br/>\nattribute: " + attr.getID();

            	          /* print each value */
            	          for (NamingEnumeration e = attr.getAll(); e.hasMore();
            	        		  output += "<br/>\nvalue: " + e.next())
            	            ;
            	        }
            	      } catch (NamingException e) {
            	        //e.printStackTrace();
            	      }
            		
        } 
        catch (AuthenticationNotSupportedException exception) 
        {
            output = "The authentication is not supported by the server";
        }

        catch (AuthenticationException exception)
        {
            output = "Incorrect password or username";
        }

        catch (NamingException exception)
        {
            output = "Error when trying to create the context";
        }		
		
		return output;
	}	

//	@RequestMapping(value="/verify")
//	@ResponseBody
//	public String verify(String ldapusername)
//	{
//		Map<String, String> map = new HashMap<String, String>();
//		Hashtable<String, String> environment = new Hashtable<String, String>();
//
//        environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//        environment.put(Context.PROVIDER_URL, "ldap://ldap.doeb.go.th:389");
//
//        try 
//        {
//            DirContext context = new InitialDirContext(environment);
//
//            SearchControls ctrls = new SearchControls();
//            ctrls.setReturningAttributes(new String[]{"givenName", "sn", "cn", "mail"});
//            ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);
//
//            NamingEnumeration<SearchResult> answers = context.search("ou=People,dc=doeb,dc=go,dc=th", "(uid=" + ldapusername + ")", ctrls);
//
//            SearchResult result = answers.next();
//
//            output += "<br/>\n"+result.getNameInNamespace();
//            
//            		try {
//            	        for (NamingEnumeration ae = result.getAttributes().getAll(); ae.hasMore();) {
//            	          Attribute attr = (Attribute) ae.next();
//            	          output += "<br/>\nattribute: " + attr.getID();
//
//            	          /* print each value */
//            	          for (NamingEnumeration e = attr.getAll(); e.hasMore();
//            	        		  output += "<br/>\nvalue: " + e.next())
//            	            ;
//            	        }
//            	      } catch (NamingException e) {
//            	        //e.printStackTrace();
//            	      }
//            		
//        } 
//        catch (AuthenticationNotSupportedException exception) 
//        {
//            output = "The authentication is not supported by the server";
//        }
//
//        catch (AuthenticationException exception)
//        {
//            output = "Incorrect password or username";
//        }
//
//        catch (NamingException exception)
//        {
//            output = "Error when trying to create the context";
//        }		
//		
//		return output;
//	}	
	
}


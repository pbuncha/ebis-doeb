package th.go.doeb.ebis.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import th.go.doeb.ebis.entity.Department;
import th.go.doeb.ebis.entity.Holiday;
import th.go.doeb.ebis.repository.DepartmentRepository;


@Controller
public class DepartmentController {
	@Autowired(required=true)
	private DepartmentRepository departmentRepository;
	
	@RequestMapping(value="/admin/manage_department")
	public String manage_department(HttpSession session, Model model)
	{
		if (session.getAttribute("isAdminLogon") != null)
		{
			if (session.getAttribute("isAdminLogon").toString().equalsIgnoreCase("1")) {
				Iterable<Department> eDepartment = this.departmentRepository.findAll();
				List<Department> lDepartment = new ArrayList<>();
				eDepartment.forEach(lDepartment::add);
				model.addAttribute("lDepartment", lDepartment);
				return "admin/form_department";
			}
		}
		return "redirect:/index";
	}
	
	@RequestMapping(value="/admin/manage_department/add_department")
	public String add_department(HttpSession session)
	{
		if (session.getAttribute("isAdminLogon") != null)
		{
			if (session.getAttribute("isAdminLogon").toString().equalsIgnoreCase("1")) {
				return "admin/add_department";
			}
		}
		return "redirect:/index";	
	}
	
	@RequestMapping(value="/admin/save_department")
	public String save_department(@ModelAttribute Department department, HttpSession session)
	{
		if (session.getAttribute("isAdminLogon") != null)
		{
			if (session.getAttribute("isAdminLogon").toString().equalsIgnoreCase("1")) {

				
				
//				BigDecimal valDouble = new BigDecimal(0);
//				department.setDeleted(valDouble);
				this.departmentRepository.save(department);
				return "redirect:/admin/manage_department";
			}
		}
		return "redirect:/index";
	}
}

package th.go.doeb.ebis.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.AuthenticationException;
import javax.naming.AuthenticationNotSupportedException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.doeb.ebis.bean.AmphurBean;
import th.go.doeb.ebis.bean.ProvinceBean;
import th.go.doeb.ebis.bean.TambonBean;
import th.go.doeb.ebis.bean.ZipcodeBean;
import th.go.doeb.ebis.entity.Company;
import th.go.doeb.ebis.entity.EngineerTraining;
import th.go.doeb.ebis.entity.Form;
import th.go.doeb.ebis.entity.FormEntry;
import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestCategory;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.entity.RequestTransaction;
import th.go.doeb.ebis.repository.AmphurRepository;
import th.go.doeb.ebis.repository.CompanyNewRepository;
import th.go.doeb.ebis.repository.CompanyRepository;
import th.go.doeb.ebis.repository.EngineerTrainingRepository;
import th.go.doeb.ebis.repository.FormEntryRepository;
import th.go.doeb.ebis.repository.FormRepository;
import th.go.doeb.ebis.repository.ProvinceRepository;
import th.go.doeb.ebis.repository.RequestCategoryRepository;
import th.go.doeb.ebis.repository.RequestRepository;
import th.go.doeb.ebis.repository.RequestTransactionRepository;
import th.go.doeb.ebis.repository.TambonRepository;
import th.go.doeb.ebis.repository.TitleNameRepository;
import th.go.doeb.ebis.repository.ZipcodeRepository;
import th.go.doeb.ebis.repository.RequestFormRepository;

@Controller
public class ApiController {

	@Autowired(required=true)
	private ProvinceRepository provinceRepository;
	
	@Autowired(required=true)
	private AmphurRepository amphurRepository;
	
	@Autowired(required=true)
	private TambonRepository tambonRepository;
	
	@Autowired(required=true)
	private ZipcodeRepository zipcodeRepository;
	
	@Autowired(required=true)
	private RequestCategoryRepository requestCategoryRepository;
	
	@Autowired(required=true)
	private RequestRepository requestRepository;
	
	@Autowired(required=true)
	private CompanyNewRepository companyNewRepository;
	
	@Autowired(required=true)
	private CompanyRepository companyRepository;
	
	@Autowired(required=true)
	private FormEntryRepository formEntryRepository;
	
	@Autowired(required=true)
	private RequestTransactionRepository requestTransactionRepository;
	
	@Autowired(required=true)
	private RequestFormRepository requestFormRepository;
	
	@Autowired(required=true)
	private FormRepository formRepository;
	
	@Autowired(required=true)
	private EngineerTrainingRepository engineerTrainingRepository;
	
	@Autowired(required=true)
	private TitleNameRepository titleNameRepository;
	
	@RequestMapping(value="/api/province")
	@ResponseBody
	public List<ProvinceBean> province()
	{
		Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
		List<ProvinceBean> lProvinceBean = new ArrayList<>();
		iProvinceBean.forEach(lProvinceBean::add);
		
		return lProvinceBean;
	}
	
	@RequestMapping(value="/api/getStatus")
	@ResponseBody
	public FormEntry getStatus(Long rtId,Long fId)
	{
		// Check form entry is Empty?
		RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(rtId);
		
		//RequestTransaction requestTransaction = this.requestTransactionRepository.findByRtId(rtId);
		Request r = requestTransaction.getRequest();
		Form f = this.formRepository.findByFormId(fId);
		RequestForm requestForm = this.requestFormRepository.findByRequestAndFormAadTypeId(r, f,requestTransaction.getRequestType().getRequestTypeId());
		//long rfId = requestForm.getRfId();
		//-------------
		
		//RequestForm requestForm = this.requestFormRepository.findByRfId(rfId);
		FormEntry formEntry = this.formEntryRepository.findByRequestTransactionAndRequestForm(requestTransaction, requestForm);
		// FormEntry formEntry = this.formEntryRepository.findByRequestTransactionAndRequestFormAndIdEngineer(requestTransaction, requestForm,idEngineer);
		return formEntry;
	}
	
	@RequestMapping(value="/api/amphur")
	@ResponseBody
	public List<AmphurBean> amphur(BigDecimal provinceId)
	{
		Iterable<AmphurBean> iAmphurBean = this.amphurRepository.findByProvinceId(provinceId);
		List<AmphurBean> lAmphurBean = new ArrayList<>();
		iAmphurBean.forEach(lAmphurBean::add);
		
		return lAmphurBean;
	}
	
	@RequestMapping(value="/api/saveEngineerTraining")
	@ResponseBody
	public String saveEngineerTraining(String TITLE,String FIRSTNAME,String LASTNAME,String DBD_ID,String ENGINEER_ID,long TYPE_ENGINEER_TRAINING,long COURSE_ENGINEERTRAINING,Long RTID)
	{
		
		
		
		EngineerTraining engineerTraining = new EngineerTraining();
		engineerTraining.setTitleName(this.titleNameRepository.findTitleNameByCode(TITLE));
		engineerTraining.setFirstname(FIRSTNAME);
		engineerTraining.setLastname(LASTNAME);
		//System.out.println("DBD_ID: "+DBD_ID);
		engineerTraining.setCompany(this.companyRepository.findByDbdId(DBD_ID.replace("-", "")));
		System.out.println("DBD_ID2: "+this.companyRepository.findByDbdId(DBD_ID.replace("-", "")));
		engineerTraining.setEngineerId(ENGINEER_ID);
		engineerTraining.setTypeEngineerTraining(TYPE_ENGINEER_TRAINING);
		engineerTraining.setCourseEngineertraining(COURSE_ENGINEERTRAINING);
		engineerTraining.setRtid(RTID);
		engineerTraining.setDeleted(0);
		this.engineerTrainingRepository.save(engineerTraining);
		return "Test";
	}
	
	@RequestMapping(value="/api/getEngineerTraining")
	@ResponseBody
	public List<EngineerTraining> saveEngineerTraining(String DBD_ID,Long RTID)
	{
		System.out.println("DBD_ID: "+DBD_ID.replace("-", ""));
		System.out.println("RTID: "+RTID);
		Iterable<EngineerTraining> iEngineerTraining = this.engineerTrainingRepository.findAllbyDbdIdAndRtid(DBD_ID.replace("-", ""),RTID);
		List<EngineerTraining> lEngineerTraining = new ArrayList<>();
		iEngineerTraining.forEach(lEngineerTraining::add);
		
		
		return lEngineerTraining;
	}
	
	@RequestMapping(value="/api/getCompanyName")
	@ResponseBody
	public String getCompanyName(String dbdId)
	{
		String iCompany = this.companyNewRepository.findCompanyNameByDbdId(dbdId);
		//List<Company> lCompany = new ArrayList<>();
		//iCompany.forEach(lCompany::add);
		
		return iCompany;
	}
	
	@RequestMapping(value="/api/tambon")
	@ResponseBody
	public List<TambonBean> tambon(Long amphurId)
	{
		Iterable<TambonBean> iTambonBean = this.tambonRepository.findByAmphurId(amphurId);
		List<TambonBean> lTambonBean = new ArrayList<>();
		iTambonBean.forEach(lTambonBean::add);
		
		return lTambonBean;
	}	

	@RequestMapping(value="/api/zipcode")
	@ResponseBody
	public List<ZipcodeBean> zipcode(String districtId)
	{
		Iterable<ZipcodeBean> iZipcodeBean = this.zipcodeRepository.findByDistrictId(districtId);
		List<ZipcodeBean> lZipcodeBean = new ArrayList<>();
		iZipcodeBean.forEach(lZipcodeBean::add);
		
		return lZipcodeBean;
	}		

	@RequestMapping(value="/api/request")
	@ResponseBody
	public List<Request> request(Long rqCategoryId)
	{
		RequestCategory rc = this.requestCategoryRepository.findByRqCategoryId(rqCategoryId);
		List<Request> lRequest = rc.getRequests();
		return lRequest;
	}			
	
	@RequestMapping(value="/api/requesttype")
	@ResponseBody
	public List<RequestForm> requestType(Long rqId)
	{
		System.out.println("rqId: "+rqId);
		//RequestForm rc = this.requestFormRepository.findByRtId(rqId);
		Request rc = this.requestRepository.findByRequestId(rqId);
		System.out.println("getRequestId: "+rc.getRequestId());
		Iterable<RequestForm> iRequestForm = this.requestFormRepository.findByRequestJustGetType(rc);
		List<RequestForm> lRequestForm = new ArrayList<>();
		iRequestForm.forEach(lRequestForm::add);
		
		//List<RequestForm> lRequest = this.requestFormRepository.findByRequest(rc);
		return lRequestForm;
	}	
	@RequestMapping(value="/api/verify")
	@ResponseBody
	public String verify(String username)
	{
		String status = "404";
		String cn = "";
		String sn = "";

		String output = "";
		Hashtable<String, String> environment = new Hashtable<String, String>();

        environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        //environment.put(Context.PROVIDER_URL, "ldap://ldap.doeb.go.th:389");
        environment.put(Context.PROVIDER_URL, "ldap://167.99.77.6:389");

        try 
        {
            DirContext context = new InitialDirContext(environment);
//            output = "Connected..";
//            output += "<br/>\n"+context.getEnvironment();
            
            SearchControls ctrls = new SearchControls();
            //ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
            ctrls.setReturningAttributes(new String[]{"givenName", "sn", "cn", "mail"});
            //ctrls.setReturningAttributes(null);
            //ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

//            String username = "netchanok";//"effective";
            NamingEnumeration<SearchResult> answers = context.search("ou=People,dc=doeb,dc=go,dc=th", "(uid=" + username + ")", ctrls);

            SearchResult result = answers.next();

            //output += "<br/>\n"+result.getNameInNamespace();
            
            		try {
            	        for (NamingEnumeration ae = result.getAttributes().getAll(); ae.hasMore();) {
            	          Attribute attr = (Attribute) ae.next();
            	          //output += "<br/>\nattribute: " + attr.getID();
            	          if (attr.getID().equalsIgnoreCase("cn"))
            	          {
                	          for (NamingEnumeration e = attr.getAll(); e.hasMore();
                	        		  cn = (String) e.next())
                	            ;            	        	  
            	          }
            	          
            	          if (attr.getID().equalsIgnoreCase("sn"))
            	          {
                	          for (NamingEnumeration e = attr.getAll(); e.hasMore();
                	        		  sn = (String) e.next())
                	            ;            	        	  
            	          }

            	          /* print each value */
//            	          for (NamingEnumeration e = attr.getAll(); e.hasMore();
//            	        		  output += "<br/>\nvalue: " + e.next())
//            	            ;
            	        }
            	        
            	        output = cn+" "+sn;
            	        
            	      } catch (NamingException e) {
            	        //e.printStackTrace();
            	      }
            		
        } 
        catch (AuthenticationNotSupportedException exception) 
        {
            //output = "The authentication is not supported by the server";
        }

        catch (AuthenticationException exception)
        {
            //output = "Incorrect password or username";
        }

        catch (NamingException exception)
        {
            //output = "Error when trying to create the context";
        }		
		
		return output;
	}	
	
}

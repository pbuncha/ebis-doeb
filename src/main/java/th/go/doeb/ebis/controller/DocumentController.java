package th.go.doeb.ebis.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import th.go.doeb.ebis.entity.Department;
import th.go.doeb.ebis.entity.Employee;
import th.go.doeb.ebis.entity.GovDocument;
import th.go.doeb.ebis.entity.GovDocumentType;
import th.go.doeb.ebis.repository.DocumentRepository;
import th.go.doeb.ebis.repository.DocumenttypeRepository;


@Controller
public class DocumentController {
	@Autowired(required=true)
	private DocumentRepository documentRepository;	
	
	@Autowired(required=true)
	private DocumenttypeRepository documenttypeRepository;	
	
	private static String UPLOAD_FOLDER = "/data/test/uploads/document/";
	
	
	@RequestMapping(value="/document/list")
	public String formList(Model model)
	{
		Iterable<GovDocument> eForm = this.documentRepository.findAll();
		List<GovDocument> lGovDocument = new ArrayList<>();
		eForm.forEach(lGovDocument::add);
		model.addAttribute("lGovDocument", lGovDocument);
		
		return "document/list";
	}
	
	@RequestMapping(value="/document/add")
	public String formAdd(Model model)
	{
		 Iterable<GovDocumentType> iGovDocumentType = this.documenttypeRepository.findAll();
			List<GovDocumentType> lGovDocumentType = new ArrayList<>();
			iGovDocumentType.forEach(lGovDocumentType::add);
			model.addAttribute("lGovDocumentType", lGovDocumentType);
		return "document/add";
	}

	@RequestMapping(value="/document/save")
	//@ResponseBody
	public String formSave(@ModelAttribute GovDocument GovDocument,WebRequest request, HttpSession session)
	{
		MultiValueMap<String, MultipartFile> uploadFiles = ((StandardMultipartHttpServletRequest) ((ServletWebRequest) request).getRequest()).getMultiFileMap();
		BigDecimal govDocumentType = new BigDecimal(request.getParameter("govDocumentType"));
		for (MultiValueMap.Entry<String, List<MultipartFile>> uploadFile : uploadFiles.entrySet()) {

			List<MultipartFile> files = uploadFile.getValue();
			
            for (MultipartFile multipartFile : files) {
            	 
                String fileName = multipartFile.getOriginalFilename();
                String fileParamName = multipartFile.getName();
                String currentSaveFileName = "";

                
                //start upload
                
                if (multipartFile.getSize() > 0)
                {
                	
		        	long millis = System.currentTimeMillis() ;
		            
		            String type = multipartFile.getContentType();
		            type = "." + type.substring(type.lastIndexOf("/") + 1);
		            
		            currentSaveFileName = millis + type;
                	
                    try {
    	                String ToolFloder = UPLOAD_FOLDER+"doctype"+govDocumentType+"/" ;
    	                File directory = new File(String.valueOf(ToolFloder));
    	                System.out.println(directory.toString());
    	                if (!directory.exists()) {
    	                    directory.mkdirs();
    	                }
    		            
    		            Path path = Paths.get(ToolFloder + currentSaveFileName);
    		            byte[] bytes = multipartFile.getBytes();
    		            Files.write(path, bytes);
    		            
                    } catch (Exception e) {
                    	e.printStackTrace();
                    }                	
                }
                

                //end upload
                
                //out += "Param. : " + fileParamName;
                //out += "File : " + fileName + "<br/>\n";
                System.out.println(fileParamName);
                System.out.println(fileName);
                System.out.println(currentSaveFileName);
                GovDocument.setDocAttach(currentSaveFileName);
                //mapParams.put(fileParamName, currentSaveFileName);
            }

		}
		
		GovDocument.setGovDocumentType(this.documenttypeRepository.findBydoctypeId(govDocumentType.longValue()));
		this.documentRepository.save(GovDocument);
		return "redirect:/document/list";
		//System.out.println(GovDocument);
		//return "AA";
		/*return request.getParameter("docId");*/
	}	
	
	
	@RequestMapping(value="/document/view/{docId}")
	public String formView(@PathVariable("docId") Long docId,Model model)
	{
		 Iterable<GovDocumentType> iGovDocumentType = this.documenttypeRepository.findAll();
			List<GovDocumentType> lGovDocumentType = new ArrayList<>();
			iGovDocumentType.forEach(lGovDocumentType::add);
			GovDocument govDocument = this.documentRepository.findBydocId(docId);
			model.addAttribute("GovDocument", govDocument);
			model.addAttribute("lGovDocumentType", lGovDocumentType);
		return "/document/view";
	}	
	
	@RequestMapping(value="/document/update")
	//@ResponseBody
	public String formUpdate(@ModelAttribute GovDocument GovDocument,WebRequest request)
	{
		BigDecimal doctypeId = new BigDecimal(request.getParameter("doctypeId"));
		GovDocument.setGovDocumentType(this.documenttypeRepository.findBydoctypeId(doctypeId.longValue()));
		
		this.documentRepository.save(GovDocument);
		return "redirect:/document/list";
		//System.out.println(departmentId);
		//return "AA";
		//return Employee.getDepartment().toString();
	}
}
	



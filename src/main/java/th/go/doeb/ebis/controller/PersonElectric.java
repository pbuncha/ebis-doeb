package th.go.doeb.ebis.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import th.go.doeb.ebis.bean.BeTechnicalTestingValidation;
import th.go.doeb.ebis.bean.PersonElectricBean;
import th.go.doeb.ebis.repository.PersonElectricRepository;

@Controller
@RequestMapping("/PersonElectric")
public class PersonElectric {
	@Autowired(required=true)
	PersonElectricRepository PersonElectricRepository;
	private static String UPLOADED_FOLDER = System.getProperty("user.dir")+"\\src\\main\\resources\\uploads\\PersonElectric\\";
	   
	@RequestMapping(value="/index")
	public String IndexPersonElectric() {
		
		return "FormPersonElectric";
	}
	
	@RequestMapping(value="/add",method = RequestMethod.POST)
	//@ResponseBody
	public String AddPersonElectric(@ModelAttribute("PersonElectricBean") PersonElectricBean PersonElectricBean,
		      BindingResult result, @RequestParam("doc_attached7_1") MultipartFile file1,
		      @RequestParam("doc_attached7_2") MultipartFile file2,@RequestParam("doc_attached7_3") MultipartFile file3,
		      @RequestParam("doc_attached7_4") MultipartFile file4,@RequestParam("doc_attached7_5") MultipartFile file5,
		      @RequestParam("doc_attached7_6") MultipartFile file6,@RequestParam("doc_attached7_7") MultipartFile file7,
		      @RequestParam("doc_attached7_8") MultipartFile file8
		      ) {
		//file doc_attached7_1
		if (file1.isEmpty()) {
            //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            //return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
        	long millis = System.currentTimeMillis() ;
            byte[] bytes = file1.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file1.getOriginalFilename());
            Files.write(path, bytes);
            PersonElectricBean.setDoc_attached7_1(millis +"_"+ file1.getOriginalFilename());
            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }
        //file doc_attached7_2
      		if (file2.isEmpty()) {
                  //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                  //return "redirect:uploadStatus";
              }

              try {

                  // Get the file and save it somewhere
              	long millis = System.currentTimeMillis() ;
                  byte[] bytes = file2.getBytes();
                  Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file2.getOriginalFilename());
                  Files.write(path, bytes);
                  PersonElectricBean.setDoc_attached7_2(millis +"_"+ file2.getOriginalFilename());
                  //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

              } catch (IOException e) {
                  e.printStackTrace();
              }
            //file doc_attached7_3
        		if (file3.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file3.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file3.getOriginalFilename());
                    Files.write(path, bytes);
                    PersonElectricBean.setDoc_attached7_3(millis +"_"+ file3.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }      
              //file doc_attached7_4
        		if (file4.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file4.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file4.getOriginalFilename());
                    Files.write(path, bytes);
                    PersonElectricBean.setDoc_attached7_4(millis +"_"+ file4.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
              //file doc_attached7_5
        		if (file5.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file5.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file5.getOriginalFilename());
                    Files.write(path, bytes);
                    PersonElectricBean.setDoc_attached7_5(millis +"_"+ file5.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
              //file doc_attached7_6
        		if (file6.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file6.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file6.getOriginalFilename());
                    Files.write(path, bytes);
                    PersonElectricBean.setDoc_attached7_6(millis +"_"+ file6.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
              //file doc_attached7_7
        		if (file7.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file7.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file7.getOriginalFilename());
                    Files.write(path, bytes);
                    PersonElectricBean.setDoc_attached7_7(millis +"_"+ file7.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
              //file doc_attached7_8
        		if (file8.isEmpty()) {
                    //redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
                    //return "redirect:uploadStatus";
                }

                try {

                    // Get the file and save it somewhere
                	long millis = System.currentTimeMillis() ;
                    byte[] bytes = file8.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ file8.getOriginalFilename());
                    Files.write(path, bytes);
                    PersonElectricBean.setDoc_attached7_8(millis +"_"+ file8.getOriginalFilename());
                    //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

                } catch (IOException e) {
                    e.printStackTrace();
                }
		PersonElectricRepository.save(PersonElectricBean);
		//return "FormPersonElectric";
		return "redirect:/PersonElectric/index";
	}
	
}

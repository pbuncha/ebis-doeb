package th.go.doeb.ebis.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.AuthenticationException;
import javax.naming.AuthenticationNotSupportedException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import th.go.doeb.ebis.bean.EModel;
import th.go.doeb.ebis.bean.Form2;
import th.go.doeb.ebis.bean.FormBean;
//import th.go.doeb.ebis.bean.Greeting;
import th.go.doeb.ebis.bean.LoginBean;
import th.go.doeb.ebis.bean.MemBean;
import th.go.doeb.ebis.bean.ProvinceBean;
import th.go.doeb.ebis.bean.TempMember;
import th.go.doeb.ebis.entity.EventLog;
import th.go.doeb.ebis.entity.Form;
import th.go.doeb.ebis.pojo.AuthenResult;
import th.go.doeb.ebis.repository.EFRepository;
import th.go.doeb.ebis.repository.ERepository;
import th.go.doeb.ebis.repository.EventLogRepository;
import th.go.doeb.ebis.repository.FormRepository;
import th.go.doeb.ebis.repository.LoginRepository;
import th.go.doeb.ebis.repository.MemRepository;
import th.go.doeb.ebis.repository.ProvinceRepository;
import th.go.doeb.ebis.repository.TcRepository;
import th.go.doeb.ebis.repository.TempMemberRepository;


@Controller
@RequestMapping(value="/")
public class EController {
	//private static String UPLOADED_FOLDER = System.getProperty("user.dir")+"\\src\\main\\resources\\uploads\\CorporatePersonElectric\\";
	private static String UPLOADED_FOLDER = "/data/test/uploads/etdi/";
	
	@Autowired(required=true)
	private EFRepository efRepository;

	@Autowired(required=true)
	private TcRepository tcRepository;
	
	@Autowired(required=true)
	private MemRepository memRepository;
	
	@Autowired(required=true)
	private LoginRepository loRepository;
	
	@Autowired(required=true)
	private ERepository eRepository;

	@Autowired(required=true)
	private TempMemberRepository tempMemberRepository;
	
	@Autowired(required=true)
	private ProvinceRepository provinceRepository;

	@Autowired(required=true)
	private FormRepository formRepository;
	
	@Autowired(required=true)
	private EventLogRepository eventLogRepository;
	
	@RequestMapping(value="/")
	public String home(HttpServletRequest requestsession, Model model) 
	{	
		model.addAttribute("textValid",requestsession.getSession().getAttribute("textValid"));
		return "index";
	}
	
	@RequestMapping(value="/index")
	public String welcome() 
	{
		return "index";
	}
	
	@RequestMapping(value="/Ex")
	public String Ex(long id,Model model) 
	{
		MemBean mem = memRepository.findById(id);
		model.addAttribute("member", mem);
		return "main_page";
	}
	@RequestMapping(value="/flogin")
	public String flogin() 
	{
		return "Login";
	}
	@RequestMapping(value="/formRegis")
	public String formregis() 
	{
		return "Register";
	}

//	@RequestMapping(value="/register")
//	@ResponseBody
//	public String Register(@ModelAttribute MemBean m,Model model) 
//	{
//		MemBean u = new MemBean();
//		u.setUser(m.getUser());
//		u.setPass(m.getPass());
//		u.setName(m.getName());
//		u.setLname(m.getLname());
//		u.setAge(m.getAge());
//		u.setIdnum(m.getIdnum());
//		u.setNoadd(m.getNoadd());
//		u.setMadd(m.getMadd());
//		u.setSadd(m.getSadd());
//		u.setRadd(m.getRadd());
//		u.setIdDT(m.getIdDT());
//		u.setPhone(m.getPhone());
//		u.setFax(m.getFax());
//		u.setEmail(m.getEmail());
//		this.memRepository.save(m);
//		return "Success";
//	}
	
	@RequestMapping(value="/register")
	//@ResponseBody
	public String register(@ModelAttribute TempMember m, Model model, HttpServletRequest request)
	{
		TempMember currentMember = new TempMember();
		currentMember.setMemberUsername(m.getMemberUsername());
		currentMember.setMemberPassword(m.getMemberPassword());
		currentMember.setMemberIp(request.getRemoteAddr());
		
		this.tempMemberRepository.save(currentMember);
		
		return "register_success";
	}

	@RequestMapping(value="/login")
	public String Login(HttpServletRequest request, Model model,String fusername,String fpassword) 
	{
		
		request.getSession().setAttribute("isUserLogon", "0");
		request.getSession().setAttribute("isAdminLogon", "0");
		
		this.logEventLog(fusername);
		
		if ((fusername.equalsIgnoreCase("ost001"))&&(fpassword.equalsIgnoreCase("doeb2018")))
		{
			request.getSession().setAttribute("role", "1");
			request.getSession().setAttribute("isUserLogon", "1");
			request.getSession().setAttribute("memberId", "1");
			request.getSession().setAttribute("Position", "เจ้าหน้าที่ One Stop");
			request.getSession().setAttribute("nameMember", "นายกรม พลังงาน");
			
			System.out.println(request.getSession().getAttribute("Position"));
			System.out.println(request.getSession().getAttribute("nameMember"));
			request.getSession().setAttribute("nameMember", "นายกรม พลังงาน");
			return "redirect:/onestop/dashboard";
		} else if ((fusername.equalsIgnoreCase("ofc001"))&&(fpassword.equalsIgnoreCase("doeb2018")))
		{
			request.getSession().setAttribute("role", "2");
			request.getSession().setAttribute("isUserLogon", "1");
			request.getSession().setAttribute("Position", "เจ้าหน้าที่ พนักงาน");
			request.getSession().setAttribute("nameMember", "นายกรม พลังงาน");
			return "redirect:/officer/dashboard";
			
		} else if ((fusername.equalsIgnoreCase("drt001"))&&(fpassword.equalsIgnoreCase("doeb2018")))
		{
			request.getSession().setAttribute("role", "3");
			request.getSession().setAttribute("isPosition", "1");
			request.getSession().setAttribute("isUserLogon", "1");
			request.getSession().setAttribute("Position", "ผู้อำนวยการกลุ่ม");
			request.getSession().setAttribute("nameMember", "นายกรม พลังงาน");
			return "redirect:/director/dashboard";
		}else if ((fusername.equalsIgnoreCase("drt002"))&&(fpassword.equalsIgnoreCase("doeb2018")))
		{
			request.getSession().setAttribute("role", "3");
			request.getSession().setAttribute("isPosition", "2");
			request.getSession().setAttribute("isUserLogon", "1");
			request.getSession().setAttribute("Position", "ผู้อำนวยการ สพพ.");
			request.getSession().setAttribute("nameMember", "นายกรม พลังงาน");
			return "redirect:/director/dashboard";
		}else if ((fusername.equalsIgnoreCase("drt003"))&&(fpassword.equalsIgnoreCase("doeb2018")))
		{
			request.getSession().setAttribute("role", "3");
			request.getSession().setAttribute("isPosition", "3");
			request.getSession().setAttribute("isUserLogon", "1");
			request.getSession().setAttribute("Position", "รองอธิบดี");
			request.getSession().setAttribute("nameMember", "นายกรม พลังงาน");
			return "redirect:/director/dashboard";
		}else if ((fusername.equalsIgnoreCase("0107550000220"))&&(fpassword.equalsIgnoreCase("doeb2018")))
		{
			request.getSession().setAttribute("isUserLogon", "1");
			request.getSession().setAttribute("Position", "E-service");
			request.getSession().setAttribute("nameMember", "นายกรม พลังงาน");
			return "redirect:/user/dashboard";
		}
		
		
		//0107550000220
		
//		MemBean us = memRepository.findByuser(fusername);
//		MemBean pa = memRepository.findBypass(fpassword);
		//MemBean member = memRepository.findByUserAndPass(fusername, fpassword);

//		if(us!=null&&pa!=null) {
		
		AuthenResult ar = this.authenLdap(fusername, fpassword);
		if (ar.isValid())
		{
    		return "redirect:/onestop/dashboard";			
		} else {
			request.getSession().setAttribute("textValid", "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง");
			return "redirect:/";
		}
		
//		AuthenResult ar = this.authenLdap(fusername, fpassword);
//		if (ar.isValid())
//		{
//    			return "redirect:/admin/dashboard";			
//		} else {
//			TempMember member = this.tempMemberRepository.findByMemberUsernameAndMemberPassword(fusername, fpassword);
//			if(member != null) {
//				request.getSession().setAttribute("isUserLogon", "1");
//				request.getSession().setAttribute("memberId", member.getMemberId());
//				return("redirect:/user/dashboard");
//			}else {
//				return("redirect:/");
//			}			
//		}
		
		//return "redirect:/";
		
	}

	public AuthenResult authenLdap(String ausername, String apassword)
	{
		
		AuthenResult authenResult = new AuthenResult();
		
		String output = "";
		Hashtable<String, String> environment = new Hashtable<String, String>();

        environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        environment.put(Context.PROVIDER_URL, "ldap://ldap.doeb.go.th:389");
        environment.put(Context.SECURITY_AUTHENTICATION, "simple");
        environment.put(Context.SECURITY_PRINCIPAL, "UID="+ausername+",OU=People,DC=doeb,DC=go,DC=th");
        environment.put(Context.SECURITY_CREDENTIALS, apassword);
        
        try 
        {
            DirContext context = new InitialDirContext(environment);
            output = "Connected..";
            output += "<br/>\n"+context.getEnvironment();
            
            SearchControls ctrls = new SearchControls();
            ctrls.setReturningAttributes(new String[]{"givenName", "sn", "cn", "mail"});
            //ctrls.setReturningAttributes(null);
            ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            NamingEnumeration<SearchResult> answers = context.search("OU=People,DC=doeb,DC=go,DC=th", "(UID=" + ausername + ")", ctrls);
            
            SearchResult result = answers.next();

            output += "<br/>\n"+result.getNameInNamespace();
            
            		try {
            	        for (NamingEnumeration ae = result.getAttributes().getAll(); ae.hasMore();) {
            	          Attribute attr = (Attribute) ae.next();
            	          output += "<br/>\nattribute: " + attr.getID();

            	          /* print each value */
            	          for (NamingEnumeration e = attr.getAll(); e.hasMore();
            	        		  output += "<br/>\nvalue: " + e.next())
            	            ;
            	        }
            	        authenResult.setValid(true);
            	        authenResult.setLdapUsername(ausername);
            	        //request.getSession().setAttribute("isAdminLogon", "1");
            	      } catch (NamingException e) {
            	        //e.printStackTrace();
            	      }
            		
        } 
        catch (AuthenticationNotSupportedException exception) 
        {
            output = "The authentication is not supported by the server";
        }

        catch (AuthenticationException exception)
        {
            output = exception.getMessage()+exception.getExplanation();
        }

        catch (NamingException exception)
        {
            output = exception.getMessage()+exception.getExplanation();
        }		
		
		//return output;
//        if (isValid)
//        {
//        		return "redirect:/admin/dashboard";
//        } else {
//        		return "redirect:/admin/authen_error";
//        }
        return authenResult;
	}	
	
	@RequestMapping(value="/dashboard")
	public String dashboard()
	{
		return "dashboard/profile";
	}	

	@RequestMapping(value="/requestForm")
	public String requestForm(Model model)
	{
		Iterable<Form> iForm = this.formRepository.findAll();
		List<Form> lForm = new ArrayList<>();
		iForm.forEach(lForm::add);
		model.addAttribute("lForm", lForm);
		
		return "dashboard/request_form";
	}		
	
	@RequestMapping(value="/select")
	public String select()
	{
		return "dashboard/select";
	}
	
	@RequestMapping(value="/FormF2")
	 public String FormF2() 
	 {
	  return "Form2_1";
	 }

	@RequestMapping(value="greeting_b")
	 public String greeting_b(@ModelAttribute FormBean E,BindingResult result,
			 @RequestParam("doc_attached1_1") MultipartFile doc_attached1_1,
			 @RequestParam("doc_attached1_2") MultipartFile doc_attached1_2,
			 @RequestParam("doc_attached1_3") MultipartFile doc_attached1_3,
			 @RequestParam("doc_attached1_4") MultipartFile doc_attached1_4,
			 @RequestParam("doc_attached1_5") MultipartFile doc_attached1_5,
			 @RequestParam("doc_attached1_6") MultipartFile doc_attached1_6,
			 @RequestParam("doc_attached1_7") MultipartFile doc_attached1_7,
			 @RequestParam("doc_attached1_8") MultipartFile doc_attached1_8,
			 @RequestParam("doc_attached2_11") MultipartFile doc_attached2_11,
			 @RequestParam("doc_attached2_2") MultipartFile doc_attached2_2,
			 @RequestParam("doc_attached2_3") MultipartFile doc_attached2_3,
			 @RequestParam("doc_attached2_4") MultipartFile doc_attached2_4,
			 @RequestParam("doc_attached2_5") MultipartFile doc_attached2_5,
			 @RequestParam("doc_attached2_6") MultipartFile doc_attached2_6,
			 @RequestParam("doc_attached2_7") MultipartFile doc_attached2_7,
			 Model model)
	 {
		try {

            // Get the file and save it somewhere
        	long millis = System.currentTimeMillis() ;
            byte[] bytes = doc_attached1_1.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached1_1.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached1_1(millis +"_"+ doc_attached1_1.getOriginalFilename());
            
            millis = System.currentTimeMillis() ;
            bytes = doc_attached1_2.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached1_2.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached1_2(millis +"_"+ doc_attached1_2.getOriginalFilename());
            
            millis = System.currentTimeMillis() ;
            bytes = doc_attached1_3.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached1_3.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached1_3(millis +"_"+ doc_attached1_3.getOriginalFilename());
           
            millis = System.currentTimeMillis() ;
            bytes = doc_attached1_4.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached1_4.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached1_4(millis +"_"+ doc_attached1_4.getOriginalFilename());
            
            millis = System.currentTimeMillis() ;
            bytes = doc_attached1_5.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached1_5.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached1_5(millis +"_"+ doc_attached1_5.getOriginalFilename());
           
            millis = System.currentTimeMillis() ;
            bytes = doc_attached1_6.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached1_6.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached1_6(millis +"_"+ doc_attached1_6.getOriginalFilename());
           
            millis = System.currentTimeMillis() ;
            bytes = doc_attached1_7.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached1_7.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached1_7(millis +"_"+ doc_attached1_7.getOriginalFilename());
           
            millis = System.currentTimeMillis() ;
            bytes = doc_attached1_8.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached1_8.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached1_8(millis +"_"+ doc_attached1_8.getOriginalFilename());
           
            millis = System.currentTimeMillis() ;
            bytes = doc_attached2_11.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached2_11.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached2_11(millis +"_"+ doc_attached2_11.getOriginalFilename());
           
            millis = System.currentTimeMillis() ;
            bytes = doc_attached2_2.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached2_2.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached2_2(millis +"_"+ doc_attached2_2.getOriginalFilename());
            
            millis = System.currentTimeMillis() ;
            bytes = doc_attached2_3.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached2_3.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached2_3(millis +"_"+ doc_attached2_3.getOriginalFilename());
            
            millis = System.currentTimeMillis() ;
            bytes = doc_attached2_4.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached2_4.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached2_4(millis +"_"+ doc_attached2_4.getOriginalFilename());
            
            millis = System.currentTimeMillis() ;
            bytes = doc_attached2_5.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached2_5.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached2_5(millis +"_"+ doc_attached2_5.getOriginalFilename());
            
            millis = System.currentTimeMillis() ;
            bytes = doc_attached2_6.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached2_6.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached2_6(millis +"_"+ doc_attached2_6.getOriginalFilename());
            
            millis = System.currentTimeMillis() ;
            bytes = doc_attached2_7.getBytes();
            path = Paths.get(UPLOADED_FOLDER + millis +"_"+ doc_attached2_7.getOriginalFilename());
            Files.write(path, bytes);
            E.setDoc_attached2_7(millis +"_"+ doc_attached2_7.getOriginalFilename());
            
            //redirectAttributes.addFlashAttribute("message","You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }
		
	  this.efRepository.save(E);
	  model.addAttribute("form", E);
	  return "Form2_1";
	 }
	@RequestMapping(value="/FormTC")
	public String FormTC() 
	{
		return "Form2";
	}
	@RequestMapping(value = "/add2")
	public String add2(Model model, String lfpp, String purpose1, String purpose2){
		Form2 f = new Form2();
		f.setLfpp(lfpp);
		f.setPurpose1(purpose1);
		f.setPurpose2(purpose2);
		this.tcRepository.save(f);
		model.addAttribute("form2", f);
		return "view2";
	}
	@RequestMapping(value="/FormTP2")
	public String FormTP2() 
	{
		return "Ko2";
	}
	
	@RequestMapping(value="/tsn1")
	public String tsn1() 
	{
		return "tsn1";
	}
	
	@RequestMapping("/spp")
	    public String greetingForm(Model model) {
	       // model.addAttribute("greeting", new Greeting());
			
	        Iterable<ProvinceBean> iProvinceBean = this.provinceRepository.findAll();
			List<ProvinceBean> lProvinceBean = new ArrayList<>();
			iProvinceBean.forEach(lProvinceBean::add);
			model.addAttribute("lProvinceBean", lProvinceBean);
			
	        return "greeting-milk";
	    }
	
	 @RequestMapping("/stc2_1")
	    public String index() 
	 {
	        return "form_stc";
	    }
	 @RequestMapping(value="/inputstc")
		@ResponseBody
		public String greeting22(@ModelAttribute EModel E,Model model)
		{
			
			this.eRepository.save(E);
			model.addAttribute("form", E);
			return "stc2_1";
		}
 @RequestMapping(value="/ko1")
	public String ko1() 
	{
		return "Ko1";
	}
 @RequestMapping(value="/ko2")
	public String ko2() 
	{
		return "ข.2-1";
	}
 
 @RequestMapping("/tpp3pView")
 public String tpp3pView() 
{
     return "tpp3p";
 }
@RequestMapping(value="/tpp3pView1")
@ResponseBody
public String tpp3pView1(@ModelAttribute EModel E,Model model)
{

this.eRepository.save(E);
model.addAttribute("form", E);
return "tpp3p";
}

public void logEventLog(String user)
{
	EventLog el = new EventLog();
	
	el.setLogIp("127.0.0.1");
	el.setLogRefer("http://ebis.redirectme.net:8080/");
	el.setLogTimestamp(new Timestamp(System.currentTimeMillis()));
	el.setLogType("login");
	el.setLogUser(user);
	
	this.eventLogRepository.save(el);
}

}

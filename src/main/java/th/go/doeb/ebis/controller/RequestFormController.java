package th.go.doeb.ebis.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import th.go.doeb.ebis.entity.Request;
import th.go.doeb.ebis.entity.RequestCategory;
import th.go.doeb.ebis.entity.RequestForm;
import th.go.doeb.ebis.repository.RequestCategoryRepository;
import th.go.doeb.ebis.repository.RequestFormRepository;
import th.go.doeb.ebis.repository.RequestRepository;

@Controller
public class RequestFormController {
	@Autowired(required=true)
	private RequestFormRepository requestForm;
	
	
	@RequestMapping(value="/admin/manage_requestform")
	public String manage_requestform(Model model)
	{
		
		Iterable<RequestForm> eForm = this.requestForm.findAll();
		List<RequestForm> lRequestForm = new ArrayList<>();
		eForm.forEach(lRequestForm::add);
		model.addAttribute("lRequestForm", lRequestForm);
		
		return "admin/form_requestform";
	}
}

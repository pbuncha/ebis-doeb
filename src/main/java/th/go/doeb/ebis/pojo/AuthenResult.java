package th.go.doeb.ebis.pojo;

public class AuthenResult {
	private boolean valid;
	private String memberId;
	private String ldapUsername;

	public AuthenResult()
	{
		this.setValid(false);
		this.setLdapUsername("0");
		this.setMemberId("0");
	}
	
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getLdapUsername() {
		return ldapUsername;
	}
	public void setLdapUsername(String ldapUsername) {
		this.ldapUsername = ldapUsername;
	}
}

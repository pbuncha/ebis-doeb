A Pen created at CodePen.io. You can find this one at https://codepen.io/josemc/pen/Kwbapp.

 INSPIRED BY SEB KAY'S 'SIGN UP FORM'
https://dribbble.com/shots/1776663-Purple-Sign-Up-Form
https://dribbble.com/shots/1779149--Free-PSD-Purple-Sign-Up-Form-Payment-Details

DO YOU HAVE A SPARE DRIBBBLE INVITE? I'D	APPRECIATE IT!
dribbble.com/josec

A Pen created at CodePen.io. You can find this one at https://codepen.io/nagasai/pen/JKKNMK.

 https://scotch.io/@nagasaiaytha/generate-pdf-from-html-using-jquery-and-jspdf
 The MIT License (MIT)
jQuery-Word-Export
==================

jQuery plugin for exporting HTML and images to a Microsoft Word document

Dependencies: [jQuery](http://jquery.com/) and [FileSaver.js](https://github.com/eligrey/FileSaver.js/)

This plugin takes advantage of the fact that MS Word can interpret HTML as a document. Specifically, this plugin leverages the [MHTML](http://en.wikipedia.org/wiki/MHTML) archive format in order to embed images directly into the file, so they can be viewed offline.

Unfortunately, LibreOffice and, probably, other similar word processors read the output file as plain text rather than interpreting it is a document, so the output of this plugin will only work with MS Word proper.

Click here for demo: [http://mark.swindoll.us/jquery-word-export/](http://mark.swindoll.us/jquery-word-export/)

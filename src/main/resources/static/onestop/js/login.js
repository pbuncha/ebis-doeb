$( document ).ready(function() {
  $("#login-btn").click(function() {
    $("#signup-btn").removeClass('selected');
    $("#login-btn").addClass('selected');
    $("#login").addClass('is-selected');
     $("#signup").removeClass('is-selected');
  });
  $("#signup-btn").click(function() {
    $("#login-btn").removeClass('selected');
    $("#signup-btn").addClass('selected');
    $("#login").removeClass('is-selected');
    $("#signup").addClass('is-selected');
  });  
  //hide or show password
  $('.hide-password').on('click', function(){
    var $this= $(this),
      $password_field = $this.prev('input');
    
    ( 'password' == $password_field.attr('type') ) ? $password_field.attr('type', 'text') : $password_field.attr('type', 'password');
    ( 'แสดงรหัสผ่าน' == $this.text() ) ? $this.text('ซ่อนรหัสผ่าน') : $this.text('แสดงรหัสผ่าน');
    //focus and move cursor to the end of input field
    $password_field.putCursorAtEnd();
  });
});

  